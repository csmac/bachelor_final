1
00:00:20,100 --> 00:00:20,100
<i>[One Percent of Something]</i>

2
00:00:20,300 --> 00:00:25,900
Subtitles by DramaFever

3
00:00:20,100 --> 00:00:20,100
<i>[One Percent of Something]</i>

4
00:00:41,199 --> 00:00:49,900
<i>[Episode 7 - The Meaning of a Gift
From a Mere Memory to a Treasured One]</i>

5
00:02:38,400 --> 00:02:39,900
How long was I asleep?

6
00:02:39,900 --> 00:02:44,300
Not too long.
About an hour or so.

7
00:02:46,900 --> 00:02:49,300
I finally feel alive again.

8
00:02:53,900 --> 00:02:55,099
What?

9
00:02:57,700 --> 00:03:00,700
You look like you've only
managed to have a meal a day.

10
00:03:01,400 --> 00:03:03,700
How many hours of sleep
have you been getting?

11
00:03:05,599 --> 00:03:07,999
About four hours in total
the past three days?

12
00:03:12,200 --> 00:03:14,900
Including the sleep you got just now?

13
00:03:25,800 --> 00:03:26,999
Boing.

14
00:03:42,099 --> 00:03:43,999
Go inside.

15
00:03:43,999 --> 00:03:45,599
And be sure to lock
your door properly.

16
00:03:49,700 --> 00:03:50,999
What is it?

17
00:03:51,999 --> 00:03:53,900
Give me your phone for a second.

18
00:04:00,499 --> 00:04:03,200
You have to unlock it for me first.

19
00:04:09,900 --> 00:04:13,599
Don't even look at your phone from
now until tomorrow morning.

20
00:04:14,099 --> 00:04:15,400
Just sleep.

21
00:04:15,400 --> 00:04:18,700
If you don't, you'll die.
You got that?

22
00:04:18,700 --> 00:04:21,299
No way.
You know what a busy person I--

23
00:04:21,299 --> 00:04:25,200
I waited two hours for you last time.

24
00:04:27,000 --> 00:04:28,900
Don't you even feel bad about that?

25
00:04:28,900 --> 00:04:30,400
Are you still mad about that?

26
00:04:30,400 --> 00:04:32,400
Well, it's not like you did anything
to try and make me feel better.

27
00:04:32,400 --> 00:04:33,700
All you did was sleep.

28
00:04:36,000 --> 00:04:40,900
Don't do anything once you get home,
and sleep until tomorrow morning.

29
00:04:40,900 --> 00:04:45,099
If you do that,
I'll forgive you for today.

30
00:04:47,400 --> 00:04:51,000
All right.
I'll call you tomorrow.

31
00:05:10,400 --> 00:05:13,799
Sorry for making you wait.

32
00:05:28,000 --> 00:05:30,000
All right, "Internet Safety
Protocol for Employees."

33
00:05:30,000 --> 00:05:31,700
There are tons of people who
aren't adhering to this lately.

34
00:05:31,700 --> 00:05:34,700
One: personal work computers,
and user information...

35
00:05:34,700 --> 00:05:36,900
Deputy Han! I know all about the
shopping app you have on your computer.

36
00:05:36,900 --> 00:05:38,200
Erase it.
Got that?

37
00:05:38,700 --> 00:05:39,700
Yes, sir.

38
00:05:39,700 --> 00:05:43,099
You will not store work-related info
externally or internally via your phones.

39
00:05:43,099 --> 00:05:45,599
- You will not store work-related info...
- You will not store work-related info...

40
00:05:45,599 --> 00:05:47,299
All together, damn it!

41
00:05:47,299 --> 00:05:48,400
One, two, three.

42
00:05:48,400 --> 00:05:53,900
You will not store work-related info
externally or internally via your phones.

43
00:05:58,099 --> 00:06:00,700
I need to crack our CEO's back open,
or something.

44
00:06:00,700 --> 00:06:01,900
Why?

45
00:06:01,900 --> 00:06:03,500
In case he has a battery there.

46
00:06:03,500 --> 00:06:05,099
Or maybe he has a USB
port there, instead?

47
00:06:05,099 --> 00:06:08,000
How can someone become
so energetic overnight?

48
00:06:08,599 --> 00:06:10,200
I guess he never gets tired.

49
00:06:12,799 --> 00:06:14,599
Good job.
Bye!

50
00:06:14,599 --> 00:06:15,799
Bye!

51
00:06:15,799 --> 00:06:18,799
All right, great job!
Bye!

52
00:06:21,200 --> 00:06:22,700
Good job.

53
00:07:05,599 --> 00:07:08,099
Oh, it's a good thing you're here.
I had something to give you.

54
00:07:08,099 --> 00:07:09,400
To me?

55
00:07:15,099 --> 00:07:20,200
I think your uncle is up to something.
That's what your grandpa told me.

56
00:07:20,599 --> 00:07:24,099
So I'm sure that
Grandpa noticed too, then?

57
00:07:24,099 --> 00:07:26,099
Of course. He's got a
sharp eye, after all.

58
00:07:26,099 --> 00:07:28,099
I heard that you aren't even
getting any sleep lately.

59
00:07:28,099 --> 00:07:29,099
Let's go.

60
00:07:29,099 --> 00:07:31,700
Oh, there's something that
I wanted to give you.

61
00:07:32,299 --> 00:07:33,500
It's a gift for you.

62
00:07:35,200 --> 00:07:36,599
A gift?

63
00:07:37,400 --> 00:07:39,700
You give people gifts now?

64
00:07:39,700 --> 00:07:43,099
I'm not sure if you'll like it,
since it's my first time giving you one.

65
00:07:47,400 --> 00:07:50,900
I'll love it no matter what it is
if you're the one giving it to me.

66
00:08:07,200 --> 00:08:09,599
CEO, about lunch...

67
00:08:09,599 --> 00:08:11,799
Oh, it's fine.
I'll be leaving work soon, anyway.

68
00:08:11,799 --> 00:08:13,000
What?

69
00:08:13,900 --> 00:08:16,700
Go home early.
It's the weekend, after all.

70
00:08:25,400 --> 00:08:27,700
What's the matter?
Did something happen?

71
00:08:27,700 --> 00:08:29,400
He says he's leaving soon.

72
00:08:31,000 --> 00:08:33,400
And he told all of us to go home early,
since it's the weekend.

73
00:08:34,199 --> 00:08:36,000
That workaholic just said that to me.

74
00:08:36,000 --> 00:08:37,799
Does that make any sense?

75
00:08:37,799 --> 00:08:40,900
Should you and I keep working
together, then, if you don't want to?

76
00:08:40,900 --> 00:08:42,799
No, I'd hate that even more!

77
00:08:45,000 --> 00:08:46,400
Heading home early!
Yes! Yes!

78
00:08:46,400 --> 00:08:48,699
- All right.
- Secretary Kang.

79
00:08:48,699 --> 00:08:50,400
Fancy seeing you here.

80
00:08:50,400 --> 00:08:52,500
Oh... oh, yes, ma'am!

81
00:08:59,100 --> 00:09:01,199
I have something to say.

82
00:09:01,199 --> 00:09:03,500
Don't be too mean to me.

83
00:09:03,500 --> 00:09:07,299
We weren't that bad together
three years ago.

84
00:09:09,500 --> 00:09:11,000
What is it that you want to say?

85
00:09:11,000 --> 00:09:12,799
You want to talk here?

86
00:09:21,400 --> 00:09:24,799
Let's start over.

87
00:09:28,000 --> 00:09:30,199
You don't have any feelings
toward me, though.

88
00:09:30,199 --> 00:09:31,600
What kind of nonsense--

89
00:09:31,600 --> 00:09:34,900
You don't care about things
like love either, anyway.

90
00:09:34,900 --> 00:09:37,799
And someone like me will
definitely be of help to you.

91
00:09:37,799 --> 00:09:39,400
SH Group and Hanjoo Chemicals--

92
00:09:39,400 --> 00:09:43,799
You're right. I don't care
about things like love.

93
00:09:43,799 --> 00:09:46,400
So I don't necessarily need to choose
Hanjoo Chemicals and you.

94
00:09:46,900 --> 00:09:50,799
Because I can find tons of
better alternatives.

95
00:09:50,799 --> 00:09:52,699
I'm sure that the chairman
would love to have me, though.

96
00:09:52,699 --> 00:09:56,400
Because he's fully aware of
what's best for the company.

97
00:09:56,400 --> 00:09:58,699
Then why don't you propose
to my grandpa instead?

98
00:10:11,500 --> 00:10:13,400
Sorry. I'm a bit late, aren't I?

99
00:10:14,199 --> 00:10:17,199
Yes, you are.
You're 15 minutes late.

100
00:10:20,199 --> 00:10:21,600
It's fine.

101
00:10:25,299 --> 00:10:27,900
Do you have a bit more free time now?

102
00:10:27,900 --> 00:10:29,400
No.

103
00:10:29,400 --> 00:10:31,400
I'm just making time,
even though I don't have any.

104
00:10:33,299 --> 00:10:36,000
A man will make time for a woman
no matter how busy he is

105
00:10:36,000 --> 00:10:38,299
if he really likes her!

106
00:10:39,900 --> 00:10:41,400
Why are you smiling?

107
00:10:41,900 --> 00:10:46,000
I was just thinking about what we should
do in this precious time you've spared.

108
00:10:47,699 --> 00:10:49,000
What do you want to do?

109
00:10:50,199 --> 00:10:51,900
Whose turn is it to choose this time?

110
00:10:54,500 --> 00:10:55,500
Is it me?

111
00:10:56,400 --> 00:10:57,400
Me?

112
00:11:06,199 --> 00:11:07,500
Hyun Jin!

113
00:11:07,500 --> 00:11:09,799
Oh, Brother!

114
00:11:10,299 --> 00:11:11,400
What brings you here?

115
00:11:11,400 --> 00:11:13,400
To see you, of course.
Why else would I come?

116
00:11:13,400 --> 00:11:14,600
Oh.

117
00:11:18,799 --> 00:11:20,400
How's business?

118
00:11:21,100 --> 00:11:22,799
Can you buy some of these?

119
00:11:24,500 --> 00:11:25,600
Hyun Jin.

120
00:11:26,299 --> 00:11:28,000
About the guy Da Hyun is seeing...

121
00:11:28,000 --> 00:11:29,600
you know who he is, right?

122
00:11:31,199 --> 00:11:35,000
He's Lee Jae In, right?
From SH Group?

123
00:11:36,400 --> 00:11:38,799
I knew his face looked familiar.

124
00:11:38,799 --> 00:11:40,699
Does Da Hyun's family
know about this, too?

125
00:11:40,699 --> 00:11:44,000
Of course not! So you better
keep your mouth shut too, okay?

126
00:11:44,000 --> 00:11:45,900
Why does she have to date him,
of all people?

127
00:11:45,900 --> 00:11:48,900
That's not really any of
our business, now is it?

128
00:11:48,900 --> 00:11:51,299
At any rate, don't go blabbing
about this anywhere.

129
00:11:51,299 --> 00:11:52,600
- Her parents will worry.
- Hey.

130
00:11:52,600 --> 00:11:54,400
I don't even have
anyone to tell, all right?

131
00:12:01,699 --> 00:12:03,400
I think it's my turn.

132
00:12:03,400 --> 00:12:05,199
What? Why?

133
00:12:05,199 --> 00:12:07,100
You came last time, and all
you did was sleep, remember?

134
00:12:07,100 --> 00:12:09,699
That's what I'm saying!
That date didn't count!

135
00:12:09,699 --> 00:12:10,900
We didn't even do anything!

136
00:12:10,900 --> 00:12:12,799
So who told you to fall asleep, then?

137
00:12:14,100 --> 00:12:16,400
Let's go to the hotel if there isn't
anything specific you want to do.

138
00:12:16,900 --> 00:12:19,100
What are we going do to there this time?

139
00:12:19,100 --> 00:12:21,400
There's an event marking
the first day of summer

140
00:12:21,400 --> 00:12:23,699
so I'm curious to see the
guests' reactions to it.

141
00:12:23,699 --> 00:12:24,900
Come on, let's go!

142
00:12:24,900 --> 00:12:27,100
There's even a fruit buffet
at the Tower Lounge.

143
00:12:28,500 --> 00:12:29,799
What?

144
00:12:31,699 --> 00:12:33,199
Nothing.

145
00:12:33,699 --> 00:12:35,000
Are you worried about something?

146
00:12:38,199 --> 00:12:39,500
Mr. Jae In.

147
00:12:39,500 --> 00:12:43,799
What do you consider to be
the most important thing in the world?

148
00:12:43,799 --> 00:12:45,100
What?

149
00:12:45,100 --> 00:12:47,900
Do you have anything that
you consider really important?

150
00:12:50,400 --> 00:12:51,799
Of course I do.

151
00:12:51,799 --> 00:12:53,199
Money.

152
00:12:53,799 --> 00:12:55,699
Money is the most
important thing to me.

153
00:12:56,500 --> 00:12:58,000
Money?
Really?

154
00:12:58,000 --> 00:13:00,299
That's the most important thing to you?

155
00:13:00,299 --> 00:13:03,400
Why are you so obsessed with money
when you're a third-generation heir?

156
00:13:03,400 --> 00:13:06,100
That's not my money.
It's all Grandpa's.

157
00:13:06,699 --> 00:13:09,699
I don't want to be a
third-generation heir.

158
00:13:09,699 --> 00:13:11,600
I want to be wealthy all on my own.

159
00:13:12,600 --> 00:13:16,900
Whatever.
I shouldn't have asked.

160
00:13:20,400 --> 00:13:22,000
Oh, Mr. Jae In!

161
00:13:24,199 --> 00:13:26,799
Are you all right?
I'm sorry!

162
00:13:27,199 --> 00:13:28,600
Are you okay?

163
00:13:29,400 --> 00:13:32,500
Watch where you're going, punk!
Are you hurt?

164
00:13:33,600 --> 00:13:34,799
Up you go.

165
00:13:36,900 --> 00:13:38,400
Be careful from now on, okay?

166
00:13:49,699 --> 00:13:51,299
What?

167
00:13:52,600 --> 00:13:54,000
What do you mean?

168
00:13:54,000 --> 00:13:58,199
You keep following me
and staring at me!

169
00:14:07,199 --> 00:14:08,199
<i>[Good job!]</i>

170
00:14:09,100 --> 00:14:11,000
What are you doing?

171
00:14:11,000 --> 00:14:12,900
It's a stamp, to reward you.

172
00:14:15,600 --> 00:14:17,900
Good job!

173
00:14:20,299 --> 00:14:21,699
Oh.

174
00:14:23,799 --> 00:14:25,600
Let's ride on that!

175
00:15:18,799 --> 00:15:21,500
I think your younger brother Jae In

176
00:15:21,500 --> 00:15:24,199
still thinks of you as his older brother.

177
00:15:24,799 --> 00:15:29,299
He made something that you loved most,
and gave it to me.

178
00:15:47,199 --> 00:15:49,000
It was bad enough that you
made me make plastic models

179
00:15:49,000 --> 00:15:50,900
now you're making me go rollerskating?

180
00:15:50,900 --> 00:15:52,799
Doesn't it look really fun?

181
00:15:52,799 --> 00:15:55,400
Is this something that
your students like to do, too?

182
00:15:55,400 --> 00:15:57,500
Hmm, probably.

183
00:15:57,500 --> 00:15:59,400
Have you done this before?

184
00:15:59,400 --> 00:16:01,000
- No.
- Me neither!

185
00:16:01,000 --> 00:16:03,400
There wasn't a place to do this
in my neighborhood.

186
00:16:04,000 --> 00:16:07,600
I couldn't do this because they told me
it was too dangerous.

187
00:16:07,600 --> 00:16:09,000
Try it now, then.

188
00:16:09,000 --> 00:16:10,699
Let's try it together!

189
00:16:10,699 --> 00:16:15,199
I'll be fine, but you'll probably break
a bone since you're so old!

190
00:16:15,799 --> 00:16:17,600
Whoa!

191
00:16:18,100 --> 00:16:19,400
Hey, what are you doing?

192
00:16:19,400 --> 00:16:21,799
All right.
Be careful, now.

193
00:16:21,799 --> 00:16:23,400
- Careful.
- Should I go this way?

194
00:16:23,400 --> 00:16:24,799
Turn. Turn.

195
00:16:25,299 --> 00:16:27,699
Whoa, whoa!

196
00:16:34,100 --> 00:16:35,799
One, two, three!

197
00:16:39,100 --> 00:16:40,699
I'm good, right?

198
00:16:46,600 --> 00:16:47,699
Slowly!

199
00:16:48,400 --> 00:16:51,299
- More!
- Whoosh!

200
00:16:51,299 --> 00:16:53,299
Whoa... whoa, whoa!

201
00:16:53,299 --> 00:16:55,100
Oh no!
Oh no!

202
00:16:55,100 --> 00:16:56,400
Ow, that hurts!

203
00:16:56,400 --> 00:16:57,600
Are you okay?

204
00:16:58,500 --> 00:17:01,100
You lied when you said that you've
never done this before, right?

205
00:17:01,100 --> 00:17:04,900
Why would I?
No, this is my first time.

206
00:17:05,999 --> 00:17:07,800
You said you've never
done this before, either.

207
00:17:07,800 --> 00:17:10,199
Yes, I really haven't.
This is my first time.

208
00:17:10,800 --> 00:17:12,400
Who was the person you last skated with?

209
00:17:12,900 --> 00:17:15,400
Geez. I told you,
I've never done this before!

210
00:17:16,100 --> 00:17:17,199
Let's get up.

211
00:17:20,199 --> 00:17:23,199
Whoa!
Oh no!

212
00:17:28,600 --> 00:17:30,400
Oh, just a second.

213
00:17:34,100 --> 00:17:37,300
Your grandpa is quite a stubborn person.

214
00:17:37,300 --> 00:17:43,999
After he saw your uncle leave the company
and his family behind because of a woman

215
00:17:43,999 --> 00:17:47,100
he didn't even spare a glance his way.

216
00:17:47,999 --> 00:17:51,900
As such, he'd have no problem throwing
either you or Jae In out in a heartbeat.

217
00:17:51,900 --> 00:17:55,999
But if Jae In hadn't been
adopted by my older uncle

218
00:17:55,999 --> 00:17:58,100
do you think that things would've
been any different now?

219
00:17:58,100 --> 00:18:03,199
Well, Jae In probably would've followed
his mom to Canada and lived there.

220
00:18:03,699 --> 00:18:07,400
Well, it's not like I have
any brothers now, so...

221
00:18:07,400 --> 00:18:10,600
my father may have brought
Jae In back here anyway.

222
00:18:11,199 --> 00:18:15,199
You don't fall short of Jae In
in any way.

223
00:18:15,199 --> 00:18:18,199
It's just that your last names
are different, is all.

224
00:18:18,699 --> 00:18:19,900
You know that, right?

225
00:18:20,400 --> 00:18:22,499
All you have to do is your best.

226
00:18:22,499 --> 00:18:25,400
A chance like this
won't ever come again.

227
00:18:34,600 --> 00:18:38,400
Wow, I don't think I'm cut out for this.
I can't do this anymore.

228
00:18:39,600 --> 00:18:41,400
What are you cut out for, then?

229
00:18:41,400 --> 00:18:43,800
I don't think you're all that skilled at
assembling plastic models, either.

230
00:18:49,199 --> 00:18:51,699
I knew that it'd been quiet
for way too long.

231
00:18:51,699 --> 00:18:53,699
There's no such thing as a
weekend for you, is there?

232
00:18:58,600 --> 00:19:00,400
Yeah, Mom?

233
00:19:03,100 --> 00:19:06,100
It's only been three months
since I started dating him, okay?

234
00:19:08,600 --> 00:19:10,199
I'll see later,
after I've dated him a bit longer!

235
00:19:10,199 --> 00:19:11,300
When the right time comes.

236
00:19:11,300 --> 00:19:14,400
Who cares if you like him or not?
I have to like him!

237
00:19:15,100 --> 00:19:16,900
I told you, we're not living together!

238
00:19:16,900 --> 00:19:17,900
Bye!

239
00:19:19,900 --> 00:19:21,800
Does she want us to come and see her?

240
00:19:21,800 --> 00:19:23,600
Don't even think about it.

241
00:19:23,600 --> 00:19:26,400
I thought she'd stop her nagging
after I got myself a man.

242
00:19:26,400 --> 00:19:29,199
Why is she so intent
on getting me married off?

243
00:19:30,199 --> 00:19:31,999
I'd be okay with meeting her.

244
00:19:36,100 --> 00:19:37,499
Then...

245
00:19:40,400 --> 00:19:42,600
do you want to...

246
00:19:44,900 --> 00:19:46,900
marry me?

247
00:19:55,999 --> 00:19:57,800
You freaked out a little just now,
didn't you?

248
00:19:59,100 --> 00:20:01,400
Don't go around saying
weird things anymore, okay?

249
00:20:01,400 --> 00:20:05,800
Because you and I can never get married.
subtitles ripped and synced by riri13

250
00:20:06,499 --> 00:20:08,300
Upsy-daisy!

251
00:20:26,199 --> 00:20:27,999
<i>You freaked out a little just now,
didn't you?</i>

252
00:20:28,800 --> 00:20:31,100
Don't go around saying
weird things anymore, okay?

253
00:20:31,100 --> 00:20:35,499
Because you and I
can never get married.

254
00:20:45,600 --> 00:20:47,900
What are you doing?

255
00:20:47,900 --> 00:20:49,300
It's a stamp, to reward you.

256
00:20:51,600 --> 00:20:53,999
Good job!

257
00:21:09,100 --> 00:21:11,999
What's up?
Something bothering you?

258
00:21:12,499 --> 00:21:14,800
Huh? No.
It's nothing.

259
00:21:19,400 --> 00:21:22,499
<i>[New singer Ji Su Has Signed Contract
With New Entertainment Agency]</i>

260
00:21:28,699 --> 00:21:31,300
Hey, what are you smiling about?

261
00:21:32,499 --> 00:21:34,800
- Oh, Ji Su got signed!
- Yeah.

262
00:21:35,300 --> 00:21:36,300
Wow.

263
00:21:36,300 --> 00:21:38,300
What time does your shop close today?

264
00:21:38,300 --> 00:21:43,100
Why? Did you want me to close early
so we can eat and get some drinks?

265
00:21:43,100 --> 00:21:46,300
No, no.
Rather than that...

266
00:21:46,300 --> 00:21:49,100
can you tell me what most guys are into?

267
00:21:49,100 --> 00:21:50,699
Guys?
Guys like who?

268
00:21:50,699 --> 00:21:51,699
Guys, as in Ji Su?

269
00:21:51,699 --> 00:21:54,499
No, not Ji Su.
Just, you know...

270
00:21:54,499 --> 00:21:56,800
Your average adult male.

271
00:21:56,800 --> 00:21:58,699
So you mean Mr. Lee Jae In, then.

272
00:21:58,699 --> 00:22:02,999
Well, I should do something for him.
He helped Ji Su so much, after all.

273
00:22:02,999 --> 00:22:05,400
A car worth 1 billion won
that a Saudi prince would drive.

274
00:22:05,400 --> 00:22:06,699
A 100 million won watch.

275
00:22:06,699 --> 00:22:08,999
- Do you have a death wish?
- No.

276
00:22:10,199 --> 00:22:12,600
Come on, just give me one hint!

277
00:22:12,600 --> 00:22:15,199
Well, there isn't much to it, really.

278
00:22:15,199 --> 00:22:16,999
- A woman?
- A woman?

279
00:22:33,499 --> 00:22:34,600
What do you want?

280
00:22:34,600 --> 00:22:38,499
Are you happy living this way?
Living someone else's life, I mean.

281
00:22:38,499 --> 00:22:40,699
I don't think you have the right
to say something like that to me.

282
00:22:40,699 --> 00:22:43,699
I've been Lee Jae In of SH Group
ever since I was born.

283
00:22:43,699 --> 00:22:48,100
I've never once lost my name,
my position, or my status.

284
00:22:48,100 --> 00:22:50,400
I've never once thought that I was
living someone else's life.

285
00:22:50,400 --> 00:22:54,100
And I've never given up on something
I wanted to do because of someone else.

286
00:22:55,100 --> 00:22:56,999
But I can't say the same for you.

287
00:23:01,199 --> 00:23:06,100
If you want to win against me,
break free from your father first.

288
00:23:06,100 --> 00:23:08,699
And don't be influenced by
what your mother says, either.

289
00:23:08,699 --> 00:23:10,999
And when you've done that,
come and find me again.

290
00:23:10,999 --> 00:23:14,699
I'll consider you as my opponent then.

291
00:23:31,699 --> 00:23:33,499
Yeah.

292
00:23:34,699 --> 00:23:38,199
Here?
At SH Mall?

293
00:23:54,699 --> 00:23:56,900
Can I see this for a moment?

294
00:24:12,699 --> 00:24:14,699
Thank you.

295
00:24:17,699 --> 00:24:19,400
Goodbye.

296
00:24:33,100 --> 00:24:34,699
Mommy!

297
00:24:35,999 --> 00:24:38,199
Oh, I'm not your mom, sweetheart.

298
00:24:38,199 --> 00:24:39,800
- Oh no.
- Mommy!

299
00:24:39,800 --> 00:24:41,600
Oh no, don't cry!

300
00:24:42,999 --> 00:24:47,100
Oh no, don't cry.
I'll find your mom for you. Okay?

301
00:24:47,100 --> 00:24:50,400
Oh, it's okay!
It's okay!

302
00:24:50,400 --> 00:24:52,400
Your mom will be here soon, okay?

303
00:24:53,100 --> 00:24:55,400
But where is your mom?
What should we do?

304
00:24:56,100 --> 00:24:57,699
Oh no!

305
00:24:57,699 --> 00:24:59,800
We should take her to the
Lost Children Center.

306
00:25:17,400 --> 00:25:19,499
Thank goodness we found her mother.

307
00:25:19,499 --> 00:25:20,800
Yes.

308
00:25:21,699 --> 00:25:23,900
Well, goodbye.

309
00:25:23,900 --> 00:25:25,100
Um, wait.

310
00:25:26,199 --> 00:25:27,900
I'm sorry.

311
00:25:27,900 --> 00:25:30,199
- But, um...
- Yes?

312
00:25:30,199 --> 00:25:33,400
I'd like to buy you a meal,
if that's okay with you.

313
00:25:33,400 --> 00:25:36,100
Just as a thank you.

314
00:25:36,100 --> 00:25:37,900
I'm SH Mall's...

315
00:25:37,900 --> 00:25:39,600
employee, you see.

316
00:25:39,600 --> 00:25:42,800
Oh... oh!
It was nothing, though.

317
00:25:42,800 --> 00:25:44,400
Thank you for your kind
sentiments, though.

318
00:25:44,400 --> 00:25:46,900
Could I have your phone number then?

319
00:25:46,900 --> 00:25:49,600
I'd like to contact you later.

320
00:25:50,699 --> 00:25:55,100
Um, I have a boyfriend.

321
00:25:55,100 --> 00:25:56,300
Pardon?

322
00:25:56,300 --> 00:25:59,699
Well, um, I felt like I had
to tell you, just in case.

323
00:25:59,699 --> 00:26:00,999
Goodbye.

324
00:26:17,999 --> 00:26:20,699
What was that all about?
Was he trying to get my number?

325
00:26:21,600 --> 00:26:24,400
Oh, stop overreacting, Kim Da Hyun.

326
00:26:34,499 --> 00:26:36,100
I left something here.

327
00:26:37,199 --> 00:26:39,300
I don't think Ms. Hyun Jin is here.

328
00:26:39,300 --> 00:26:42,600
It's a bit early for her to have
closed up her shop, though.

329
00:26:51,999 --> 00:26:54,999
I borrowed her entire store,
just for you!

330
00:26:54,999 --> 00:26:56,999
I'm amazing, aren't I?

331
00:26:59,100 --> 00:27:00,199
Come here.

332
00:27:05,999 --> 00:27:09,300
I thought I saw on that contract we
signed that today was your birthday.

333
00:27:09,300 --> 00:27:10,600
Was I right?

334
00:27:11,499 --> 00:27:12,999
I think that's right.

335
00:27:13,900 --> 00:27:16,699
What do you mean, "I think?"
It's a yes-or-no question.

336
00:27:16,699 --> 00:27:19,100
You didn't have any seaweed soup
(eaten on birthdays) this morning?

337
00:27:22,699 --> 00:27:25,900
This is why you need to make sure
to get three square meals per day.

338
00:27:27,100 --> 00:27:29,999
Did you call your family?

339
00:27:30,499 --> 00:27:33,499
And say what? "Give me a present
because it's my birthday?"

340
00:27:33,499 --> 00:27:35,100
That's kind of silly.

341
00:27:35,100 --> 00:27:37,999
Who'd call their family to ask
for presents at your age?

342
00:27:37,999 --> 00:27:39,600
You're not a child.

343
00:27:40,100 --> 00:27:44,400
You should call and say, "thank you
for giving birth to me, and raising me."

344
00:27:47,199 --> 00:27:51,900
"Thank you for giving birth to me,
and raising me?"

345
00:27:56,400 --> 00:28:02,600
<i>Happy birthday to you,
Happy birthday to you!</i>

346
00:28:02,600 --> 00:28:04,699
<i>Happy birthday, dear--</i>

347
00:28:04,699 --> 00:28:13,400
<i>I mean, Mr. Jae In!
Happy birthday to you!</i>

348
00:28:15,499 --> 00:28:16,800
Here.

349
00:28:18,199 --> 00:28:20,100
Happy birthday.

350
00:28:51,699 --> 00:28:52,699
What are you doing?

351
00:28:52,699 --> 00:28:54,900
Hurry up and make a wish
and blow out the candles.

352
00:28:55,999 --> 00:28:58,900
Have you never blown out
candles on a cake before?

353
00:29:04,300 --> 00:29:05,999
It's just been so long, is all.

354
00:29:05,999 --> 00:29:07,800
How long, exactly?

355
00:29:07,800 --> 00:29:10,999
About twenty...three years.

356
00:29:12,900 --> 00:29:15,800
Then, I'm sure you haven't
done this, either!

357
00:29:19,499 --> 00:29:22,699
I thought you taught your students
not to play with their food.

358
00:29:22,699 --> 00:29:24,600
Kids don't really listen, though.

359
00:29:24,600 --> 00:29:26,699
But I do see the appeal now.

360
00:29:29,400 --> 00:29:32,699
Hey, come on. Smile a little!
These will all be cherished memories!

361
00:29:32,699 --> 00:29:34,600
Here.
Smile!

362
00:29:35,800 --> 00:29:37,800
One, two, three!

363
00:29:37,800 --> 00:29:39,100
There we go.

364
00:29:39,800 --> 00:29:41,699
Let's cut the cake now.

365
00:29:41,699 --> 00:29:43,999
Just a moment.
Not yet.

366
00:29:43,999 --> 00:29:46,100
We have to do this together.

367
00:29:48,800 --> 00:29:50,600
Don't do it!
No! No!

368
00:29:51,999 --> 00:29:54,699
I thought you said that we
shouldn't play with our food!

369
00:29:54,699 --> 00:29:57,199
I'm as disobedient as your kids.

370
00:30:00,499 --> 00:30:01,999
All right.

371
00:30:01,999 --> 00:30:03,999
Smile a little!

372
00:30:05,900 --> 00:30:07,600
One, two, three!

373
00:30:10,999 --> 00:30:15,199
Geez. You just refuse to be
the only one who suffers, huh?

374
00:30:17,300 --> 00:30:19,400
Have you been like this
since you were a child?

375
00:30:19,400 --> 00:30:21,900
I told you. I've never done
anything like this before.

376
00:30:21,900 --> 00:30:23,999
Really?
Wow.

377
00:30:23,999 --> 00:30:25,800
Do rich people usually not celebrate
their birthdays, or something?

378
00:30:25,800 --> 00:30:27,800
I thought they'd throw
extravagant parties, and such.

379
00:30:27,800 --> 00:30:31,400
Today's the anniversary
of my older brother's death.

380
00:30:34,199 --> 00:30:36,100
Was he directly related to you?

381
00:30:37,100 --> 00:30:39,100
Legally, yes.

382
00:30:39,100 --> 00:30:40,999
But he was actually my older cousin.

383
00:30:41,499 --> 00:30:45,400
I was adopted into his
family after he died.

384
00:30:45,900 --> 00:30:49,100
Then, what about the person
who's your mother now?

385
00:30:49,100 --> 00:30:52,100
She's my mother.
She's a good person.

386
00:30:53,300 --> 00:30:56,100
You don't have to look
at me with such pity.

387
00:30:57,100 --> 00:30:59,100
Oh... I don't feel any pity
toward you at all!

388
00:30:59,100 --> 00:31:02,199
You have two great mothers, after all!

389
00:31:02,199 --> 00:31:03,400
Is that right?

390
00:31:03,400 --> 00:31:09,199
Someone said to me today
that I'm living someone else's life.

391
00:31:11,600 --> 00:31:13,900
What kind of complete nonsense...

392
00:31:13,900 --> 00:31:17,199
as if you'd allow yourself to do that,
with that personality of yours!

393
00:31:17,699 --> 00:31:19,800
And you're not the type to
take orders from anyone, either.

394
00:31:19,800 --> 00:31:21,800
You're so stubborn.

395
00:31:22,300 --> 00:31:27,600
There's nobody as stupidly
stubborn as you are.

396
00:31:27,600 --> 00:31:31,199
Today's my birthday, you know.
And yet you're insulting me?

397
00:31:32,300 --> 00:31:35,800
Yes, but the truth is still the truth.

398
00:31:40,499 --> 00:31:41,999
Where's the knife?

399
00:31:41,999 --> 00:31:43,199
In here, probably.

400
00:31:50,999 --> 00:31:52,600
It's a gift for you.

401
00:31:53,199 --> 00:31:54,699
You're not going to open it?

402
00:31:54,699 --> 00:31:59,300
I'm sorry it's not a billion-won car
or a 100 million-won watch.

403
00:32:00,199 --> 00:32:02,600
But in exchange, you don't have
to buy me such things, either.

404
00:32:02,600 --> 00:32:04,400
I already have cars and watches.

405
00:32:04,900 --> 00:32:07,999
Why? Did someone tell you
that I'd need something like that?

406
00:32:07,999 --> 00:32:09,400
Yes, Hyun Jin did.

407
00:32:09,400 --> 00:32:11,199
Sun W--

408
00:32:13,100 --> 00:32:17,999
I mean, um, Old Mister Sun Woo
apparently likes those things.

409
00:32:19,100 --> 00:32:20,900
I like that!
"Old Mister!"

410
00:32:21,699 --> 00:32:23,699
Make sure you call him that
from now on, okay?

411
00:32:23,699 --> 00:32:25,499
Hurry up and open this first!

412
00:32:27,400 --> 00:32:29,999
This is something
that you definitely need.

413
00:32:29,999 --> 00:32:31,900
It'll help you get along
better with others.

414
00:32:32,999 --> 00:32:34,699
"For ages 8 and up?"

415
00:32:34,699 --> 00:32:36,400
I thought you said
that I was an old man, though.

416
00:32:36,400 --> 00:32:38,400
And you got a year older today!

417
00:32:38,400 --> 00:32:41,900
That's why I also prepared
an adult-friendly gift, too.

418
00:32:42,100 --> 00:32:44,100
Here.
Ta-da!

419
00:32:45,499 --> 00:32:46,800
Want me to open it?

420
00:32:47,499 --> 00:32:49,900
I wanted to get you
a tie with that

421
00:32:49,900 --> 00:32:53,900
but I didn't want you to say
that I had lascivious thoughts.

422
00:32:53,900 --> 00:32:55,400
Lascivious thoughts?

423
00:32:55,900 --> 00:32:56,999
You don't know what
I'm talking about?

424
00:32:56,999 --> 00:32:58,699
No, what are you talking about?

425
00:32:59,300 --> 00:33:02,400
Well, um... never mind, then.

426
00:33:02,400 --> 00:33:04,400
Hey, you should explain things
to me if I don't know!

427
00:33:04,400 --> 00:33:05,499
What is it?

428
00:33:05,499 --> 00:33:07,699
- Oh...
- What is it?

429
00:33:19,900 --> 00:33:21,699
"You're mine."

430
00:33:24,400 --> 00:33:26,300
That's what gifting a tie means.

431
00:33:30,300 --> 00:33:33,699
That's what Hyun Jin told me.

432
00:33:34,800 --> 00:33:36,600
Buy me a tie, then.

433
00:33:36,600 --> 00:33:38,900
You get what you get,
and you don't get upset.

434
00:33:38,900 --> 00:33:43,400
You're supposed to buy people what
they want for it to be a great gift!

435
00:33:43,400 --> 00:33:45,999
This is the first birthday present I've
gotten in twenty-something years.

436
00:33:46,499 --> 00:33:48,999
Complaining about a
gift isn't polite, you know.

437
00:33:48,999 --> 00:33:51,300
There's one present that
I really want, though.

438
00:34:07,699 --> 00:34:11,599
<i>[Happy Birthday, Mr. Jae In]</i>

439
00:34:19,099 --> 00:34:21,200
Get home safe.

440
00:34:22,900 --> 00:34:24,700
Oh yeah.

441
00:34:24,700 --> 00:34:26,900
Let's meet up near my
school next time, okay?

442
00:34:26,900 --> 00:34:29,200
No thanks.
I'm busy.

443
00:34:29,200 --> 00:34:31,599
I thought you said that the issue
at the hotel got resolved already.

444
00:34:31,599 --> 00:34:33,099
And I'm busy too, you know.

445
00:34:33,099 --> 00:34:35,099
You have more free time
than I do, though!

446
00:34:35,099 --> 00:34:36,999
That's just your opinion.

447
00:34:36,999 --> 00:34:39,599
Let's do this fair and square, okay?

448
00:34:40,200 --> 00:34:43,099
- Fair and square?
- Yeah. Fair and square.

449
00:34:43,099 --> 00:34:46,900
You have to come see me once
if I come to see you once

450
00:34:46,900 --> 00:34:49,800
and I get to solve one of my issues
once you've solved one of yours.

451
00:34:49,800 --> 00:34:51,499
We have to do things like this
for all of this to be fair!

452
00:34:51,499 --> 00:34:53,400
There was nothing like that
in the contract.

453
00:34:53,400 --> 00:34:54,900
Of course there is!

454
00:34:54,900 --> 00:34:56,900
"We will respect each others' opinions."

455
00:34:56,900 --> 00:34:59,499
That was in there as the third clause.

456
00:34:59,499 --> 00:35:01,300
Today's my birthday, though.

457
00:35:02,400 --> 00:35:06,300
That's why I'm saying
we'll do this after today.

458
00:35:11,900 --> 00:35:14,200
Happy birthday.

459
00:35:14,200 --> 00:35:16,999
Even though you only have an hour and
a half left until your birthday ends.

460
00:35:25,499 --> 00:35:26,900
Thanks.

461
00:35:29,700 --> 00:35:30,999
Bye.

462
00:35:36,900 --> 00:35:37,900
Go in.

463
00:36:03,900 --> 00:36:05,099
Yes, Mother?

464
00:36:05,099 --> 00:36:09,099
Today's my birthday, apparently.

465
00:36:15,599 --> 00:36:18,400
J... Jae In.

466
00:36:20,200 --> 00:36:21,800
<i>I'm sorry.</i>

467
00:36:22,400 --> 00:36:26,099
I'm so... so sorry--

468
00:36:26,099 --> 00:36:30,400
<i>Thank you for raising me well.</i>

469
00:36:30,400 --> 00:36:32,999
I don't think I've ever
thanked you for that.

470
00:36:33,900 --> 00:36:35,099
Okay.

471
00:36:37,099 --> 00:36:38,599
<i>Thanks.</i>

472
00:36:43,800 --> 00:36:46,300
Thanks, Jae In.

473
00:36:49,099 --> 00:36:52,400
Goodnight, Mother.
I'll be hanging up now.

474
00:37:23,999 --> 00:37:26,400
Here's the result of the survey
that 1,000 Chinese tourists took.

475
00:37:26,400 --> 00:37:32,700
The mean number they spend on
accommodations is 80,000 won per day.

476
00:37:32,700 --> 00:37:35,400
They stay at cheap short-term
rentals, hostels, motels

477
00:37:35,400 --> 00:37:38,499
or rent an apartment that is illegally
being used as a bed-and-breakfast...

478
00:37:42,099 --> 00:37:45,999
- Bye, everyone!
- Goodbye!

479
00:37:50,800 --> 00:37:53,400
Make sure you write up and send me
the minutes for the meeting.

480
00:37:53,400 --> 00:37:54,499
Yes, sir.

481
00:37:54,499 --> 00:37:57,700
And as for the plan to attract
Youker to us... we'll be doing it.

482
00:37:59,400 --> 00:38:00,900
Um, yes, sir.

483
00:38:10,200 --> 00:38:13,700
<i>I'm sorry, but I have to cancel today.</i>

484
00:38:15,400 --> 00:38:17,400
<i>[What?]</i>

485
00:38:19,800 --> 00:38:21,800
<i>Something urgent came up.</i>

486
00:38:21,800 --> 00:38:25,800
<i>Think of us as being even now
for you not showing up to our date.</i>

487
00:38:34,200 --> 00:38:37,400
<i>[So what's this "urgent matter"
that came up, then?]</i>

488
00:38:44,099 --> 00:38:45,999
What the hell is with this woman?

489
00:38:46,499 --> 00:38:48,900
So she's just ignoring my
texts completely now, huh?

490
00:38:54,026 --> 00:39:04,026
Subtitles by DramaFever

491
00:39:15,099 --> 00:39:17,200
<i>Is our CEO even really dating someone?</i>

492
00:39:17,200 --> 00:39:18,999
<i>I heard it through the grapevine
that he is.</i>

493
00:39:18,999 --> 00:39:21,400
<i>I meant it when I said
let's start over.</i>

494
00:39:21,400 --> 00:39:23,400
<i>Sun Woo told me something strange.</i>

495
00:39:23,400 --> 00:39:24,999
<i>So the man you're dating is a rich guy?</i>

496
00:39:24,999 --> 00:39:26,400
<i>- You can't trust me?
- You wench!</i>

497
00:39:26,400 --> 00:39:27,599
<i>Break up with him!</i>

498
00:39:27,599 --> 00:39:28,599
<i>Why does she not like
the fact that I'm rich?</i>

499
00:39:28,599 --> 00:39:31,400
<i>I thought about it, and no
other man compares to you.</i>

500
00:39:31,400 --> 00:39:32,900
<i>It's just a business transaction!</i>

501
00:39:32,900 --> 00:39:34,900
<i>- Just a business transaction?
- I made it clear to my mom</i>

502
00:39:34,900 --> 00:39:35,900
<i>that we're not getting married.</i>

503
00:39:35,900 --> 00:39:37,999
<i>I think we'll have to take
some measures.</i>

504
00:39:38,499 --> 00:39:40,599
<i>Are you our teacher's boyfriend, Mister?</i>

505
00:39:40,599 --> 00:39:42,599
<i>There are going to be tons of rumors
circulating around the school now!</i>

506
00:39:42,599 --> 00:39:44,200
<i>What are you going to do about it?</i>

507
00:39:46,900 --> 00:39:48,727
<i>[One Percent of Something]</i>
