﻿1
00:00:05,967 --> 00:00:07,877
2,500 years ago

2
00:00:08,053 --> 00:00:10,378
Prince Siddharta Gautama

3
00:00:10,555 --> 00:00:14,173
set out from his home
in the foothills of the Himalayas

4
00:00:14,351 --> 00:00:18,395
and embarked on a journey
in search of the truth.

5
00:00:18,563 --> 00:00:20,105
Many years later,

6
00:00:20,273 --> 00:00:23,477
he arrived in the lowlands
of the Ganges river

7
00:00:23,652 --> 00:00:26,190
near the village of Bodh Gaya.

8
00:00:26,363 --> 00:00:30,526
The scene must have been very similar
to the one we see here.

9
00:00:31,576 --> 00:00:35,277
He sat down under a tree
and attained enlightenment.

10
00:00:35,455 --> 00:00:40,247
Since then he has been known
as the Buddha, The Enlightened One.

11
00:00:47,217 --> 00:00:53,255
WHEEL OF TIME

12
00:00:59,062 --> 00:01:04,602
A FILM BY WERNER HERZOG

13
00:01:10,907 --> 00:01:16,531
KALACHAKRA INITIATION 2002
(BODH GAYA, INDIA / GRAZ, AUSTRIA)

14
00:01:21,918 --> 00:01:27,043
WITH THE COLLABORATION
OF HIS HOLINESS, THE XIV DALAI LAMA

15
00:01:32,053 --> 00:01:36,347
For the Buddhist world,
this is sacred ground.

16
00:01:37,184 --> 00:01:40,268
Pilgrims flock here
from across the world.

17
00:01:41,563 --> 00:01:44,315
The modern-day village of Bodh Gaya

18
00:01:44,483 --> 00:01:48,611
still has a bustling market,
rickshaws and beggars.

19
00:01:48,778 --> 00:01:50,855
The temple in the background,

20
00:01:51,031 --> 00:01:52,822
called a stupa,

21
00:01:52,991 --> 00:01:57,203
marks the spot
where the Buddha sat in meditation.

22
00:02:02,584 --> 00:02:07,827
His Holiness, the Dalai Lama,
has called upon the Buddhist world

23
00:02:08,006 --> 00:02:12,051
to gather here
for the Kalachakra Initiation.

24
00:02:12,719 --> 00:02:18,224
This ceremony takes place
at a specially designated location,

25
00:02:18,558 --> 00:02:22,307
at irregular intervals,
around every two to three years.

26
00:02:23,104 --> 00:02:26,355
Half a million pilgrims
heeded the call.

27
00:02:26,525 --> 00:02:29,229
No other religious event

28
00:02:29,402 --> 00:02:33,103
is so eagerly anticipated
by Buddhist followers.

29
00:02:37,661 --> 00:02:40,697
At the base of the stupa
stands the Bodhi Tree,

30
00:02:40,872 --> 00:02:42,746
The Tree of Enlightenment.

31
00:02:42,916 --> 00:02:47,874
It is the fifth-generation descendant
of the original tree.

32
00:02:48,839 --> 00:02:51,793
Monks have gathered under its branches

33
00:02:51,967 --> 00:02:54,802
to prepare for the ceremonies.

34
00:03:03,937 --> 00:03:07,935
Kalachakra literally means
"wheel of time".

35
00:03:08,108 --> 00:03:12,188
Kalachakra involves
a complex initiation rite

36
00:03:12,362 --> 00:03:16,276
which aims to activate
the seed of enlightenment,

37
00:03:16,449 --> 00:03:19,237
lying dormant in all living beings.

38
00:03:21,580 --> 00:03:25,198
Butter lamps symbolize
this enlightenment.

39
00:03:35,844 --> 00:03:40,173
The central ritual revolves
around the creation of a picture,

40
00:03:40,348 --> 00:03:42,804
known as a Sand Mandala.

41
00:03:43,435 --> 00:03:45,807
It depicts an inner landscape,

42
00:03:45,979 --> 00:03:48,387
which represents the Wheel of Time.

43
00:03:48,565 --> 00:03:53,143
It is viewed as a Buddha's dwelling,
comprising more than 700 deities

44
00:03:53,320 --> 00:03:56,486
or manifestations of Buddha nature.

45
00:03:56,656 --> 00:03:58,898
The Sand Mandala is so complex

46
00:03:59,242 --> 00:04:03,110
that we turned to the Dalai Lama
for further explanation.

47
00:05:51,855 --> 00:05:55,769
The Kalachakra Initiation
is comprised of teachings,

48
00:05:55,942 --> 00:05:59,276
prayers,
the composition of a Sand Mandala,

49
00:05:59,446 --> 00:06:01,771
and the initiation itself.

50
00:06:01,948 --> 00:06:07,619
500,000 believers will assemble here
over the next ten days and nights.

51
00:06:19,591 --> 00:06:22,675
The pilgrims have travelled
great distances

52
00:06:22,844 --> 00:06:25,964
from Tibet, Nepal and Bhutan.

53
00:06:26,139 --> 00:06:30,600
From the steppes of Mongolia.
From Thailand and Sri Lanka.

54
00:07:28,285 --> 00:07:32,496
They have journeyed
thousands of kilometres by truck.

55
00:07:33,415 --> 00:07:38,041
But many pilgrims from central Tibet
completed their voyage on foot.

56
00:08:08,783 --> 00:08:10,657
A number of these pilgrims

57
00:08:10,827 --> 00:08:13,912
covered
the entire journey in this way.

58
00:10:04,149 --> 00:10:07,981
On the second day of festivities
we met this monk.

59
00:10:08,153 --> 00:10:12,696
He walked over 4,000 km,
prostrating himself at every step,

60
00:10:12,866 --> 00:10:15,701
all the way from the Amdo province,

61
00:10:15,869 --> 00:10:18,953
far beyond
the Tibet Autonomous Region.

62
00:10:19,664 --> 00:10:24,622
He explains that his journey
lasted over three and a half years.

63
00:10:26,379 --> 00:10:29,131
He comes from such a remote region

64
00:10:29,299 --> 00:10:32,798
that his dialect
has to be translated twice.

65
00:10:32,969 --> 00:10:37,216
Firstly into standard Tibetan,
and then once again for us.

66
00:10:45,732 --> 00:10:49,860
On his journey, he experienced
hunger, thirst, heat and cold.

67
00:10:50,028 --> 00:10:52,863
Word spread about his pilgrimage.

68
00:10:53,031 --> 00:10:55,949
Sometimes nomads would approach him

69
00:10:56,117 --> 00:10:59,118
and offer him water
and something to eat.

70
00:11:01,248 --> 00:11:04,202
He prefers not to make a fuss
about his voyage.

71
00:11:04,543 --> 00:11:08,161
Ganglions began to form on his hands

72
00:11:08,338 --> 00:11:11,789
although he wore wooden clogs
to protect them.

73
00:11:12,050 --> 00:11:15,585
Touching the ground
several million times

74
00:11:15,762 --> 00:11:19,546
left a wound on his forehead
which has yet to heal.

75
00:11:38,201 --> 00:11:41,036
Would he like to comment
on his journey?

76
00:11:41,204 --> 00:11:45,783
"Yes," he replies, " I have learned
of the magnitude of the earth."

77
00:11:45,959 --> 00:11:49,660
"I measured it with my body,
from head to toe.

78
00:11:49,838 --> 00:11:51,831
Now I understand."

79
00:11:52,007 --> 00:11:54,498
He battled on tirelessly

80
00:11:54,676 --> 00:11:58,211
and now a deep serenity
has descended upon him

81
00:11:58,388 --> 00:12:02,053
having reached
The Tree of Enlightenment.

82
00:12:21,578 --> 00:12:26,323
The Sand Mandala will be created
in a specially enclosed area.

83
00:12:28,752 --> 00:12:32,666
The Mandala will be assembled
on this surface.

84
00:12:35,675 --> 00:12:39,376
Before work begins,
the surface must be consecrated.

85
00:15:15,502 --> 00:15:20,330
The teaching and prayer area
is situated directly outside.

86
00:15:28,390 --> 00:15:32,174
The area is presided over
by high-ranking lamas.

87
00:15:33,687 --> 00:15:37,850
The Dalai Lama
should be conducting the teachings,

88
00:15:38,024 --> 00:15:39,567
but he has fallen ill

89
00:15:39,734 --> 00:15:42,854
and is unable to attend
the first few days.

90
00:16:14,644 --> 00:16:18,263
Back to the stupa,
only a few hundred metres away.

91
00:16:19,524 --> 00:16:21,683
Pilgrims flock to this pillar,

92
00:16:21,860 --> 00:16:25,311
believing it
to possess curative powers.

93
00:17:20,127 --> 00:17:23,875
At the stupa,
pilgrims circle the inner sanctum.

94
00:17:24,047 --> 00:17:27,630
Others are immersed
in prayer or meditation.

95
00:20:09,504 --> 00:20:12,873
Work on the Sand Mandala
has commenced.

96
00:20:14,801 --> 00:20:19,629
The Dalai Lama would normally
construct the initial lines.

97
00:20:19,806 --> 00:20:22,214
However, he is still in bad health

98
00:20:22,392 --> 00:20:26,390
and learned monks
must proceed without him.

99
00:20:27,939 --> 00:20:31,889
At the centre of the pattern
lies the Wheel of Time.

100
00:20:34,279 --> 00:20:38,324
Whole libraries exist
on the Mandala's symbols

101
00:20:38,492 --> 00:20:42,738
to help guide the monks
through its intricate maze.

102
00:23:04,846 --> 00:23:07,468
Facing The Tree of Enlightenment,

103
00:23:07,641 --> 00:23:10,844
the faithful perform prostrations.

104
00:23:11,353 --> 00:23:14,804
Their goal is to repeat this
100,000 times.

105
00:23:15,816 --> 00:23:20,524
The fittest among them
will manage this in about six weeks.

106
00:25:52,722 --> 00:25:56,388
One of the learned monks
has the privilege

107
00:25:56,560 --> 00:26:01,222
of defending his doctoral thesis
on Buddhist philosophy

108
00:26:01,398 --> 00:26:05,348
in public under the Bodhi Tree.

109
00:26:06,194 --> 00:26:08,816
He is the monk sitting on the right.

110
00:26:17,622 --> 00:26:22,830
The debate unfolds in a manner
typical of Tibetan monastic life.

111
00:26:25,714 --> 00:26:30,672
The scholarly debate revolves
around two aspects of reality.

112
00:26:30,844 --> 00:26:34,427
The functional reality
of our everyday life

113
00:26:34,598 --> 00:26:36,140
and emptiness

114
00:26:36,308 --> 00:26:39,059
which is viewed in Buddhist teachings

115
00:26:39,227 --> 00:26:41,932
as the ultimate reality.

116
00:27:09,341 --> 00:27:14,335
The debate is adjourned for lunch
without reaching a conclusive outcome.

117
00:27:35,575 --> 00:27:38,861
In the sprawling tent cities
around Bodh Gaya,

118
00:27:39,037 --> 00:27:42,572
monks are assigned
various tasks in the kitchen.

119
00:27:42,916 --> 00:27:46,996
The brethren each take it in turns
to do particular chores.

120
00:29:49,084 --> 00:29:50,875
As night falls,

121
00:29:51,044 --> 00:29:53,879
the debate gets into full swing.

122
00:32:15,730 --> 00:32:20,476
As the Sand Mandala symbolizes
the blueprint of an inner universe,

123
00:32:20,652 --> 00:32:23,404
the vision of a sacred cosmos,

124
00:32:23,572 --> 00:32:25,611
we felt compelled

125
00:32:25,782 --> 00:32:29,317
to visit
the most sacred of all Buddhist sites,

126
00:32:29,494 --> 00:32:31,866
Mount Kailash.

127
00:32:32,831 --> 00:32:36,781
The holy mountain,
located in Western Tibet,

128
00:32:36,960 --> 00:32:41,622
is venerated in equal measure
by both Buddhists and Hindus,

129
00:32:41,798 --> 00:32:45,298
who consider it to be
the seat of the deity Shiva.

130
00:32:45,469 --> 00:32:48,220
Long before the existence of Buddhism,

131
00:32:48,388 --> 00:32:53,015
followers of the Tibetan Bön religion
venerated the mountain

132
00:32:53,185 --> 00:32:55,224
and still do today.

133
00:32:55,604 --> 00:32:57,929
The mountain and the Mandala

134
00:32:58,273 --> 00:33:01,642
are both viewed
as a Buddha's dwelling place.

135
00:33:04,154 --> 00:33:08,365
In the shadow of Mount Kailash
lies Lake Manasarovar,

136
00:33:08,533 --> 00:33:10,692
also believed to be sacred

137
00:33:10,869 --> 00:33:14,867
and viewed
as the mountain's female consort.

138
00:33:29,304 --> 00:33:32,922
The Tibetans christened
the mount Kang Rinpoche,

139
00:33:33,099 --> 00:33:35,507
"precious snow jewel".

140
00:33:43,318 --> 00:33:47,482
All year long,
pilgrims flock to the mountain.

141
00:33:47,656 --> 00:33:52,234
Numbers soar dramatically
during full moon at the end of May.

142
00:33:52,410 --> 00:33:55,495
This day coincides
with the birth of Buddha

143
00:33:55,664 --> 00:33:57,455
and, at the same time,

144
00:33:57,624 --> 00:34:00,708
marks the moment
he attained enlightenment.

145
00:34:01,711 --> 00:34:05,163
Every twelve years,
during the Year of the Horse,

146
00:34:05,340 --> 00:34:08,341
vast numbers of pilgrims gather here

147
00:34:08,510 --> 00:34:11,001
to assemble on the holy land.

148
00:34:11,513 --> 00:34:14,798
The pilgrims begin their prostrations

149
00:34:14,975 --> 00:34:18,723
the moment Mount Kailash
comes into view.

150
00:34:34,035 --> 00:34:38,329
The pilgrims have travelled here
without any Western comforts

151
00:34:38,498 --> 00:34:41,250
perched on top of open trucks.

152
00:34:41,418 --> 00:34:44,039
Using primitive bellows,

153
00:34:44,212 --> 00:34:47,462
they kindle fires of dry yak dung

154
00:34:47,632 --> 00:34:49,175
and make tea.

155
00:34:49,885 --> 00:34:53,467
They add barley flour
and rancid yak butter.

156
00:34:53,638 --> 00:34:58,135
These are nearly
the only provisions they have.

157
00:36:34,523 --> 00:36:38,188
A tent city has emerged
at the base of the mountain.

158
00:36:39,278 --> 00:36:41,769
The tents are considered a luxury.

159
00:36:41,947 --> 00:36:46,526
Most pilgrims will spend
the freezing nights out in the open,

160
00:36:46,702 --> 00:36:49,027
wrapped only in their overcoats.

161
00:37:02,134 --> 00:37:06,049
The first day begins
with the Saka Dawa festival.

162
00:37:06,222 --> 00:37:11,347
The festival culminates
with the erection of a tall mast

163
00:37:11,519 --> 00:37:15,433
using ropes adorned
with chains of prayer flags.

164
00:40:32,428 --> 00:40:34,884
After the mast has been erected,

165
00:40:35,056 --> 00:40:38,756
the pilgrims set off on the Kora,

166
00:40:38,934 --> 00:40:41,852
or circumambulation of Mount Kailash.

167
00:40:44,732 --> 00:40:48,564
Trekking around
the entire base of the mountain

168
00:40:48,736 --> 00:40:50,942
covers a distance of 52 km.

169
00:40:51,113 --> 00:40:53,485
The Kora will take them three days

170
00:40:53,658 --> 00:40:57,572
at an average altitude
of well over 5,000 metres.

171
00:40:57,745 --> 00:40:59,536
On the second day,

172
00:40:59,705 --> 00:41:03,750
they will have to cross a pass
at an altitude of 5,600 metres.

173
00:41:03,918 --> 00:41:08,081
Each year, several pilgrims
die from altitude sickness.

174
00:41:08,255 --> 00:41:11,755
Many of the faithful
come from the Indian lowlands

175
00:41:11,926 --> 00:41:15,840
and are not adequately
accustomed to the climate.

176
00:41:28,526 --> 00:41:32,475
As it is the Year of the Horse,
pilgrims are rewarded

177
00:41:32,655 --> 00:41:36,273
with a twelvefold absolution
from negative karma

178
00:41:36,450 --> 00:41:41,408
which would have burdened
their existence after reincarnation.

179
00:41:47,253 --> 00:41:50,586
The pilgrims walk
in a clockwise direction.

180
00:41:51,132 --> 00:41:55,081
Followers of the B? N religion
are easily recognizable.

181
00:41:55,261 --> 00:41:58,012
They move in the opposite direction.

182
00:44:11,105 --> 00:44:13,726
The central part of the Mandala

183
00:44:13,899 --> 00:44:17,399
is evocative of the composition
of Mount Kailash,

184
00:44:17,570 --> 00:44:19,728
viewed by many believers

185
00:44:19,905 --> 00:44:24,152
as the centre of the physical
and spiritual universe.

186
00:44:24,952 --> 00:44:28,950
We asked the Dalai Lama
for his views on this theory.

187
00:48:58,559 --> 00:49:03,719
These cauldrons are used
to prepare the monks' afternoon tea.

188
00:49:05,232 --> 00:49:08,317
Eager young monks
battle for the honour

189
00:49:08,486 --> 00:49:10,644
of being the first to serve

190
00:49:10,821 --> 00:49:13,491
their group of high-ranking monks.

191
00:51:53,067 --> 00:51:57,444
These are the preparations
for the Long Life Ceremony.

192
00:51:59,657 --> 00:52:02,693
Gifts will be distributed
among the faithful.

193
00:52:02,868 --> 00:52:07,281
The most coveted of these
are the consecrated barley dumplings

194
00:52:07,456 --> 00:52:10,991
which hold the promise
of a long and happy life.

195
00:55:36,499 --> 00:55:39,868
The enclosed area
containing the Sand Mandala

196
00:55:40,044 --> 00:55:44,421
is now open
and on display to the general public.

197
00:55:44,674 --> 00:55:47,081
It is protected by plexiglas,

198
00:55:47,259 --> 00:55:50,426
as touching
or breathing on the Mandala

199
00:55:50,596 --> 00:55:52,589
could destroy it.

200
00:57:24,607 --> 00:57:29,185
The Dalai Lama has yet
to make a public appearance

201
00:57:29,362 --> 00:57:33,822
and rumours are spreading
about the severity of his illness.

202
00:57:34,950 --> 00:57:37,620
Loud speakers announce

203
00:57:37,787 --> 00:57:42,912
that His Holiness will address
the pilgrims the following morning.

204
00:57:49,965 --> 00:57:55,007
Hundreds of thousands of queues
have been forming since 3 a.m.

205
00:57:55,179 --> 00:57:59,343
Lines of pilgrims stretch
for kilometres into the distance.

206
00:59:04,081 --> 00:59:07,331
Pilgrims throw
prayer shawls to the front

207
00:59:07,501 --> 00:59:11,120
symbolizing their wish
to be close to the Dalai Lama.

208
01:00:16,862 --> 01:00:20,066
Speaking in Tibetan,
the Dalai Lama explains

209
01:00:20,241 --> 01:00:22,364
that due to his serious illness

210
01:00:22,702 --> 01:00:26,367
he will be unable
to preside over the ceremonies,

211
01:00:26,539 --> 01:00:28,994
each of which lasts many hours.

212
01:00:33,087 --> 01:00:38,426
He will be unable to perform
the principal initiation rites.

213
01:00:40,553 --> 01:00:42,426
He calls upon the pilgrims

214
01:00:42,596 --> 01:00:45,800
to reconvene here in a year's time.

215
01:00:46,308 --> 01:00:51,433
Preparations for the next Kalachakra,
to be held in Graz in Austria,

216
01:00:51,605 --> 01:00:53,349
have long been underway

217
01:00:53,524 --> 01:00:56,644
and it will go ahead
in the autumn as planned.

218
01:00:57,611 --> 01:01:00,980
The Dalai Lama's news
does not come as a surprise.

219
01:01:01,157 --> 01:01:03,564
However, the pilgrims are shocked.

220
01:01:03,743 --> 01:01:07,575
Most of them stay up the whole night
holding a vigil,

221
01:01:07,747 --> 01:01:11,198
praying for the Dalai Lama's recovery.

222
01:01:27,683 --> 01:01:30,008
He seems about to add something.

223
01:01:30,353 --> 01:01:32,926
However, tormented by the fact

224
01:01:33,105 --> 01:01:36,937
that he has disappointed so many
who have come so far,

225
01:01:37,109 --> 01:01:39,018
he remains silent.

226
01:01:55,920 --> 01:01:57,663
Graz in Austria

227
01:01:57,838 --> 01:02:02,299
is home to a small but
extremely active Buddhist community.

228
01:02:02,468 --> 01:02:06,845
In cooperation with the City of Graz,
it worked incredibly hard

229
01:02:07,014 --> 01:02:10,763
to have the chance
to host a Kalachakra Initiation.

230
01:02:11,143 --> 01:02:14,144
The Dalai Lama has rarely agreed

231
01:02:14,313 --> 01:02:17,896
to perform
this important ceremony in the West.

232
01:02:19,151 --> 01:02:24,775
He recently expressed concerns
about believers who change religions

233
01:02:24,949 --> 01:02:28,567
and cut themselves off
from their cultural milieu.

234
01:02:28,744 --> 01:02:30,820
But he does endorse efforts

235
01:02:30,997 --> 01:02:34,163
to study and understand
other religions.

236
01:02:34,333 --> 01:02:35,875
Only in this way

237
01:02:36,043 --> 01:02:39,993
can long-lasting peace
ever be secured on this planet.

238
01:02:43,009 --> 01:02:47,884
The initiation will take place
in the recently opened convention hall

239
01:02:48,055 --> 01:02:51,222
which can easily accommodate
8,000 people.

240
01:02:52,768 --> 01:02:55,971
Despite being tired
and evidently jet-lagged,

241
01:02:56,147 --> 01:02:58,305
the Dalai Lama still radiates

242
01:02:58,482 --> 01:03:01,400
his characteristic
warmth and kindness.

243
01:03:15,291 --> 01:03:17,746
Unsure of what he may do next,

244
01:03:17,918 --> 01:03:21,003
he is a constant worry
to his bodyguards.

245
01:03:21,172 --> 01:03:23,710
He spontaneously sets off

246
01:03:23,883 --> 01:03:26,374
to mingle with the crowd.

247
01:04:26,487 --> 01:04:31,826
The Dalai Lama now performs the duties
he was unable to fulfil in Bodh Gaya.

248
01:04:31,993 --> 01:04:37,034
He lays down the first lines
of the template for the Sand Mandala.

249
01:04:41,627 --> 01:04:44,830
It will be
an exact mathematical replica

250
01:04:45,006 --> 01:04:47,497
of the design used in India.

251
01:04:49,260 --> 01:04:52,593
The people assembled
in the hall fall silent.

252
01:05:51,530 --> 01:05:54,152
The Dalai Lama assumes his role

253
01:05:54,325 --> 01:05:57,160
as he leads prayers, teachings

254
01:05:57,328 --> 01:06:00,827
and presides over
the composition of the Mandala.

255
01:06:51,090 --> 01:06:54,257
The Dalai Lama
takes his place on his throne

256
01:06:54,427 --> 01:06:58,045
from which he will lead
several days of instruction.

257
01:07:01,809 --> 01:07:03,932
This man caught our attention

258
01:07:04,103 --> 01:07:08,599
because he was invited
to sit with the high-ranking lamas.

259
01:07:09,066 --> 01:07:13,479
We learned that his name
was Takna Jigme Sangpo.

260
01:07:14,113 --> 01:07:16,686
A former schoolteacher,

261
01:07:16,866 --> 01:07:22,156
this man spent many years
as a political prisoner in Tibet.

262
01:07:22,330 --> 01:07:25,533
He had only recently been released.

263
01:07:25,708 --> 01:07:27,250
Two days ago,

264
01:07:27,418 --> 01:07:31,581
he set eyes on the Dalai Lama
for the first time in his life.

265
01:13:20,354 --> 01:13:22,762
Before the initiation begins,

266
01:13:22,940 --> 01:13:26,309
the Dalai Lama performs
a number of rituals.

267
01:13:26,944 --> 01:13:29,779
One of the first rituals
is the oracle.

268
01:13:29,947 --> 01:13:35,024
This determines the auspicious outcome
of the Kalachakra Initiation.

269
01:13:36,287 --> 01:13:38,956
The decisive factor in this ritual

270
01:13:39,123 --> 01:13:42,492
is the direction
in which the stick falls.

271
01:13:50,051 --> 01:13:52,090
The only Western layperson

272
01:13:52,261 --> 01:13:55,630
to have the privilege
of taking part in this oracle

273
01:13:55,806 --> 01:13:57,348
is Manfred Klell,

274
01:13:57,516 --> 01:14:01,135
the chairman
of Graz Buddhist Association.

275
01:14:06,067 --> 01:14:08,937
Another preparatory ritual

276
01:14:09,111 --> 01:14:12,611
is performed
using kusha grass from India.

277
01:14:13,532 --> 01:14:16,901
The faithful lay it
under their pillows at night

278
01:14:17,078 --> 01:14:20,411
and their dreams
are stored in its blades.

279
01:14:21,916 --> 01:14:23,873
Following other rituals,

280
01:14:24,460 --> 01:14:28,956
the protective curtain is lifted
and the Sand Mandala unveiled.

281
01:14:29,131 --> 01:14:32,796
The Dalai Lama
will now initiate the students

282
01:14:32,968 --> 01:14:35,506
into this sacred landscape.

283
01:14:39,308 --> 01:14:42,262
The assembled believers
don red headbands

284
01:14:42,436 --> 01:14:46,683
to symbolize
the blindness of their minds thus far.

285
01:14:48,109 --> 01:14:50,267
Some lay seeds on their heads

286
01:14:50,444 --> 01:14:55,153
to represent the activation
of their consciousness.

287
01:14:56,325 --> 01:14:59,694
This is followed
by many hours of meditation

288
01:14:59,870 --> 01:15:02,196
and rituals to purify the mind

289
01:15:02,373 --> 01:15:06,834
until the faithful have reached
a higher state of consciousness.

290
01:16:27,333 --> 01:16:30,334
The zenith
of this purely spiritual event,

291
01:16:30,503 --> 01:16:34,832
invisible to a camera lens,
thus draws to a close.

292
01:16:41,222 --> 01:16:46,429
All that remains is for the Dalai Lama
to destroy the Sand Mandala.

293
01:17:54,045 --> 01:17:57,829
As a symbol of the transitory nature
of living creatures,

294
01:17:58,007 --> 01:18:01,376
the Mandala sand is scattered
into the Mur river

295
01:18:01,552 --> 01:18:05,929
from where it will flow out
as a blessing to the earth.

296
01:18:40,091 --> 01:18:44,040
The Sand Mandala
has been released into the world

297
01:18:44,220 --> 01:18:47,719
and the Dalai Lama
has left the country.

298
01:18:47,890 --> 01:18:52,054
Yet a solitary bodyguard
remains on duty,

299
01:18:52,311 --> 01:18:55,727
evidently
still waiting to be discharged.

300
01:19:01,988 --> 01:19:06,899
Or is he meant to symbolize
the Buddhist idea of emptiness:

301
01:19:07,076 --> 01:19:08,950
He has no one to protect

302
01:19:09,120 --> 01:19:11,871
and hardly anyone
to protect them from.

303
01:19:47,742 --> 01:19:50,945
Back in Bodh Gaya
we meet a similar scene.

304
01:19:51,120 --> 01:19:54,121
400,000 discarded cushions.

305
01:20:16,354 --> 01:20:19,105
And one solitary monk.

306
01:20:19,273 --> 01:20:21,515
Still immersed in prayer.

307
01:20:39,877 --> 01:20:41,834
Is he praying for us?

308
01:20:42,463 --> 01:20:45,132
Has he found enlightenment?

309
01:20:46,217 --> 01:20:49,134
And then, like a nocturnal animal

310
01:20:49,303 --> 01:20:51,379
emerging from the earth,

311
01:20:51,555 --> 01:20:54,343
a thought creeps into my mind.

312
01:20:54,517 --> 01:20:58,301
Is it possible
that he is sitting on the spot

313
01:20:58,479 --> 01:21:01,563
which marks
the centre of the universe?

