1
00:00:01,800 --> 00:00:03,365
Subtitles by DramaFever

2
00:00:06,800 --> 00:00:09,200
<i>[Love Cells]
[A dating virgin seduces an idol]</i>

3
00:00:09,200 --> 00:00:11,600
<i>[Episode 14]</i>

4
00:00:11,600 --> 00:00:14,800
<i>[Chapter 3: Love Cell Maturation]
['Confession']</i>

5
00:00:26,400 --> 00:00:27,500
Nebi.

6
00:00:28,500 --> 00:00:29,700
But we...

7
00:00:29,700 --> 00:00:31,700
It's the last week.

8
00:00:31,700 --> 00:00:35,900
If you confess,
you can become Seo Rin's man.

9
00:00:37,100 --> 00:00:38,400
Yeah.

10
00:00:46,100 --> 00:00:47,500
I...

11
00:00:47,500 --> 00:00:49,500
can trust you, right?

12
00:00:51,500 --> 00:00:52,500
Yeah.

13
00:00:57,300 --> 00:00:58,400
<i>[Operation Proposal]</i>

14
00:01:02,300 --> 00:01:03,800
<i>From now on...</i>

15
00:01:04,600 --> 00:01:06,700
<i>my own beautiful love story... </i>

16
00:01:07,800 --> 00:01:08,900
<i>will begin. </i>

17
00:01:25,300 --> 00:01:26,300
<i>[Love Cell Death D-1]</i>

18
00:01:26,300 --> 00:01:27,900
- Hello.
- Hello.

19
00:01:28,600 --> 00:01:29,900
We'll start the interview.

20
00:01:30,600 --> 00:01:34,500
Then, that means Seo Rin
is your kind of woman right?

21
00:01:35,100 --> 00:01:37,300
How can I say she's not,
when she's right next to me?

22
00:01:38,000 --> 00:01:40,700
Thank you,
I'm a fan of Chun Ji Woon too.

23
00:01:40,700 --> 00:01:45,100
There are a lot of people who
want you to become real lovers.

24
00:01:45,100 --> 00:01:47,300
How do you feel about that Ji Woon?

25
00:01:47,300 --> 00:01:52,700
I'm pretty sure every man
would like Seo Rin.

26
00:01:52,700 --> 00:01:53,800
Did he eat something wrong?

27
00:01:53,800 --> 00:01:55,300
He's saying crazy things.

28
00:01:55,300 --> 00:01:59,400
Okay, let's say every man
likes Seo Rin.

29
00:02:00,100 --> 00:02:02,400
Who is the man...

30
00:02:03,000 --> 00:02:04,000
that you like, Seo Rin?

31
00:02:05,000 --> 00:02:06,000
I wonder...

32
00:02:06,300 --> 00:02:09,600
I mean, you know that there's already
a scandal concerning you two, right?

33
00:02:10,400 --> 00:02:11,919
I am aware that we've
already released

34
00:02:11,945 --> 00:02:13,400
an official statement
on that matter.

35
00:02:13,400 --> 00:02:16,400
You've already said
that you've never

36
00:02:16,426 --> 00:02:19,425
thought of Seo Rin
that way before, right?

37
00:02:21,300 --> 00:02:22,700
No...

38
00:02:22,700 --> 00:02:25,300
the official statement expressed
the company's thoughts.

39
00:02:26,800 --> 00:02:29,400
I am thinking of Seo Rin as a woman.

40
00:02:29,400 --> 00:02:31,800
That means Ji Woon, you...

41
00:02:31,800 --> 00:02:32,800
No.

42
00:02:37,300 --> 00:02:38,600
I love her.

43
00:02:39,700 --> 00:02:42,400
I am currently in love with Seo Rin.

44
00:02:49,000 --> 00:02:50,100
No...

45
00:02:51,200 --> 00:02:52,300
It's okay.

46
00:03:01,500 --> 00:03:02,500
Ji Woon.

47
00:03:03,000 --> 00:03:04,500
Are you kidding me right now?

48
00:03:04,500 --> 00:03:06,300
What are you doing?

49
00:03:06,700 --> 00:03:08,900
I don't want to lose Seo Rin anymore.

50
00:03:08,900 --> 00:03:10,700
So, I want you to tell
the company nicely...

51
00:03:10,700 --> 00:03:11,800
No.

52
00:03:11,800 --> 00:03:13,800
Seo Rin is not a good partner.

53
00:03:13,800 --> 00:03:17,500
Neither an actress nor an idol
will last very long.

54
00:03:17,500 --> 00:03:20,500
It's okay if it's just for fun,
but this really isn't right.

55
00:03:20,500 --> 00:03:21,500
What?

56
00:03:22,200 --> 00:03:25,000
We'll stop the network
from airing the show

57
00:03:25,000 --> 00:03:26,400
by handing them another scoop.

58
00:03:27,000 --> 00:03:28,900
I'll schedule a dinner
with the staff here.

59
00:03:29,600 --> 00:03:32,000
Just for a crazy girl like Seo Rin...

60
00:03:38,300 --> 00:03:39,400
What did you just do?

61
00:03:39,400 --> 00:03:43,000
If you say that one more time,
you're not my manager.

62
00:03:43,000 --> 00:03:46,400
You can't be like this to me, Ji Woon.

63
00:03:46,400 --> 00:03:47,400
No...

64
00:03:47,800 --> 00:03:48,817
Even if it's not you

65
00:03:48,843 --> 00:03:50,800
I'll be like this to anyone
who tries to stop me.

66
00:03:50,800 --> 00:03:52,800
I'm Chun Ji Woon's manager.

67
00:03:52,800 --> 00:03:55,000
I'm your manager!

68
00:03:55,000 --> 00:03:57,000
A jerk who doesn't see
a person as a person...

69
00:03:57,600 --> 00:03:59,000
doesn't deserve to be a manager.

70
00:04:00,900 --> 00:04:02,100
Let's end it here.

71
00:04:03,200 --> 00:04:04,200
Ji Woon.

72
00:04:04,800 --> 00:04:06,100
Ji Woon!

73
00:04:06,600 --> 00:04:08,300
Chun Ji Woon!

74
00:04:13,500 --> 00:04:18,000
It might be true that I was forgetting
the feelings we had when we first met.

75
00:04:18,000 --> 00:04:22,300
I'm not always remembering them either.

76
00:04:23,000 --> 00:04:27,500
The things I did to protect you
were probably also for my sake.

77
00:04:27,500 --> 00:04:31,500
People are all full of mistakes.

78
00:04:32,300 --> 00:04:35,800
Can you give me one last chance?

79
00:04:36,600 --> 00:04:37,800
But...

80
00:04:38,800 --> 00:04:42,100
Can you give me a little time?

81
00:04:43,600 --> 00:04:45,000
Just a little bit.

82
00:04:49,000 --> 00:04:52,000
- Good work today.
- Good work today.

83
00:04:52,300 --> 00:04:55,300
Seo Rin,
I want to buy you dinner tonight.

84
00:04:55,300 --> 00:04:58,200
Don't worry about it.
You don't have the money.

85
00:04:58,200 --> 00:04:59,800
I got my first paycheck.

86
00:04:59,800 --> 00:05:03,200
It's the first time I've earned money,
so I want to treat you...

87
00:05:03,200 --> 00:05:05,500
- Did you pay your rent?
- Excuse me?

88
00:05:06,500 --> 00:05:07,500
I'm kidding.

89
00:05:08,600 --> 00:05:10,700
Then, you have to buy me
something really delicious.

90
00:05:10,700 --> 00:05:12,200
Seo Rin!

91
00:05:13,400 --> 00:05:15,000
I'll take you home today.

92
00:05:15,000 --> 00:05:17,000
We can also finish our
conversation from earlier.

93
00:05:17,000 --> 00:05:19,000
I have a date with Dae Choong.

94
00:05:22,400 --> 00:05:23,800
Does it have to be today?

95
00:05:23,800 --> 00:05:26,700
Yes, there's a reason why I can't wait.

96
00:05:26,700 --> 00:05:28,700
Ma Dae Choong.

97
00:05:28,700 --> 00:05:31,700
Do you sincerely like Seo Rin?

98
00:05:33,600 --> 00:05:35,500
I also...

99
00:05:36,700 --> 00:05:39,300
I was sincerely...

100
00:05:40,400 --> 00:05:45,000
going to do everything
for Seo Rin and...

101
00:05:45,000 --> 00:05:49,000
I know you're not a bad person,
Dae Choong.

102
00:05:56,000 --> 00:05:58,000
I'm a huge fan of yours, Seo Rin.

103
00:05:58,000 --> 00:06:00,600
- Have some water.
- Thank you.

104
00:06:24,800 --> 00:06:26,900
<i>[Love Cells]
[A dating virgin seduces an idol]</i>

105
00:06:26,900 --> 00:06:29,500
We've run out of items to use...

106
00:06:29,500 --> 00:06:31,100
What do we do?

107
00:06:31,100 --> 00:06:33,100
I trust in you.

108
00:06:34,300 --> 00:06:37,000
<i>I'm pretty sure that
I've given you enough chances.</i>

109
00:06:38,300 --> 00:06:40,400
<i>I thought you should say goodbye.</i>

110
00:06:40,400 --> 00:06:42,400
Who are you?

111
00:06:44,300 --> 00:06:46,600
Please, give me one more chance.

112
00:06:59,226 --> 00:07:04,226
Subtitles by DramaFever

