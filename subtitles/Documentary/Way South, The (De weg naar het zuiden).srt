1
00:00:40,240 --> 00:00:42,549
'Pushkin's Poems'

2
00:01:46,200 --> 00:01:48,509
Luggage?

3
00:01:56,320 --> 00:01:58,151
Have a safe journey, Doctor.

4
00:10:48,080 --> 00:10:51,595
My name is Nicolai Borodinski,
I'm from Russia.

5
00:10:52,680 --> 00:10:55,194
I am traveling from Moscow to Paris.

6
00:11:03,320 --> 00:11:06,676
One night please. Thank you.

7
00:11:06,880 --> 00:11:08,438
You have to pay in advance.

8
00:11:56,720 --> 00:11:57,596
Goodbye.

9
00:11:58,360 --> 00:12:00,112
Good day.

10
00:12:00,320 --> 00:12:01,878
Good night.

11
00:12:02,080 --> 00:12:03,229
Russian? Me too.

12
00:14:33,200 --> 00:14:34,713
Where are you from?

13
00:14:36,080 --> 00:14:37,957
Odintsovo, close to Moscow.

14
00:14:40,480 --> 00:14:42,835
I am from Novgorod.

15
00:14:43,040 --> 00:14:45,873
Aren't you going to ask me
if I would like a drink?

16
00:14:47,080 --> 00:14:48,593
I'm sorry.

17
00:14:48,800 --> 00:14:50,677
What would you like to drink?

18
00:14:51,840 --> 00:14:53,831
And what brings you here?

19
00:14:54,040 --> 00:14:56,235
I'm on my way to Paris...

20
00:14:56,440 --> 00:14:58,476
...to study, science - medicine...

21
00:14:58,680 --> 00:15:00,557
...and to improve my French.

22
00:15:03,080 --> 00:15:04,718
A Doctor...

23
00:15:06,960 --> 00:15:09,155
I also wanted to go to Paris...

24
00:15:09,360 --> 00:15:11,237
...to follow my love.

25
00:15:13,760 --> 00:15:15,876
But he turned out to be a bastard...

26
00:15:16,080 --> 00:15:18,116
...and now I'm stuck in this swamp.
Stupid.

27
00:15:18,320 --> 00:15:20,993
Love destroys everything.

28
00:15:21,920 --> 00:15:23,797
Love is an illusion.

29
00:15:27,200 --> 00:15:29,077
Why don't you return to Russia?

30
00:15:30,480 --> 00:15:31,993
That's a long story.

31
00:15:33,560 --> 00:15:35,391
I will go back, to Moscow,
in a few years.

32
00:15:35,600 --> 00:15:37,989
When I have finished my studies.

33
00:15:40,680 --> 00:15:42,557
You're homesick already.

34
00:15:44,000 --> 00:15:45,877
But time goes so fast...

35
00:15:47,000 --> 00:15:48,911
Maybe I'll see you again...

36
00:15:49,120 --> 00:15:51,190
...on your way back home as a Doctor...

37
00:15:51,440 --> 00:15:53,192
...then I'll go with you.

38
00:15:54,880 --> 00:15:56,757
I'm Nina.

39
00:15:57,680 --> 00:15:59,557
Borodinski... Nicolai.

40
00:16:20,800 --> 00:16:22,791
Meet Bruno...

41
00:16:23,720 --> 00:16:26,518
...he's obsessed with me.

42
00:16:26,720 --> 00:16:29,518
A stupid bastard and very jealous.

43
00:16:32,640 --> 00:16:33,993
What did she say?

44
00:16:45,360 --> 00:16:46,713
What did you say?

45
00:18:39,920 --> 00:18:41,797
Her name is Elise...

46
00:18:42,000 --> 00:18:44,036
...and that was the Count.

47
00:18:45,040 --> 00:18:49,113
He is not a real Count,
he bought the title...

48
00:18:49,320 --> 00:18:52,551
...like he bought this hotel,
like he buys everything...

49
00:18:52,760 --> 00:18:55,877
...and everyone. All this... is his.

50
00:18:56,080 --> 00:18:58,435
He's a dirty old man.

51
00:18:58,640 --> 00:19:00,835
But he's not to be trifled with.
Don't you understand?

52
00:19:01,720 --> 00:19:05,395
He owns everything.
Including Elise...

53
00:19:07,080 --> 00:19:08,877
She's lucky.

54
00:19:09,080 --> 00:19:12,595
He does everything for her,
he's good for her.

55
00:19:12,800 --> 00:19:15,633
Forget about her...

56
00:19:15,840 --> 00:19:17,831
...and go on to Paris.

57
00:19:20,360 --> 00:19:22,271
You have no chance.

58
00:20:19,480 --> 00:20:21,789
One night... again.

59
00:20:22,000 --> 00:20:22,716
Again?

60
00:20:23,080 --> 00:20:26,470
I want to have flowers delivered.
A big, beautiful bunch of flowers.

61
00:20:27,880 --> 00:20:28,915
I want...

62
00:20:35,000 --> 00:20:36,831
Not a good idea...

63
00:20:37,040 --> 00:20:39,395
...and a waste of good money.

64
00:20:39,560 --> 00:20:40,879
I would like...

65
00:20:44,360 --> 00:20:48,911
He wants to have a bunch
of flowers delivered to Elise.

66
00:20:50,080 --> 00:20:52,116
Large! Very large!

67
00:20:54,440 --> 00:20:56,396
The accompanying words.

68
00:21:07,640 --> 00:21:10,791
I remember a wonderful moment.

69
00:21:11,000 --> 00:21:13,560
As before my eyes you appeared.

70
00:21:16,880 --> 00:21:20,589
Like a vision, fleeting, momentary.

71
00:21:22,120 --> 00:21:25,396
Like a spirit of the purest beauty.

72
00:21:25,600 --> 00:21:28,194
Then to my soul an awakening came.

73
00:21:28,440 --> 00:21:29,953
A Godlike face, an inspiration.

74
00:21:30,160 --> 00:21:38,556
And life, and tears, and love.

75
00:21:47,720 --> 00:21:49,039
Your signature.

76
00:23:32,960 --> 00:23:36,669
You see? You don't have a chance.

77
00:23:36,880 --> 00:23:39,713
Tell me about her. Who is she?

78
00:23:41,760 --> 00:23:44,877
Listen. I love her very much.

79
00:23:45,080 --> 00:23:47,389
She has been through so much.

80
00:23:48,520 --> 00:23:50,511
You could never understand.

81
00:23:50,720 --> 00:23:52,312
Now she is happy, leave her be.

82
00:23:52,520 --> 00:23:54,033
You have nothing to offer her.

83
00:24:02,040 --> 00:24:04,600
I must tell you something.

84
00:24:04,800 --> 00:24:06,119
No one knows this...

85
00:24:07,000 --> 00:24:10,470
It is a secret and hard to understand.

86
00:24:12,120 --> 00:24:17,752
You have never known poverty
and humiliation.

87
00:24:18,920 --> 00:24:22,435
Elise comes from a small village in Holland.

88
00:24:23,320 --> 00:24:25,629
She was the daughter of a day-laborer...

89
00:24:25,840 --> 00:24:28,513
...and never knew love.

90
00:24:28,720 --> 00:24:32,315
Her father beat and abused her as a child.

91
00:24:34,160 --> 00:24:36,628
He also murdered her mother...

92
00:24:37,560 --> 00:24:42,315
...beat her to death.

93
00:24:47,960 --> 00:24:49,791
God rest her soul.

94
00:24:53,360 --> 00:24:56,716
So she learned to hate men.

95
00:24:56,920 --> 00:25:01,118
Her father was caught,
and later himself killed, in prison.

96
00:25:02,120 --> 00:25:06,750
Elise was lucky, and ended up
in a German orphanage...

97
00:25:06,960 --> 00:25:10,509
...which hired her out to a noble family,
where she worked as a domestic servant.

98
00:25:10,720 --> 00:25:14,679
They taught her to speak French,
play the piano and good manners.

99
00:25:14,880 --> 00:25:19,192
Good manners...
but these people also abused her.

100
00:25:20,120 --> 00:25:22,759
She fled and ended up here.

101
00:25:22,960 --> 00:25:24,916
Under my protection.

102
00:25:25,120 --> 00:25:30,717
Now she no longer suffers poverty,
she has everything her heart desires.

103
00:25:31,880 --> 00:25:33,199
She is happy...

104
00:25:34,800 --> 00:25:39,669
...even though she doesn't know
what love is.

105
00:25:56,600 --> 00:25:58,079
What is this?

106
00:26:00,560 --> 00:26:02,391
You can have it.

107
00:26:35,120 --> 00:26:38,112
Don't throw these out.

108
00:28:27,000 --> 00:28:29,230
Elise is coming soon.

109
00:28:32,760 --> 00:28:34,716
Don't forget, she is a whore.

110
00:28:37,080 --> 00:28:38,559
She costs money...

111
00:28:39,840 --> 00:28:43,674
...a lot of money, money you don't have.

112
00:28:45,320 --> 00:28:49,438
I can arrange a 'rendezvous' for you.

113
00:28:49,640 --> 00:28:51,119
But just once.

114
00:28:52,000 --> 00:28:56,152
No one, no one at all, may know!

115
00:29:00,120 --> 00:29:02,190
If someone finds out,
everything will be lost.

116
00:29:03,440 --> 00:29:06,637
You have to give me your word of honor.

117
00:29:06,840 --> 00:29:07,909
Yes or no?

118
00:29:21,920 --> 00:29:25,037
Then you will go away for ever.

119
00:29:26,280 --> 00:29:30,432
You will never return. Never.

120
00:29:33,440 --> 00:29:34,873
For my services...

121
00:29:49,640 --> 00:29:53,952
I do this for you.

122
00:29:54,160 --> 00:29:58,392
If the Count finds out
something very bad will happen.

123
00:29:58,600 --> 00:30:01,068
It would be a disaster for Elise.

124
00:31:05,560 --> 00:31:10,759
You have to wait.
Remember our agreement.

125
00:31:10,960 --> 00:31:12,518
The key.

126
00:31:12,720 --> 00:31:14,870
To her room.

127
00:34:52,920 --> 00:34:54,478
Stay here.

128
00:35:16,480 --> 00:35:18,038
What?

129
00:35:21,280 --> 00:35:22,838
What do you want?

130
00:35:25,920 --> 00:35:27,478
What do you want!

131
00:35:30,080 --> 00:35:37,236
I am leaving and wanted to say
goodbye before I depart...

132
00:35:38,280 --> 00:35:39,838
...only to say goodbye to you.

133
00:35:41,440 --> 00:35:43,670
Good. Well... goodbye.

134
00:35:43,880 --> 00:35:44,471
Farewell then.

135
00:35:44,840 --> 00:35:46,398
Yours?

136
00:37:59,880 --> 00:38:01,632
Another night.

137
00:38:01,840 --> 00:38:03,796
Another night.

138
00:39:09,640 --> 00:39:11,198
Please go away.

139
00:39:17,920 --> 00:39:19,069
Leave me alone.

140
00:39:21,760 --> 00:39:23,637
You asked me...

141
00:39:24,920 --> 00:39:26,399
...to come here.

142
00:39:26,600 --> 00:39:28,158
Forget what happened.

143
00:39:36,840 --> 00:39:37,750
Forgive me.

144
00:39:38,920 --> 00:39:39,955
It is my fault.

145
00:39:42,480 --> 00:39:45,870
If you really care for me,
you will leave now.

146
00:39:51,080 --> 00:39:52,399
Please...

147
00:39:52,600 --> 00:39:54,158
Go away.

148
00:40:00,560 --> 00:40:01,436
Tomorrow.

149
00:40:02,000 --> 00:40:04,389
I will just stay tonight.

150
00:41:33,440 --> 00:41:34,714
Where were you?

151
00:42:32,760 --> 00:42:36,116
Young man... take a seat.

152
00:42:39,760 --> 00:42:43,673
Anyone can win.
Fortune favors the bold.

153
00:42:48,960 --> 00:42:50,473
I've not seen you here before.

154
00:42:50,680 --> 00:42:52,238
Who are you?

155
00:42:52,440 --> 00:42:54,829
I can't understand you,
I don't speak German.

156
00:43:00,360 --> 00:43:04,911
l-no-ski speak-i Germani,
Russki, Russki.

157
00:43:55,760 --> 00:43:57,034
What's that!?

158
00:47:22,120 --> 00:47:23,712
Send for a Doctor immediately.

159
00:47:24,640 --> 00:47:26,073
A Doctor straightaway!

160
00:47:26,280 --> 00:47:27,872
You must go now. Quickly.

161
00:47:30,240 --> 00:47:31,832
I want to thank you.

162
00:47:40,480 --> 00:47:43,631
I am ashamed
you have to see me like this.

163
00:47:48,800 --> 00:47:51,951
I like having you near me...

164
00:47:53,440 --> 00:47:55,670
...but we cannot be together.

165
00:48:06,880 --> 00:48:09,075
This is our destiny.

166
00:48:11,880 --> 00:48:14,633
I have nothing...

167
00:48:14,840 --> 00:48:17,673
I have nothing to offer you.

168
00:48:17,880 --> 00:48:20,075
I don't have a choice.

169
00:48:43,880 --> 00:48:44,790
Where is she?

170
00:48:46,600 --> 00:48:48,591
She was unwell, she is sleeping.

171
00:48:51,920 --> 00:48:54,673
But... she is sleeping!

172
00:48:54,880 --> 00:48:58,475
She feels bad, she is asleep,
let her rest.

173
00:49:15,880 --> 00:49:16,790
I promise you...

174
00:49:17,760 --> 00:49:19,557
I promise you I will come back.

175
00:49:19,760 --> 00:49:21,990
And then I will take you away with me.

176
00:49:23,480 --> 00:49:26,552
But first you have to get well.

177
00:51:03,680 --> 00:51:05,272
Forgive me.

178
00:51:05,480 --> 00:51:07,550
I am sorry.

179
00:51:09,120 --> 00:51:13,875
You know I have
your best interests at heart.

180
00:51:23,920 --> 00:51:27,151
'I will come back. Wait for me.'

181
00:52:30,080 --> 00:52:31,593
The lady cannot see you now.

182
00:52:31,800 --> 00:52:35,156
I have to know how she is.
I have a gift, then I will go.

183
00:52:41,320 --> 00:52:45,279
I only wanted to see you one more time.

184
00:52:58,440 --> 00:53:01,398
I beg you... please don't come back.

185
00:53:11,520 --> 00:53:14,512
I beg you... forget me.

186
00:53:22,920 --> 00:53:24,478
Please.

187
00:54:29,520 --> 00:54:33,479
I am sure that he is leaving now,
and will not return.

188
00:54:33,680 --> 00:54:36,240
We will not be seeing him again.

189
00:57:55,080 --> 00:57:56,638
What do you want?

190
01:02:49,000 --> 01:02:51,389
You have slept for two whole days.

191
01:03:15,040 --> 01:03:17,873
A lot of rest, not too much movement.

192
01:03:18,080 --> 01:03:20,594
Most likely your bone isn't broken...

193
01:03:20,800 --> 01:03:23,951
...but is it a bad bruising of your leg.

194
01:03:24,160 --> 01:03:25,479
I think.

195
01:03:25,680 --> 01:03:30,151
You were lucky.
That will be 150 Marks.

196
01:03:42,440 --> 01:03:42,997
Here.

197
01:03:43,360 --> 01:03:43,951
Thank you.

198
01:03:44,480 --> 01:03:45,037
Goodbye.

199
01:03:47,840 --> 01:03:54,473
I am very sorry, but now that the Count
is no longer paying for your stay here...

200
01:03:54,680 --> 01:03:57,114
...someone else must foot the bill.

201
01:03:57,320 --> 01:04:02,314
Once again, my question is not only
entirely justified, but also self-evident.

202
01:04:04,600 --> 01:04:06,318
Who is going to pay for all this?

203
01:04:06,520 --> 01:04:09,398
This is all we have.
- That's not enough.

204
01:04:36,200 --> 01:04:39,636
The Doctor said I have to stay here,
in your bed.

205
01:04:43,520 --> 01:04:45,397
Your French is perfect.

206
01:04:47,520 --> 01:04:49,078
My compliments.

207
01:04:50,280 --> 01:04:53,750
You must rest, sleep and eat well.

208
01:04:56,440 --> 01:04:58,556
How are you, Elise?

209
01:05:22,560 --> 01:05:24,471
For you. From Paris.

210
01:05:43,360 --> 01:05:44,998
It is much too expensive.

211
01:05:45,200 --> 01:05:46,918
I don't want you to give me things.

212
01:05:47,120 --> 01:05:50,271
What use is it to me? It's too late.

213
01:07:03,120 --> 01:07:06,032
That's all I could get for the jewelry.

214
01:07:09,960 --> 01:07:12,872
I am going. See you tomorrow.

215
01:07:25,960 --> 01:07:28,713
My days dragged quietly by...

216
01:07:28,920 --> 01:07:32,117
...nothing was new.

217
01:07:32,320 --> 01:07:33,673
No Godlike face...

218
01:07:33,880 --> 01:07:35,871
...no inspiration.

219
01:07:36,080 --> 01:07:37,399
No tears...

220
01:07:37,560 --> 01:07:38,993
...no life...

221
01:07:39,200 --> 01:07:40,758
...no love...

222
01:07:40,960 --> 01:07:42,075
...no you.

223
01:07:50,920 --> 01:07:53,036
Then to my soul an awakening came...

224
01:07:53,240 --> 01:07:55,800
...and there again your face appeared.

225
01:08:03,360 --> 01:08:05,715
Like a vision, fleeting, momentary...

226
01:08:05,920 --> 01:08:07,956
...like a spirit of the purest beauty.

227
01:08:10,280 --> 01:08:12,555
And my heart beat with a rapture new...

228
01:08:12,760 --> 01:08:14,637
...and for its sake arose again.

229
01:08:25,800 --> 01:08:28,951
A Godlike face, and inspiration...

230
01:08:29,160 --> 01:08:30,798
...and life...

231
01:08:31,000 --> 01:08:32,194
...and love...

232
01:08:32,440 --> 01:08:33,793
...and tears...

233
01:08:34,000 --> 01:08:35,115
...and you.

234
01:09:25,640 --> 01:09:28,200
It is enough. For the time being.

235
01:09:32,240 --> 01:09:35,232
For now. But we will be back.

236
01:09:42,200 --> 01:09:43,519
Who let them in?

237
01:09:43,720 --> 01:09:44,869
They came in all by themselves.

238
01:09:45,080 --> 01:09:47,958
They will take everything.
What are we supposed to do?

239
01:09:49,880 --> 01:09:51,757
Have you any idea what you are doing?

240
01:09:54,520 --> 01:09:56,511
Of course, it is nothing to do with me.

241
01:09:56,720 --> 01:10:00,918
But how do you think we can carry on?
How will it end?

242
01:10:01,120 --> 01:10:03,076
It is no good.

243
01:10:03,280 --> 01:10:04,998
She has nothing but debts.

244
01:10:12,840 --> 01:10:14,034
She is not well.

245
01:10:14,240 --> 01:10:16,800
Since you turned up,
she has got ill again.

246
01:10:17,000 --> 01:10:19,389
She must go outside...
she needs fresh air.

247
01:10:19,560 --> 01:10:20,788
We must leave here.

248
01:10:21,000 --> 01:10:22,752
Outside? Fresh air?

249
01:10:22,960 --> 01:10:24,188
She cannot leave here.

250
01:10:24,440 --> 01:10:26,749
Do you still not understand?

251
01:10:52,640 --> 01:10:54,312
We are leaving this place.

252
01:10:56,880 --> 01:10:58,632
Going away from here.

253
01:11:12,680 --> 01:11:14,272
The coach is here.

254
01:11:16,880 --> 01:11:17,995
We must go.

255
01:11:35,640 --> 01:11:37,392
We are leaving.

256
01:11:44,560 --> 01:11:46,312
We are leaving.

257
01:11:46,520 --> 01:11:48,556
Your departure is in order...

258
01:11:49,800 --> 01:11:53,395
...but the lady has some unpaid bills.

259
01:11:53,600 --> 01:11:56,194
And they are quite large.

260
01:12:01,040 --> 01:12:03,190
You are free to go...

261
01:12:04,360 --> 01:12:07,033
...but the lady must stay here
until the debt has been cleared.

262
01:12:07,240 --> 01:12:10,118
The lady has made her own bed...

263
01:12:13,360 --> 01:12:14,236
Scandal. Blackmail.

264
01:12:14,600 --> 01:12:17,433
It is much too large, this is impossible.

265
01:12:17,640 --> 01:12:20,154
It is impossible for the bill
to be so large.

266
01:12:20,360 --> 01:12:22,590
This is blackmail.

267
01:12:22,800 --> 01:12:23,710
A scandal!

268
01:12:24,080 --> 01:12:27,231
What is this, it cannot be.

269
01:12:30,720 --> 01:12:32,233
Swindle!

270
01:12:34,760 --> 01:12:37,479
The lady cannot leave
without paying her bill.

271
01:12:39,000 --> 01:12:40,991
The gentleman may go.

272
01:12:43,760 --> 01:12:45,751
You must go alone.

273
01:12:45,960 --> 01:12:48,918
Please. It is hopeless.

274
01:12:49,120 --> 01:12:50,235
Leave me.

275
01:13:19,680 --> 01:13:22,638
I swear I will never leave you.

276
01:14:01,760 --> 01:14:03,557
It is impossible to leave the hotel.

277
01:14:03,760 --> 01:14:05,716
He has posted guards everywhere.

278
01:14:07,280 --> 01:14:08,952
Every day, the debts just get bigger.

279
01:14:10,200 --> 01:14:12,316
You should never have come back.

280
01:14:12,520 --> 01:14:14,988
Elise's love for you is destroying her.

281
01:14:17,600 --> 01:14:19,875
She has tuberculosis...

282
01:14:20,080 --> 01:14:21,195
...and needs peace and quiet.

283
01:14:21,440 --> 01:14:23,396
That is the only thing that helps.

284
01:14:27,440 --> 01:14:29,032
Please, go away.

285
01:14:57,560 --> 01:14:58,959
I'm going.

286
01:14:59,160 --> 01:15:00,991
Actually you should leave.

287
01:15:01,200 --> 01:15:03,236
That would be best for Elise.

288
01:15:33,520 --> 01:15:36,193
Maybe it is better that I leave.

289
01:15:39,640 --> 01:15:40,789
Go away...

290
01:15:42,160 --> 01:15:45,311
...so that everything goes back
to the way it was before.

291
01:15:58,760 --> 01:16:00,512
There is someone there.

292
01:16:04,800 --> 01:16:06,552
There is someone.

293
01:16:06,760 --> 01:16:08,159
There is someone.

294
01:16:10,320 --> 01:16:11,719
There is someone there.

295
01:16:51,040 --> 01:16:52,439
Sleep, my sweet girl...

296
01:16:53,640 --> 01:16:55,039
...my beautiful...

297
01:16:58,160 --> 01:17:02,199
Bayushki Bayu.

298
01:17:04,320 --> 01:17:09,269
Quietly the moon is peeking...

299
01:17:10,640 --> 01:17:14,792
...into your cradle.

300
01:17:16,560 --> 01:17:22,032
I will tell you fairy tales...

301
01:17:22,240 --> 01:17:26,438
...and sing you sweet songs.

302
01:18:52,920 --> 01:18:54,558
I want you to leave.

303
01:18:58,200 --> 01:18:59,599
I don't want...

304
01:19:01,840 --> 01:19:03,717
I want you to forget me.

305
01:19:05,760 --> 01:19:08,672
I want us to say goodbye, for good.

306
01:19:10,240 --> 01:19:11,639
You must forget me.

307
01:19:12,800 --> 01:19:14,392
It is better.

308
01:19:14,560 --> 01:19:15,959
Better for you.

309
01:19:18,120 --> 01:19:19,519
Don't touch me.

310
01:19:29,680 --> 01:19:31,079
It's all my fault.

311
01:19:32,040 --> 01:19:34,031
I have made up my mind.

312
01:19:34,960 --> 01:19:36,393
It's over.

313
01:19:37,840 --> 01:19:39,239
We have to stop.

314
01:19:41,240 --> 01:19:42,468
Go now.

315
01:19:42,680 --> 01:19:44,079
I beg you!

316
01:19:56,720 --> 01:19:58,756
I don't love you any more.

317
01:20:00,280 --> 01:20:02,236
I have nothing more to give you...

318
01:20:02,440 --> 01:20:04,271
...and you have nothing more for me.

319
01:20:05,760 --> 01:20:07,273
You know nothing of me.

320
01:20:10,480 --> 01:20:11,913
Life is temporary.

321
01:20:12,120 --> 01:20:13,519
We will have a beautiful memory.

322
01:20:19,800 --> 01:20:21,677
You must set yourself free from me.

323
01:20:23,320 --> 01:20:25,390
Life is one big goodbye.

324
01:20:26,240 --> 01:20:30,472
We each have to follow our own path
and each lead our own life.

325
01:20:34,680 --> 01:20:36,875
You will find a new companion.

326
01:20:37,080 --> 01:20:38,479
I am sure of it.

327
01:20:40,520 --> 01:20:44,559
I hope she will love you as much as I do.

328
01:30:50,920 --> 01:30:53,832
Be strong, you can not leave now.

329
01:31:35,040 --> 01:31:36,632
Damn.

330
01:31:57,240 --> 01:32:00,676
Trust me, there is nothing wrong
with these cards.

331
01:32:00,880 --> 01:32:04,714
Trust me. It is a brand new deck.

332
01:35:40,320 --> 01:35:41,833
Help Nicolai.

333
01:35:47,440 --> 01:35:48,236
Here...

334
01:35:48,600 --> 01:35:51,956
...for your services rendered...

335
01:35:52,160 --> 01:35:55,709
Once a whore, always a whore.

336
01:37:40,640 --> 01:37:44,519
Elise never loved anyone but you.

337
01:37:44,720 --> 01:37:47,792
I beg you, give her one more chance.

338
01:37:49,880 --> 01:37:53,111
Everything Elise did,
was out of love for you.

339
01:37:53,320 --> 01:37:56,630
And I know how much you love her.

340
01:37:56,840 --> 01:37:59,274
I beg you, don't go away.

341
01:38:01,440 --> 01:38:03,192
Don't go away.

342
01:38:51,320 --> 01:38:53,038
Where is he?

343
01:38:53,240 --> 01:38:55,151
He is gone.

344
01:38:56,320 --> 01:38:57,833
He escaped.

345
01:38:58,880 --> 01:39:00,677
The Count is dead.

346
01:39:08,120 --> 01:39:10,395
He is really gone.

347
01:39:11,440 --> 01:39:14,557
He will return and I will wait for him.

348
01:39:15,680 --> 01:39:17,477
I know he loves me.

349
01:40:14,320 --> 01:40:16,436
She has tuberculosis...

350
01:40:17,720 --> 01:40:20,951
...her resistance is too low.
There is nothing more I can do.

351
01:40:23,880 --> 01:40:27,111
She won't live to see the spring.

352
01:40:27,320 --> 01:40:29,276
The disease can't be stopped.

353
01:40:29,480 --> 01:40:31,710
The hotel is closing...

354
01:40:31,920 --> 01:40:33,433
...where can she go?

355
01:40:37,640 --> 01:40:39,551
What can we do now?

356
01:40:39,760 --> 01:40:41,273
I will stay with her.

357
01:40:41,480 --> 01:40:42,833
You can leave without worrying.

358
01:40:43,760 --> 01:40:45,034
It will not take long.

359
01:40:45,240 --> 01:40:46,389
She is exhausted.

360
01:40:46,560 --> 01:40:48,949
In my opinion, a matter of days.

361
01:40:49,160 --> 01:40:50,957
Thank you, Doctor.

362
01:41:15,080 --> 01:41:16,877
What is it?

363
01:41:17,080 --> 01:41:18,559
What are you doing?

364
01:41:20,840 --> 01:41:22,034
Are you leaving?

365
01:41:51,520 --> 01:41:52,999
I love everything...

366
01:41:54,520 --> 01:41:57,034
...that has a warm enchanted feeling...

367
01:41:58,520 --> 01:42:01,637
...like the spring and love...

368
01:42:02,560 --> 01:42:04,994
...and dreams and fantasies.

369
01:42:07,160 --> 01:42:10,072
Everything we call poetry.

370
01:42:10,280 --> 01:42:13,158
When spring comes again...

371
01:42:14,120 --> 01:42:15,917
...everything will be well.

372
01:42:28,480 --> 01:42:30,232
They are waiting for you...

373
01:42:30,440 --> 01:42:32,237
...you must go.

374
01:42:33,760 --> 01:42:35,830
It will be alright.

375
01:42:36,040 --> 01:42:36,916
I love you.

376
01:42:54,920 --> 01:42:56,717
This is Nicolai's...

377
01:42:58,520 --> 01:43:01,717
...would you give it to him
if you see him.

378
01:43:01,920 --> 01:43:03,717
You promise?

379
01:43:06,080 --> 01:43:07,877
I promise.

380
01:43:17,560 --> 01:43:19,391
I have to wait here for him...

381
01:43:21,040 --> 01:43:23,759
...otherwise he won't be able to find me.

382
01:43:28,440 --> 01:43:30,237
Olga will take care of me.

383
01:47:58,080 --> 01:47:59,274
Thank you, Doctor.

384
01:48:22,320 --> 01:48:23,514
Nicolai?

385
01:48:25,080 --> 01:48:27,196
What are your symptoms?

386
01:48:27,440 --> 01:48:29,192
Nicolai, it's you!

387
01:48:33,280 --> 01:48:35,510
It's me. Nina.

388
01:48:39,000 --> 01:48:40,797
Don't send me away.

389
01:48:45,840 --> 01:48:50,231
You still have those beautiful hands.

390
01:48:51,280 --> 01:48:53,077
A little older.

391
01:49:11,680 --> 01:49:12,715
Nina...

392
01:49:13,920 --> 01:49:17,117
How many years?
How many winters?

393
01:49:17,320 --> 01:49:19,117
You know how it goes.

394
01:49:20,040 --> 01:49:21,678
The hotel closed down...

395
01:49:21,880 --> 01:49:24,030
...after the death of the Count.

396
01:49:24,240 --> 01:49:26,037
I went with Bruno...

397
01:49:26,960 --> 01:49:30,032
...but he soon found another woman.

398
01:49:30,240 --> 01:49:33,755
I later found out he was killed in the war.

399
01:49:35,440 --> 01:49:38,989
I have only just been allowed
to return to Moscow.

400
01:49:41,600 --> 01:49:44,876
It wasn't easy to find you.

401
01:49:45,080 --> 01:49:46,911
Why did you come looking for me?

402
01:49:49,040 --> 01:49:50,837
Elise is dead...

403
01:49:51,800 --> 01:49:53,597
She is buried there.

404
01:49:55,680 --> 01:49:57,910
How have you been, all these years?

405
01:50:02,240 --> 01:50:03,878
Not bad.

406
01:50:04,080 --> 01:50:05,195
Not bad at all.

407
01:50:06,880 --> 01:50:10,111
Although...
the body is not so strong as it was.

408
01:50:12,320 --> 01:50:17,394
I'm alone, my wife died two years ago.

409
01:50:18,680 --> 01:50:20,193
Children?

410
01:50:20,440 --> 01:50:21,589
Two sons...

411
01:50:23,080 --> 01:50:24,479
...killed in the war.

412
01:50:31,600 --> 01:50:33,477
God rest their souls.

413
01:50:33,680 --> 01:50:35,477
Terrible.

414
01:50:37,560 --> 01:50:40,028
That wretched war.

415
01:50:41,520 --> 01:50:47,117
That's also the reason
I couldn't find you sooner.

416
01:50:48,440 --> 01:50:51,000
But I had to find you...

417
01:50:51,200 --> 01:50:53,998
...I promised Elise.

418
01:50:57,240 --> 01:50:59,515
Did you ever go back?

419
01:51:03,280 --> 01:51:05,999
When I left the hotel...

420
01:51:06,200 --> 01:51:09,636
...Elise stayed behind.

421
01:51:09,840 --> 01:51:13,753
She said she was going to wait for you.

422
01:51:14,720 --> 01:51:17,188
I said I would give you this, from her.

423
01:51:22,640 --> 01:51:24,153
Thanks, Nina.

424
01:51:39,280 --> 01:51:46,789
'Dear Nicolai. I know you love me,
and I will wait for you, forever. Elise.'

425
01:51:50,800 --> 01:51:52,313
Where is that woman?

426
01:51:54,960 --> 01:51:56,951
I let her out. Fifteen minutes ago.

427
01:55:52,040 --> 01:55:56,955
A Godlike face, an inspiration;
And life, and tears, and love...

428
01:55:57,160 --> 01:55:58,513
...and you.

429
01:56:07,520 --> 01:56:08,919
Sleep, my sweet girl...

430
01:56:09,800 --> 01:56:12,394
...my beautiful...

431
01:56:12,600 --> 01:56:17,151
Bayushki Bayu.

432
01:56:17,360 --> 01:56:22,639
Quietly the moon is peeking...

