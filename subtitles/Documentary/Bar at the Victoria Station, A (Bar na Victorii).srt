1
00:00:00,427 --> 00:00:02,427
Hello members of the press.

2
00:00:03,003 --> 00:00:05,003
Thank you for joining me for this
satellite conference today

3
00:00:05,636 --> 00:00:07,636
to discuss the new "Decorum Act"

4
00:00:07,937 --> 00:00:10,237
As you know, domestically we

5
00:00:10,557 --> 00:00:13,757
are dealing with a war
far more destructive and insidious

6
00:00:14,125 --> 00:00:18,125
than our ongoing wars in
Australia, Africa and Canada.

7
00:00:18,772 --> 00:00:22,372
It is the war on
our population explosion.

8
00:00:23,138 --> 00:00:25,638
The new laws of our 
great President Han,

9
00:00:26,014 --> 00:00:28,014
may seem harsh ...

10
00:00:28,501 --> 00:00:31,501
but they have been made with the
good of our country in mind.

11
00:00:31,771 --> 00:00:33,771
This great nation of ours

12
00:00:34,496 --> 00:00:37,496
has been built on the
extermination of its people.

13
00:00:38,076 --> 00:00:40,076
What??

14
00:00:44,232 --> 00:00:46,232
Uh, 

15
00:00:46,454 --> 00:00:48,454
the DEtermination ...
the determination of its people.

16
00:00:51,438 --> 00:00:52,638
Oh, God.

17
00:00:53,004 --> 00:00:55,004
Okay ... okay.  

18
00:00:55,319 --> 00:00:57,319
Don't panic ...just, don't panic
Forget it!

19
00:01:29,880 --> 00:01:31,380
Uh!
- Senator?

20
00:01:33,841 --> 00:01:35,841
Surveillance went down,
I got worried.

21
00:01:37,030 --> 00:01:39,030
I turned it off.

22
00:01:40,084 --> 00:01:42,584
Is it true, the death penalty?
- Yes.

23
00:01:43,500 --> 00:01:45,500
Well, as least you're not ...

24
00:01:51,432 --> 00:01:53,432
Senator, ...

25
00:02:06,004 --> 00:02:07,204
Are you okay?

26
00:02:07,561 --> 00:02:09,561
Huh, uh.

27
00:02:14,020 --> 00:02:17,020
Bar Karma  1x06
"The Expectant Mother"

28
00:02:17,676 --> 00:02:22,176
Subtitles by josmndsn

29
00:02:22,978 --> 00:02:24,978
# o-o-o, yeah,

30
00:02:34,249 --> 00:02:36,249
# aaaaah,oooooooo,

31
00:02:37,772 --> 00:02:39,772
# Yo-o-u-u will always

32
00:02:40,716 --> 00:02:42,216
# reap what

33
00:02:42,450 --> 00:02:44,450
# you sow.

34
00:02:54,846 --> 00:02:56,846
What's going on?

35
00:02:57,250 --> 00:02:59,250
Would you like a
glass of water?

36
00:03:04,025 --> 00:03:05,225
Oh, my!

37
00:03:05,275 --> 00:03:06,475
Zack?

38
00:03:08,401 --> 00:03:10,401
Go in there and
tell me what you see.

39
00:03:11,489 --> 00:03:13,489
Sure.

40
00:03:15,404 --> 00:03:18,404
What am I supposed to be...
looking for?

41
00:03:19,492 --> 00:03:21,492
Is that a bathroom?

42
00:03:23,450 --> 00:03:25,450
Yes, of course.

43
00:03:25,999 --> 00:03:28,499
You're not in a bar with
three people in it?

44
00:03:28,975 --> 00:03:30,175
Okay, ...

45
00:03:30,540 --> 00:03:32,240
so, you're halucinating.

46
00:03:32,522 --> 00:03:34,522
I'm going to call med services.
- No, don't do that.

47
00:03:34,740 --> 00:03:35,940
It's probably just that virus,

48
00:03:36,017 --> 00:03:38,017
the same one that my wife
and son had.

49
00:03:39,215 --> 00:03:41,715
I'm going to get you some aspirin
and some chicken soup.  - Okay.

50
00:03:42,148 --> 00:03:43,348
I'll be right back.

51
00:03:56,871 --> 00:03:58,871
Hello, again.
Feeling better?

52
00:04:05,526 --> 00:04:07,526
What's your name?

53
00:04:08,129 --> 00:04:10,129
Senator Knowles.

54
00:04:10,531 --> 00:04:12,531
Sarah.

55
00:04:12,778 --> 00:04:14,778
Do you know why you're
here, Sarah?

56
00:04:15,465 --> 00:04:16,565
Well, that's a first.

57
00:04:16,838 --> 00:04:18,838
I'm here because I have
a high fever.

58
00:04:19,142 --> 00:04:21,142
So, you think we're a ...
halucination?

59
00:04:25,412 --> 00:04:27,412
Where's that music
coming from?

60
00:04:28,100 --> 00:04:30,100
The jukebox.
- Aah!

61
00:04:30,909 --> 00:04:32,909
Jukebox! Oh!

62
00:04:34,539 --> 00:04:36,539
I've seen these in pictures.

63
00:04:37,207 --> 00:04:39,207
Oh, , ah, oh.

64
00:04:41,095 --> 00:04:42,123
He can't have me arrested
 for listening

65
00:04:42,173 --> 00:04:43,995
to music, in my own
halucination, can he?

66
00:04:45,284 --> 00:04:47,284
Who?
- Him.

67
00:04:47,495 --> 00:04:49,495
Who is that?

68
00:04:50,393 --> 00:04:52,393
President Han, of course.

69
00:04:53,377 --> 00:04:56,377
Your President has people arrested
for listening to music?

70
00:04:58,308 --> 00:05:00,308
He'd have you arrested for
wearing that dress.

71
00:05:01,490 --> 00:05:03,490
By the Fashion Police?
- It's a bad influence.

72
00:05:03,900 --> 00:05:05,900
It makes people think about sex,

73
00:05:06,194 --> 00:05:08,694
which equals offspring,
which equals overpopulation,

74
00:05:09,446 --> 00:05:10,946
which equals starvation.

75
00:05:11,300 --> 00:05:13,500
What year is it?
- 2058.

76
00:05:14,906 --> 00:05:16,906
Oh, ...

77
00:05:18,283 --> 00:05:20,283
oh, I get why I'm having
this halucination.

78
00:05:20,668 --> 00:05:21,868
Why?

79
00:05:22,175 --> 00:05:24,175
Ny mind wants me to remember

80
00:05:24,282 --> 00:05:26,282
what the world was like
before it got so dark 

81
00:05:26,603 --> 00:05:28,103
and sad and ugly.

82
00:05:28,497 --> 00:05:30,497
So, we're figments of
your imagination,

83
00:05:31,004 --> 00:05:33,004
representing a
happier time?

84
00:05:33,467 --> 00:05:35,467
Yeah.

85
00:05:38,434 --> 00:05:39,934
You remind me of
my grandfather,

86
00:05:40,198 --> 00:05:41,698
when I was 
a little girl.

87
00:05:43,051 --> 00:05:45,551
And, you, are like

88
00:05:45,681 --> 00:05:47,681
one of those sexy,
charming, punks I used to

89
00:05:48,223 --> 00:05:49,723
fall in love with
when I was young.

90
00:05:50,003 --> 00:05:51,203
You think I'm a punk?

91
00:05:51,651 --> 00:05:53,151
Better than being
her  grandfather.

92
00:05:53,478 --> 00:05:55,478
So, who am I in
your halucination?

93
00:05:56,055 --> 00:05:58,255
I think ... you're me

94
00:06:00,217 --> 00:06:02,217
when I was young
and happy 

95
00:06:02,471 --> 00:06:04,471
and I could hang out in bars

96
00:06:04,984 --> 00:06:06,984
and have sex and ...

97
00:06:07,493 --> 00:06:09,493
and read whatever I wanted to 

98
00:06:10,553 --> 00:06:12,553
and laugh.

99
00:06:12,671 --> 00:06:14,471
And, you can't do those
things now?

100
00:06:15,875 --> 00:06:17,875
You can't make a move without
being watched.

101
00:06:18,108 --> 00:06:20,108
But, I'm ever hardly allowed
to see anyone.

102
00:06:20,786 --> 00:06:22,286
Why are you so isolated?

103
00:06:22,659 --> 00:06:24,659
... Because everybody
wants to kill me.

104
00:06:26,581 --> 00:06:28,281
They think, because I'm
a Senator, I have a say

105
00:06:28,374 --> 00:06:29,874
in the laws of
my country.

106
00:06:30,026 --> 00:06:32,526
They don't realize that
I'm as powerless as they are.

107
00:06:33,731 --> 00:06:35,731
In America?  How?

108
00:06:36,128 --> 00:06:38,128
Apathy. Complacency. 
Fear.

109
00:06:42,048 --> 00:06:44,048
This is fun, ha.

110
00:06:44,335 --> 00:06:46,335
I feel so free here.

111
00:06:49,244 --> 00:06:51,244
(Sarah chuckles)

112
00:06:51,506 --> 00:06:53,506
I think am just going to
dance naked on the bar.

113
00:06:54,083 --> 00:06:55,283
Yeah, go for it. 

114
00:06:55,753 --> 00:06:57,753
No, no, no, no, no.

115
00:06:58,334 --> 00:07:01,234
Wait ...
is that a television?

116
00:07:06,536 --> 00:07:09,536
I remember these. 
Let's watch a television program.

117
00:07:10,270 --> 00:07:12,270
Let's watch a Sci-Fi 
classic, Fringe.

118
00:07:14,075 --> 00:07:16,075
Let's try this one over here.

119
00:07:24,542 --> 00:07:25,742
How could they know yet?

120
00:07:26,467 --> 00:07:27,667
Know what?

121
00:07:27,910 --> 00:07:29,910
That I'm the only one who knows.

122
00:07:38,355 --> 00:07:40,355
Sarah, wake up.

123
00:07:43,681 --> 00:07:45,681
You fainted.

124
00:07:51,288 --> 00:07:53,288
So, this wasn't a halucination,
was it?

125
00:07:53,720 --> 00:07:55,720
No.

126
00:07:57,872 --> 00:07:59,072
Are you going to arrest me?

127
00:07:59,615 --> 00:08:01,415
No, we're here to help you.

128
00:08:02,110 --> 00:08:04,110
Who's the man in the newscast?

129
00:08:04,808 --> 00:08:06,808
Who is Zack Sizer?

130
00:08:09,074 --> 00:08:11,074
He's my Chief of Staff.

131
00:08:11,675 --> 00:08:13,675
So, its true then,

132
00:08:14,183 --> 00:08:15,383
he's dead?

133
00:08:15,828 --> 00:08:17,328
He's not dead, yet.

134
00:08:17,690 --> 00:08:19,690
That was a projection of the future.

135
00:08:21,066 --> 00:08:22,266
That's what could happen,

136
00:08:22,571 --> 00:08:24,571
if you don't
dramatically change

137
00:08:25,006 --> 00:08:26,806
some path you're
about to go down.

138
00:08:27,108 --> 00:08:28,808
Sarah, what's the
"Decorum Act"?

139
00:08:29,143 --> 00:08:31,143
What do you have
to do with it?

140
00:08:33,433 --> 00:08:35,433
We're not judging you
or threatening you.

141
00:08:37,349 --> 00:08:39,849
You're here because your life has
been thrown off balance, somehow.

142
00:08:41,522 --> 00:08:43,522
We're here to help you ...
- I have to go find him.

143
00:08:49,579 --> 00:08:51,079
Well, that went well.

144
00:08:53,895 --> 00:08:55,895
Oh!  My God!
Oh, my God.

145
00:08:56,581 --> 00:08:57,781
Are you still alive?

146
00:08:58,623 --> 00:09:00,623
Oh, what am I doing.
- No, wait, it's all right.

147
00:09:00,976 --> 00:09:02,676
Surveillance is still down.

148
00:09:03,265 --> 00:09:05,265
Here's some aspirin
and soup tabs.

149
00:09:07,243 --> 00:09:09,243
Aaw. 

150
00:09:10,421 --> 00:09:12,421
Hey, where's your wig?

151
00:09:13,310 --> 00:09:14,510
Uh, ...

152
00:09:15,145 --> 00:09:16,445
Are you okay?

153
00:09:16,808 --> 00:09:18,808
I have to talk to you.

154
00:09:21,003 --> 00:09:22,503
Is it about this?

155
00:09:22,930 --> 00:09:24,430
Yes.

156
00:09:25,283 --> 00:09:27,283
You should have let me
follow her out.

157
00:09:27,632 --> 00:09:28,832
Patience, Doug.

158
00:09:29,098 --> 00:09:31,098
There's a time and
place for everything.

159
00:09:31,493 --> 00:09:33,193
But these gadgets are
all your thing.

160
00:09:33,486 --> 00:09:34,986
And, someday they'll be yours.

161
00:09:35,034 --> 00:09:37,034
You gotta learn
how to interpret them.

162
00:09:37,762 --> 00:09:39,062
And, it takes focus.

163
00:09:39,328 --> 00:09:41,328
What's it telling you?

164
00:09:43,609 --> 00:09:45,609
President Han's Health
and Safety law

165
00:09:46,120 --> 00:09:48,120
requires mandatory sterilization

166
00:09:48,897 --> 00:09:50,897
for married couples after their
first child.

167
00:09:51,239 --> 00:09:53,239
And, ... 

168
00:09:54,182 --> 00:09:56,682
makes it illegal for unmarried
women to parachute on Sundays?

169
00:09:59,473 --> 00:10:01,473
You went south and 
back in time.

170
00:10:02,450 --> 00:10:04,450
That's Florida.  Wrong state.
Wrong century.

171
00:10:05,291 --> 00:10:07,291
That makes no sense.

172
00:10:07,841 --> 00:10:09,641
I know.  Parachuting is
just as dangerous

173
00:10:10,093 --> 00:10:11,593
any other day of the week.

174
00:10:11,925 --> 00:10:13,925
No-o, if Han is so

175
00:10:14,187 --> 00:10:16,187
worried about a population
explosion,

176
00:10:16,249 --> 00:10:18,249
Why doesn't he just lift
his ban on 

177
00:10:18,416 --> 00:10:20,416
birth control and abortion?
- Because then he'd have to

178
00:10:20,689 --> 00:10:22,189
admit he made a mistake.

179
00:10:22,411 --> 00:10:24,711
How many dictators do you know
who would do that?

180
00:10:25,185 --> 00:10:26,785
So, what does he do instead?

181
00:10:27,029 --> 00:10:29,029
Pass the Decorum Act.

182
00:10:29,340 --> 00:10:31,340
What is the Decorum Act?

183
00:10:31,720 --> 00:10:34,720
It says, any man who
refuses sterilization

184
00:10:35,855 --> 00:10:38,355
and has more than one child, 
in or out of wedlock,

185
00:10:38,698 --> 00:10:40,698
is branded a traitor to his country

186
00:10:41,185 --> 00:10:42,685
and given the death sentence.

187
00:10:43,252 --> 00:10:45,252
Decorum Act, sounds more
like Hold on 

188
00:10:45,370 --> 00:10:47,370
to Your Balls
for Dear Life Act.

189
00:11:02,781 --> 00:11:04,481
It's my pregnancy test.

190
00:11:04,785 --> 00:11:07,285
And, it's red because ...
I'm pregnant.

191
00:11:10,753 --> 00:11:12,753
Zack is already married
and has a son.

192
00:11:15,153 --> 00:11:17,153
Shouldn't he have been 
sterilized, then?

193
00:11:17,802 --> 00:11:19,802
A lot of people were refusing
to do it, that's why 

194
00:11:20,195 --> 00:11:22,195
President Han created the
Decorum Act

195
00:11:22,583 --> 00:11:23,883
to motivate them.

196
00:11:24,233 --> 00:11:26,533
I'd say, execution by
one of those scary

197
00:11:27,010 --> 00:11:29,410
futuristic laser things would
motivate men sufficiently.

198
00:11:29,988 --> 00:11:31,988
I knew Zack hadn't been
sterilized.

199
00:11:32,567 --> 00:11:34,567
I knew he's married ...

200
00:11:35,539 --> 00:11:37,539
I knew he had a little boy.

201
00:11:38,385 --> 00:11:40,885
And, now their going to kill
him and take my baby away from me.

202
00:11:41,233 --> 00:11:43,233
That's where we come in,

203
00:11:43,261 --> 00:11:44,461
to help you through this.

204
00:11:44,803 --> 00:11:47,003
I already know my options,
they're all bad.

205
00:11:47,319 --> 00:11:49,319
Wanna bet?

206
00:11:49,833 --> 00:11:51,833
What? We're going to
play a card game?

207
00:11:52,261 --> 00:11:54,261
It's no game.

208
00:12:03,893 --> 00:12:05,893
What does that mean?

209
00:12:06,237 --> 00:12:07,737
Possible outcome for you.

210
00:12:08,064 --> 00:12:10,064
If another man marries you
and claims that

211
00:12:10,420 --> 00:12:11,620
the child is his,

212
00:12:12,135 --> 00:12:14,135
that could get you out of this.

213
00:12:14,393 --> 00:12:17,393
I don't even have contact
with anyone else, except Zack.

214
00:12:17,584 --> 00:12:19,584
It's just a possibility.

215
00:12:21,942 --> 00:12:24,442
The Decorum Act will safeguard
our resources,

216
00:12:25,052 --> 00:12:27,052
which are currently
threatened by our

217
00:12:27,549 --> 00:12:28,749
surplus population.

218
00:12:29,191 --> 00:12:31,191
This card requires that
you break the law.

219
00:12:31,782 --> 00:12:34,282
But, the option shows you
back at work as if

220
00:12:34,672 --> 00:12:36,672
nothing ever happened.

221
00:12:36,976 --> 00:12:38,976
Abortion.

222
00:12:39,166 --> 00:12:41,166
Do I ever get pregnant again?

223
00:12:46,503 --> 00:12:48,503
What is that image mean?

224
00:12:48,948 --> 00:12:50,448
Not sure.
But you're alone.

225
00:12:52,447 --> 00:12:53,947
Whew, I don't want 
to be alone.

226
00:12:54,733 --> 00:12:56,733
There is one more card.

227
00:13:07,387 --> 00:13:09,187
That can't be me?
- It's you.

228
00:13:09,564 --> 00:13:11,564
This place exists, Sarah. 

229
00:13:12,028 --> 00:13:14,028
And, it appears
you'll find it.

230
00:13:14,558 --> 00:13:16,558
How?  I can't just 
get in a car

231
00:13:16,849 --> 00:13:18,849
and hop on a
plane somewhere.

232
00:13:19,153 --> 00:13:21,153
I am tracked.
Wherever I go.

233
00:13:22,122 --> 00:13:23,822
They can't have cameras
everywhere?

234
00:13:24,060 --> 00:13:27,360
Forget cameras, I have a 
security chip inside my body.

235
00:13:28,677 --> 00:13:30,677
There is no escaping
from that.

236
00:13:42,130 --> 00:13:44,630
(sobbing) I hate my life.
I hate my life.

237
00:13:48,766 --> 00:13:50,766
Then, change it.

238
00:13:50,920 --> 00:13:52,920
Haven't you been listening?

239
00:13:53,269 --> 00:13:55,269
How can I change if my
hands are tied?  I can't

240
00:13:55,532 --> 00:13:56,732
do anything!

241
00:13:57,040 --> 00:13:59,040
The cards say otherwise.

242
00:13:59,870 --> 00:14:02,370
The question is, Sarah,
what is it you really want?

243
00:14:03,524 --> 00:14:05,524
I want to marry Zack.
I want to have my baby.

244
00:14:06,660 --> 00:14:09,160
I want my country to be a
beautiful place to live again.

245
00:14:09,652 --> 00:14:12,652
Can you wave your magic wand
and make all that happen?

246
00:14:15,132 --> 00:14:17,132
Do you see this?

247
00:14:19,397 --> 00:14:20,597
Beautiful.

248
00:14:20,921 --> 00:14:22,921
It was my grandmother's.

249
00:14:25,593 --> 00:14:27,593
And, she was a woman
who never compromised.

250
00:14:29,418 --> 00:14:31,418
Not in love, marriage

251
00:14:31,849 --> 00:14:33,849
or anything.

252
00:14:35,876 --> 00:14:37,876
I wish I were more 
like your grandmother.

253
00:14:38,344 --> 00:14:39,544
You can be. 

254
00:14:39,950 --> 00:14:41,750
You can lead the life
you want,

255
00:14:42,020 --> 00:14:44,020
if you have the courage
to fight for it.

256
00:14:47,155 --> 00:14:49,155
I'm so tired
of compromising.

257
00:14:49,611 --> 00:14:51,611
I have been compromising
all my life and

258
00:14:52,126 --> 00:14:54,126
look where it's gotten me.

259
00:14:56,080 --> 00:14:58,080
I don't want to
do it anymore.

260
00:15:00,214 --> 00:15:02,214
And, we can help you.

261
00:15:04,824 --> 00:15:06,824
So, you'll follow me out and
then what?

262
00:15:07,060 --> 00:15:08,560
I'll figure it out when I get there.

263
00:15:08,787 --> 00:15:10,787
How do I know you'll
even show up?

264
00:15:11,437 --> 00:15:13,437
We're asking you 
to trust us.

265
00:15:14,367 --> 00:15:16,367
All right.

266
00:15:22,298 --> 00:15:24,298
Thank you ...
for giving me hope.

267
00:15:25,935 --> 00:15:27,135
You're up.

268
00:15:27,440 --> 00:15:29,440
Finally.  Thought
I'd never make it out.

269
00:15:33,131 --> 00:15:34,331
UH!

270
00:15:34,620 --> 00:15:36,620
What the hell just happened?

271
00:15:37,567 --> 00:15:38,767
Security.

272
00:15:39,071 --> 00:15:40,571
President Han runs
a tight ship.

273
00:15:41,232 --> 00:15:42,432
How are we going to
get around it?

274
00:15:42,740 --> 00:15:44,740
Remember that chip Sarah
had implanted

275
00:15:44,929 --> 00:15:46,129
inside her body,

276
00:15:46,601 --> 00:15:47,801
Yeah.

277
00:15:48,453 --> 00:15:50,453
We need to get
one of those.

278
00:16:09,011 --> 00:16:10,211
Hi.

279
00:16:12,401 --> 00:16:13,601
I'm Dayna.

280
00:16:14,047 --> 00:16:15,247
Wow.

281
00:16:16,238 --> 00:16:18,238
I only see women like you
in my dreams.

282
00:16:21,227 --> 00:16:23,227
Let me buy you a drink.

283
00:16:27,573 --> 00:16:29,573
Waiter.

284
00:16:30,672 --> 00:16:33,772
God, I have such a weakness
for men in uniform.

285
00:16:40,040 --> 00:16:42,040
Cheers.

286
00:16:46,313 --> 00:16:47,813
I need to get a body
scan of you.

287
00:16:50,548 --> 00:16:52,548
Take your clothes off.

288
00:16:57,150 --> 00:16:58,350
Ouh!
Ouh!

289
00:16:59,465 --> 00:17:00,965
You're being very
brave Irving.

290
00:17:00,982 --> 00:17:02,182
Thanks.

291
00:17:05,448 --> 00:17:07,448
Huh.

292
00:17:09,553 --> 00:17:11,553
So, there.  What war
were you fighting?

293
00:17:11,997 --> 00:17:13,197
Australia.

294
00:17:14,662 --> 00:17:16,362
What did we ever
do to you?

295
00:17:16,650 --> 00:17:18,650
It's nothing personal,
just following orders.

296
00:17:19,524 --> 00:17:20,524
Huh.

297
00:17:20,787 --> 00:17:22,787
If it was up to me,
I'd be home with my family.

298
00:17:23,093 --> 00:17:25,593
Well, that's a decision you

299
00:17:25,904 --> 00:17:27,904
can now make for yourself.

300
00:17:33,555 --> 00:17:35,555
So, am I going to
see you again?

301
00:17:36,299 --> 00:17:38,299
That depends on what
you decide.

302
00:17:55,277 --> 00:17:57,277
You're next.

303
00:17:59,000 --> 00:18:01,000
You know it's
the only way.

304
00:18:01,385 --> 00:18:03,385
I understand.

305
00:18:06,011 --> 00:18:08,011
Surveillance is still on.

306
00:18:09,592 --> 00:18:11,592
They're waiting.

307
00:18:21,802 --> 00:18:23,302
(Sarah, off screen) Members of the 
press and dignitaries ...

308
00:18:23,614 --> 00:18:25,614
Who are you?
- Thank you for joining me for

309
00:18:25,844 --> 00:18:27,844
this satellite conference today.
As you know ...

310
00:18:28,410 --> 00:18:29,910
I'm Lieutenant Irving
Grossman.

311
00:18:31,001 --> 00:18:33,001
I'm here to ...

312
00:18:34,458 --> 00:18:36,458
I'm Sarah's husband.

313
00:18:39,512 --> 00:18:41,512
Could I borrow
that for a minute.

314
00:18:44,403 --> 00:18:46,403
Take good care of her.

315
00:18:49,555 --> 00:18:51,555
... our great leader President Han,
the Decorum Act.

316
00:18:52,562 --> 00:18:54,562
Hold that thought ... Senator.

317
00:18:57,431 --> 00:18:58,631
Allow me to

318
00:18:59,111 --> 00:19:01,111
introduce myself,

319
00:19:01,460 --> 00:19:04,460
I am twice decorated
Lieutenant Irving Grossman.

320
00:19:06,380 --> 00:19:08,580
And, I think the Senator
is being a little shy.

321
00:19:09,868 --> 00:19:12,368
I believe the people of
this great nation,

322
00:19:14,024 --> 00:19:16,024
under our leader President Ho ...
- Han.

323
00:19:16,681 --> 00:19:18,981
President Han would like
to hear the happy news.

324
00:19:21,177 --> 00:19:22,377
Sweetie.

325
00:19:26,483 --> 00:19:28,483
Senator Knowles and I
secretly married got married

326
00:19:28,765 --> 00:19:30,765
last week.
See.

327
00:19:31,431 --> 00:19:33,231
Now, unfortunately,
we have to run

328
00:19:33,530 --> 00:19:35,530
because I'm stationed in ...

329
00:19:36,182 --> 00:19:38,182
a top secret location.

330
00:19:38,552 --> 00:19:40,852
So, even though, right now,
she's too choked up

331
00:19:41,500 --> 00:19:43,500
to talk, she would like
to say goodbye

332
00:19:43,717 --> 00:19:45,217
and good luck to
you all.

333
00:19:45,958 --> 00:19:47,458
Actually, I would like
to say something.

334
00:19:47,632 --> 00:19:48,832
Go right ahead.

335
00:19:49,271 --> 00:19:51,771
I would like to choose Zack Sizer,
my Chief of Staff

336
00:19:52,196 --> 00:19:53,396
to replace me.

337
00:19:53,538 --> 00:19:55,538
Because he is a great patriot.

338
00:19:56,550 --> 00:19:58,550
And, also because it is
 my right to do so 

339
00:19:58,825 --> 00:20:01,325
under the Wasilla Act of 2016,

340
00:20:01,751 --> 00:20:03,751
which says that government 
representatives who leave

341
00:20:03,944 --> 00:20:06,444
office early are allowed to choose 
their own successors.

342
00:20:06,979 --> 00:20:08,979
We have to leave, goodbye.

343
00:20:10,190 --> 00:20:11,390
Military escort is outside.

344
00:20:11,754 --> 00:20:12,954
Thanks, Senator.

345
00:20:13,248 --> 00:20:14,448
Good luck.

346
00:20:17,695 --> 00:20:19,695
I love you.

347
00:20:37,129 --> 00:20:38,029
How did it go?

348
00:20:38,283 --> 00:20:40,283
Good.
Good news and bad news.

349
00:20:40,414 --> 00:20:41,614
I'll take the good first.

350
00:20:42,023 --> 00:20:44,023
Aah, our plan went
off without a hitch. 

351
00:20:45,474 --> 00:20:47,474
Sarah's going to be just fine.

352
00:20:47,984 --> 00:20:49,984
Well, let's not make
any assumptions.

353
00:20:52,216 --> 00:20:54,216
We all deserve the right
to choose how

354
00:20:54,622 --> 00:20:56,122
we're going to
live our lives.

355
00:20:56,356 --> 00:20:58,356
It is time to rise up 
against the

356
00:20:58,921 --> 00:21:00,921
monster who has oppressed
our great nation,

357
00:21:01,224 --> 00:21:03,224
for 21 long years.

358
00:21:04,070 --> 00:21:06,570
Who has tried to make
us forget the beauty of music

359
00:21:07,778 --> 00:21:09,778
and love ...
and family

360
00:21:11,688 --> 00:21:13,688
and self expression.

361
00:21:14,358 --> 00:21:16,358
It is time to rise up 
against

362
00:21:16,813 --> 00:21:18,313
 President Han.

363
00:21:19,118 --> 00:21:21,118
Pregnant and leading
a revolution.

364
00:21:22,813 --> 00:21:24,813
And, they say, woman
can't have it all.

365
00:21:27,028 --> 00:21:29,028
So, what was the bad news?

366
00:21:30,221 --> 00:21:32,221
Oh, you didn't get a chance
to see me in my uniform.

367
00:21:32,988 --> 00:21:34,988
Looked pretty good.

368
00:21:35,667 --> 00:21:36,667
Huh.

369
00:21:36,748 --> 00:21:38,748
Try this one
on for size.

370
00:21:42,786 --> 00:21:47,786
Subtitles by josmndsn

