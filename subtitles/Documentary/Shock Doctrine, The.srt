1
00:05:47,380 --> 00:05:49,644
Steven Spurrier.

2
00:05:51,217 --> 00:05:54,618
I own the winery Madeleine...

3
00:05:55,321 --> 00:05:59,223
and founder of the Academy of Wine.

4
00:06:00,560 --> 00:06:05,054
Sorry, sir,
but your name is not on the list.

5
00:06:06,799 --> 00:06:09,324
The Englishman is not in the list.

6
00:06:11,804 --> 00:06:15,638
The receipt.
I bought a ticket two weeks ago.

7
00:06:20,079 --> 00:06:21,410
Follow me sir.

8
00:07:45,731 --> 00:07:49,531
<i>The Bicentanial of the United States.</i>
<i>Cross the Atlantic</i>

9
00:12:53,372 --> 00:12:54,361
�Mr Tari?

10
00:12:56,041 --> 00:12:56,973
Hello.

11
00:12:59,411 --> 00:13:01,743
We met at dinner
of the New Federation...

12
00:13:01,814 --> 00:13:03,748
the other night.

13
00:13:06,752 --> 00:13:08,151
Sorry, I don't remember you.

14
00:13:08,420 --> 00:13:09,387
Look.

15
00:13:09,789 --> 00:13:11,450
The poor and reviled...

16
00:13:11,657 --> 00:13:13,591
St. Est�phe Cos D'Estournel del 59...

17
00:13:13,659 --> 00:13:15,820
neglected by negligible acidity...

18
00:13:15,928 --> 00:13:18,089
and low fiber.

19
00:13:18,397 --> 00:13:19,887
I hear you.

20
00:13:20,132 --> 00:13:22,657
I predict that aging...

21
00:13:22,735 --> 00:13:24,760
will prove his critics
were wrong.

22
00:13:25,437 --> 00:13:27,428
So, �I am accused of having open it...

23
00:13:27,506 --> 00:13:29,440
15 years too early?

24
00:13:30,242 --> 00:13:31,971
20 would be better.

25
00:13:32,578 --> 00:13:34,239
Do you like vintage.

26
00:13:34,313 --> 00:13:36,042
I prefer them young.

27
00:13:37,082 --> 00:13:39,710
�I can sit with you for a moment?

28
01:31:45,097 --> 01:31:45,756
�We're here!

29
01:31:47,633 --> 01:31:48,429
Hi. I'm fine.

30
01:31:53,706 --> 01:31:55,765
�Mr. Kahn, delited!

31
01:31:59,512 --> 01:32:01,241
Agreed. <i>Allons-y!</i> �Lets go, Lets go!

32
01:32:07,953 --> 01:32:13,619
Ladies and Gentlemen,
my name is Steven Spurrier. Welcome.

33
01:32:19,031 --> 01:32:22,797
I conceived this wine tasting
Franco-American...

34
01:32:22,868 --> 01:32:26,099
to educate my palate...

35
01:32:26,171 --> 01:32:31,165
and others in this new forum...

36
01:32:31,243 --> 01:32:36,340
about winemaking in the United States.

37
01:32:37,216 --> 01:32:42,415
1976, the year of the Bicentennial of the United States.

38
01:32:44,557 --> 01:32:48,118
I thought an event like this...

39
01:32:48,193 --> 01:32:51,492
we remember the importance of France...

40
01:32:51,564 --> 01:32:56,467
in American history
200 years ago.

41
01:33:03,208 --> 01:33:06,644
I was surprised at the quality...

42
01:33:06,712 --> 01:33:09,772
of California wines.

43
01:33:10,749 --> 01:33:13,684
And their originality.

44
01:33:14,787 --> 01:33:17,278
There is some interest in the Napa Valley...

45
01:33:17,356 --> 01:33:19,586
to experiment in winemaking...

46
01:33:20,893 --> 01:33:24,989
with novel methods.

47
01:33:36,976 --> 01:33:38,807
that said...

48
01:33:38,877 --> 01:33:42,745
I've selected the best wines of California...

49
01:33:43,449 --> 01:33:44,746
made in the French style...

50
01:33:44,817 --> 01:33:48,753
together with some
from my favorite French wines.

51
01:33:51,123 --> 01:33:54,217
And in order to be objective...

52
01:33:54,827 --> 01:33:57,193
tasting will...

53
01:33:58,130 --> 01:33:59,188
blind.

54
01:33:59,798 --> 01:34:00,765
I now...

55
01:34:01,600 --> 01:34:03,761
I present to you...

56
01:34:04,236 --> 01:34:05,601
the Judges.

57
01:34:05,838 --> 01:34:07,829
Mr Christian Vannequ�...

58
01:34:07,906 --> 01:34:10,238
the sommelier of La Tour d'Argent...

59
01:34:10,576 --> 01:34:12,339
Mr Jean-Claude Vrinat...

60
01:34:12,411 --> 01:34:15,437
owner of the famous restaurant Taillevent...

61
01:34:15,514 --> 01:34:17,379
Mr Pierre Br�joux...

62
01:34:17,449 --> 01:34:20,247
Inspector general of the A.O. C...

63
01:34:20,486 --> 01:34:22,351
Mrs Odette Kahn...

64
01:34:22,421 --> 01:34:24,912
chief writer...

65
01:34:24,990 --> 01:34:27,823
of <i>The magazine of French Wines...</i>

66
01:34:28,260 --> 01:34:30,228
Mr Pierre Tari...

67
01:34:30,295 --> 01:34:34,595
secretary general of the Association
of "Grands Crus Class�s"...

68
01:34:34,867 --> 01:34:37,199
Mr Raymond Oliver...

69
01:34:37,269 --> 01:34:40,102
owner and chef del Grand Vefour...

70
01:34:40,305 --> 01:34:42,068
Mr Claude Dubois-Millot...

71
01:34:42,141 --> 01:34:44,701
director of marketing of Gault Millau...

72
01:34:44,777 --> 01:34:46,938
and Mr Aubert de Villaine...

73
01:34:47,012 --> 01:34:51,176
part-owner
of Domaine de la Roman�e-Conti.

74
01:34:51,650 --> 01:34:53,948
Thank You everyone.

75
01:34:54,820 --> 01:34:56,344
�Lets start!

76
01:35:22,915 --> 01:35:25,145
My dear Br�joux, �what do you think?

77
01:35:26,018 --> 01:35:27,246
It is solid.

78
01:35:27,419 --> 01:35:29,284
This is not a French wine.

79
01:35:29,354 --> 01:35:32,619
We are here to judge the quality,
not the country of origin.

80
01:35:35,094 --> 01:35:36,686
�Kidding?

81
01:35:39,832 --> 01:35:41,197
This is surely a French wine.

82
01:35:41,266 --> 01:35:41,789
Clear.

83
01:35:42,334 --> 01:35:43,528
leathery...

84
01:35:43,836 --> 01:35:46,498
and a little dominant and its mellow.

85
01:35:48,240 --> 01:35:50,174
Has an acceptable taste.

86
01:35:51,944 --> 01:35:53,377
For once you're right.

87
01:35:55,047 --> 01:35:56,173
without a doubt from California.

88
01:35:56,248 --> 01:35:57,442
No, Br�joux youre right.

89
01:35:58,217 --> 01:35:59,149
This one is French.

90
01:35:59,418 --> 01:36:00,544
Its acceptable.

91
01:36:00,619 --> 01:36:01,381
Of course.

92
01:36:16,869 --> 01:36:19,565
Unpleasant, with no taste.

93
01:36:32,217 --> 01:36:34,310
Ladies and Gentlemen...

94
01:36:35,420 --> 01:36:36,910
let's take a little break...

95
01:36:36,989 --> 01:36:38,684
before moving on to the reds.

96
01:36:38,757 --> 01:36:39,655
Take this opportunity...

97
01:36:40,626 --> 01:36:43,789
to do a vote recount for the
white wines.

98
01:37:27,506 --> 01:37:28,302
�Who won?

99
01:37:32,778 --> 01:37:33,767
�Who won?

100
01:37:39,051 --> 01:37:40,279
One second...

101
01:37:42,487 --> 01:37:43,385
Please.

102
01:38:28,100 --> 01:38:31,695
Sir, I would like to see
the results.

103
01:38:37,843 --> 01:38:39,504
Please.

104
01:38:46,451 --> 01:38:48,112
We are all waiting impatiently.

105
01:38:48,186 --> 01:38:49,346
Yes, yes.

106
01:38:50,389 --> 01:38:51,913
Here we go.

107
01:38:54,393 --> 01:38:55,417
In First Place...

108
01:38:56,895 --> 01:39:00,194
with a total of 132 points...

109
01:39:02,401 --> 01:39:03,390
is for...

110
01:39:06,772 --> 01:39:08,740
Ch�teau Montelena...

111
01:39:09,207 --> 01:39:10,640
of Napa Valley...

112
01:39:11,543 --> 01:39:12,669
in California.

113
01:39:17,049 --> 01:39:19,279
Representing Montelena...

114
01:39:21,219 --> 01:39:23,551
Mr Bo Barrett.

115
01:39:33,732 --> 01:39:37,099
In Second Place, with 126...
