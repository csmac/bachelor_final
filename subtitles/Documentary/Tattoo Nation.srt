1
00:00:00,300 --> 00:00:04,152
A Girl With Dragon Tatoo
Trailer 

2
00:00:07,300 --> 00:00:10,283
She's one of the best investigators I have.

3
00:00:10,316 --> 00:00:11,322
She's different.

4
00:00:11,357 --> 00:00:12,344
In what way?

5
00:00:12,379 --> 00:00:13,297
In every way.

6
00:00:13,332 --> 00:00:16,209
Something wrong with the report?

7
00:00:16,244 --> 00:00:18,172
Anything you chose not to disclose?

8
00:00:19,365 --> 00:00:21,376
He's clean, in my opinion.

9
00:00:22,965 --> 00:00:23,693
He's honest.

10
00:00:23,728 --> 00:00:25,397
Our credibility isn't dead yet.

11
00:00:25,432 --> 00:00:26,367
Mine is.

12
00:00:26,403 --> 00:00:30,690
He's had a long standing sexual relationship
 with his co-editor of the magazine.

13
00:00:30,725 --> 00:00:32,400
Sometimes he pleasures her.

14
00:00:32,435 --> 00:00:35,191
Not often enough, in my opinion.

15
00:00:35,451 --> 00:00:37,462
No, you're right not to include that.

16
00:00:38,468 --> 00:00:43,083
I need your help. You come, stay on the island.

17
00:00:43,119 --> 00:00:47,430
A way of avoiding all those people
 you might want to avoid right now.

18
00:00:47,516 --> 00:00:51,503
You will be investigating thieves, misers, bullies,

19
00:00:51,538 --> 00:00:56,655
the most detestable collection
 of people that you will ever meet.

20
00:00:56,690 --> 00:00:57,570
My family.

21
00:00:58,576 --> 00:01:00,551
This is Harriet.

22
00:01:00,586 --> 00:01:02,562
Someone in the family murdered Harriet,

23
00:01:02,597 --> 00:01:07,635
And for the past 40 years has been trying to drive me insane.

24
00:01:09,635 --> 00:01:14,646
Those are from her, and the rest, from her killer.

25
00:01:15,667 --> 00:01:17,678
You failed to adopt to four foster homes,

26
00:01:17,713 --> 00:01:19,689
arrested twice for intoxication,

27
00:01:19,724 --> 00:01:21,665
twice for assault.

28
00:01:21,700 --> 00:01:23,711
How many partners have you had in the last month?

29
00:01:23,746 --> 00:01:26,692
And how many of those were men?

30
00:01:26,727 --> 00:01:27,697
I should have control of my money.

31
00:01:27,732 --> 00:01:30,749
And you will. Once you learn to be sociable.

32
00:01:31,754 --> 00:01:33,765
Why don't we start with me.

33
00:01:33,800 --> 00:01:35,776
You know what to do.

34
00:01:37,591 --> 00:01:39,762
How can I find something you've
 been unable to find in 40 years?

35
00:01:39,797 --> 00:01:43,899
You don't know them, and you 
have a very keen investigative mind.

36
00:01:44,824 --> 00:01:45,794
You were here that day.

37
00:01:45,830 --> 00:01:47,840
It was a terrible day. Searching, not finding.

38
00:01:48,846 --> 00:01:52,840
I never found a body. Was it spontaneous? Was it calculated?

39
00:01:52,875 --> 00:01:55,577
Did she know something someone wished she didn't?

40
00:01:55,884 --> 00:02:01,218
The last time I reported on something 
without being absolutely sure, I lost my life savings.

41
00:02:01,253 --> 00:02:02,969
I need a research assistant.

42
00:02:03,004 --> 00:02:05,910
I know an excellent one. She did the background check on you.

43
00:02:05,938 --> 00:02:08,954
You don't think we could hire just
 anyone for something like this?

44
00:02:10,965 --> 00:02:12,976
It's Mikael Blomkvist. May I come in?

45
00:02:15,992 --> 00:02:16,962
We need to talk.

46
00:02:16,997 --> 00:02:20,170
Hey. Hey! Who do you think you are?

47
00:02:20,205 --> 00:02:22,468
Throw some clothes on. Get rid of your girlfriend.

48
00:02:22,503 --> 00:02:24,423
Can I call you Lisbeth?

49
00:02:24,458 --> 00:02:27,553
I want you to help me catch a killer of women.

50
00:02:28,057 --> 00:02:32,894
I've got absolutely no idea how they 
are connected to the death of a sixteen year old girl.

51
00:02:32,929 --> 00:02:34,461
Don't you need to look over these?
I got it.

52
00:02:35,094 --> 00:02:39,382
It's better to look for what I am about
 to show you, on an empty stomach.

53
00:02:40,121 --> 00:02:41,136
What are you doing?

54
00:02:41,136 --> 00:02:42,097
Reading your notes.

55
00:02:42,132 --> 00:02:43,138
They're encrypted.

56
00:02:43,173 --> 00:02:44,143
Please.

57
00:02:45,148 --> 00:02:47,159
Rape, torture, fire, animals, religion.

58
00:02:47,194 --> 00:02:48,165
Am I missing anything?

59
00:02:48,200 --> 00:02:49,135
The names.

60
00:02:49,170 --> 00:02:50,176
I may have something.

61
00:02:51,181 --> 00:02:52,186
They never really liked people poking around in their lives.

62
00:02:52,222 --> 00:02:54,197
Everybody knows why you're here.

63
00:02:56,208 --> 00:02:59,224
Someone killed her. Someone on the island that day.

64
00:03:00,230 --> 00:03:03,246
If a woman approaches any beast and lies with it...

65
00:03:03,281 --> 00:03:06,032
they shall kill the woman and the beast.

66
00:03:07,267 --> 00:03:09,278
These people are insane. Soon you will know us all.

67
00:03:09,313 --> 00:03:11,254
Only to well.

68
00:03:11,289 --> 00:03:13,300
With my apologies.

69
00:03:13,778 --> 00:03:16,685
FROM THE INTERNATIONAL BEST SELLING
TRILOGY

70
00:03:20,834 --> 00:03:22,330
A David Fincher Film

71
00:03:24,414 --> 00:03:25,501
Daniel Craig

72
00:03:25,536 --> 00:03:28,006
Rooney Mara

73
00:03:29,068 --> 00:03:32,155
The Girl with the Dragon Tatoo

74
00:03:32,190 --> 00:03:34,174
Subtitle by 
Amir_T6262

75
00:03:34,209 --> 00:03:35,772
amir_t6262@yahoo.com

76
00:03:36,502 --> 00:03:39,157
...

