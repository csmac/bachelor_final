1
00:00:36,700 --> 00:00:44,900
English Subitles

2
00:01:11,600 --> 00:01:14,700
Our planet is a blue planet,

3
00:01:17,300 --> 00:01:23,000
deep blue water dwarfing
the patches of land we call earth.

4
00:01:27,500 --> 00:01:31,600
Deep Blue - the source of life itself.

5
00:04:35,700 --> 00:04:42,100
Here, half a million albatross jostle for space to breed
on a tiny island in the Southern Ocean.

6
00:06:22,700 --> 00:06:29,100
After thousand of miles of searching,
at last the deep blue offers it's bounty.

7
00:06:52,300 --> 00:06:55,300
The Ocean sustains far more life
than all of the land,

8
00:06:56,600 --> 00:07:01,300
but it is unevenly spread
hiding and shifting in blue space.

9
00:07:22,300 --> 00:07:28,400
Gathering seabirds betray a discovery.
The fish shoals have broken their cover.

10
00:10:50,500 --> 00:10:52,500
This is a world of constant jeopardy,

11
00:10:53,200 --> 00:10:56,400
an endless cycle of birth death and renewal.

12
00:11:21,700 --> 00:11:25,300
For a new generation of sea lions,
life begins at the coast.

13
00:11:26,000 --> 00:11:30,000
A place of safety to which
their mothers return only to give birth.

14
00:11:53,600 --> 00:11:55,900
The unfamiliar land becomes
a gentle nursery,

15
00:11:56,700 --> 00:12:02,100
a place where the young pups gather strength
as they grow in readyness for a harsher life at sea.

16
00:15:22,800 --> 00:15:26,600
Killer whales have only a few days
to take advantage of the naive young pups.

17
00:15:27,300 --> 00:15:30,500
Soon the sea lions will learn to avoid
the shallows of high tide.

18
00:15:33,900 --> 00:15:38,000
Meanwhile, the whales risk being stranded
on the shore as they take their prey.

19
00:15:54,200 --> 00:15:58,200
It's a dangerous business and the young
killers must be taught to hunt in open water

20
00:15:58,800 --> 00:16:00,600
before they can risk the shore.

21
00:17:34,900 --> 00:17:38,600
At the coastline, the earth and sea meet
to form a borderland,

22
00:17:39,800 --> 00:17:42,700
a world of tension
where moods shift with the tides.

23
00:20:20,400 --> 00:20:27,400
Away from the coastline in the bright warm water,
a tiny portion of the ocean holds its greatest riches.

24
00:20:29,800 --> 00:20:37,900
The coral reefs, a narrow band of sun fed life
only found in the shallows of tropical seas.

25
00:21:22,600 --> 00:21:26,700
This is a fragile world
built upon the smallest of organisms,

26
00:21:27,500 --> 00:21:34,400
the coral polyps whose skeletons form the reef,
colonies that may strech for hundred of miles.

27
00:23:16,100 --> 00:23:19,600
A gentle whale shark,
the largest fish in the sea,

28
00:23:20,600 --> 00:23:26,200
visits the reef during the green of a plankton bloom,
sifting the water through its gills.

29
00:24:13,600 --> 00:24:16,100
The moon has a powerful force upon the deep.

30
00:24:17,100 --> 00:24:21,800
Her ghostly realm is a different world,
when new rules apply.

31
00:25:07,400 --> 00:25:11,600
Rooted to the spot, even the coral
must fight a wall for territoty,

32
00:25:12,600 --> 00:25:17,200
shooting out deadly stinging cells to paralyse
and then eat their neighbours alive.

33
00:25:58,600 --> 00:26:00,200
The darkness brings it's own mood,

34
00:26:00,900 --> 00:26:05,800
A change of rhythm where the creatures of
the light must find a hiding place.

35
00:26:20,900 --> 00:26:24,300
A crevice under a coral head,
will provide shelter for the night,

36
00:26:25,600 --> 00:26:27,900
but no certainty that you will see the dawn.

37
00:26:32,200 --> 00:26:34,600
For others,
the darkness is feeding time.

38
00:27:18,300 --> 00:27:20,000
The night feeders have no need of light

39
00:27:20,900 --> 00:27:24,200
Special sensors allow them to
detect the slightest movement.

40
00:27:27,900 --> 00:27:31,600
Even the smallest electrical twitch
of muscle brings them close...

41
00:27:34,300 --> 00:27:36,100
...ready to choose their victim.

42
00:31:52,800 --> 00:31:58,500
Storm and rain and wind and cloud
churn the seas with their own power.

43
00:32:20,700 --> 00:32:26,900
The violent mixing of the waters brings life awakened
and fueled by light from the sun.

44
00:33:34,200 --> 00:33:37,200
Underwater currents mix and blend
the liquid world,

45
00:33:38,000 --> 00:33:43,500
Whipping up the nourishing swarms of tiny
organisms on which so many other animals feed.

46
00:35:54,200 --> 00:35:56,500
The boneless creatures
have their place too,

47
00:35:57,100 --> 00:36:00,000
trailing their tentacles behind their pulsing forms,

48
00:36:00,900 --> 00:36:03,500
capturing their own prey as they drift.

49
00:40:41,400 --> 00:40:43,900
At the ends of the earth,
north and south,

50
00:40:44,600 --> 00:40:48,000
the frozen oceans wait
for the return of the sun,

51
00:40:48,800 --> 00:40:52,200
the force that draws life
from the still waters.

52
00:41:51,700 --> 00:41:57,000
Polar bears, no longer safe on firm ice,
are forced to take to the water.

53
00:42:48,100 --> 00:42:52,800
In the far south, Emperor penguins gather
in vast numbers at the ice edge.

54
00:42:54,100 --> 00:42:58,300
They are sleek and fast, ready to face
the harshest weather conditions on earth.

55
00:44:29,700 --> 00:44:33,600
As the Antarctic summer ends,
almost all life heads to the north,

56
00:44:34,400 --> 00:44:39,000
away from the freezing waters,
but still the Emperor penguins remain.

57
00:44:48,100 --> 00:44:50,400
They face a long and arduous journey south,

58
00:44:51,300 --> 00:44:55,300
100 Miles across the frozen
wasteland, to breed.

59
00:45:03,400 --> 00:45:08,900
For three months, the male birds will starve
enduring temperatures of -70 degrees

60
00:45:09,600 --> 00:45:12,100
and 100 mile an hour winds.

61
00:45:55,600 --> 00:45:59,000
In the arctic, winter turns liquid sea
into solid ground,

62
00:46:00,100 --> 00:46:02,600
and for the first time since
summer melted the ice,

63
00:46:03,200 --> 00:46:06,000
the polar bears can move across
the ocean with ease.

64
00:46:09,700 --> 00:46:14,600
After months of near starvation,
the bears are on a desperate search for food,

65
00:46:15,600 --> 00:46:18,000
and a new generation must learn to hunt.

66
00:46:22,600 --> 00:46:28,000
They scour their frozen world for signs
of nesting seals hidden in ice caves.

67
00:47:34,900 --> 00:47:38,100
The freezing sea brings danger
as well as opportunity.

68
00:47:39,000 --> 00:47:43,000
Locked under a sheet of winter ice,
these Beluga whales are trapped.

69
00:47:44,500 --> 00:47:48,800
It will be many weeks before the ice opens up
and they can swim to clear water.

70
00:47:57,800 --> 00:48:01,600
For now, they are forced to share
this tiny breathing space,

71
00:48:02,300 --> 00:48:05,900
surfacing to snatch air and
battling to keep the ice away.

72
00:49:49,700 --> 00:49:53,100
This time the bear fails,
but the Belugas will keep

73
00:50:19,600 --> 00:50:23,800
the scars of the hunters claws on their
arching, streamlined backs.

74
00:50:31,900 --> 00:50:37,100
Those that survive their icy prison will eventually
return to the freedom of the open seas.

75
00:51:30,600 --> 00:51:33,400
The warming sun once again
chases away the ice,

76
00:51:34,400 --> 00:51:39,500
and gray whales travel 6000 miles
to gorge in the polar seas.

77
00:51:58,200 --> 00:52:01,200
This three month old baby is
new to the open sea,

78
00:52:02,100 --> 00:52:06,300
and the mother grey whale must swim slowly
as she guides it to the feeding grounds.

79
00:52:44,700 --> 00:52:47,400
Wary of the 30 ton mother,
who is twice their size,

80
00:52:48,200 --> 00:52:50,500
the hunters target the vulnerable calf.

81
00:53:57,200 --> 00:54:01,500
After hours of harrying by the killers,
the baby whale is exhausted.

82
00:54:05,500 --> 00:54:07,700
Now the hunters try to push
him away from his mother.

83
00:54:19,400 --> 00:54:20,800
They try to drown him.

84
00:56:03,300 --> 00:56:04,500
The calf is dead.

85
00:56:07,500 --> 00:56:12,100
Six hours after the chase began,
the killers finally have their prize.

86
00:56:21,300 --> 00:56:25,500
The mother, bereft of the baby she carried
in her womb for 30 months,

87
00:56:26,500 --> 00:56:28,700
must continue her journey alone.

88
00:56:51,300 --> 00:56:55,900
The killers have taken only the tongue
and lower jaw of the baby whale.

89
00:57:33,100 --> 00:57:36,500
Away from all land,
the open ocean,

90
00:57:37,500 --> 00:57:42,400
an endless expanse of blue,
a vast watery desert.

91
00:58:52,700 --> 00:59:00,200
Here, wandering giants travel thousands of miles
on the oceanic currents in search of food.

92
00:59:53,500 --> 00:59:56,500
A lonely traveller in liquid space.

93
01:00:29,800 --> 01:00:32,400
In just a few secret places in the blue,

94
01:00:33,500 --> 01:00:38,100
normally solitary wanderers come together
in great gatherings.

95
01:05:37,000 --> 01:05:41,900
Amid the fury, a 30 ton
giant rises from the blue,

96
01:06:59,700 --> 01:07:03,100
leaves the narrow strip of rich sunlit waters
of the ocean surface,

97
01:07:04,100 --> 01:07:07,000
and journeys downwards into perpetual night.

98
01:07:27,100 --> 01:07:31,600
With each sinking meter pressure builds,
the temperature falls,

99
01:07:31,800 --> 01:07:35,300
all light from the surface fades.

100
01:07:40,900 --> 01:07:44,600
More men have been into space
than have visited the depest riches

101
01:07:44,200 --> 01:07:45,900
of this alien world.

102
01:10:57,900 --> 01:11:02,000
Go deeper and life under pressure adapts, changes,

103
01:11:03,300 --> 01:11:06,300
ready to play a deadly game
of hide and seek.

104
01:12:01,300 --> 01:12:05,900
Down here, the only light comes from
the inhabitants of the deep themselves.

105
01:12:07,000 --> 01:12:09,600
Some use it as a lure to attract prey.

106
01:12:24,900 --> 01:12:31,000
For others the light is a decoy,
flashes which dazzle and confuse their hunters.

107
01:13:45,200 --> 01:13:48,600
The deep ocean floor is by far
the largest habitat on earth,

108
01:13:49,300 --> 01:13:51,800
covering more than a half of all the sea bed.

109
01:13:52,500 --> 01:13:56,500
Here, the pressure is 500 times
greater than at the surface.

110
01:14:06,200 --> 01:14:09,900
It is mostly flat, but here and there
it is scarred by massive wounds.

111
01:14:13,900 --> 01:14:16,600
The oceans' deepest reach,
the Marianas trench,

112
01:14:17,200 --> 01:14:20,100
plummets seven miles down
towards the earths core.

113
01:14:45,200 --> 01:14:49,100
The mid-ocean ridges are the largest
geological structures on earth.

114
01:14:57,100 --> 01:14:59,900
They stretch 30,000 miles.

115
01:16:10,600 --> 01:16:16,400
Water, hot as molted lead, gushes from chimneys
as high as 16 storey buildings.

116
01:16:25,400 --> 01:16:26,800
The Black Smokers...

117
01:16:27,700 --> 01:16:34,200
...highly poisonous hydrogen sulphide billows
into the water creating biological hell.

118
01:16:52,200 --> 01:16:57,200
And yet, even here, there is life
in enormous quantities.

119
01:17:07,800 --> 01:17:11,900
These chimneys are some of the most
densely inhabited spots on earth,

120
01:17:13,400 --> 01:17:17,100
pillars of life where animals exist
without energy from the sun.

121
01:18:22,400 --> 01:18:26,900
Every single night, life without number
emerges from the deep.

122
01:18:27,900 --> 01:18:30,100
They come to feed in the rich surface water,

123
01:18:31,000 --> 01:18:35,200
A daily ritual which is the greatest
biological migration on the planet.

124
01:20:45,300 --> 01:20:50,300
We have sought out the stars and our moon
without getting to know the deep.

125
01:20:51,800 --> 01:20:54,900
Yet it is the ocean
that continues to surprise us.

126
01:24:24,800 --> 01:24:28,300
So far, we have explored only
a tiny fraction of the deep,

127
01:24:29,400 --> 01:24:32,100
home to the largest creature
that has ever existed.

128
01:24:50,100 --> 01:24:53,500
There were once 300,000 blue whales
in our oceans,

129
01:24:57,500 --> 01:25:01,600
Now just 1% of these glorious animals remain,

130
01:25:06,800 --> 01:25:09,900
and yet we continue to plunder their secret world.

