1
00:02:50,668 --> 00:02:55,172
<i>Sometimes, late at night,</i>

2
00:02:55,174 --> 00:02:59,075
<i>you can still feel the loneliness...</i>

3
00:03:00,244 --> 00:03:04,781
<i>Like an animal in the dark,</i>

4
00:03:04,783 --> 00:03:07,984
<i>ready to pounce.</i>

5
00:03:07,986 --> 00:03:11,688
<i>It can make a warm memory</i>

6
00:03:11,690 --> 00:03:16,660
<i>seem empty and unfamiliar.</i>

7
00:03:17,228 --> 00:03:22,832
<i>Past things seem insignificant.</i>

8
00:03:22,834 --> 00:03:28,939
<i>And even faith can seem trivial</i>

9
00:03:28,941 --> 00:03:32,209
<i>and worthless.</i>

10
00:03:36,981 --> 00:03:39,950
<i>My dear Leon.</i>

11
00:03:39,952 --> 00:03:42,886
<i>I remember</i>
<i>the day you were born.</i>

12
00:03:42,888 --> 00:03:47,657
<i>You were my little angel.</i>

13
00:03:48,793 --> 00:03:51,861
<i>Always so full of joy.</i>

14
00:03:53,130 --> 00:03:57,634
<i>So full of life.</i>

15
00:04:03,741 --> 00:04:08,878
<i>And then, one day...</i>

16
00:04:10,782 --> 00:04:12,916
<i>Everything changed.</i>

17
00:04:12,918 --> 00:04:16,720
Our father who art in heaven,

18
00:04:16,722 --> 00:04:18,088
hallowed be thy name.

19
00:04:18,090 --> 00:04:19,823
<i>More than anyone,</i>

20
00:04:19,825 --> 00:04:23,960
<i>you deserved an explanation.</i>

21
00:04:25,162 --> 00:04:26,730
<i>You needed to know</i>

22
00:04:26,732 --> 00:04:31,101
<i>why your father did what he did.</i>

23
00:04:31,902 --> 00:04:36,006
<i>But that is God's secret.</i>

24
00:04:37,174 --> 00:04:42,646
<i>You were too young then,</i>
<i>too angry to accept it.</i>

25
00:04:42,648 --> 00:04:48,385
<i>Too stubborn to see</i>
<i>that the soul lives on</i>

26
00:04:48,387 --> 00:04:50,820
<i>long after death.</i>

27
00:04:50,822 --> 00:04:55,925
<i>You closed yourself away,</i>

28
00:04:55,927 --> 00:04:57,761
<i>and as you grew,</i>

29
00:04:57,763 --> 00:05:01,064
<i>i prayed that</i>
<i>your faith would return.</i>

30
00:05:01,066 --> 00:05:06,036
<i>But you only became</i>
<i>more distant.</i>

31
00:05:08,873 --> 00:05:12,809
<i>And sometimes, late at night,</i>

32
00:05:12,811 --> 00:05:15,345
<i>you still feel the loneliness,</i>

33
00:05:15,347 --> 00:05:22,719
<i>and you've learned that pain</i>
<i>doesn't give up that easily.</i>

34
00:05:23,254 --> 00:05:27,357
<i>That despair is an affliction</i>
<i>of the godless.</i>

35
00:05:30,195 --> 00:05:31,895
<i>My dear Leon,</i>

36
00:05:31,897 --> 00:05:34,030
<i>you are in a terrible danger</i>

37
00:05:34,032 --> 00:05:39,235
<i>unless you can recognize</i>
<i>that your soul lives on</i>

38
00:05:39,237 --> 00:05:41,037
<i>long after death.</i>

39
00:05:41,039 --> 00:05:45,308
<i>It is the only thing</i>
<i>that can save you...</i>

40
00:05:47,378 --> 00:05:51,715
<i>And bring us together again.</i>

41
00:07:35,352 --> 00:07:37,854
<i>Hi. This is Bill Phillips</i>
<i>with Tribal trade.</i>

42
00:07:37,856 --> 00:07:40,356
<i>I'm not available at the moment.</i>

43
00:07:40,358 --> 00:07:41,491
<i>Please leave a message,</i>

44
00:07:41,493 --> 00:07:44,027
<i>and I'll return your call</i>
<i>within 24 hours.</i>

45
00:07:44,029 --> 00:07:47,464
<i>Thank you for</i>
<i>using voicemail.</i>

46
00:07:47,466 --> 00:07:51,000
Bill, it's Leon. Give me a call.

47
00:08:54,031 --> 00:08:58,301
<i>I wish we could talk</i>
<i>like we used to.</i>

48
00:09:01,972 --> 00:09:06,643
<i>But once you left,</i>
<i>you never came back.</i>

49
00:09:07,311 --> 00:09:12,682
<i>It's been so long</i>
<i>since you even called.</i>

50
00:09:16,654 --> 00:09:20,189
<i>It's so hard being alone.</i>

51
00:09:21,058 --> 00:09:23,626
<i>There are always</i>
<i>strangers at the door,</i>

52
00:09:23,628 --> 00:09:27,964
<i>and they always</i>
<i>want something.</i>

53
00:10:37,167 --> 00:10:40,637
<i>This house</i>
<i>is a holy place.</i>

54
00:10:44,575 --> 00:10:49,212
<i>And even though you never</i>
<i>visited me here...</i>

55
00:10:50,648 --> 00:10:55,718
<i>I have left behind</i>
<i>a few reminders for you.</i>

56
00:11:00,791 --> 00:11:03,126
<i>I leave everything I have</i>

57
00:11:03,128 --> 00:11:07,397
<i>to the only person in my life.</i>

58
00:11:10,735 --> 00:11:13,436
<i>You.</i>

59
00:15:32,396 --> 00:15:34,563
I spent the entire week
preparing for this,

60
00:15:34,565 --> 00:15:36,832
and I walk in,
and it's all right here.

61
00:15:36,834 --> 00:15:38,901
<i>I don't know what to tell you, Leon.</i>

62
00:15:38,903 --> 00:15:40,970
<i>I had strict orders</i>
<i>to buy everything you sold.</i>

63
00:15:40,972 --> 00:15:44,640
And you didn't think
that I'd find that interesting?

64
00:15:44,642 --> 00:15:46,075
<i>Uh, had no idea.</i>

65
00:15:46,077 --> 00:15:47,944
You don't think
it's a little weird

66
00:15:47,946 --> 00:15:50,313
that somebody is only
interested in items

67
00:15:50,315 --> 00:15:51,547
from my personal
collection?

68
00:15:51,549 --> 00:15:53,449
<i>I'm a broker,</i>
<i>you know the drill.</i>

69
00:15:53,451 --> 00:15:56,519
<i>Confidentiality</i>
<i>is always assured.</i>

70
00:15:56,521 --> 00:15:58,955
<i>Listen to me. I swear,</i>

71
00:15:58,957 --> 00:16:01,023
<i>I had no idea</i>
<i>it was your mother</i>

72
00:16:01,025 --> 00:16:03,893
<i>who was buying up</i>
<i>all your items.</i>

73
00:16:03,895 --> 00:16:06,929
<i>You gotta believe me, Leon.</i>

74
00:16:06,931 --> 00:16:09,465
<i>Hello?</i>

75
00:16:09,467 --> 00:16:12,001
<i>You there?</i>

76
00:16:12,003 --> 00:16:13,002
Yeah.

77
00:16:13,004 --> 00:16:15,738
<i>I got enough on my mind</i>
<i>just getting the best price,</i>

78
00:16:15,740 --> 00:16:18,441
<i>never mind keeping tabs</i>
<i>on who's buying.</i>

79
00:16:18,443 --> 00:16:20,543
- <i>You know what I'm saying?</i>
- Yeah.

80
00:16:20,545 --> 00:16:23,679
<i>When was the last time</i>
<i>you saw her anyway?</i>

81
00:16:23,681 --> 00:16:26,949
It's been a while.

82
00:16:26,951 --> 00:16:30,720
<i>I'm sorry. I know</i>
<i>it's a tough time.</i>

83
00:16:30,722 --> 00:16:32,788
<i>Listen, if you wanna put</i>
<i>any of that stuff</i>

84
00:16:32,790 --> 00:16:34,623
<i>back on the market,</i>
<i>just let me know, okay?</i>

85
00:16:34,625 --> 00:16:37,893
<i>I can move it for you all over</i>
<i>again, double your take.</i>

86
00:16:37,895 --> 00:16:39,595
Okay. Yeah.

87
00:16:39,597 --> 00:16:41,597
Um... thanks, bill.

88
00:16:41,599 --> 00:16:42,965
<i>Yeah. Don't mention it.</i>

89
00:16:42,967 --> 00:16:45,735
<i>Call me if you need</i>
<i>anything, okay?</i>

90
00:16:45,737 --> 00:16:47,603
Yeah.

91
00:16:49,506 --> 00:16:51,640
Bye.

92
00:19:43,246 --> 00:19:46,215
Come back here.

93
00:21:11,868 --> 00:21:14,903
<i>Believe.</i>

94
00:22:41,291 --> 00:22:42,391
<i>... angry God!</i>

95
00:22:42,393 --> 00:22:44,960
<i>Ours is a fearful God!</i>

96
00:22:44,962 --> 00:22:47,463
<i>We have the signs.</i>

97
00:22:48,498 --> 00:22:51,400
<i>We have the word...</i>

98
00:22:51,402 --> 00:22:53,369
<i>Of his ministers</i>
<i>on the earth,</i>

99
00:22:53,371 --> 00:22:56,038
<i>and by our faith, we see</i>

100
00:22:56,040 --> 00:22:58,874
<i>what the unbelievers cannot.</i>

101
00:23:02,879 --> 00:23:04,780
<i>...hosts of angels.</i>

102
00:23:04,782 --> 00:23:08,851
<i>The world is full</i>
<i>of atheists,</i>

103
00:23:08,853 --> 00:23:11,520
<i>and sinners of the mind,</i>

104
00:23:11,522 --> 00:23:14,957
<i>but we are</i>
<i>people of the faith...</i>

105
00:24:49,986 --> 00:24:51,453
Hi.

106
00:24:51,455 --> 00:24:53,489
Hello. I'm from next door.

107
00:24:53,491 --> 00:24:56,959
I was a good friend
of your mother.

108
00:24:56,961 --> 00:24:59,261
Oh, hi, I'm Leon.

109
00:24:59,263 --> 00:25:01,129
I know who you are.

110
00:25:01,131 --> 00:25:03,332
She spoke about you often.

111
00:25:03,334 --> 00:25:05,434
We're very saddened
by what happened.

112
00:25:05,436 --> 00:25:09,338
We mourned her long,
and miss her deeply.

113
00:25:09,340 --> 00:25:11,573
Thanks. I appreciate that.

114
00:25:11,575 --> 00:25:13,208
May I come in?

115
00:25:13,210 --> 00:25:16,011
Now's not really
a great time, but...

116
00:25:16,013 --> 00:25:18,480
Perhaps later then?

117
00:25:18,482 --> 00:25:20,449
Yeah.

118
00:25:21,117 --> 00:25:24,520
We are a close
community here.

119
00:25:24,522 --> 00:25:27,122
We've had many
special moments

120
00:25:27,124 --> 00:25:30,158
in this old house.

121
00:25:30,160 --> 00:25:32,661
It is
truly blessed.

122
00:25:32,663 --> 00:25:36,298
Do you believe
in angels, Leon?

123
00:25:36,300 --> 00:25:39,568
Your mother
believed in them deeply,

124
00:25:39,570 --> 00:25:40,969
as do we.

125
00:25:40,971 --> 00:25:45,173
Perhaps you will join us
once you're settled in.

126
00:25:45,175 --> 00:25:49,311
Yeah, sure.
Thanks for stopping by.

127
00:25:49,313 --> 00:25:51,346
One more thing.

128
00:25:51,348 --> 00:25:55,350
There's an animal that's
come out of the woods.

129
00:25:55,352 --> 00:25:57,553
It's probably sick,

130
00:25:57,555 --> 00:26:00,389
so it might be dangerous.

131
00:26:00,391 --> 00:26:03,158
You should be careful.

132
00:26:03,160 --> 00:26:04,960
Okay, I will.

133
00:26:04,962 --> 00:26:07,462
The woods belong to God,

134
00:26:07,464 --> 00:26:09,031
but every so often,

135
00:26:09,033 --> 00:26:13,135
something bad
comes out of them.

136
00:26:13,137 --> 00:26:15,137
Okay, I'll keep that in mind.

137
00:26:15,139 --> 00:26:19,341
Bye, Leon. God bless you.

138
00:26:19,343 --> 00:26:21,443
Yeah.

139
00:28:49,325 --> 00:28:50,358
Anna.

140
00:28:50,360 --> 00:28:51,493
<i>Leon, what's up?</i>

141
00:28:51,495 --> 00:28:54,362
Hey. Uh, not much.
How are you?

142
00:28:54,364 --> 00:28:57,365
<i>Well, I'm busy, as usual.</i>

143
00:28:57,367 --> 00:29:00,435
Yeah. Uh...

144
00:29:00,437 --> 00:29:02,471
<i>You called.</i>

145
00:29:02,473 --> 00:29:05,707
Yeah. I, uh...

146
00:29:07,110 --> 00:29:10,579
I... I can't remember why I called.

147
00:29:10,581 --> 00:29:11,446
Um...

148
00:29:11,448 --> 00:29:13,882
<i>Look, I really don't</i>
<i>have time for this.</i>

149
00:29:13,884 --> 00:29:17,385
Okay, no, no, no.
It's just, um...

150
00:29:18,488 --> 00:29:21,323
I'm at
my mom's place now.

151
00:29:21,325 --> 00:29:23,492
<i>Uh-huh.</i>

152
00:29:23,494 --> 00:29:25,594
Yeah, it's...

153
00:29:25,596 --> 00:29:27,295
I had no idea.

154
00:29:27,297 --> 00:29:29,297
Um...

155
00:29:29,299 --> 00:29:33,568
Something happened that
I can't really explain.

156
00:29:33,570 --> 00:29:36,404
<i>What are you talking about?</i>

157
00:29:36,406 --> 00:29:37,606
Um...

158
00:29:37,608 --> 00:29:42,144
You know, I was
just around and settling in,

159
00:29:42,146 --> 00:29:44,613
and, uh, I left
the room for a second,

160
00:29:44,615 --> 00:29:46,715
I came back,
and there's this statue

161
00:29:46,717 --> 00:29:48,550
on the TV when
I didn't put it there.

162
00:29:48,552 --> 00:29:51,920
In fact, I distinctly remember
seeing it in the basement.

163
00:29:51,922 --> 00:29:55,557
<i>- And you're sure?</i>
- I'm positive.

164
00:29:57,927 --> 00:30:00,295
<i>How are you feeling?</i>

165
00:30:00,297 --> 00:30:02,264
How am I feeling?

166
00:30:02,266 --> 00:30:04,266
Uh...

167
00:30:04,268 --> 00:30:07,602
I wanna know why
that statue's on the TV.

168
00:30:07,604 --> 00:30:10,806
<i>Tunnel reality.</i>

169
00:30:10,808 --> 00:30:12,574
Tu... what?

170
00:30:12,576 --> 00:30:13,441
<i>I'm surmising,</i>

171
00:30:13,443 --> 00:30:16,678
<i>but the statue has</i>
<i>connotations for you.</i>

172
00:30:16,680 --> 00:30:18,947
<i>Probably not</i>
<i>positive ones.</i>

173
00:30:18,949 --> 00:30:20,816
<i>Is that true?</i>

174
00:30:20,818 --> 00:30:22,384
- Yeah.
- <i>Yeah.</i>

175
00:30:22,386 --> 00:30:24,252
<i>So look, you didn't</i>
<i>see the statue,</i>

176
00:30:24,254 --> 00:30:26,588
<i>because you didn't</i>
<i>wanna see it.</i>

177
00:30:26,590 --> 00:30:28,256
<i>It's probably because</i>
<i>you didn't feel</i>

178
00:30:28,258 --> 00:30:30,325
<i>emotionally ready</i>
<i>to see it, and yet,</i>

179
00:30:30,327 --> 00:30:32,694
<i>you clearly have a need</i>
<i>to come to terms with it</i>

180
00:30:32,696 --> 00:30:34,963
<i>since you were the one</i>
<i>that brought it up the stairs,</i>

181
00:30:34,965 --> 00:30:38,567
<i>and put it in plain view</i>
<i>so that you would notice it.</i>

182
00:30:38,569 --> 00:30:43,538
<i>It's the statue your mother</i>
<i>gave you, isn't it?</i>

183
00:30:43,540 --> 00:30:46,274
Yeah, yeah.

184
00:30:46,276 --> 00:30:48,910
<i>Same one she used</i>
<i>to punish you?</i>

185
00:30:48,912 --> 00:30:51,246
It wasn't...

186
00:30:51,248 --> 00:30:52,881
It was more of a test.

187
00:30:52,883 --> 00:30:54,950
<i>No, you don't test</i>
<i>a nine-year-old boy</i>

188
00:30:54,952 --> 00:30:57,319
<i>by lighting candles</i>
<i>and asking him</i>

189
00:30:57,321 --> 00:30:59,387
<i>if he believes in God.</i>

190
00:30:59,389 --> 00:31:01,423
Could've said yes.

191
00:31:01,425 --> 00:31:02,691
<i>Yes, but you said no.</i>

192
00:31:02,693 --> 00:31:05,961
<i>And then, she'd start to</i>
<i>blow out all the candles,</i>

193
00:31:05,963 --> 00:31:08,897
<i>and what would she say</i>
<i>was gonna happen</i>

194
00:31:08,899 --> 00:31:13,235
<i>when all the candles</i>
<i>were blown out?</i>

195
00:31:15,638 --> 00:31:17,672
<i>Leon.</i>

196
00:31:17,674 --> 00:31:19,274
What?

197
00:31:19,276 --> 00:31:21,977
<i>I want to hear you</i>
<i>say it.</i>

198
00:31:21,979 --> 00:31:25,881
Said the angel would
turn its back and...

199
00:31:25,883 --> 00:31:28,250
Stop protecting me.

200
00:31:28,252 --> 00:31:31,853
<i>And that's why</i>
<i>i never liked your mother.</i>

201
00:31:36,692 --> 00:31:39,594
Well, you cleared
it up for me.

202
00:31:39,596 --> 00:31:42,797
<i>Well, it's far from</i>
<i>clear to me.</i>

203
00:31:42,799 --> 00:31:46,034
<i>Okay. I think you need</i>
<i>to give it some more thought,</i>

204
00:31:46,036 --> 00:31:49,804
<i>and allow the feelings</i>
<i>associated with the statue</i>

205
00:31:49,806 --> 00:31:51,940
<i>to express themselves.</i>

206
00:31:51,942 --> 00:31:53,742
Yeah, yeah.

207
00:31:53,744 --> 00:31:55,410
<i>You need to.</i>

208
00:31:55,412 --> 00:31:57,345
<i>So is that all?</i>

209
00:31:57,347 --> 00:32:00,582
You still talk
like a doctor.

210
00:32:00,584 --> 00:32:03,051
<i>I am a doctor.</i>

211
00:32:03,753 --> 00:32:06,454
<i>Good night, Leon.</i>

212
00:32:06,456 --> 00:32:08,690
Night, doctor.

213
00:34:03,072 --> 00:34:07,675
<i>There was a time</i>
<i>when I was everything to you.</i>

214
00:34:07,677 --> 00:34:09,911
<i>A time when</i>
<i>you loved me</i>

215
00:34:09,913 --> 00:34:12,947
<i>as much as</i>
<i>i loved you.</i>

216
00:34:13,916 --> 00:34:16,451
<i>A time when you sought</i>
<i>my comfort</i>

217
00:34:16,453 --> 00:34:19,087
<i>whenever you were sad,</i>

218
00:34:19,089 --> 00:34:21,656
<i>or afraid.</i>

219
00:34:23,159 --> 00:34:27,529
<i>But you never liked</i>
<i>the game of candles.</i>

220
00:34:29,832 --> 00:34:32,867
<i>You never could</i>
<i>forget it.</i>

221
00:34:34,503 --> 00:34:39,574
<i>And a desperate thought</i>
<i>entered your head.</i>

222
00:34:40,776 --> 00:34:44,579
<i>That's when you</i>
<i>started to hate me,</i>

223
00:34:44,581 --> 00:34:46,448
<i>your own mother.</i>

224
00:34:46,450 --> 00:34:51,019
<i>You left me</i>
<i>all alone.</i>

225
00:34:51,021 --> 00:34:54,189
<i>The funeral was empty</i>
<i>without you.</i>

226
00:36:29,319 --> 00:36:31,753
<i>When communicating</i>
<i>with the dead,</i>

227
00:36:31,755 --> 00:36:36,024
<i>it is not necessary to resort</i>
<i>to elaborate techniques</i>

228
00:36:36,026 --> 00:36:39,727
<i>or obscure</i>
<i>occult rituals.</i>

229
00:36:40,663 --> 00:36:45,033
<i>Spirits communicate through</i>
<i>the most refined apparatus</i>

230
00:36:45,035 --> 00:36:47,936
<i>known to us--</i>

231
00:36:47,938 --> 00:36:49,904
<i>our senses.</i>

232
00:36:49,906 --> 00:36:52,774
<i>It is merely a matter of</i>
<i>tuning ourselves</i>

233
00:36:52,776 --> 00:36:56,077
<i>to the right</i>
<i>frequency</i>

234
00:36:56,079 --> 00:36:59,581
<i>to achieve discourse</i>
<i>with the dead.</i>

235
00:36:59,583 --> 00:37:00,949
<i>All that is required</i>

236
00:37:00,951 --> 00:37:04,352
<i>is a quiet place</i>
<i>close to the deceased,</i>

237
00:37:04,354 --> 00:37:07,655
<i>focused concentration,</i>

238
00:37:07,657 --> 00:37:11,259
<i>and a bit</i>
<i>of patience.</i>

239
00:37:11,261 --> 00:37:16,998
<i>Find a comfortable place</i>
<i>and a relaxing position.</i>

240
00:37:17,666 --> 00:37:20,101
<i>Close your eyes.</i>

241
00:37:20,103 --> 00:37:24,772
<i>Empty yourself</i>
<i>of all your thoughts.</i>

242
00:37:24,774 --> 00:37:28,009
<i>All of your feelings.</i>

243
00:37:28,011 --> 00:37:31,980
<i>All of your worries.</i>

244
00:37:31,982 --> 00:37:34,916
<i>Empty yourself now.</i>

245
00:37:34,918 --> 00:37:39,988
<i>All that is left</i>
<i>is yourself.</i>

246
00:37:39,990 --> 00:37:42,357
<i>The self you know.</i>

247
00:37:42,359 --> 00:37:46,127
<i>The self that is</i>
<i>pure and safe.</i>

248
00:37:46,129 --> 00:37:50,865
<i>You are exactly where</i>
<i>you want to be.</i>

249
00:38:06,382 --> 00:38:08,850
<i>You are aware</i>
<i>of your self</i>

250
00:38:08,852 --> 00:38:13,087
<i>in a way you have</i>
<i>never been before.</i>

251
00:38:22,231 --> 00:38:25,767
<i>Spread out into it.</i>

252
00:38:36,679 --> 00:38:41,849
<i>Feel the warmth</i>
<i>of your living self.</i>

253
00:38:52,227 --> 00:38:54,262
<i>Feel it.</i>

254
00:38:59,435 --> 00:39:03,805
<i>Someone else is here.</i>

255
00:39:08,043 --> 00:39:09,177
Leon.

256
00:39:09,179 --> 00:39:14,082
<i>You know who it is.</i>

257
00:39:20,356 --> 00:39:23,091
<i>Feel them.</i>

258
00:39:29,932 --> 00:39:33,334
<i>Feel their presence.</i>

259
00:39:46,482 --> 00:39:49,484
<i>Do not rush it.</i>

260
00:40:20,983 --> 00:40:25,286
<i>Think of what</i>
<i>you would like to say.</i>

261
00:40:34,430 --> 00:40:39,500
<i>Think of what you would</i>
<i>like to say to that person,</i>

262
00:40:39,502 --> 00:40:42,437
<i>and say it.</i>

263
00:46:32,487 --> 00:46:36,857
<i>We can see</i>
<i>what the unbelievers don't.</i>

264
00:46:39,928 --> 00:46:41,362
<i>Raise your arms.</i>

265
00:46:41,364 --> 00:46:45,833
<i>Raise your arms,</i>
<i>and ask the holy host</i>

266
00:46:45,835 --> 00:46:50,771
<i>to look upon us</i>
<i>as he has in the past,</i>

267
00:46:50,773 --> 00:46:53,941
<i>and by his gaze,</i>

268
00:46:53,943 --> 00:46:57,711
<i>strengthen our faith.</i>

269
00:46:57,713 --> 00:46:59,814
<i>Ask, children.</i>

270
00:46:59,816 --> 00:47:02,950
<i>Ask, sister.</i>
<i>Look upon her.</i>

271
00:47:02,952 --> 00:47:06,854
<i>Ask, brother.</i>
<i>Look upon him.</i>

272
00:47:06,856 --> 00:47:12,326
<i>Look upon him. Look upon her.</i>
<i>Look upon us.</i>

273
00:47:12,328 --> 00:47:14,695
<i>Raise your arms, sisters.</i>

274
00:47:14,697 --> 00:47:16,530
<i>Ask the holy host.</i>

275
00:47:16,532 --> 00:47:18,799
<i>Look upon this brother.</i>

276
00:47:18,801 --> 00:47:21,235
<i>This brother of faith.</i>

277
00:47:21,237 --> 00:47:23,437
<i>Look upon us.</i>

278
00:47:23,439 --> 00:47:25,773
<i>Look upon us!</i>

279
00:47:29,978 --> 00:47:33,747
<i>Look upon us.</i>
<i>Look upon us.</i>

280
00:47:33,749 --> 00:47:37,384
<i>Look upon us.</i>

281
00:47:37,386 --> 00:47:40,020
<i>Look upon us.</i>

282
00:48:09,651 --> 00:48:12,319
<i>Believe or suffer.</i>

283
00:48:12,321 --> 00:48:15,556
<i>This is the message</i>
<i>we've been handed.</i>

284
00:48:15,558 --> 00:48:17,524
<i>Our God is a loving God,</i>

285
00:48:17,526 --> 00:48:23,631
<i>but he is also</i>
<i>a God of wrath and anger.</i>

286
00:48:23,633 --> 00:48:27,501
<i>A God of punishment.</i>

287
00:49:37,005 --> 00:49:39,974
<i>I remember</i>
<i>the day you told me</i>

288
00:49:39,976 --> 00:49:44,044
<i>that you didn't</i>
<i>believe anymore.</i>

289
00:49:45,581 --> 00:49:49,450
<i>That people</i>
<i>don't have souls.</i>

290
00:49:49,452 --> 00:49:53,387
<i>We come from nothing</i>
<i>and go to nothing.</i>

291
00:49:53,389 --> 00:49:56,824
<i>That's what you said.</i>

292
00:49:56,826 --> 00:49:59,860
<i>How, in one second,</i>
<i>you were alive,</i>

293
00:49:59,862 --> 00:50:04,865
<i>and in the next,</i>
<i>you were gone.</i>

294
00:50:05,934 --> 00:50:08,002
<i>Just like your father.</i>

295
00:50:13,842 --> 00:50:17,978
<i>And all the time you spoke,</i>
<i>i listened patiently...</i>

296
00:50:19,949 --> 00:50:22,883
<i>And then, it was time to play</i>

297
00:50:22,885 --> 00:50:25,119
<i>the game of candles,</i>

298
00:50:25,121 --> 00:50:28,889
<i>but you got angry</i>
<i>and blew them all out.</i>

299
00:50:28,891 --> 00:50:34,128
<i>That's when you felt</i>
<i>the darkness touch you,</i>

300
00:50:34,130 --> 00:50:39,433
<i>and it felt cold</i>
<i>and empty.</i>

301
00:50:41,636 --> 00:50:45,039
<i>I re-lit it just in time.</i>

302
00:50:45,041 --> 00:50:46,907
<i>But you were scared,</i>

303
00:50:46,909 --> 00:50:50,611
<i>and you blamed me</i>
<i>for scratching you.</i>

304
00:50:50,613 --> 00:50:54,548
<i>But I was only trying</i>
<i>to protect you.</i>

305
00:50:54,550 --> 00:50:57,918
<i>Only trying to stop you</i>
<i>from making</i>

306
00:50:57,920 --> 00:51:01,855
<i>a terrible mistake.</i>

307
00:52:06,087 --> 00:52:08,655
Leon.

308
00:52:08,657 --> 00:52:10,657
Hello.

309
00:52:10,659 --> 00:52:13,994
Hello! Anybody there?

310
00:56:18,106 --> 00:56:19,773
<i>Hello, Mr. Leigh.</i>

311
00:56:19,775 --> 00:56:21,041
<i>How can I help you tonight?</i>

312
00:56:21,043 --> 00:56:24,878
I just had a disturbance
a few minutes ago.

313
00:56:24,880 --> 00:56:28,081
I think, um, someone
may have been trying

314
00:56:28,083 --> 00:56:29,316
to break in my house.

315
00:56:29,318 --> 00:56:31,485
<i>Would you like me to call</i>
<i>the police, Mr. Leigh?</i>

316
00:56:31,487 --> 00:56:33,987
Uh, no, no. It's okay now.

317
00:56:33,989 --> 00:56:35,155
<i>Okay. Very well, then.</i>

318
00:56:35,157 --> 00:56:37,758
<i>You said this happened</i>
<i>a few minutes ago.</i>

319
00:56:37,760 --> 00:56:39,059
Yeah, that's right.

320
00:56:39,061 --> 00:56:40,761
<i>If you like, we can</i>
<i>review your data online</i>

321
00:56:40,763 --> 00:56:43,530
- <i>with one of our surveillance experts.</i>
- Yeah, that'd be fine.

322
00:56:43,532 --> 00:56:47,100
- Please hold.
- Thank you.

323
00:56:55,977 --> 00:56:58,845
<i>And that was</i>
<i>"and the angel said unto them,"</i>

324
00:56:58,847 --> 00:57:01,114
<i>from Philip fournier's</i>
grey childhood.

325
00:57:01,116 --> 00:57:03,784
<i>You are listening to ascension,</i>

326
00:57:03,786 --> 00:57:05,318
<i>music for your salvation.</i>

327
00:57:05,320 --> 00:57:07,053
<i>Good evening, Mr. Leigh.</i>

328
00:57:07,055 --> 00:57:09,022
<i>I understand</i>
<i>you've had an incident</i>

329
00:57:09,024 --> 00:57:12,359
- <i>occur outside your home.</i>
- Yes.

330
00:57:12,361 --> 00:57:15,262
<i>I also understand</i>
<i>you would like to review</i>

331
00:57:15,264 --> 00:57:17,097
- <i>the data online.</i>
- Yes.

332
00:57:17,099 --> 00:57:21,868
<i>Are you currently in front</i>
<i>of a working computer?</i>

333
00:57:21,870 --> 00:57:24,104
Yes.

334
00:57:24,106 --> 00:57:29,543
<i>Please access our site</i>
<i>at Winchester surveillance now.</i>

335
00:57:34,148 --> 00:57:35,582
Okay.

336
00:57:35,584 --> 00:57:38,485
<i>Please click on the account tab.</i>

337
00:57:40,154 --> 00:57:40,987
Okay.

338
00:57:40,989 --> 00:57:43,990
<i>Can you tell me</i>
<i>your password, please?</i>

339
00:57:48,563 --> 00:57:51,965
L-e-o-n.

340
00:57:54,268 --> 00:57:56,236
<i>Thank you.</i>

341
00:57:56,238 --> 00:57:58,438
<i>You have been connected.</i>

342
00:57:58,440 --> 00:58:01,575
<i>You have a single camera</i>
<i>outside your home.</i>

343
00:58:01,577 --> 00:58:04,244
- <i>Is that correct?</i>
- Yes.

344
00:58:04,246 --> 00:58:07,314
<i>I will pull the data</i>
<i>beginning from midnight.</i>

345
00:58:07,316 --> 00:58:10,383
<i>Does that sound</i>
<i>reasonable to you?</i>

346
00:58:10,385 --> 00:58:11,284
Yes.

347
00:58:11,286 --> 00:58:15,655
<i>You should be able to see</i>
<i>our data on your screen.</i>

348
00:58:19,160 --> 00:58:22,329
<i>I'm afraid the lighting</i>
<i>is not very good.</i>

349
00:58:22,331 --> 00:58:25,899
<i>I will try to</i>
<i>enhance the image.</i>

350
00:58:25,901 --> 00:58:27,868
<i>Just a moment, please.</i>

351
00:58:29,237 --> 00:58:31,371
What is that moving?

352
00:58:31,373 --> 00:58:34,174
<i>It appears to be an animal.</i>

353
00:58:34,176 --> 00:58:37,043
What kind of animal?

354
00:58:37,045 --> 00:58:38,178
<i>I regret to inform you</i>

355
00:58:38,180 --> 00:58:41,081
<i>that our information</i>
<i>is incomplete.</i>

356
00:58:41,083 --> 00:58:42,949
<i>Just a mo...</i>

357
00:58:42,951 --> 00:58:44,317
hello?

358
00:58:44,319 --> 00:58:47,354
Hello.

359
01:00:18,446 --> 01:00:21,548
Fuck.

360
01:00:28,389 --> 01:00:30,457
All right.

361
01:00:33,594 --> 01:00:35,095
<i>How are you, Leon?</i>

362
01:00:35,097 --> 01:00:38,565
<i>Leon.</i>

363
01:00:38,567 --> 01:00:40,200
<i>Hello?</i>

364
01:00:42,070 --> 01:00:44,004
Um... there's something
happening.

365
01:00:44,006 --> 01:00:45,505
<i>Calm down.</i>
<i>What's the matter?</i>

366
01:00:45,507 --> 01:00:47,307
There's... there's
somebody in the house.

367
01:00:47,309 --> 01:00:50,010
- <i>Who? Who is it?</i>
- I don't know. I just...

368
01:00:50,012 --> 01:00:51,711
<i>Have you called</i>
<i>the police?</i>

369
01:00:51,713 --> 01:00:53,113
No, no, no. It's, um...

370
01:00:53,115 --> 01:00:55,415
It's not that kind
of somebody.

371
01:00:55,417 --> 01:00:56,683
- <i>Okay, Leon?</i>
- Yeah.

372
01:00:56,685 --> 01:00:59,285
<i>- I want you to do something for me, okay?</i>
- What?

373
01:00:59,287 --> 01:01:00,587
<i>I can tell you're upset,</i>

374
01:01:00,589 --> 01:01:02,656
<i>so I want you</i>
<i>to trust me, okay?</i>

375
01:01:02,658 --> 01:01:04,624
Yeah. Yeah.

376
01:01:04,626 --> 01:01:07,027
<i>I want you to close your eyes.</i>

377
01:01:07,029 --> 01:01:09,796
- Oh, no...
- <i>Trust me, Leon.</i>

378
01:01:09,798 --> 01:01:12,198
<i>Can you do that for me?</i>

379
01:01:13,301 --> 01:01:15,101
- Yeah.
- <i>Okay, good.</i>

380
01:01:15,103 --> 01:01:17,137
<i>Now listen to me.</i>

381
01:01:17,139 --> 01:01:19,472
<i>Leon, I want you to picture</i>

382
01:01:19,474 --> 01:01:21,241
<i>A pool of water inside of you.</i>

383
01:01:21,243 --> 01:01:22,809
<i>Can you do that for me?</i>

384
01:01:22,811 --> 01:01:24,177
Yeah.

385
01:01:24,179 --> 01:01:26,346
<i>The water is warm</i>
<i>and calm,</i>

386
01:01:26,348 --> 01:01:30,150
<i>and every second,</i>
<i>it is rising in you.</i>

387
01:01:30,152 --> 01:01:31,651
<i>Feel it rising</i>
<i>in your body,</i>

388
01:01:31,653 --> 01:01:34,187
<i>through your chest.</i>
<i>Can you feel it?</i>

389
01:01:34,189 --> 01:01:36,089
Yeah.

390
01:01:36,091 --> 01:01:39,626
<i>Okay. Can you feel it</i>
<i>spreading out from you,</i>

391
01:01:39,628 --> 01:01:44,197
<i>warming and calming</i>
<i>everything around you?</i>

392
01:01:44,199 --> 01:01:46,066
<i>Can you feel it, Leon?</i>

393
01:01:46,068 --> 01:01:47,233
Yeah.

394
01:01:47,235 --> 01:01:49,803
<i>Flowing through you, calming you,</i>

395
01:01:49,805 --> 01:01:52,305
<i>washing away</i>
<i>everything around you,</i>

396
01:01:52,307 --> 01:01:55,275
<i>until the only thing that is left</i>

397
01:01:55,277 --> 01:01:56,576
<i>is the calm.</i>

398
01:01:56,578 --> 01:01:58,178
<i>Now open your eyes.</i>

399
01:02:04,119 --> 01:02:07,620
<i>Leon, are you there?</i>

400
01:02:07,622 --> 01:02:10,590
- <i>Is everything okay?</i>
<i>-</i> Yeah.

401
01:02:10,592 --> 01:02:12,659
<i>Good. Now listen, it's late,</i>

402
01:02:12,661 --> 01:02:16,229
<i>and I really need</i>
<i>to get some rest.</i>

403
01:02:16,231 --> 01:02:17,397
Okay.

404
01:02:17,399 --> 01:02:19,632
<i>So no more calls, okay?</i>

405
01:02:19,634 --> 01:02:21,301
No.

406
01:02:21,303 --> 01:02:23,369
<i>Good night, Leon.</i>

407
01:02:23,371 --> 01:02:26,639
Yeah, good night. Thank you.

408
01:06:44,865 --> 01:06:47,367
<i>Leon.</i>

409
01:07:26,707 --> 01:07:29,175
<i>Believe.</i>

410
01:07:29,177 --> 01:07:31,778
No.

411
01:07:38,085 --> 01:07:41,587
- <i>Believe.</i>
- No!

412
01:07:49,830 --> 01:07:51,664
<i>Believe.</i>

413
01:07:52,833 --> 01:07:54,834
No.

414
01:07:56,003 --> 01:07:58,805
I don't believe it.

415
01:09:26,027 --> 01:09:28,861
<i>Tribal trade. Bill Phillips here.</i>

416
01:09:28,863 --> 01:09:30,930
Bill, it's Leon.

417
01:09:30,932 --> 01:09:34,167
<i>Leon? What time is it?</i>

418
01:09:34,169 --> 01:09:36,569
I wanna sell everything.

419
01:09:36,571 --> 01:09:38,004
I want it all gone.

420
01:09:38,006 --> 01:09:39,772
I'll send you
a full inventory

421
01:09:39,774 --> 01:09:41,741
in a couple of days.

422
01:09:41,743 --> 01:09:45,044
<i>Um... yeah. Sure.</i>

423
01:09:45,046 --> 01:09:49,248
Okay? Thanks.
Sorry for waking you up.

424
01:10:42,069 --> 01:10:44,904
<i>Don't go.</i>

425
01:10:48,909 --> 01:10:53,145
<i>There's so much</i>
<i>I want to say to you.</i>

426
01:10:53,147 --> 01:10:58,251
<i>So much I need to explain.</i>

427
01:10:59,219 --> 01:11:03,322
<i>I waited so long</i>
<i>for you to come back.</i>

428
01:11:03,324 --> 01:11:08,394
<i>I waited so long.</i>

429
01:11:10,297 --> 01:11:15,835
<i>Until I realized</i>
<i>you never would.</i>

430
01:11:16,169 --> 01:11:22,742
<i>That night, I saw</i>
<i>something through the window,</i>

431
01:11:22,744 --> 01:11:26,646
<i>and I told myself</i>

432
01:11:26,648 --> 01:11:29,348
<i>it was just a cat.</i>

433
01:11:29,350 --> 01:11:34,186
<i>I tried to occupy myself,</i>

434
01:11:34,188 --> 01:11:36,956
<i>but I was afraid.</i>

435
01:11:36,958 --> 01:11:42,128
<i>I could feel it watching me.</i>

436
01:11:42,130 --> 01:11:46,098
<i>I wanted to talk to you</i>
<i>to tell you</i>

437
01:11:46,100 --> 01:11:48,367
<i>that I'd made a mistake,</i>

438
01:11:48,369 --> 01:11:50,903
<i>that you were</i>
<i>more important to me</i>

439
01:11:50,905 --> 01:11:54,874
<i>than anything else in the world.</i>

440
01:11:54,876 --> 01:11:59,312
<i>But my thoughts</i>
<i>were confused,</i>

441
01:11:59,314 --> 01:12:02,214
<i>and I kept thinking</i>

442
01:12:02,216 --> 01:12:07,987
<i>it was too big to be a cat.</i>

443
01:12:13,393 --> 01:12:17,830
<i>That's when I realized</i>
<i>what I'd been trying to ignore</i>

444
01:12:17,832 --> 01:12:20,966
<i>for so long.</i>

445
01:12:20,968 --> 01:12:26,038
<i>That you had turned</i>
<i>your back on me.</i>

446
01:12:27,741 --> 01:12:31,110
<i>That you abandoned me.</i>

447
01:12:32,112 --> 01:12:35,748
<i>That you no longer cared.</i>

448
01:12:37,084 --> 01:12:44,890
<i>That my little angel</i>
<i>would no longer protect me.</i>

449
01:12:46,227 --> 01:12:49,762
<i>And when faith is gone...</i>

450
01:12:51,231 --> 01:12:57,203
<i>Loneliness can be</i>
<i>a monster.</i>

451
01:13:28,135 --> 01:13:32,037
<i>I thought you would</i>
<i>come to me then.</i>

452
01:13:33,407 --> 01:13:36,342
<i>But you never did.</i>

453
01:13:42,015 --> 01:13:50,289
<i>The funeral</i>
<i>was empty without you.</i>

454
01:13:57,264 --> 01:13:59,532
<i>The loneliness is worse now</i>

455
01:13:59,534 --> 01:14:02,334
<i>than it ever was.</i>

456
01:14:05,539 --> 01:14:11,243
<i>And all I can do is remember</i>

457
01:14:11,245 --> 01:14:14,146
<i>the way you were,</i>

458
01:14:14,148 --> 01:14:18,250
<i>and make believe.</i>

459
01:14:18,252 --> 01:14:24,056
<i>Make believe that</i>
<i>you came looking for me.</i>

460
01:14:24,058 --> 01:14:29,094
<i>Make believe that you felt.</i>

461
01:14:29,963 --> 01:14:34,133
<i>Make believe that you hoped.</i>

462
01:14:34,135 --> 01:14:40,239
<i>Make believe</i>
<i>that you remembered.</i>

463
01:14:40,241 --> 01:14:45,010
<i>Remembered that</i>
<i>the soul lives on</i>

464
01:14:45,012 --> 01:14:46,846
<i>long after death,</i>

465
01:14:46,848 --> 01:14:53,118
<i>eternally, and without end.</i>

466
01:14:56,156 --> 01:14:59,992
<i>Make believe that you cared.</i>

467
01:15:03,430 --> 01:15:07,399
<i>Make believe that you miss me...</i>

468
01:15:09,202 --> 01:15:13,906
<i>As much as I miss you.</i>

