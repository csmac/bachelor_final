﻿1
00:00:00,811 --> 00:00:02,563
(Episode 21) 
-Completed Episode-
www.facebook.com/IKSubs
I will

2
00:00:02,563 --> 00:00:06,203
accept our breakup.

3
00:00:06,203 --> 00:00:09,347
You don't have to come back to me.

4
00:00:09,347 --> 00:00:15,011
But be a father to Saebyeol.

5
00:00:15,011 --> 00:00:17,708
You must tell Sejin.

6
00:00:17,708 --> 00:00:19,906
Tell her about Saebyeol

7
00:00:19,907 --> 00:00:22,491
and get Sejin to accept her.

8
00:00:22,491 --> 00:00:25,603
And openly be there

9
00:00:25,603 --> 00:00:32,314
for Saebyeol as her dad.

10
00:00:32,314 --> 00:01:32,091
I came here to tell you that.

11
00:01:32,091 --> 00:01:35,161
Do I have a twin sister?

12
00:01:35,162 --> 00:01:37,154
You hid my twin sister

13
00:01:37,154 --> 00:01:44,506
somewhere out there in this world, right?

14
00:01:44,506 --> 00:01:46,817
Why are you overreacting?

15
00:01:46,817 --> 00:01:51,631
I was just joking.

16
00:01:51,632 --> 00:01:54,385
I mean, someone insisted he met a woman

17
00:01:54,385 --> 00:01:57,952
who looked exactly like me.

18
00:01:57,952 --> 00:01:59,872
Okay.

19
00:01:59,872 --> 00:02:02,192
Okay, I won't make jokes like that again.

20
00:02:02,192 --> 00:02:04,015
Okay.

21
00:02:04,016 --> 00:02:08,783
I'll see you soon.

22
00:02:08,783 --> 00:02:12,414
What is it? Your mother got angry?

23
00:02:12,414 --> 00:02:15,186
She did, and now I feel even more suspicious.

24
00:02:15,187 --> 00:02:18,875
What if there's a secret behind my birth?

25
00:02:18,875 --> 00:02:20,923
Maybe there is.

26
00:02:20,923 --> 00:02:22,235
What?

27
00:02:22,235 --> 00:02:25,578
There are lots of people with secrets like that.

28
00:02:25,578 --> 00:02:27,644
That's why so many TV dramas and movies

29
00:02:27,644 --> 00:02:30,098
use birth secrets as a plot device.

30
00:02:30,098 --> 00:02:34,338
I have a secret behind my birth, too.

31
00:02:34,338 --> 00:02:38,620
What is it?

32
00:02:38,620 --> 00:02:42,173
Oh, no...

33
00:02:42,173 --> 00:02:43,732
You should've been more careful.

34
00:02:43,732 --> 00:02:48,372
You'll cut yourself. Move.

35
00:02:48,372 --> 00:02:50,954
You never make mistakes like this, so what's wrong?

36
00:02:50,954 --> 00:02:52,386
I know.

37
00:02:52,386 --> 00:02:53,667
Would you like some tea?

38
00:02:53,667 --> 00:02:56,594
No, I'm fine.

39
00:02:56,594 --> 00:02:58,194
Dohui hasn't come home yet?

40
00:02:58,194 --> 00:03:01,601
She's on her way.

41
00:03:01,602 --> 00:03:04,074
I...

42
00:03:04,074 --> 00:03:09,706
I really don't like him.

43
00:03:09,706 --> 00:03:11,891
Mr. Park's son, I mean.

44
00:03:11,891 --> 00:03:14,018
With all the rumors about him,

45
00:03:14,018 --> 00:03:15,971
I don't want him as Dohui's husband.

46
00:03:15,971 --> 00:03:18,501
You're her mom, so you'd say that about any man.

47
00:03:18,501 --> 00:03:20,434
Leave it up to Dohui.

48
00:03:20,434 --> 00:03:22,590
I'm going to bed.

49
00:03:22,590 --> 00:03:45,096
You should, too.

50
00:03:45,096 --> 00:03:46,017
Mom.

51
00:03:46,017 --> 00:03:47,426
Did you meet up with her?

52
00:03:47,426 --> 00:03:48,617
You weren't late?

53
00:03:48,617 --> 00:03:50,527
You weren't rude?

54
00:03:50,527 --> 00:03:54,548
Why didn't you drive her home?

55
00:03:54,549 --> 00:03:56,278
You must have been dying to call me.

56
00:03:56,278 --> 00:03:57,509
I fought the urge,

57
00:03:57,509 --> 00:04:00,077
since I didn't want you to complain.

58
00:04:00,077 --> 00:04:01,308
I'll answer your questions now,

59
00:04:01,308 --> 00:04:03,084
so pay attention.

60
00:04:03,085 --> 00:04:04,526
I wasn't late. I showed up on time

61
00:04:04,526 --> 00:04:06,309
and met with her.

62
00:04:06,309 --> 00:04:08,452
I don't remember doing anything rude.

63
00:04:08,452 --> 00:04:09,974
I was going to drive her home,

64
00:04:09,974 --> 00:04:12,492
but she declined, so I didn't.

65
00:04:12,492 --> 00:04:15,789
Happy now?

66
00:04:15,789 --> 00:04:17,197
How was the girl?

67
00:04:17,197 --> 00:04:19,060
You're going to see her again?

68
00:04:19,060 --> 00:04:21,453
I was just doing it for father's sake.

69
00:04:21,454 --> 00:04:23,309
There's nothing more to it,

70
00:04:23,309 --> 00:04:25,434
so don't get your hopes up.

71
00:04:25,434 --> 00:04:26,537
Your father will be suspicious,

72
00:04:26,537 --> 00:04:29,465
so meet her a few more times, okay?

73
00:04:29,465 --> 00:04:31,215
Oh, I'm hungry.

74
00:04:31,216 --> 00:04:32,930
I'm hungry. Can you fix me something?

75
00:04:32,930 --> 00:04:35,049
You didn't even have dinner?

76
00:04:35,049 --> 00:04:38,323
You should've had a delicious meal together.

77
00:04:38,324 --> 00:04:41,433
The chicken feet didn't have much meat.

78
00:04:41,433 --> 00:04:54,097
Let me go get changed.

79
00:04:54,097 --> 00:04:56,065
At your age, you should be eating your wife's food,

80
00:04:56,065 --> 00:04:58,715
not your mom's food.

81
00:04:58,715 --> 00:05:00,969
Oh, don't say that.

82
00:05:00,969 --> 00:05:04,760
Mom's cooking is the best.

83
00:05:04,761 --> 00:05:08,046
This is getting hard for me.

84
00:05:08,046 --> 00:05:11,354
Do you know how many years your father's been sick?

85
00:05:11,354 --> 00:05:15,733
If I didn't have to worry about you, I could relax.

86
00:05:15,733 --> 00:05:18,853
I'm not a kid. Why worry about me?

87
00:05:18,853 --> 00:05:22,405
How can I not?

88
00:05:22,405 --> 00:05:24,500
You're such a softie

89
00:05:24,500 --> 00:05:26,942
that you can't even take what's rightfully yours.

90
00:05:26,942 --> 00:05:28,701
Sejin is tough,

91
00:05:28,701 --> 00:05:32,585
but her future husband seems really ambitious, too.

92
00:05:32,585 --> 00:05:34,248
That's great.

93
00:05:34,249 --> 00:05:36,569
I'm afraid of them pushing you out.

94
00:05:36,569 --> 00:05:38,145
Your big sister

95
00:05:38,145 --> 00:05:40,361
is dying to get rid of you,

96
00:05:40,361 --> 00:05:43,849
so if your father passes away, what will you do?

97
00:05:43,849 --> 00:05:44,808
Don't worry.

98
00:05:44,809 --> 00:05:47,281
I won't let you starve.

99
00:05:47,281 --> 00:05:48,529
It may not be an extravagant life,

100
00:05:48,529 --> 00:05:51,336
but I won't make you suffer. So relax.

101
00:05:51,336 --> 00:05:54,793
You think I'm afraid of starving?

102
00:05:54,793 --> 00:05:56,522
I wouldn't be acting this way

103
00:05:56,522 --> 00:05:58,952
if there was another person in the world

104
00:05:58,952 --> 00:06:00,960
who was on your side, other than me.

105
00:06:00,960 --> 00:06:03,322
Wouldn't it be great if you had powerful in-laws

106
00:06:03,322 --> 00:06:22,916
who could help you when you needed it?

107
00:06:22,916 --> 00:06:25,524
Oh, this is terrible!

108
00:06:25,524 --> 00:06:28,444
What does she do to her chicken?

109
00:06:28,444 --> 00:06:30,275
There are fried chicken places everywhere,

110
00:06:30,275 --> 00:06:36,436
so why do I only like Yang Malsuk's chicken?

111
00:06:36,437 --> 00:06:39,412
Hello?

112
00:06:39,412 --> 00:06:41,380
Well, who might this be?

113
00:06:41,380 --> 00:06:43,756
You're still alive and breathing?

114
00:06:43,756 --> 00:06:44,955
I didn't heard from you for so long,

115
00:06:44,956 --> 00:06:47,198
I thought you were dead and buried.

116
00:06:47,198 --> 00:06:50,347
What? Who?

117
00:06:50,348 --> 00:06:52,572
Why are you asking me about him?

118
00:06:52,572 --> 00:06:55,164
You didn't let me see him for the last 20 years.

119
00:06:55,164 --> 00:06:58,068
You said he'd be better off without a mom like me.

120
00:06:58,069 --> 00:06:59,726
What did you do to him

121
00:06:59,726 --> 00:07:02,366
to make him leave his dad and never call?

122
00:07:02,366 --> 00:07:08,313
Whatever! Bye!

123
00:07:08,313 --> 00:07:15,080
Oh, I'm so upset.

124
00:07:15,081 --> 00:07:16,153
If he took after me,

125
00:07:16,153 --> 00:07:18,992
he must be pretty handsome.

126
00:07:18,992 --> 00:07:22,371
Manjeong, you've had a wretched life.

127
00:07:22,371 --> 00:08:02,207
You don't even know your own kid's face.

128
00:08:02,207 --> 00:08:04,933
Did he really meet her?

129
00:08:04,934 --> 00:08:05,958
Are you sure?

130
00:08:05,958 --> 00:08:09,191
Your son never tells lies.

131
00:08:09,191 --> 00:08:10,885
And from now on,

132
00:08:10,886 --> 00:08:14,094
you can't lie, either.

133
00:08:14,094 --> 00:08:16,246
He went on a blind date,

134
00:08:16,246 --> 00:08:19,150
so you want me to change his position at work?

135
00:08:19,150 --> 00:08:24,150
Of course.

136
00:08:24,150 --> 00:08:26,157
What's with that face?

137
00:08:26,158 --> 00:08:27,958
That's your stubborn face!

138
00:08:27,958 --> 00:08:30,359
What is that?

139
00:08:30,359 --> 00:08:32,933
Not every blind date leads to marriage.

140
00:08:32,933 --> 00:08:34,246
Set the wedding date.

141
00:08:34,246 --> 00:08:37,511
And if the woman agrees to wear a wedding gown,

142
00:08:37,511 --> 00:08:41,054
Hwigyeong can move to a different position.

143
00:08:41,054 --> 00:08:41,894
Honey!

144
00:08:41,894 --> 00:08:43,549
And have Dr. Ju

145
00:08:43,549 --> 00:08:46,966
prepare to discharge me from the hospital.

146
00:08:46,966 --> 00:08:50,183
Is today the Day of Stubbornness or what?

147
00:08:50,183 --> 00:08:51,638
It's too early!

148
00:08:51,638 --> 00:08:54,662
I know my body best.

149
00:08:54,662 --> 00:09:05,935
So go and tell him.

150
00:09:05,935 --> 00:09:08,919
I can't skimp on onions for Onion Chicken,

151
00:09:08,919 --> 00:09:10,734
but onions are expensive these days,

152
00:09:10,734 --> 00:09:15,070
so I won't make any profit.

153
00:09:15,070 --> 00:09:19,231
Who put a newspaper in here?

154
00:09:19,231 --> 00:09:26,569
(Baekdo Group's chairman's granddaughter...)

155
00:09:26,569 --> 00:09:28,562
If Taejun didn't work there,

156
00:09:28,562 --> 00:09:30,441
I'd be praying every day

157
00:09:30,441 --> 00:09:32,577
for that company to go bankrupt.

158
00:09:32,577 --> 00:09:47,112
Of all places, why did he get a job at Baekdo?

159
00:09:47,112 --> 00:09:55,170
(Her fiancé is a Baekdo employee, Kang Taejun)

160
00:09:55,171 --> 00:09:57,132
Ms. Yang.

161
00:09:57,132 --> 00:10:00,738
Why are you acting like this? What have you heard?

162
00:10:00,738 --> 00:10:03,496
Nothing's been decided yet.

163
00:10:03,496 --> 00:10:06,281
Aren't all marriages the same way?

164
00:10:06,282 --> 00:10:07,760
We're not like other families,

165
00:10:07,760 --> 00:10:12,162
so we don't know until the ceremony...

166
00:10:12,162 --> 00:10:14,593
Oh Manjeong,

167
00:10:14,593 --> 00:10:19,799
you've been deceiving us up till now?

168
00:10:19,799 --> 00:10:29,667
That woman... Today I'm going to get her!

169
00:10:29,667 --> 00:10:35,995
Oh Manjeong!

170
00:10:35,995 --> 00:10:39,082
Right, if you want to stay alive,

171
00:10:39,082 --> 00:10:41,482
you can't show your face.

172
00:10:41,482 --> 00:10:43,432
You have to hide and avoid my calls

173
00:10:43,432 --> 00:10:48,737
if you want to live.

174
00:10:48,737 --> 00:10:49,986
Your call is being forwarded to...

175
00:10:49,986 --> 00:10:54,064
(Moving Company)

176
00:10:54,064 --> 00:10:58,593
Right, if I do this,

177
00:10:58,593 --> 00:11:05,104
you'll have to come and see me.

178
00:11:05,105 --> 00:11:07,576
What? What is mom doing?

179
00:11:07,576 --> 00:11:11,120
Why are we all meeting here?

180
00:11:11,120 --> 00:11:13,599
What have I done wrong recently?

181
00:11:13,599 --> 00:11:16,525
No, I haven't been out drinking lately,

182
00:11:16,525 --> 00:11:20,102
because I'm on a diet.

183
00:11:20,102 --> 00:11:23,198
Did you two get into trouble?

184
00:11:23,198 --> 00:11:27,717
Are you stupid? Must you ask?

185
00:11:27,717 --> 00:11:29,549
Oh, no...

186
00:11:29,549 --> 00:11:31,478
Mom's seen the article, right?

187
00:11:31,478 --> 00:11:35,709
The article about Taejun? Right?

188
00:11:35,710 --> 00:11:39,221
Did you think you could keep it from me forever?

189
00:11:39,221 --> 00:11:41,029
Mom...

190
00:11:41,029 --> 00:11:42,542
We didn't.

191
00:11:42,542 --> 00:11:46,389
We didn't keep anything from you.

192
00:11:46,389 --> 00:11:50,054
Nayeon, tell me the truth.

193
00:11:50,054 --> 00:11:52,270
If you don't want me to go crazy,

194
00:11:52,270 --> 00:11:54,314
tell me the truth.

195
00:11:54,314 --> 00:11:56,250
Why is Taejun

196
00:11:56,250 --> 00:11:58,898
marrying that Baekdo woman?

197
00:11:58,898 --> 00:12:02,545
His wife and child are right here!

198
00:12:02,545 --> 00:12:05,866
So why is he marrying another woman? Why?

199
00:12:05,866 --> 00:12:07,059
Mom.

200
00:12:07,059 --> 00:12:09,044
It's not like that. It's a misunderstanding.

201
00:12:09,044 --> 00:12:10,633
I'm a reporter, too,

202
00:12:10,633 --> 00:12:12,425
and sometimes we make mistakes.

203
00:12:12,425 --> 00:12:14,728
So ignore it, mom. Just ignore it.

204
00:12:14,729 --> 00:12:16,514
You think I can trust you girls?

205
00:12:16,514 --> 00:12:17,754
When you all got together

206
00:12:17,754 --> 00:12:21,345
and deceived me?

207
00:12:21,345 --> 00:12:24,320
You tell me.

208
00:12:24,320 --> 00:12:25,584
That it's not true.

209
00:12:25,584 --> 00:12:34,439
That it's not happening! Tell me!

210
00:12:34,440 --> 00:12:36,551
It's true.

211
00:12:36,551 --> 00:12:49,367
It's all true, mom.

212
00:12:49,367 --> 00:12:52,423
That cursed company Baekdo...

213
00:12:52,423 --> 00:12:55,614
After getting my children's father killed,

214
00:12:55,614 --> 00:12:59,448
they're taking Taejun from you?

215
00:12:59,448 --> 00:13:02,880
I'm going to go there and...

216
00:13:02,880 --> 00:13:05,040
Mom, please don't do this.

217
00:13:05,041 --> 00:13:08,042
Mom, reacting this way won't help!

218
00:13:08,042 --> 00:13:09,739
Mom, please calm down.

219
00:13:09,739 --> 00:13:13,442
Getting upset won't help!

220
00:13:13,442 --> 00:13:15,649
Go. You go

221
00:13:15,649 --> 00:13:17,938
and bring Taejun back,

222
00:13:17,938 --> 00:13:20,497
or go to that woman's house and resolve this!

223
00:13:20,498 --> 00:13:21,913
Tell them you're Taejun's wife,

224
00:13:21,913 --> 00:13:23,498
and that he has a daughter,

225
00:13:23,498 --> 00:13:25,465
and bring Taejun back! Hurry!

226
00:13:25,465 --> 00:13:27,793
Mom, please.

227
00:13:27,793 --> 00:13:29,985
Taejun has his reasons.

228
00:13:29,985 --> 00:13:32,498
He needs time, too.

229
00:13:32,498 --> 00:13:34,587
Saebyeol...

230
00:13:34,587 --> 00:13:36,689
What about Saebyeol?

231
00:13:36,690 --> 00:13:39,925
He'll turn his back on his child?

232
00:13:39,925 --> 00:13:43,303
I'm going to protect Saebyeol.

233
00:13:43,303 --> 00:13:45,637
I'm going to get Saebyeol's dad back.

234
00:13:45,637 --> 00:13:48,417
I'm going to do that, mom.

235
00:13:48,418 --> 00:13:50,873
You gave him

236
00:13:50,873 --> 00:13:53,970
everything you had

237
00:13:53,970 --> 00:13:56,810
and the best years of your life,

238
00:13:56,810 --> 00:13:58,873
and look at what happened...

239
00:13:58,874 --> 00:14:01,276
All you have left are tears...

240
00:14:01,276 --> 00:14:04,556
How will I face your mom in the afterlife?

241
00:14:04,557 --> 00:14:08,875
How?

242
00:14:08,875 --> 00:14:15,315
Mom...

243
00:14:15,315 --> 00:14:18,188
Mom, I'm sorry.

244
00:14:18,188 --> 00:14:23,971
Mom, I'm so sorry...

245
00:14:23,971 --> 00:14:26,660
Nayeon...

246
00:14:26,660 --> 00:14:32,558
- That jerk... / - Don't cry.

247
00:14:32,558 --> 00:14:37,109
It's okay.

248
00:14:37,109 --> 00:14:38,414
This is the list of companies

249
00:14:38,414 --> 00:14:40,773
Baekdo is looking at for mergers and acquisitions.

250
00:14:40,773 --> 00:14:43,504
And these companies are not on the list,

251
00:14:43,504 --> 00:14:46,295
but I'd like you to review them.

252
00:14:46,295 --> 00:14:48,495
You've been interested in tourism?

253
00:14:48,495 --> 00:14:50,176
I think it's one area

254
00:14:50,176 --> 00:14:51,434
where Baekdo's combined power

255
00:14:51,434 --> 00:14:54,043
would yield the greatest effect.

256
00:14:54,043 --> 00:14:57,828
Okay. I'll review it.

257
00:14:57,828 --> 00:14:59,324
Thank you.

258
00:14:59,325 --> 00:15:03,524
Have you met your child?

259
00:15:03,524 --> 00:15:06,072
I saw her for a bit.

260
00:15:06,072 --> 00:15:08,188
That must have affected you.

261
00:15:08,188 --> 00:15:13,091
Do you see the mother sometimes?

262
00:15:13,091 --> 00:15:15,187
I'm sorry.

263
00:15:15,187 --> 00:15:17,685
She must be in a state of shock.

264
00:15:17,685 --> 00:15:20,116
She'll need time to process it.

265
00:15:20,116 --> 00:15:22,699
Refrain from meeting her.

266
00:15:22,700 --> 00:15:26,710
And keep it a secret from Sejin for now.

267
00:15:26,710 --> 00:15:29,605
This would be such a big shock for her.

268
00:15:29,605 --> 00:15:32,111
I'm sorry.

269
00:15:32,111 --> 00:15:35,039
It's "Yes, sir," not "I'm sorry."

270
00:15:35,039 --> 00:15:42,063
That's the answer I want.

271
00:15:42,063 --> 00:15:46,207
Wow!

272
00:15:46,207 --> 00:15:49,095
Mr. Kang, you've become a big star.

273
00:15:49,095 --> 00:15:51,679
Everyone is talking about you in management.

274
00:15:51,679 --> 00:15:52,631
What are you talking about?

275
00:15:52,631 --> 00:15:54,591
The material you presented at the last meeting

276
00:15:54,591 --> 00:15:56,903
was distributed to everyone!

277
00:15:56,903 --> 00:15:58,943
About selling AP Foods,

278
00:15:58,943 --> 00:16:02,591
which the president and chairman are opposed to.

279
00:16:02,591 --> 00:16:03,856
Right.

280
00:16:03,856 --> 00:16:05,270
Everyone's been walking on eggshells,

281
00:16:05,270 --> 00:16:07,310
but you said what needed to be said,

282
00:16:07,310 --> 00:16:11,295
so imagine how happy they must be.

283
00:16:11,295 --> 00:16:12,895
You're being too kind.

284
00:16:12,895 --> 00:16:22,861
I just did what needed to be done.

285
00:16:22,861 --> 00:16:24,388
If the article is true,

286
00:16:24,388 --> 00:16:26,693
your wedding must be coming up soon.

287
00:16:26,693 --> 00:16:29,247
Have you told Sejin about Saebyeol yet?

288
00:16:29,247 --> 00:16:31,676
Saebyeol is your daughter,

289
00:16:31,677 --> 00:16:33,348
and she comes first.

290
00:16:33,348 --> 00:16:36,277
Don't keep her a secret.

291
00:16:36,278 --> 00:16:42,582
If you can't tell Sejin, I will.

292
00:16:42,582 --> 00:16:53,588
(Delete message)

293
00:16:53,588 --> 00:16:55,603
Which bed will make us happier

294
00:16:55,604 --> 00:17:18,343
when we wake up every morning?

295
00:17:18,343 --> 00:17:20,407
Wouldn't this smaller bed

296
00:17:20,406 --> 00:17:30,342
keep us closer together?

297
00:17:30,343 --> 00:17:32,872
I want this one, mom.

298
00:17:32,872 --> 00:17:35,055
The color's not too flashy,

299
00:17:35,055 --> 00:17:39,957
and the size and height are what I wanted.

300
00:17:39,957 --> 00:17:42,900
And it's not too hard or soft.

301
00:17:42,900 --> 00:17:45,600
You never use that many words.

302
00:17:45,600 --> 00:17:47,910
What? What are you talking about?

303
00:17:47,911 --> 00:17:49,848
You always had a big bed

304
00:17:49,848 --> 00:17:52,056
so you could sleep like a queen.

305
00:17:52,056 --> 00:17:54,432
So why are you going for the shabby one?

306
00:17:54,432 --> 00:17:55,647
You find this kind of color boring,

307
00:17:55,647 --> 00:17:57,664
and you think the frame is old-fashioned.

308
00:17:57,664 --> 00:18:00,847
Why has your taste changed overnight?

309
00:18:00,847 --> 00:18:03,424
You're acting like a total stranger, Sejin.

310
00:18:03,424 --> 00:18:07,791
This kind of simple style works well

311
00:18:07,791 --> 00:18:09,528
in a newlywed bedroom.

312
00:18:09,528 --> 00:18:11,184
It's the harmony that's important.

313
00:18:11,184 --> 00:18:12,870
It might work well for Taejun,

314
00:18:12,871 --> 00:18:15,205
but it doesn't work well for you!

315
00:18:15,205 --> 00:18:16,987
Why are you tainted?

316
00:18:16,988 --> 00:18:18,259
Why must your taste be spoiled

317
00:18:18,259 --> 00:18:21,243
because of that jerk? Why?

318
00:18:21,243 --> 00:18:22,362
Mom!

319
00:18:22,363 --> 00:18:24,821
"Tainted"? "That jerk"? How can you say that?

320
00:18:24,821 --> 00:18:27,076
We're shopping for me and my future husband.

321
00:18:27,076 --> 00:18:29,788
You've given me permission to marry him,

322
00:18:29,788 --> 00:18:31,164
and now we're planning the wedding together.

323
00:18:31,164 --> 00:18:32,827
So why are you doing this again?

324
00:18:32,827 --> 00:18:35,477
Just get the one I picked!

325
00:18:35,477 --> 00:18:38,138
I have better taste than you!

326
00:18:38,138 --> 00:18:41,338
It's mine, so I'll decide.

327
00:18:41,338 --> 00:18:43,250
The bed is for me and Taejun,

328
00:18:43,250 --> 00:18:47,330
so I'll choose it.

329
00:18:47,330 --> 00:18:49,285
Excuse me.

330
00:18:49,286 --> 00:18:51,462
Can I buy this bed right now?

331
00:18:51,462 --> 00:18:54,343
Of course, ma'am.

332
00:18:54,343 --> 00:18:57,631
- How many do you have? / - Excuse me?

333
00:18:57,631 --> 00:18:59,942
How many do you have available?

334
00:18:59,942 --> 00:19:01,952
Outside the display model, we have one in storage

335
00:19:01,952 --> 00:19:03,497
and one at the factory.

336
00:19:03,497 --> 00:19:08,520
Then I'll buy all three.

337
00:19:08,520 --> 00:19:10,950
I'll buy them all, so let me pay.

338
00:19:10,950 --> 00:19:13,412
And take all of them apart

339
00:19:13,412 --> 00:19:15,562
and throw them away.

340
00:19:15,562 --> 00:19:28,195
Mom!

341
00:19:28,195 --> 00:19:29,164
Hello?

342
00:19:29,164 --> 00:19:31,788
It's me. Sejin's mom.

343
00:19:31,788 --> 00:19:32,779
Hello, mother.

344
00:19:32,779 --> 00:19:34,883
I need to see you, so I'm coming to your area.

345
00:19:34,883 --> 00:19:36,899
Don't tell Sejin about this.

346
00:19:36,899 --> 00:19:57,494
Yes, mother.

347
00:19:57,494 --> 00:19:59,646
I'm sorry for making you wait.

348
00:19:59,646 --> 00:20:02,110
Have you waited long?

349
00:20:02,111 --> 00:20:04,758
I didn't interrupt your work, did I?

350
00:20:04,758 --> 00:20:07,566
No, I've finished all urgent business.

351
00:20:07,566 --> 00:20:09,989
I ordered tea while I was waiting.

352
00:20:09,989 --> 00:20:11,382
You should order something.

353
00:20:11,382 --> 00:20:14,569
It's okay. I've already had some.

354
00:20:14,569 --> 00:20:23,710
What did you want to tell me?

355
00:20:23,710 --> 00:20:34,727
Open it.

356
00:20:34,727 --> 00:20:36,886
I'm not paying you to break up with Sejin,

357
00:20:36,886 --> 00:20:40,651
so don't get nervous.

358
00:20:40,651 --> 00:20:43,218
I'm so relieved.

359
00:20:43,218 --> 00:20:45,617
I'm not giving that to you because I like you.

360
00:20:45,617 --> 00:20:47,513
I don't want Sejin

361
00:20:47,513 --> 00:20:50,544
to be affected by your cheap taste,

362
00:20:50,545 --> 00:20:56,233
so I want you to fill up your wallet.

363
00:20:56,233 --> 00:21:02,528
Do you know why I said no to you?

364
00:21:02,528 --> 00:21:05,135
Because of your eyes.

365
00:21:05,135 --> 00:21:07,247
Others might say they see

366
00:21:07,247 --> 00:21:10,465
spunk, spirit and ambition in your eyes,

367
00:21:10,465 --> 00:21:12,514
but I don't.

368
00:21:12,515 --> 00:21:15,102
It's an inferiority complex

369
00:21:15,102 --> 00:21:18,179
and greed from wanting to overcome it.

370
00:21:18,179 --> 00:21:20,594
That's all it is.

371
00:21:20,594 --> 00:21:25,370
And Sejin fell into that trap.

372
00:21:25,370 --> 00:21:29,725
You have a keen eye, mother.

373
00:21:29,725 --> 00:21:33,918
You're correct.

374
00:21:33,918 --> 00:21:35,992
I grew up so poor

375
00:21:35,992 --> 00:21:38,264
that I always feel inferior.

376
00:21:38,264 --> 00:21:39,823
I've worked very hard

377
00:21:39,824 --> 00:21:42,245
to get over my inferiority complex,

378
00:21:42,245 --> 00:21:44,669
but it's not easy.

379
00:21:44,670 --> 00:21:46,238
Actually,

380
00:21:46,238 --> 00:21:49,422
I've been getting over it after meeting Sejin.

381
00:21:49,422 --> 00:21:51,014
When I'm with her,

382
00:21:51,014 --> 00:21:57,216
I'm so full of confidence.

383
00:21:57,217 --> 00:22:02,938
So I was even more determined not to lose her.

384
00:22:02,938 --> 00:22:06,506
You're really something.

385
00:22:06,506 --> 00:22:10,345
Sejin is your daughter.

386
00:22:10,345 --> 00:22:12,073
I know I can get along with you

387
00:22:12,073 --> 00:22:17,137
considering how much I love Sejin.

388
00:22:17,137 --> 00:22:19,999
And I don't need this.

389
00:22:19,999 --> 00:22:23,551
My taste led me to choose Sejin.

390
00:22:23,551 --> 00:22:25,351
And that proves

391
00:22:25,351 --> 00:22:28,183
that I have pretty good taste.

392
00:22:28,183 --> 00:22:29,168
Don't make excuses,

393
00:22:29,168 --> 00:23:02,988
and just put it in your empty pocket.

394
00:23:02,989 --> 00:23:05,349
What a sly jerk.

395
00:23:05,349 --> 00:23:11,213
Sejin fell for him because she's so naive.

396
00:23:11,213 --> 00:23:12,949
Then again,

397
00:23:12,949 --> 00:23:26,110
he's better than someone stupid.

398
00:23:26,110 --> 00:23:31,093
You should try smiling.

399
00:23:31,094 --> 00:23:32,429
When did you get here?

400
00:23:32,429 --> 00:23:33,597
Just now.

401
00:23:33,597 --> 00:23:34,841
I wanted to have dinner with you,

402
00:23:34,841 --> 00:23:36,614
so I got some food.

403
00:23:36,615 --> 00:23:43,776
I worried that you'd say you were too busy to eat.

404
00:23:43,776 --> 00:23:46,783
I went to a department store today with my mom

405
00:23:46,783 --> 00:23:49,215
to look at beds and had an argument.

406
00:23:49,215 --> 00:23:52,294
Why? Why didn't you humor her?

407
00:23:52,295 --> 00:23:55,096
It's true that couples develop similar taste.

408
00:23:55,096 --> 00:23:58,472
Your taste has become mine.

409
00:23:58,472 --> 00:24:01,831
Don't mind me, and go with what your mother wants.

410
00:24:01,831 --> 00:24:03,255
Okay.

411
00:24:03,256 --> 00:24:05,625
Go get washed up. I'm hungry.

412
00:24:05,625 --> 00:24:23,149
Okay, I'll do that.

413
00:24:23,149 --> 00:24:29,979
(Mother)

414
00:24:29,980 --> 00:24:30,851
Well...

415
00:24:30,851 --> 00:24:32,781
Is this Taejun?

416
00:24:32,781 --> 00:24:34,148
I can't believe

417
00:24:34,148 --> 00:24:38,283
how heartless she is!

418
00:24:38,284 --> 00:24:40,877
This is crazy!

419
00:24:40,877 --> 00:24:43,269
What kind of life is this?

420
00:24:43,269 --> 00:24:45,461
Why is every day worse than the one before?

421
00:24:45,461 --> 00:24:48,589
Do you know what Yang Malsuk did to me?

422
00:24:48,589 --> 00:24:50,636
She used the measly deposit money

423
00:24:50,636 --> 00:24:53,189
on the rooftop apartment to kick me out!

424
00:24:53,189 --> 00:24:54,849
Come here and do something about it!

425
00:24:54,849 --> 00:24:57,393
Your mom's going to freeze to death!

426
00:24:57,393 --> 00:24:59,471
Hello? Taejun?

427
00:24:59,471 --> 00:25:03,142
Are you listening?

428
00:25:03,142 --> 00:25:04,656
I'm sorry.

429
00:25:04,656 --> 00:25:07,406
Taejun can't take your call right now.

430
00:25:07,406 --> 00:25:08,908
Oh, my!

431
00:25:08,908 --> 00:25:11,637
Who is this?

432
00:25:11,637 --> 00:25:14,779
Hello, I'm Jang Sejin.

433
00:25:14,779 --> 00:25:17,972
I'm sorry I have to say hello on the phone.

434
00:25:17,972 --> 00:25:19,620
Oh!

435
00:25:19,620 --> 00:25:22,800
Are you the young lady

436
00:25:22,800 --> 00:25:24,873
who's going to marry my Taejun?

437
00:25:24,873 --> 00:25:27,997
Yes. I'm sorry I haven't been able to see you .

438
00:25:27,997 --> 00:25:29,957
It's okay.

439
00:25:29,957 --> 00:25:34,649
You have such a pretty voice.

440
00:25:34,649 --> 00:25:37,321
I'll tell Taejun to call you back.

441
00:25:37,321 --> 00:25:51,170
Okay. Good night.

442
00:25:51,171 --> 00:26:16,676
(Missed call from Nayeon)

443
00:26:16,676 --> 00:26:18,100
What are you doing?

444
00:26:18,100 --> 00:26:20,448
Nothing.

445
00:26:20,449 --> 00:26:23,672
What is it? Your morning sickness?

446
00:26:23,672 --> 00:26:27,273
It's okay. I can handle it.

447
00:26:27,273 --> 00:26:29,237
Oh, no...

448
00:26:29,238 --> 00:26:33,410
Did you break up with that woman?

449
00:26:33,410 --> 00:26:35,211
Almost.

450
00:26:35,211 --> 00:26:38,734
Almost? It hasn't happened yet?

451
00:26:38,734 --> 00:26:41,298
Why is she so persistent and pathetic?

452
00:26:41,298 --> 00:26:43,458
Why can't she play it cool?

453
00:26:43,458 --> 00:26:47,059
She needs time to process it.

454
00:26:47,059 --> 00:26:49,228
I know it's hard on you, but please give me time.

455
00:26:49,228 --> 00:26:50,724
But I'm worried.

456
00:26:50,724 --> 00:26:52,572
What if she tries to hold onto you

457
00:26:52,572 --> 00:26:54,531
and ends up ruining our wedding?

458
00:26:54,531 --> 00:26:57,820
The reporters are dying to find trouble,

459
00:26:57,820 --> 00:26:59,635
so what if they find out?

460
00:26:59,635 --> 00:27:01,252
I'm so worried and afraid.

461
00:27:01,252 --> 00:27:02,787
I'm on pins and needles!

462
00:27:02,787 --> 00:27:38,496
Why can't you understand? Why not?

463
00:27:38,496 --> 00:27:42,620
Mom, it's because bears have thick fur.

464
00:27:42,620 --> 00:27:45,373
What are you talking about?

465
00:27:45,374 --> 00:27:47,205
Bears have such thick fur

466
00:27:47,205 --> 00:27:48,837
that when it's too warm,

467
00:27:48,837 --> 00:27:50,645
they feel hot and get angry.

468
00:27:50,645 --> 00:27:55,665
My teacher told me that.

469
00:27:55,665 --> 00:27:58,601
Daddy Bear might be angry

470
00:27:58,601 --> 00:28:05,065
because you're too warm to him, mom.

471
00:28:05,065 --> 00:28:07,586
Bears are like that?

472
00:28:07,586 --> 00:28:10,593
I didn't know.

473
00:28:10,593 --> 00:28:13,121
I should study harder, right?

474
00:28:13,121 --> 00:28:25,728
Now you know, so it's okay.

475
00:28:25,728 --> 00:28:30,619
Hello?

476
00:28:30,619 --> 00:28:32,269
Who is this?

477
00:28:32,269 --> 00:28:37,805
I don't know how to introduce myself.

478
00:28:37,805 --> 00:28:42,773
You can just say it like it is.

479
00:28:42,773 --> 00:28:47,572
I'm Kang Taejun's fiancée.

480
00:28:47,573 --> 00:28:49,469
Our wedding is coming up soon,

481
00:28:49,469 --> 00:28:52,012
and I want to meet you before that.

482
00:28:52,012 --> 00:28:56,606
Can we?

483
00:28:56,606 --> 00:28:58,764
There's no reason why we can't meet.

484
00:28:58,764 --> 00:29:00,139
Right.

485
00:29:00,140 --> 00:29:03,180
There's no reason.

486
00:29:03,180 --> 00:29:07,308
Then let's meet now. Right now.

487
00:29:07,308 --> 00:29:10,101
There's no reason for us to stall.

488
00:29:10,101 --> 00:29:15,644
Right. There's no reason for that.

489
00:29:15,644 --> 00:29:18,236
Yes, I know that place.

490
00:29:18,236 --> 00:29:29,244
I'll see you in an hour.

491
00:29:29,244 --> 00:29:39,204
Yes? Saebyeol?

492
00:29:39,204 --> 00:29:41,428
Hello?

493
00:29:41,428 --> 00:29:45,581
Yes.

494
00:29:45,581 --> 00:30:24,051
Who is this again?

495
00:30:24,051 --> 00:30:26,099
Hello.

496
00:30:26,099 --> 00:30:44,558
I'm the person you called earlier.

497
00:30:44,558 --> 00:30:48,006
My prediction was wrong.

498
00:30:48,006 --> 00:30:49,800
I thought that

499
00:30:49,800 --> 00:30:53,384
you'd be shocked and scared to hear from me.

500
00:30:53,384 --> 00:30:55,627
I was worried for no reason.

501
00:30:55,627 --> 00:30:59,350
Are you upset because

502
00:30:59,350 --> 00:31:02,389
I'm not scared or shocked?

503
00:31:02,390 --> 00:31:06,477
I see why Taejun's having trouble letting you go.

504
00:31:06,477 --> 00:31:08,989
This is actually great.

505
00:31:08,989 --> 00:31:14,544
You seem gutsy enough for a decent conversation.

506
00:31:14,544 --> 00:31:17,071
Don't worry about your child.

507
00:31:17,071 --> 00:31:19,266
We will take full responsibility.

508
00:31:19,267 --> 00:31:22,921
Responsibility?

509
00:31:22,921 --> 00:31:25,394
How will you do that?

510
00:31:25,394 --> 00:31:28,627
Tell me what you want.

511
00:31:28,627 --> 00:31:32,499
You can take your child abroad.

512
00:31:32,499 --> 00:31:35,626
That would be smartest thing for everyone.

513
00:31:35,626 --> 00:31:38,633
But if you don't want that,

514
00:31:38,633 --> 00:31:42,083
you can give the child to us

515
00:31:42,083 --> 00:31:48,259
and make a fresh start.

516
00:31:48,259 --> 00:31:50,915
I'm sorry,

517
00:31:50,915 --> 00:31:54,404
but that's not what I want.

518
00:31:54,404 --> 00:31:56,667
What I want

519
00:31:56,667 --> 00:32:00,788
is for my child to get her one and only dad back.

520
00:32:00,788 --> 00:32:03,531
That's all.

521
00:32:03,531 --> 00:32:04,643
You're saying that

522
00:32:04,643 --> 00:32:06,868
you want to steal Taejun from my daughter.

523
00:32:06,868 --> 00:32:10,226
But I'm not the one who stole him.

524
00:32:10,227 --> 00:32:12,676
If someone has to get hurt,

525
00:32:12,676 --> 00:32:14,466
it won't be my daughter,

526
00:32:14,467 --> 00:32:18,091
because I will stop it.

527
00:32:18,091 --> 00:32:23,491
You must love your daughter so much.

528
00:32:23,491 --> 00:32:26,348
I never felt like this before.

529
00:32:26,348 --> 00:32:27,995
I am so angry at my dad,

530
00:32:27,996 --> 00:32:32,140
whom I never met.

531
00:32:32,140 --> 00:32:34,892
I envy Sejin

532
00:32:34,892 --> 00:32:40,387
for having a father.

533
00:32:40,387 --> 00:32:43,370
You are

534
00:32:43,371 --> 00:32:46,667
nothing I like expected, either.

535
00:32:46,667 --> 00:32:51,235
When I first got your call,

536
00:32:51,236 --> 00:32:55,189
I had a glimmer of hope.

537
00:32:55,189 --> 00:33:00,172
I thought that you of all people

538
00:33:00,172 --> 00:33:02,179
would understand my situation

539
00:33:02,179 --> 00:33:07,485
and make the right decision.

540
00:33:07,485 --> 00:33:13,661
Because you were like that 20 years ago.

541
00:33:13,662 --> 00:33:15,592
20 years ago?

542
00:33:15,592 --> 00:33:19,399
It's natural for you not to recognize me.

543
00:33:19,399 --> 00:33:22,023
The girl

544
00:33:22,023 --> 00:33:24,744
who was sent to an orphanage

545
00:33:24,744 --> 00:33:27,052
for stealing your wife's jewelry 20 years ago

546
00:33:27,052 --> 00:33:29,995
is all grown up.

547
00:33:29,996 --> 00:33:36,731
It's natural that you wouldn't recognize me.

548
00:33:36,731 --> 00:33:41,036
Are you...

549
00:33:41,036 --> 00:34:04,025
Nayeon?

550
00:34:04,025 --> 00:34:05,465
Please help me.

551
00:34:05,465 --> 00:34:06,408
Stop this wedding.

552
00:34:06,409 --> 00:34:08,208
If you bother Sejin and give her a hard time,

553
00:34:08,208 --> 00:34:09,193
you won't get away with it.

554
00:34:09,193 --> 00:34:10,321
She asked you to meet up?

555
00:34:10,321 --> 00:34:11,288
She wants to see you?

556
00:34:11,288 --> 00:34:13,431
You have to keep hiding,

557
00:34:13,431 --> 00:34:14,799
so I can have fun looking for you.

558
00:34:14,800 --> 00:34:16,441
Compared to how I feel about you,

559
00:34:16,440 --> 00:34:17,303
it's cheap.

560
00:34:17,304 --> 00:34:18,240
Thanks.

561
00:34:18,239 --> 00:34:19,507
The owners of this company

562
00:34:19,507 --> 00:34:20,859
are going to be my in-laws.

563
00:34:20,859 --> 00:34:22,060
I'm not really attracted to you.

564
00:34:22,060 --> 00:34:23,020
So let's focus on our deal.

565
00:34:23,021 --> 00:34:25,004
I'm confident that I won't fall for a man.

566
00:34:25,004 --> 00:34:26,428
Taejun isn't coming back.

567
00:34:26,428 --> 00:34:28,491
He's not the Taejun we used to know!

568
00:34:28,492 --> 00:34:29,492
Taejun.

