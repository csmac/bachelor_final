﻿1
00:00:07,342 --> 00:00:08,542
D'oh!

2
00:00:37,443 --> 00:00:39,344
Item being procured,

3
00:00:39,412 --> 00:00:41,847
one standard-sized paper clip.

4
00:00:43,483 --> 00:00:44,650
Hmm.

5
00:00:48,504 --> 00:00:50,489
Did you guys see
that stupid foul call

6
00:00:50,573 --> 00:00:52,157
in the game last night?

7
00:00:52,242 --> 00:00:55,844
See it? We followed the ref home
and beat the crap out of him.

8
00:00:55,912 --> 00:00:58,497
Yeah, I ill think that might've been
a kid who worked at Foot Locker.

9
00:00:58,581 --> 00:00:59,848
Hey, the supply
room's open.

10
00:00:59,916 --> 00:01:00,882
I better close it.

11
00:01:03,953 --> 00:01:07,773
Unguarded stuff...!

12
00:01:10,009 --> 00:01:12,344
Attention, lovers
of free office supplies!

13
00:01:12,412 --> 00:01:15,897
Come and steal things
you can easily afford!

14
00:01:38,805 --> 00:01:40,161
Can someone open this
bottle of mother wolf

15
00:01:40,186 --> 00:01:41,386
placenta for me?

16
00:01:42,075 --> 00:01:44,476
Mr. Burns!

17
00:01:44,544 --> 00:01:46,161
Save me, panic!
Huh?

18
00:01:47,397 --> 00:01:49,548
Oh!

19
00:01:49,615 --> 00:01:50,665
Whoo-hoo!

20
00:01:51,884 --> 00:01:53,735
My rubberized bands!

21
00:01:53,803 --> 00:01:55,137
My binder clips!

22
00:01:55,204 --> 00:01:57,406
My accordion Post-its!

23
00:02:01,144 --> 00:02:04,012
Oh, Lenny, why would
you steal my bear?

24
00:02:04,080 --> 00:02:05,981
I just wanted something
to cuddle at night.

25
00:02:06,049 --> 00:02:07,249
That's my cuddle bear!

26
00:02:07,317 --> 00:02:09,501
I loved him, I shot
him- he's mine!

27
00:02:10,920 --> 00:02:12,321
It has come to my attention

28
00:02:12,388 --> 00:02:13,989
that you lunch-bagging
wage lizards

29
00:02:14,057 --> 00:02:15,524
are robbing me blind.

30
00:02:15,591 --> 00:02:19,161
If you paid us better,
we wouldn't have to steal!

31
00:02:19,228 --> 00:02:20,729
You don't even work here!

32
00:02:20,797 --> 00:02:23,565
Wha...?! You mean I've been
calling in sick for nothing?

33
00:02:23,633 --> 00:02:25,884
I might not be here
tomorrow, that's for sure.

34
00:02:25,952 --> 00:02:27,536
Only one of you monkeys
wasn't caught

35
00:02:27,603 --> 00:02:29,488
with his sticky paw
in my tin cup.

36
00:02:29,555 --> 00:02:30,839
Homer Simpson.

37
00:02:30,907 --> 00:02:32,441
How you doing there?

38
00:02:32,508 --> 00:02:33,859
Wow, he threw us

39
00:02:33,926 --> 00:02:35,510
under the bus,
and now he's sitting

40
00:02:35,578 --> 00:02:36,962
in the bus driver's lap.

41
00:02:37,030 --> 00:02:39,197
Now, Simpson, how
is it that you alone

42
00:02:39,265 --> 00:02:42,050
managed to keep your moral
compass pointed to true north?

43
00:02:42,101 --> 00:02:44,453
Well, sir, before
I do anything,

44
00:02:44,520 --> 00:02:45,764
I stop and ask myself,

45
00:02:45,789 --> 00:02:49,017
"What would Jesus
and Mr. Burns do?"

46
00:02:49,459 --> 00:02:51,360
You are garbage made flesh!

47
00:02:51,427 --> 00:02:52,761
This is a mockery!

48
00:02:52,829 --> 00:02:55,197
Hush! Now, while Simpson
gets the day off,

49
00:02:55,264 --> 00:02:57,999
the rest of you will write
30-page essays entitled

50
00:02:58,067 --> 00:03:00,752
"Homer Simpson,
Moral Lodestar. "

51
00:03:00,820 --> 00:03:02,354
Question, Can we
have fun with it?

52
00:03:02,422 --> 00:03:03,522
No, you may not.

53
00:03:06,793 --> 00:03:08,160
You know, boy,

54
00:03:08,227 --> 00:03:11,129
my dad used to take me fishing
just like this.

55
00:03:13,533 --> 00:03:16,668
More angry texts from work.

56
00:03:16,736 --> 00:03:19,704
That's a lot of eights.

57
00:03:19,772 --> 00:03:22,040
I can't believe you got all your
friends in trouble

58
00:03:22,108 --> 00:03:23,642
and you got the day off!

59
00:03:23,709 --> 00:03:25,577
Hey, it's called karma.

60
00:03:27,113 --> 00:03:29,081
Isn't karma where if
you do something bad,

61
00:03:29,148 --> 00:03:30,849
bad things
happen to you?

62
00:03:30,917 --> 00:03:33,368
A common misconception.

63
00:03:34,554 --> 00:03:36,371
Suck it, karma! Yeah!

64
00:03:36,439 --> 00:03:38,156
I'm talking to you, karma!

65
00:03:38,224 --> 00:03:39,441
Ha-ha-ha!

66
00:03:39,509 --> 00:03:41,376
Karma's a bitch, karma!

67
00:03:44,113 --> 00:03:47,849
Ah, nothing better
than a lazy Saturday morning

68
00:03:47,917 --> 00:03:50,519
lying in a warm, moist bed,

69
00:03:50,586 --> 00:03:52,287
because weekends are...

70
00:03:52,355 --> 00:03:53,755
Moist?!

71
00:03:53,823 --> 00:03:55,924
I wet the bed!

72
00:03:55,992 --> 00:03:58,794
The one embarrassing thing
I've never done!

73
00:04:01,168 --> 00:04:02,802
I...

74
00:04:04,204 --> 00:04:06,405
Marge, from time to time
I've heard you speak

75
00:04:06,473 --> 00:04:09,408
of a "washing machine. "

76
00:04:09,476 --> 00:04:11,677
Where would I find
this marvelous contraption?

77
00:04:11,745 --> 00:04:14,246
Why? Are you going
to do the laundry?

78
00:04:15,515 --> 00:04:16,682
Not just the laundry.

79
00:04:16,750 --> 00:04:17,950
That would be weird,

80
00:04:18,018 --> 00:04:19,585
and you might ask questions.

81
00:04:19,653 --> 00:04:21,487
I'm gonna do all the chores.

82
00:04:21,555 --> 00:04:23,572
What the hell are you doing!

83
00:04:23,640 --> 00:04:24,840
Don't yell at me, brain.

84
00:04:24,908 --> 00:04:26,592
This happened on your watch.

85
00:04:26,660 --> 00:04:29,095
You have two jobs-
thinking and bladder control!

86
00:04:29,162 --> 00:04:31,330
I'm doing the best
with what I got.

87
00:04:31,398 --> 00:04:33,265
All you feed me
is reality shows!

88
00:04:33,333 --> 00:04:35,901
I like to watch
Lamar Odom play Xbox

89
00:04:35,969 --> 00:04:38,004
while his giant wife
yells at him.

90
00:04:38,071 --> 00:04:39,739
Is that so nuts?!

91
00:04:40,941 --> 00:04:41,841
Huh?!

92
00:04:43,076 --> 00:04:44,927
Yeah, shut you up.

93
00:04:47,547 --> 00:04:49,615
Now, I'm sure
yesterday's incident

94
00:04:49,683 --> 00:04:52,752
of urination domination
was a one-time deal.

95
00:04:52,819 --> 00:04:55,154
But just to make sure...

96
00:04:56,456 --> 00:04:59,408
Oh, why can't
I cork my wang-wine?

97
00:04:59,476 --> 00:05:00,976
Homer...

98
00:05:01,044 --> 00:05:03,979
you do not yet understand
the meaning of karma.

99
00:05:04,047 --> 00:05:06,732
But isn't karma just an
expression of the dharma?

100
00:05:08,035 --> 00:05:09,168
That is
beside the point, okay?

101
00:05:09,236 --> 00:05:11,037
If something bad
is happening to you,

102
00:05:11,104 --> 00:05:13,622
it must be because of
something you did to others.

103
00:05:14,858 --> 00:05:16,191
Deep down, I must be
feeling guilty

104
00:05:16,243 --> 00:05:18,094
about getting
my friends in trouble!

105
00:05:18,161 --> 00:05:22,031
And my problem won't stop till
I make things right with them!

106
00:05:22,099 --> 00:05:25,901
But first, a little
more sleep, hm?

107
00:05:30,090 --> 00:05:32,825
This is the best "I'm sorry"
party Homer's ever thrown.

108
00:05:32,893 --> 00:05:34,360
Who the hell are you?

109
00:05:34,428 --> 00:05:35,861
I'm sorry.
Cheese on that?

110
00:05:35,929 --> 00:05:37,463
Sorry. Hey, Lenny.

111
00:05:37,531 --> 00:05:38,631
Sorry.

112
00:05:38,698 --> 00:05:39,965
One more announcement.

113
00:05:40,033 --> 00:05:41,600
Make sure you whack

114
00:05:41,668 --> 00:05:44,420
"piñata me"
and not "real me. "

115
00:05:47,057 --> 00:05:48,641
A lot of work

116
00:05:48,708 --> 00:05:50,126
went into this thing.

117
00:05:52,212 --> 00:05:53,712
Die! Die!

118
00:05:53,780 --> 00:05:55,681
Die!
So, I got to know,

119
00:05:55,749 --> 00:05:56,949
do you guys
forgive me?

120
00:05:57,017 --> 00:05:58,451
Oh, yeah!

121
00:05:58,535 --> 00:06:02,955
Ain't no problem that free food
and free booze won't fix.

122
00:06:03,023 --> 00:06:04,423
Free?! Uh...

123
00:06:04,491 --> 00:06:06,292
Oh...

124
00:06:06,359 --> 00:06:07,827
Free it is!

125
00:06:09,229 --> 00:06:10,763
Thanks, guys.

126
00:06:11,865 --> 00:06:13,132
Tomorrow morning

127
00:06:13,200 --> 00:06:16,235
my sheets will be as dry
as the surface of Mars,

128
00:06:16,303 --> 00:06:17,870
except for the poles.

129
00:06:23,827 --> 00:06:25,194
What the...?

130
00:06:25,262 --> 00:06:26,729
Wet? Again?

131
00:06:26,797 --> 00:06:29,498
Oh, I did the right thing
for nothing!

132
00:06:29,566 --> 00:06:32,435
There's only one solution left.

133
00:06:35,939 --> 00:06:37,173
Mm-hmm.

134
00:06:37,240 --> 00:06:39,041
Mm-hmm-hmm.

135
00:06:46,583 --> 00:06:48,517
Homer Simpson!

136
00:06:48,585 --> 00:06:52,087
You forgot your receipt for
your adult bedwetting product!

137
00:06:52,155 --> 00:06:53,656
Homer Simpson!

138
00:06:53,723 --> 00:06:55,191
Are you there?!

139
00:07:01,798 --> 00:07:04,266
Mm-hmm, okay.

140
00:07:04,334 --> 00:07:07,920
Place detection pad here.

141
00:07:07,988 --> 00:07:11,841
Connect alarm hook-up... here.

142
00:07:11,908 --> 00:07:15,344
Now, that's what I call
looking out for number one.

143
00:07:21,084 --> 00:07:22,868
What's going on?

144
00:07:22,936 --> 00:07:24,937
Uh, uh, that was
just the fire alarm.

145
00:07:25,005 --> 00:07:26,722
Try to go back to sleep.

146
00:07:26,790 --> 00:07:28,374
Shut up!

147
00:07:28,441 --> 00:07:30,276
Oh...

148
00:07:30,343 --> 00:07:31,410
Whoa-hoa-hoa!

149
00:07:31,478 --> 00:07:32,378
What's going on?

150
00:07:32,462 --> 00:07:33,979
Is this a joke?

151
00:07:34,047 --> 00:07:37,483
Son, I'm afraid the
Uralarm Whiz-no-more 9000

152
00:07:37,551 --> 00:07:39,185
is no joke.

153
00:07:39,252 --> 00:07:40,553
What is going on?

154
00:07:41,855 --> 00:07:43,622
Kids, there's something
I have to tell you.

155
00:07:43,690 --> 00:07:45,991
Your mother and I
are wetting the bed.

156
00:07:46,059 --> 00:07:47,693
"We're" wetting the bed?

157
00:07:47,761 --> 00:07:50,429
Hey, when you were pregnant,
everything was "we. "

158
00:07:53,233 --> 00:07:54,700
I'm so turned on.

159
00:07:58,572 --> 00:08:00,239
What?

160
00:08:00,307 --> 00:08:01,874
I'm sorry, Homie.

161
00:08:01,942 --> 00:08:03,842
A diaper just isn't sexy.

162
00:08:03,910 --> 00:08:05,344
What about Cupid?

163
00:08:05,412 --> 00:08:06,679
He's smokin' hot.

164
00:08:06,746 --> 00:08:08,380
He's a baby with wings.

165
00:08:08,448 --> 00:08:12,251
Marge, it's not the diaper,
it's what's inside.

166
00:08:19,926 --> 00:08:21,961
That's just not
doing it for me.

167
00:08:22,028 --> 00:08:23,362
Oh...

168
00:08:39,779 --> 00:08:41,847
Professor Frink,
are you all right?

169
00:08:41,915 --> 00:08:43,999
Oh, yes, yes,
I'm fine, my dear.

170
00:08:44,067 --> 00:08:46,952
I was just trying to get past
the New York Times pay-wall,

171
00:08:47,037 --> 00:08:48,370
and then kaboy!

172
00:08:48,438 --> 00:08:50,606
And what brings you out
in the middle of the night?

173
00:08:50,674 --> 00:08:52,775
It's kind of embarrassing.

174
00:08:52,842 --> 00:08:55,794
Yes, I heard about your
husband's bedwetting problem.

175
00:08:55,862 --> 00:08:57,596
How do you know
about it?

176
00:08:57,664 --> 00:09:00,432
Tweeted by Bart,
re-tweeted by Krusty.

177
00:09:01,735 --> 00:09:03,002
Well, perhaps I can help.

178
00:09:03,069 --> 00:09:05,137
You see, I have
invented a device

179
00:09:05,205 --> 00:09:07,973
that allows you to enter
someone else's dreams

180
00:09:08,041 --> 00:09:10,276
and explore their
subconscious.

181
00:09:10,343 --> 00:09:12,778
So we can go inside
Homer's sleeping mind

182
00:09:12,846 --> 00:09:15,230
and find out why
he's wetting the bed?

183
00:09:15,298 --> 00:09:17,166
Uh, yes. In fact,
I just used it

184
00:09:17,233 --> 00:09:20,302
to cure another Springfielder
of his particular obsession.

185
00:09:20,370 --> 00:09:22,871
Normal Stu likes
normal things!

186
00:09:22,939 --> 00:09:24,006
Hmm.

187
00:09:28,511 --> 00:09:31,013
We're actually entering
Dad's dreams?

188
00:09:31,081 --> 00:09:33,349
Yes, yes. You see,
it's the only way

189
00:09:33,416 --> 00:09:36,418
you can uncover the
psychological trauma

190
00:09:36,486 --> 00:09:39,188
that is causing your
father's secret shame.

191
00:09:39,255 --> 00:09:41,357
Why does Maggie
have to go?

192
00:09:41,424 --> 00:09:43,058
What am I,
a babysitter?!

193
00:09:44,244 --> 00:09:46,512
So no
school for me.

194
00:09:53,536 --> 00:09:55,971
Hey, guys,
whatcha doing in my dream?

195
00:09:56,022 --> 00:09:57,606
Trying to fix your broken brain.

196
00:09:57,674 --> 00:09:59,074
My brain's fine.

197
00:09:59,142 --> 00:10:01,360
In my dreams, I'm
an intermediate skier!

198
00:10:10,453 --> 00:10:12,254
Ski patrol!
Everybody be cool!

199
00:10:12,321 --> 00:10:14,173
It's Death!
I recognize him

200
00:10:14,257 --> 00:10:16,324
from 40th birthday cards.

201
00:10:18,178 --> 00:10:19,845
Homie, this
might be a clue.

202
00:10:19,929 --> 00:10:21,096
What's in that coffin

203
00:10:21,164 --> 00:10:23,682
could be behind your
nighttime oopsies.

204
00:10:23,767 --> 00:10:25,217
You wet the bed?!

205
00:10:25,301 --> 00:10:27,686
Oh... great!
Now Death knows!

206
00:10:33,543 --> 00:10:34,526
What do you see?

207
00:10:34,611 --> 00:10:36,344
Uh, nothing,
typical dream nonsense.

208
00:10:41,451 --> 00:10:42,701
Relax.

209
00:10:42,786 --> 00:10:44,820
Everyone knows
that if you die in a dream,

210
00:10:44,871 --> 00:10:46,021
you just wake up.

211
00:10:47,540 --> 00:10:49,241
Oh, uh, actually,

212
00:10:49,325 --> 00:10:53,328
because I neglected to install
the latest Adobe Acrobat update,

213
00:10:53,396 --> 00:10:56,465
if you die in the dream,
you die in real life.

214
00:10:57,434 --> 00:10:58,901
Incidentally, I've also proven

215
00:10:58,968 --> 00:11:01,570
that Hell is real,
and everyone goes there.

216
00:11:01,638 --> 00:11:02,588
Frink out.

217
00:11:08,742 --> 00:11:10,860
Wait a minute,
I can't die.

218
00:11:12,863 --> 00:11:13,997
Mommy.

219
00:11:16,717 --> 00:11:17,884
Listen, everyone.

220
00:11:17,968 --> 00:11:19,719
We should fall asleep
in this dream.

221
00:11:19,804 --> 00:11:21,171
One minute at this level

222
00:11:21,222 --> 00:11:23,990
equals two hours one
dream level below.

223
00:11:24,058 --> 00:11:25,374
Wait, dreams have rules?

224
00:11:25,426 --> 00:11:27,227
Everything has rules, Bart.

225
00:11:27,311 --> 00:11:28,878
Not me, when I hit
the dance floor.

226
00:11:35,569 --> 00:11:37,320
Ooh-ooh-ooh!
Ah-ah-ah!

227
00:11:47,631 --> 00:11:49,365
Bart!

228
00:11:49,433 --> 00:11:52,218
Can the chatter
and fetch me a baloney sandwich.

229
00:11:52,286 --> 00:11:54,020
Why don't you
fetch it yourself, man.

230
00:11:55,356 --> 00:11:57,457
Bart's making faces!

231
00:11:57,525 --> 00:12:00,143
Shut up, you kids,
and bring me baloney.

232
00:12:02,213 --> 00:12:04,314
Wait nicely.

233
00:12:04,381 --> 00:12:06,249
Simpson family?

234
00:12:08,819 --> 00:12:11,588
Now, I believe that to
solve Homer's problem,

235
00:12:11,655 --> 00:12:13,389
he must face what's
inside the coffin.

236
00:12:13,457 --> 00:12:15,425
Forget it, Doc.

237
00:12:15,492 --> 00:12:17,193
What if that's
my marriage in there?

238
00:12:17,261 --> 00:12:18,261
Marriage, shmarriage.

239
00:12:18,329 --> 00:12:19,696
What's in the box, man?

240
00:12:23,984 --> 00:12:26,569
See what your monkeyshines
have done, boy?

241
00:12:26,620 --> 00:12:28,738
Still smells better than
your gym socks, man.

242
00:12:28,789 --> 00:12:30,223
Why you little...

243
00:12:30,291 --> 00:12:33,676
I'll teach you to make fun
of my socks.

244
00:12:33,744 --> 00:12:34,694
Here, quick!

245
00:12:34,762 --> 00:12:36,296
Everyone into my dream!

246
00:12:44,021 --> 00:12:48,791
What brings thy merry band
to Stratford's plains?

247
00:12:48,859 --> 00:12:49,943
Forsooth,
a myst'ry doth confound...

248
00:12:50,010 --> 00:12:51,878
In your dreams.

249
00:12:54,815 --> 00:12:56,649
We never do my thing.

250
00:13:00,037 --> 00:13:01,738
Ooh...

251
00:13:01,805 --> 00:13:02,772
Huh?

252
00:13:05,042 --> 00:13:08,644
I know this dream.

253
00:13:08,696 --> 00:13:10,513
It's the land of my
innermost thoughts

254
00:13:10,581 --> 00:13:12,315
and fondest desires.

255
00:13:12,383 --> 00:13:14,217
At last we'll get
to the bottom of...

256
00:13:14,285 --> 00:13:15,652
Welcome back, handsome.

257
00:13:19,173 --> 00:13:21,341
Uh, Marge,
this is my friend Keggy.

258
00:13:21,408 --> 00:13:23,393
Mmm...

259
00:13:23,460 --> 00:13:24,727
Oh...

260
00:13:58,295 --> 00:14:02,298
In this fantasy Kwik-E-Mart,
you get your change in bacon.

261
00:14:02,366 --> 00:14:03,516
Whoo-hoo!

262
00:14:08,906 --> 00:14:11,774
In this place mothers
are for drunk driving.

263
00:14:11,842 --> 00:14:14,043
Chug! Chug! Chug!

264
00:14:14,111 --> 00:14:15,878
Hey Dad, if this is
your fantasy world,

265
00:14:15,946 --> 00:14:17,430
how come Flanders is here?

266
00:14:17,498 --> 00:14:18,831
Hi-diddly-ho, dream team!

267
00:14:20,234 --> 00:14:21,601
Where's my God now?

268
00:14:23,170 --> 00:14:24,454
Homie!

269
00:14:24,521 --> 00:14:26,689
We're here to find answers
to your problem.

270
00:14:26,757 --> 00:14:28,324
We know there's a
marriage in trouble,

271
00:14:28,392 --> 00:14:29,892
and it has something
to do with fish.

272
00:14:31,061 --> 00:14:32,595
Oh, lighten up, Marge.

273
00:14:32,663 --> 00:14:34,364
I take you
to the Disneyland of me,

274
00:14:34,431 --> 00:14:36,532
and you just want
to go to the lost and found.

275
00:14:36,600 --> 00:14:37,767
Well, guess what?

276
00:14:37,835 --> 00:14:40,169
We're staying
in this dream forever.

277
00:14:44,675 --> 00:14:47,643
Whoo-hoo!
Wheeee!

278
00:14:47,711 --> 00:14:49,779
I'm in me!

279
00:14:49,847 --> 00:14:52,315
Oh, I love the down part!

280
00:14:52,383 --> 00:14:53,850
Here it comes! Whoo-hoo!

281
00:14:59,723 --> 00:15:02,992
Unplug these people,
Dr. Ker-dork-ian!

282
00:15:03,060 --> 00:15:05,211
Oh, you foolish man,
if I unhook them now,

283
00:15:05,279 --> 00:15:07,814
I won't know if this
is safe to use on chimps!

284
00:15:07,881 --> 00:15:09,215
All right,
I'll do it myself.

285
00:15:09,283 --> 00:15:11,117
Give me that, give me it!
No!

286
00:15:11,185 --> 00:15:12,268
Oh, the largeness!

287
00:15:35,426 --> 00:15:36,759
Quick!

288
00:15:36,827 --> 00:15:38,127
Gum up the gears with Moes.

289
00:15:42,850 --> 00:15:44,584
Hey, you know what's good for
cleaning Moe gunk

290
00:15:44,651 --> 00:15:45,685
out of your gears?

291
00:15:45,753 --> 00:15:47,153
White vinegar. Yeah.

292
00:15:59,960 --> 00:16:01,797
Uh, we're goona be here
a long time.

293
00:16:21,494 --> 00:16:24,042
Oh Death, you're a life saver.

294
00:16:31,369 --> 00:16:32,541
Thank you, Death.

295
00:16:32,566 --> 00:16:35,035
May I ask, what's
taken so long with Larry King?

296
00:16:35,060 --> 00:16:36,773
I am not...

297
00:16:37,612 --> 00:16:38,345
Death.

298
00:16:38,370 --> 00:16:39,280
Mom?!

299
00:16:39,305 --> 00:16:40,505
Grandma

300
00:16:40,530 --> 00:16:42,135
So your alive?

301
00:16:42,160 --> 00:16:45,202
No. But I live on
in Homer's dreams.

302
00:16:45,227 --> 00:16:47,628
Just like my hair.

303
00:16:47,653 --> 00:16:50,152
It's Jennifer Aniston's hair
on Friends.

304
00:16:50,177 --> 00:16:53,560
Exactly like Chandler,
always criticizing.

305
00:16:54,330 --> 00:16:56,222
Now, I have something
to show you.

306
00:16:56,247 --> 00:16:59,437
Something that just
might help Homer wake up dry.

307
00:17:01,425 --> 00:17:02,925
Hmm.
Wow.

308
00:17:04,940 --> 00:17:06,140
Roomy.

309
00:17:09,646 --> 00:17:11,329
While you boys are out
playing in the boat

310
00:17:11,354 --> 00:17:13,573
I'll go to the store
and pick us up some dinner.

311
00:17:13,598 --> 00:17:15,803
No need. With two Simpson
men in the boat

312
00:17:15,828 --> 00:17:18,715
will bring you back
a pile of fish.

313
00:17:18,740 --> 00:17:20,773
I'm in charge
of the tackle box

314
00:17:24,478 --> 00:17:26,472
Ha-ha. And we're off

315
00:17:26,497 --> 00:17:27,697
Bye.

316
00:17:28,485 --> 00:17:31,456
Man Homer, you've
always been a loser.

317
00:17:31,949 --> 00:17:34,547
Why you little...
there's nothing in this dream

318
00:17:34,572 --> 00:17:36,591
world that can't strangle you.

319
00:17:38,172 --> 00:17:41,890
Um, I hear you and mommy
yelling again last night.

320
00:17:41,915 --> 00:17:43,952
Oh, no, no.

321
00:17:43,977 --> 00:17:46,077
It was just a TV show.

322
00:17:46,112 --> 00:17:49,214
Mitch Miller was yelling
at one of his idiot singers.

323
00:17:49,282 --> 00:17:51,850
You just concentrate
on catching the fish

324
00:17:51,917 --> 00:17:55,020
and not whether there'll be
someone there to cook it.

325
00:17:55,087 --> 00:17:56,321
I got a bite!

326
00:17:56,389 --> 00:17:58,089
We got a fish!
We got a fish!

327
00:17:58,140 --> 00:17:59,941
All right,
settle down there, boy.

328
00:18:00,026 --> 00:18:01,760
After we eat it,
can we let it go?

329
00:18:01,827 --> 00:18:03,128
Can we catch
a submarine?

330
00:18:07,183 --> 00:18:09,351
You got home hours late
with no fish.

331
00:18:09,418 --> 00:18:10,952
It was only
a few weeks later

332
00:18:11,020 --> 00:18:12,704
that I left your father
for good.

333
00:18:12,772 --> 00:18:15,790
You left him because
I tipped the boat over

334
00:18:15,858 --> 00:18:17,525
and ruined the vacation?

335
00:18:17,593 --> 00:18:18,893
Pathetic.

336
00:18:18,961 --> 00:18:20,545
A kid who can't keep
his parents' marriage together

337
00:18:20,613 --> 00:18:21,946
is no kid at all.

338
00:18:22,014 --> 00:18:23,098
It's true!

339
00:18:23,165 --> 00:18:25,633
I failed the basic duty
of childhood.

340
00:18:27,003 --> 00:18:28,570
And then when
I took Bart fishing,

341
00:18:28,637 --> 00:18:31,706
it all came back, and the guilt
made me wet the bed.

342
00:18:31,774 --> 00:18:33,625
Case closed.

343
00:18:33,693 --> 00:18:34,759
Mm-hmm.

344
00:18:34,827 --> 00:18:36,761
Now, let's return
our dream skis.

345
00:18:36,829 --> 00:18:38,396
Case not closed.
Hmm?

346
00:18:38,464 --> 00:18:41,016
Homer, you have nothing
to feel guilty about,

347
00:18:41,083 --> 00:18:42,884
and I can show you.

348
00:18:42,952 --> 00:18:43,852
Roll the
film, Cletus.

349
00:18:43,919 --> 00:18:45,286
"Roll the film, Cletus. "

350
00:18:45,354 --> 00:18:46,454
"Kiss me, Cletus. "

351
00:18:46,522 --> 00:18:49,057
Whatever you say, boss lady.

352
00:18:50,259 --> 00:18:51,876
Sorry, Mona.

353
00:18:51,944 --> 00:18:54,679
We had more of an adventure
than we planned on.

354
00:18:54,747 --> 00:18:56,081
It's okay, Abe.

355
00:18:56,148 --> 00:18:58,516
You brought back
the only treasure I care about.

356
00:19:00,536 --> 00:19:03,638
And when the time came that
I had to leave your father,

357
00:19:03,706 --> 00:19:05,523
I knew you were
in good hands.

358
00:19:05,591 --> 00:19:06,875
I'm cured!

359
00:19:06,959 --> 00:19:08,843
I'll never
wet the bed again.

360
00:19:08,928 --> 00:19:10,745
And maybe you'll stop
overeating, too.

361
00:19:10,813 --> 00:19:12,147
No can do, baby.

362
00:19:15,968 --> 00:19:17,869
And never
forget, Homer,

363
00:19:17,937 --> 00:19:21,756
the three of us will always
be together, in your memory.

364
00:19:21,824 --> 00:19:23,608
Right next to
the movie trivia.

365
00:19:23,676 --> 00:19:25,510
Stanley Kubrick
wanted Robin Williams

366
00:19:25,578 --> 00:19:27,095
to star in The Shining.

367
00:19:27,163 --> 00:19:30,498
Casablanca was originally
going to star Ronald Reagan.

368
00:19:30,566 --> 00:19:32,934
There was a Grease 2,
and I wasn't in it.

369
00:19:33,002 --> 00:19:34,002
Whoa!
Hey, watch it!

370
00:19:34,070 --> 00:19:35,320
There you go again.
Shazbot!

371
00:19:35,371 --> 00:19:39,591
And now you'd better
run along, sweetheart.

372
00:19:39,658 --> 00:19:41,126
Good-bye.

373
00:19:43,779 --> 00:19:47,098
Hmm?

374
00:19:47,166 --> 00:19:48,767
Huh?

375
00:19:55,541 --> 00:19:57,542
Whoo-hoo! I'm dry!

376
00:19:57,610 --> 00:20:00,478
Come on, everybody,
feel Daddy's underpants!

377
00:20:00,546 --> 00:20:02,080
We'll take your
word for it.

378
00:20:03,482 --> 00:20:04,849
So what do I do again?

379
00:20:04,917 --> 00:20:06,184
Spin the top.

380
00:20:06,252 --> 00:20:08,436
If it falls over,
we're in reality.

381
00:20:08,504 --> 00:20:11,039
If it spins forever,
we're still in a dream.

382
00:20:11,107 --> 00:20:11,973
All right.

383
00:20:14,376 --> 00:20:15,910
Look at it go!

384
00:20:15,978 --> 00:20:17,412
All right!

385
00:20:17,480 --> 00:20:18,813
Hey, since it's a dream,

386
00:20:18,881 --> 00:20:21,116
let's ride our bikes naked
around town.

387
00:20:21,356 --> 00:20:23,124
Sounds like a plan.

388
00:20:29,114 --> 00:20:30,414
Is that hail coming down?

389
00:20:30,482 --> 00:20:31,549
It's just dream hail.

390
00:20:31,617 --> 00:20:32,817
Ow! Ooh! Ow!

391
00:20:32,884 --> 00:20:35,481
Dream hail!

392
00:20:35,556 --> 00:20:37,157
Hey, there's a dream truck.

393
00:20:37,225 --> 00:20:38,892
Ow!

394
00:20:42,181 --> 00:20:46,912
<i>When you were little,
you dreamed you were big</i>

395
00:20:47,012 --> 00:20:51,883
<i>You must have been something,
a real tiny kid</i>

396
00:20:51,950 --> 00:20:56,587
<i>When you were big
and needed advice</i>

397
00:20:56,655 --> 00:20:59,090
<i>You reached for your mom</i>

398
00:20:59,158 --> 00:21:03,389
<i>You dreamed me alive</i>

399
00:21:04,809 --> 00:21:09,999
<i>And your dream had a name</i>

400
00:21:10,024 --> 00:21:15,721
<i>And the name told your story</i>

401
00:21:15,746 --> 00:21:20,265
<i>It's called, growing up</i>

402
00:21:20,290 --> 00:21:25,420
<i>You're the dream operator</i>

