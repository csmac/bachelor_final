﻿1
00:00:10,259 --> 00:00:11,099
<i>Who is it?</i>

2
00:00:11,100 --> 00:00:14,778
<i>- What's going on?- My cat...
give her back.</i>

3
00:00:14,779 --> 00:00:18,008
<i>If Bok Gil were really Haru, then I
think I could really sleep comfortably.</i>

4
00:00:18,009 --> 00:00:20,878
<i>If we do a DNA test with Haru's fur and
Bok Gil's fur, we will be able to know.</i>

5
00:00:20,879 --> 00:00:22,208
<i>Can I really do this?</i>

6
00:00:22,209 --> 00:00:25,168
If we do the DNA test, we can
find out if Bok Gil is Haru.

7
00:00:25,169 --> 00:00:27,488
<i>Don't appear in front of me ever again.</i>

8
00:00:27,489 --> 00:00:30,448
<i>You submitted only your name to the
competition, and you have nothing to say?</i>

9
00:00:30,449 --> 00:00:34,398
<i>Do you have proof? Do you have
proof that it's really yours?!</i>

10
00:00:34,399 --> 00:00:36,238
<i>I don't need a friend like you.</i>

11
00:00:36,239 --> 00:00:39,908
<i>The collar I made Haru is a one-of-a-kind.</i>

12
00:00:39,909 --> 00:00:43,259
<i>The fact that Bok Gil has
it on doesn't make sense.</i>

13
00:00:44,559 --> 00:00:46,438
You anticipated that. Why
don't we go check it out.

14
00:00:46,439 --> 00:00:50,398
<i>The test results are out.
Bok Gil is not Haru.</i>

15
00:00:50,399 --> 00:00:52,768
<i>I think I'll be able to
sleep really well tonight.</i>

16
00:00:52,769 --> 00:00:57,889
<i>If Jong Hyeon knows that Bok Gil is
Haru, won't he probably hate me?</i>

17
00:01:03,729 --> 00:01:07,398
<i>It looks as though Bok
Gil was Haru after all.</i>

18
00:01:07,399 --> 00:01:10,458
<i>Do you want to see Bok Gil?</i>

19
00:01:10,459 --> 00:01:13,748
<i>Episode 7</i>Bok Gil, open your eyes.

20
00:01:13,749 --> 00:01:15,508
Listen to me carefully.

21
00:01:15,509 --> 00:01:17,538
Bok Gil.

22
00:01:17,539 --> 00:01:21,408
Bok Gil, you're listening to me, right?

23
00:01:21,409 --> 00:01:26,508
You are... mmm... Bok Gil,
but Haru at the same time.

24
00:01:26,509 --> 00:01:29,648
<i>Ah, I'm sleepy.</i>

25
00:01:29,649 --> 00:01:32,788
I can't tell if this world has
really gotten better or not.

26
00:01:32,789 --> 00:01:37,448
Even a cat can get a DNA test.

27
00:01:37,449 --> 00:01:40,768
You were too young, so you
probably don't remember.

28
00:01:40,769 --> 00:01:43,858
The fact that you are Haru has
been proven scientifically.

29
00:01:43,859 --> 00:01:46,648
<i>Stop talking nonsense and go away.</i>

30
00:01:46,649 --> 00:01:51,838
After losing you, Na Woo has not been
able to have a comfortable night's sleep.

31
00:01:51,839 --> 00:01:55,578
So, Na Woo will be coming to visit you
once in a while. Do you understand?

32
00:01:55,579 --> 00:01:59,909
Okay? Why do you keep on calling me Haru?
Are you talking in your sleep?

33
00:02:02,349 --> 00:02:04,239
She must be here.

34
00:02:13,049 --> 00:02:14,729
Hi!

35
00:02:17,689 --> 00:02:20,449
<i>What? She's here again?</i>

36
00:02:24,079 --> 00:02:32,079
<i>♪13: The One in My Heart Is You.</i>

37
00:02:32,839 --> 00:02:34,578
Bok Gil.

38
00:02:34,579 --> 00:02:37,658
<i>Let me go. Let me go!</i>

39
00:02:37,659 --> 00:02:41,218
You wanted to do this so badly?

40
00:02:41,219 --> 00:02:43,818
Giving Haru a bath was my wish.

41
00:02:43,819 --> 00:02:48,398
<i>I don't like taking baths! Don't touch me!</i>

42
00:02:48,399 --> 00:02:50,308
Cover Bok Gil's ears.

43
00:02:50,309 --> 00:02:54,708
Hey, Bok Gil hates bath,
so let's do it quickly.

44
00:02:54,709 --> 00:02:56,838
No. You have to do it thoroughly.

45
00:02:56,839 --> 00:02:59,698
<i>So, you both are going to
keep doing this? Move!</i>

46
00:02:59,699 --> 00:03:02,249
Ah! Hurry! Hurry!

47
00:03:03,659 --> 00:03:05,598
Bok Gil!

48
00:03:05,599 --> 00:03:08,079
You're all wet!

49
00:03:10,469 --> 00:03:13,559
Hey, you dry off first.

50
00:03:15,759 --> 00:03:18,479
I'll bring you something to change into.

51
00:03:22,139 --> 00:03:26,119
Ah, I-I'll do it.

52
00:03:30,309 --> 00:03:33,979
Bok Gil, Unni brought you presents.

53
00:03:36,739 --> 00:03:38,658
Hey, what's that?

54
00:03:38,659 --> 00:03:42,248
What else? Presents for Bok Gil.

55
00:03:42,249 --> 00:03:45,798
Ah, that's enough. Enough, enough.

56
00:03:45,799 --> 00:03:48,288
Why? Bok Gil likes it too.

57
00:03:48,289 --> 00:03:52,398
We can only accept one item from that box.

58
00:03:52,399 --> 00:03:53,249
Just one?

59
00:03:53,250 --> 00:03:57,458
Why don't we take the rest
to the animal shelter.

60
00:03:57,459 --> 00:04:01,488
I wanted to do all the things I
couldn't do during the past 7 years.

61
00:04:01,489 --> 00:04:05,488
You don't have to do that. There's plenty
of time to do things in the future.

62
00:04:05,489 --> 00:04:11,028
And, this may be Bok Gil's house,
but it's also my space as well.

63
00:04:11,029 --> 00:04:15,788
You're right. I can't just bring in anything
I want to someone else's home. Sorry.

64
00:04:15,789 --> 00:04:18,868
- Especially, there.- The desk?

65
00:04:18,869 --> 00:04:24,258
When I'm working, never,
ever touch anything.

66
00:04:24,259 --> 00:04:29,578
I didn't even think that things could get
uncomfortable because of me. Sorry for that too.

67
00:04:29,579 --> 00:04:34,398
Haru. Bok Gil.

68
00:04:34,399 --> 00:04:38,499
<i>Since you brought me toys,
I will let it slide.</i>

69
00:04:42,359 --> 00:04:44,819
<i>Hold me as much as you want.</i>

70
00:04:49,069 --> 00:04:50,698
Ah, please! Seriously!

71
00:04:50,699 --> 00:04:53,858
Wait, you're saying Bok Gil
was really Na Woo's cat?

72
00:04:53,859 --> 00:04:56,068
Yeah.

73
00:04:56,069 --> 00:04:59,748
To think that Bok Gil had that
kind of a birth secret story.

74
00:04:59,749 --> 00:05:02,189
It's so touching.

75
00:05:03,059 --> 00:05:05,078
Hey, then, are you dating Na Woo now?

76
00:05:05,079 --> 00:05:07,179
Ah, seriously!

77
00:05:09,839 --> 00:05:12,559
Think however you want.

78
00:05:14,379 --> 00:05:17,798
Well, anyway, it's not like
you have time to even date.

79
00:05:17,799 --> 00:05:20,408
This is an important time in your life since it's
all about whether you get to make a debut or not.

80
00:05:20,409 --> 00:05:26,228
It looks like it's going to be you anyway. Jin Seong
isn't good enough to be competition with you.

81
00:05:26,229 --> 00:05:29,218
He got first place for our
bookstore's bestseller.

82
00:05:29,219 --> 00:05:31,318
Really?

83
00:05:31,319 --> 00:05:33,408
Forget it. Let's go to church.

84
00:05:33,409 --> 00:05:34,888
Church? Why?

85
00:05:34,889 --> 00:05:39,158
You only go during Christmas because the
pretty girls give you good food to eat.

86
00:05:39,159 --> 00:05:42,089
We should pray in the name of the
Father, Son, and The Holy Spirit.

87
00:05:43,889 --> 00:05:46,649
The company said they are
making their decision tomorrow.

88
00:05:47,709 --> 00:05:49,668
My baby...

89
00:05:49,669 --> 00:05:54,428
Editor Dokgo said that she'll be supporting
yours, so we have one supporter for sure.

90
00:05:54,429 --> 00:05:58,028
For the rest, we can just pray about it.

91
00:05:58,029 --> 00:06:01,188
Hey, live nicely every day.

92
00:06:01,189 --> 00:06:03,508
Don't get in trouble with God for going
to him only when you are like this.

93
00:06:03,509 --> 00:06:06,758
This is not the time for
you to be so relaxed.

94
00:06:06,759 --> 00:06:11,398
You don't want to lobby. You don't want
to pray. Then what are you going to do?

95
00:06:11,399 --> 00:06:14,098
I better get to work.

96
00:06:14,099 --> 00:06:16,968
Hey, how can you work in this situation?

97
00:06:16,969 --> 00:06:21,788
You can't even debut, and you have tons of stacked up
drawings to do. Are you going to make origami or what?

98
00:06:21,789 --> 00:06:23,958
Hey! Every time I am talking to you...

99
00:06:23,959 --> 00:06:28,159
Am I a person that has to
always look at your back, huh?

100
00:06:38,919 --> 00:06:42,399
Ah, they are still contacting me.
It's already so late...

101
00:06:43,559 --> 00:06:44,578
Oh, oh!

102
00:06:44,579 --> 00:06:47,438
What are you doing during work hours, Mr.
Part-Timer?

103
00:06:47,439 --> 00:06:49,258
Manager, please give that back.

104
00:06:49,259 --> 00:06:52,478
I'm supposed to get an
important call today. Please.

105
00:06:52,479 --> 00:06:55,618
What is most important to you, Mr.
Part-Timer?

106
00:06:55,619 --> 00:06:57,068
Customer Service.

107
00:06:57,069 --> 00:07:00,988
No. It's your superior's command. Until
you get off I will be holding on to this.

108
00:07:00,989 --> 00:07:02,619
Go on.

109
00:07:05,619 --> 00:07:08,959
Princess Heo's number has
to be in here for sure.

110
00:07:10,099 --> 00:07:13,658
I will try 100,00 times
and certainly find it.

111
00:07:13,659 --> 00:07:15,719
100,000 times.

112
00:07:16,549 --> 00:07:19,659
7224, 7225

113
00:07:20,289 --> 00:07:23,809
Bok Gil's flower has bloomed.

114
00:07:25,099 --> 00:07:31,898
Bok Gil's flower has bloomed.
Bok Gil's flower has bloomed.

115
00:07:31,899 --> 00:07:33,928
<i>You're playing well all by yourself.</i>

116
00:07:33,929 --> 00:07:37,699
Bok Gil's flower has bloomed!

117
00:07:40,989 --> 00:07:43,568
What... you don't feel
like playing right now?

118
00:07:43,569 --> 00:07:47,768
<i>Human, send her away. She's annoying.</i>

119
00:07:47,769 --> 00:07:50,529
Then, should I get you some snacks?

120
00:07:54,179 --> 00:07:55,868
There's no water.

121
00:07:55,869 --> 00:07:59,169
Jong Hyeon, do you have any water?

122
00:08:04,079 --> 00:08:06,618
I'll go buy some.

123
00:08:06,619 --> 00:08:07,658
<i>Where are you going?</i>

124
00:08:07,659 --> 00:08:10,878
<i>Don't leave me alone with this girl.
I don't want it!</i>

125
00:08:10,879 --> 00:08:13,969
Bok Gil. Play well.

126
00:08:15,709 --> 00:08:18,418
Should we play, just the two of us?

127
00:08:18,419 --> 00:08:21,678
<i>Ah! Don't come any closer.</i>

128
00:08:21,679 --> 00:08:24,518
No! If you go up there,
Jong Hyeon will get mad!

129
00:08:24,519 --> 00:08:25,618
<i>I'll do what I want.</i>

130
00:08:25,619 --> 00:08:29,979
Aiyoo, seriously. Get down.

131
00:08:32,139 --> 00:08:34,809
It looks fine.

132
00:08:41,259 --> 00:08:44,378
Ooh, it's good.

133
00:08:44,379 --> 00:08:48,359
<i>Oh. Hey, hey.</i>

134
00:08:59,299 --> 00:09:01,058
What are you doing?

135
00:09:01,059 --> 00:09:04,559
Oh, the thing is...

136
00:09:05,559 --> 00:09:07,358
I told you not to touch my things!

137
00:09:07,359 --> 00:09:09,838
I didn't look at it because I wanted to.

138
00:09:09,839 --> 00:09:14,228
I went to get Bok Gil and saw it.
But, it was so interesting.

139
00:09:14,229 --> 00:09:15,659
That's enough now. Leave.

140
00:09:17,189 --> 00:09:19,248
Aren't you going?

141
00:09:19,249 --> 00:09:23,928
What did I do that was so wrong? You said I
could come over to take care of Bok Gil.

142
00:09:23,929 --> 00:09:26,828
I also told you not to touch my things!

143
00:09:26,829 --> 00:09:28,668
Did I do it on purpose?

144
00:09:28,669 --> 00:09:34,958
While you when to buy water, Bok Gil went on your desk.
I was trying to stop her and I saw your drawings.

145
00:09:34,959 --> 00:09:39,108
Then, was I supposed to keep my eyes closed? Have
you been like this towards Bok Gil all these years?

146
00:09:39,109 --> 00:09:39,669
What?

147
00:09:39,670 --> 00:09:46,569
You seem to be feeling extra sensitive because you
are working. I'll take Bok Gil with me to the cafe.

148
00:09:51,689 --> 00:09:53,068
What the heck are you doing?

149
00:09:53,069 --> 00:09:58,798
Lately, Bok Gil has not been eating well and she's
not doing well. But you want to take her with you?

150
00:09:58,799 --> 00:10:04,919
Are you in your right mind? If you are
going to do this, don't ever come back!

151
00:10:06,319 --> 00:10:09,069
You're really mean!

152
00:10:30,559 --> 00:10:32,428
Yes, Editor.

153
00:10:32,429 --> 00:10:37,218
<i>Jong Hyeon, I think we have to
meet tomorrow. Do you have time?</i>

154
00:10:37,219 --> 00:10:39,999
What is this about?

155
00:10:42,209 --> 00:10:45,968
Hey, hey, hey, hey. Sit down.
Ah, seriously.

156
00:10:45,969 --> 00:10:51,248
Oh, you awesome kid! I
knew you could do it!

157
00:10:51,249 --> 00:10:54,278
- I think your prayers worked. 
- Whatever.

158
00:10:54,279 --> 00:10:57,748
Prayer no prayer, when it's
meant to be it will happen.

159
00:10:57,749 --> 00:11:00,189
I told you, you were
going to make it right?

160
00:11:01,369 --> 00:11:03,249
Thanks.

161
00:11:11,809 --> 00:11:14,679
Writer Hyeon Jong Hyeon.

162
00:11:15,789 --> 00:11:21,478
Writer Hyeon Jong Hyeon.
You must be feeling great!

163
00:11:21,479 --> 00:11:23,559
Next, it's your turn.

164
00:11:24,939 --> 00:11:30,419
I decided to just help out my
father's gas station business.

165
00:11:34,209 --> 00:11:36,948
What do you mean? What about drawing?

166
00:11:36,949 --> 00:11:38,918
Not all dreams come true.

167
00:11:38,919 --> 00:11:44,828
Hey! You haven't even tried that long. Just
keep trying for a little longer. If you do...

168
00:11:44,829 --> 00:11:51,418
Yeah, I was thinking the same thing.
But I know myself too well.

169
00:11:51,419 --> 00:11:53,389
I have no talent.

170
00:11:54,489 --> 00:11:56,508
Are you confident you won't regret it?

171
00:11:56,509 --> 00:12:01,988
No, but is there anything in life
you can guarantee you won't regret?

172
00:12:01,989 --> 00:12:08,659
Sometimes you give up, you regret
at times, and that's just life.

173
00:12:12,669 --> 00:12:16,219
When I was in high school, I really
loved going to the art studio.

174
00:12:17,549 --> 00:12:22,708
You were there, Soo In was there too.

175
00:12:22,709 --> 00:12:26,868
Back then, I loved drawing.

176
00:12:26,869 --> 00:12:31,178
It was good... crazy good.

177
00:12:31,179 --> 00:12:37,608
But now, one of us is no
longer in this world,

178
00:12:37,609 --> 00:12:43,379
the other has given up. Now
there's just you left.

179
00:12:44,829 --> 00:12:50,979
Hey, you know you have to succeed
enough for me and Soo Im.

180
00:13:02,389 --> 00:13:05,169
Writer Hyeon Jong Hyeon, fighting!

181
00:13:17,219 --> 00:13:20,738
<i>Aigoo. It fell.</i>

182
00:13:20,739 --> 00:13:23,559
<i>Should I go down?</i>

183
00:13:33,469 --> 00:13:36,749
<i>Soo In isn't in this world anymore, punk.</i>

184
00:13:39,199 --> 00:13:44,918
♪<i>There is only a faint glimmer ♪</i>

185
00:13:44,919 --> 00:13:46,719
<i>It's all done.</i>

186
00:13:47,509 --> 00:13:51,628
<i>Your drawings are really
good.♪ Strewn on the path</i> ♪<

187
00:13:51,629 --> 00:13:54,778
<i>I like you.</i>

188
00:13:54,779 --> 00:14:02,779
<i>I... don't seem to know anything about you though.
 ♪ The faint glimmer I caught is slipping away ♪</i>

189
00:14:03,019 --> 00:14:05,448
♪<i>like the path to a rainbow </i>♪

190
00:14:05,449 --> 00:14:11,468
<i>But still... you liked me.♪ Somewhere it's waiting
for me but even though I look, I can't find it. ♪</i>

191
00:14:11,469 --> 00:14:16,568
<i>Back then, why couldn't I tell her?
 ♪ Darling, be strong. ♪</i>

192
00:14:16,569 --> 00:14:19,178
<i>That I liked her. ♪ The path
which was given to me ♪</i>

193
00:14:19,179 --> 00:14:25,468
♪<i> So that you can find the path given to
me</i> ♪ subtitles ripped and synced by riri13

194
00:14:25,469 --> 00:14:31,039
♪<i> Darling lead me to the path. </i>♪

195
00:14:32,069 --> 00:14:36,949
<i>Oh my joints... from jumping
off that measly height.</i>

196
00:14:42,209 --> 00:14:47,578
<i>Human, take a look at me. My leg hurts.</i>

197
00:14:47,579 --> 00:14:49,599
Miss Bok Gil.

198
00:14:55,799 --> 00:14:58,418
<i>Just because you are doing this, you
think I'm going to forgive you?</i>

199
00:14:58,419 --> 00:15:01,339
<i>Coming home late meant
you came home late <\i></i>

200
00:15:07,619 --> 00:15:10,299
- Bok Gil.<i>- Hm?</i>

201
00:15:11,649 --> 00:15:17,209
I finally am able to make
my debut, but why... why...

202
00:15:18,239 --> 00:15:21,059
Why am I...? Why?

203
00:15:29,999 --> 00:15:32,118
Why?

204
00:15:32,119 --> 00:15:34,429
<i>Human.</i>

205
00:15:51,059 --> 00:15:56,719
Do you think I'm here because I want to see you,
Jong Hyeon? I'm here because I want to see Bok Gil.

206
00:15:58,869 --> 00:16:04,489
Fine, I did wrong! So forgive me.

207
00:16:05,349 --> 00:16:11,269
No. I'm sorry. Please forgive
me just this once, huh?

208
00:16:13,399 --> 00:16:17,279
Who told you to make your
drawing so interesting anyway!

209
00:16:20,019 --> 00:16:23,849
Fine, let's do this.

210
00:16:25,259 --> 00:16:31,459
<i>What's going on today? My cushion is
staying here for a very long time.</i>

211
00:16:33,019 --> 00:16:36,578
<i>It feels much warmer than normal.</i>

212
00:16:36,579 --> 00:16:38,309
<i>I like it.</i>

213
00:16:39,409 --> 00:16:40,998
- Bok Gil.<i>- Huh?</i>

214
00:16:40,999 --> 00:16:46,118
<i>Human, what's wrong? Why are you so weak?</i>

215
00:16:46,119 --> 00:16:49,558
- You're hungry, right?<i>- No.</i>

216
00:16:49,559 --> 00:16:54,468
<i>You're sick, aren't you? How sick are you?</i>

217
00:16:54,469 --> 00:16:56,858
I'll get you food. Hold on.

218
00:16:56,859 --> 00:17:01,118
<i>No, just stay here. I'm not
here and I don't have to poop.</i>

219
00:17:01,119 --> 00:17:04,079
<i>Just stay still.</i>

220
00:17:08,479 --> 00:17:10,829
<i>I'm fine.</i>

221
00:17:13,809 --> 00:17:15,168
I'm sorry, Bok Gil.

222
00:17:15,169 --> 00:17:17,078
<i>It's fine, it's fine. Huh?</i>

223
00:17:17,079 --> 00:17:18,118
I'll get it for you later.

224
00:17:18,119 --> 00:17:20,869
<i>Yeah, I can wait.</i>

225
00:17:21,879 --> 00:17:23,879
<i>This is a big problem.</i>

226
00:17:25,059 --> 00:17:26,918
<i>Huh?</i>

227
00:17:26,919 --> 00:17:29,508
<i>Excuse me? Is there someone there?</i>

228
00:17:29,509 --> 00:17:32,649
<i>Please help my human. Okay?</i>

229
00:17:39,709 --> 00:17:41,709
Jong Hyeon.

230
00:17:47,429 --> 00:17:49,309
Jong Hyeon.

231
00:17:55,009 --> 00:17:57,239
<i>That's a relief.</i>

232
00:18:27,159 --> 00:18:30,749
<i>That girl. I guess she can be useful.</i>

233
00:18:33,439 --> 00:18:38,359
Jong Hyeon, what is it?
Should I get you water?

234
00:18:46,239 --> 00:18:48,379
Soo In.

235
00:18:54,869 --> 00:18:57,029
Soo In.

236
00:19:01,569 --> 00:19:08,569
<i>♪14 Goodbye, and Hi</i>

237
00:19:25,929 --> 00:19:28,788
- You're up?- Why are you here?

238
00:19:28,789 --> 00:19:31,489
You don't remember yesterday?

239
00:19:33,019 --> 00:19:34,939
<i>Jong Hyeon.</i>

240
00:19:40,739 --> 00:19:44,419
Ah, that was you.

241
00:19:47,319 --> 00:19:50,879
Your fever's gone down.

242
00:19:52,029 --> 00:19:54,558
It was serious last night.

243
00:19:54,559 --> 00:19:57,968
You had a fever, broke out in
a sweat, and mumbled a lot.

244
00:19:57,969 --> 00:19:59,158
Mumbled?

245
00:19:59,159 --> 00:20:01,418
You were calling for someone.

246
00:20:01,419 --> 00:20:04,729
For Soo In.

247
00:20:06,649 --> 00:20:09,109
I did?

248
00:20:10,109 --> 00:20:13,348
Who is Soo In?

249
00:20:13,349 --> 00:20:16,728
You didn't have to do all this.
I'm really sorry.

250
00:20:16,729 --> 00:20:18,588
I think I'm okay now, so...

251
00:20:18,589 --> 00:20:20,658
I should go?

252
00:20:20,659 --> 00:20:22,718
No, just...

253
00:20:22,719 --> 00:20:26,748
I made you some porridge. Can't I stay
until after I see you eating some of it?

254
00:20:26,749 --> 00:20:29,008
I'll eat it later. I have an
appointment to go to today.

255
00:20:29,009 --> 00:20:31,799
You are so stubborn.

256
00:20:35,609 --> 00:20:37,609
Excuse me...

257
00:20:40,929 --> 00:20:43,068
Thank you.

258
00:20:43,069 --> 00:20:45,549
If you know, then it's fine.

259
00:20:56,059 --> 00:21:00,558
- Miss Bok Gil. You were
surprised, right?<i>- Yeah.</i>

260
00:21:00,559 --> 00:21:03,098
<i>- Don't get sick again.</i>- I understand.

261
00:21:03,099 --> 00:21:07,319
I'm sorry. I won't get sick again, Bok Gil.

262
00:21:13,609 --> 00:21:15,688
- Where are you coming from?- Huh?

263
00:21:15,689 --> 00:21:18,758
Gong Ju seemed very angry.

264
00:21:18,759 --> 00:21:20,498
Really?

265
00:21:20,499 --> 00:21:22,929
I should just go home.

266
00:21:23,809 --> 00:21:25,828
You're coughing.

267
00:21:25,829 --> 00:21:27,458
My throat feels a bit scratchy.

268
00:21:27,459 --> 00:21:30,958
I know that once you start
coughing, it lasts a while.

269
00:21:30,959 --> 00:21:35,238
If Gong Ju find out, she going
to send me back to Jejoodo.

270
00:21:35,239 --> 00:21:36,998
Where did you go?

271
00:21:36,999 --> 00:21:39,938
- Were you with Bok Gil again?- Yeah.

272
00:21:39,939 --> 00:21:42,048
With that person?

273
00:21:42,049 --> 00:21:45,568
Jong Hyeon was really sick,
so there was nothing...

274
00:21:45,569 --> 00:21:50,729
But you... why do have to take care of him?

275
00:21:52,059 --> 00:21:53,498
Let's go to the hospital.

276
00:21:53,499 --> 00:21:54,998
I'm okay.

277
00:21:54,999 --> 00:21:57,099
I'm not okay.

278
00:21:59,879 --> 00:22:02,758
<i>PLUS Webtoon Contents</i>

279
00:22:02,759 --> 00:22:07,468
Congratulations Writer Hyeon. I
knew you were going to make it.

280
00:22:07,469 --> 00:22:09,748
Thank you.

281
00:22:09,749 --> 00:22:14,758
From now on, you are going to hate me.

282
00:22:14,759 --> 00:22:19,758
That's how much I'm going
to be pushing you.

283
00:22:19,759 --> 00:22:22,658
You do know about my
evil reputation, right?

284
00:22:22,659 --> 00:22:25,018
You better prepare yourself.

285
00:22:25,019 --> 00:22:28,478
Yes. I'm confident I'll be able to do well.

286
00:22:28,479 --> 00:22:33,538
Up to now, it's been practice. The
real work will be starting now.

287
00:22:33,539 --> 00:22:35,618
So going forward...

288
00:22:35,619 --> 00:22:37,028
Editor Dokgo.

289
00:22:37,029 --> 00:22:38,988
How can I help you Writer Park?

290
00:22:38,989 --> 00:22:41,188
Exactly, what happened here?

291
00:22:41,189 --> 00:22:43,658
Hyeon Jong Hyeon, rather
than I, was selected?

292
00:22:43,659 --> 00:22:46,009
Does this make any sense?

293
00:22:47,359 --> 00:22:49,208
What doesn't make sense?

294
00:22:49,209 --> 00:22:51,409
How could I...?

295
00:22:52,209 --> 00:22:55,958
How can I lose to an amateur like him?

296
00:22:55,959 --> 00:23:01,029
Writer Park. I didn't want
to have to tell you this...

297
00:23:02,449 --> 00:23:04,908
The vote was 8 to 3.

298
00:23:04,909 --> 00:23:08,329
Jong Hyeon was the front runner.

299
00:23:12,269 --> 00:23:14,269
It doesn't make sense.

300
00:23:19,929 --> 00:23:22,129
<i>Hyeon Jong Hyeon</i>

301
00:23:30,599 --> 00:23:34,039
You guys will regret this.

302
00:23:40,429 --> 00:23:42,249
Congratulations.

303
00:23:44,549 --> 00:23:46,429
Thank you.

304
00:23:52,239 --> 00:23:53,879
Here.

305
00:23:56,059 --> 00:23:58,968
Do you have something
you want to say to me?

306
00:23:58,969 --> 00:24:00,569
No.

307
00:24:01,369 --> 00:24:04,058
You told me last time you're
working on a webcomic.

308
00:24:04,059 --> 00:24:05,528
How did that go?

309
00:24:05,529 --> 00:24:07,868
Oh that? I signed the contract today.

310
00:24:07,869 --> 00:24:12,169
Contract? Then your book should
be coming out in no time.

311
00:24:12,179 --> 00:24:15,228
Well, I'm not sure...

312
00:24:15,229 --> 00:24:18,508
With Writer Park, the webtoon...

313
00:24:18,509 --> 00:24:21,228
Once the webtoon is a hit, the
book comes out shortly thereafter.

314
00:24:21,229 --> 00:24:24,528
Yes, that usually is how things work.

315
00:24:24,529 --> 00:24:27,488
Do you have any issues
working part-time here?

316
00:24:27,489 --> 00:24:31,468
Do you know why I've always spoken to
you with honorifics, Mr. Part Timer?

317
00:24:31,469 --> 00:24:35,458
Ah! That's why you asked
for time off that time.

318
00:24:35,459 --> 00:24:40,018
Then you should have told me more clearly in
detail. I defintiely would have let you go.

319
00:24:40,019 --> 00:24:41,618
- Oh, what is it?- My arm, my arm.

320
00:24:41,619 --> 00:24:44,338
- Ah, this too.- Ah, this.

321
00:24:44,339 --> 00:24:46,508
Would you like to take a vacation day?
A day? Two? Or indefinitely?

322
00:24:46,509 --> 00:24:49,608
No... I mean, I can give you
as many days as you need.

323
00:24:49,609 --> 00:24:53,108
Well, that would be great for me.
I was needing more time to write.

324
00:24:53,109 --> 00:24:56,298
You know, I have an amazing item.

325
00:24:56,299 --> 00:24:59,418
It is such an entertaining story.

326
00:24:59,419 --> 00:25:01,158
A dog is the main character.

327
00:25:01,159 --> 00:25:03,958
But the dog can speak it's thoughts.
What do you think?

328
00:25:03,959 --> 00:25:06,028
Bok Gil, look.

329
00:25:06,029 --> 00:25:09,808
It's not enough with just the grooming.
Cats need baths.

330
00:25:09,809 --> 00:25:11,278
See, you have to take a bath!

331
00:25:11,279 --> 00:25:13,778
<i>No, I hate taking a bath.</i>

332
00:25:13,779 --> 00:25:17,078
Here.

333
00:25:17,079 --> 00:25:19,958
Is this cats cafe's concept is camping?

334
00:25:19,959 --> 00:25:21,638
You want to go?

335
00:25:21,639 --> 00:25:23,398
<i>Yeah, it looks fun.</i>

336
00:25:23,399 --> 00:25:25,978
- Wow, it looks nice.<i>- Ohhh.</i>

337
00:25:25,979 --> 00:25:27,248
<i>It looks nice.</i>

338
00:25:27,249 --> 00:25:29,488
Ah, hey, let go, let go.

339
00:25:29,489 --> 00:25:31,178
What are you going to buy me?

340
00:25:31,179 --> 00:25:33,188
What? Buy what? You should pay.

341
00:25:33,189 --> 00:25:35,768
Wow you are something.

342
00:25:35,769 --> 00:25:39,959
Noonim, give us two of your
most expensive coffee.

343
00:25:42,169 --> 00:25:44,848
I don't see Na Woo today.

344
00:25:44,849 --> 00:25:47,868
<i>Is she busy preparing for her exhibition?</i>

345
00:25:47,869 --> 00:25:51,448
Have you told Na Woo? She
would be so excited for you.

346
00:25:51,449 --> 00:25:54,648
Later. I can just tell her later.

347
00:25:54,649 --> 00:25:59,569
Wow. If Soo In found out you
debuted, she would be ecstatic.

348
00:26:01,589 --> 00:26:04,348
I wish I could have been there.

349
00:26:04,349 --> 00:26:07,908
What did Jin Seong's face look like?
He was totally frustrated, right?

350
00:26:07,909 --> 00:26:10,628
Regardless, he always has
that frustrated look.

351
00:26:10,629 --> 00:26:13,048
He must have looked even more frustrated.

352
00:26:13,049 --> 00:26:14,788
Tell me more specifically.

353
00:26:14,789 --> 00:26:16,848
How many times do I need to
tell you that he just went out.

354
00:26:16,849 --> 00:26:20,708
How can you lack descriptive
ability being a webtoon writer?

355
00:26:20,709 --> 00:26:23,138
Look... He'd say, "No way, that can't
be"... with his nostrils flaring.

356
00:26:23,139 --> 00:26:25,849
Stop it. Stop it Yook Hae Gong.

357
00:26:27,859 --> 00:26:30,778
Look like something really
exciting has happened.

358
00:26:30,779 --> 00:26:35,058
Of course! It's the best day
for Writer Hyeon Jong Hyeon.

359
00:26:35,059 --> 00:26:37,359
- Is that so?- Yes.

360
00:26:38,739 --> 00:26:42,858
I've tried to be patient, but
listen here Mr. Hyeon Jong Hyeon!

361
00:26:42,859 --> 00:26:43,709
W-what?

362
00:26:43,710 --> 00:26:48,309
Because she took care of you all night,
do you know she had to go to the ER?

363
00:26:49,459 --> 00:26:51,658
The ER?

364
00:26:51,659 --> 00:26:53,058
What? Sick?

365
00:26:53,059 --> 00:26:55,968
Who? Na Woo took care of you?

366
00:26:55,969 --> 00:26:56,909
You were sick, punk?

367
00:26:56,910 --> 00:27:00,158
You! Please shut your mouth.

368
00:27:00,159 --> 00:27:03,238
Excuse me, is Na Woo really sick?

369
00:27:03,239 --> 00:27:06,348
If you don't see someone, should you
be concerned about their health first?

370
00:27:06,349 --> 00:27:09,238
How can you be so cruel to someone
who took care of you all night!

371
00:27:09,239 --> 00:27:11,688
I-I-I'm sorry.

372
00:27:11,689 --> 00:27:13,238
Is Na Woo at the hospital right now?

373
00:27:13,239 --> 00:27:15,278
She was released.

374
00:27:15,279 --> 00:27:19,358
Do you know that Na Woo used to be
very sick when she was a child?

375
00:27:19,359 --> 00:27:21,038
I heard.

376
00:27:21,039 --> 00:27:24,068
Can I perhaps meet with Na Woo now?

377
00:27:24,069 --> 00:27:25,548
Forget it.

378
00:27:25,549 --> 00:27:29,468
I should have stopped her when she was
following you around like a lost puppy.

379
00:27:29,469 --> 00:27:31,389
So upsetting.

380
00:27:34,529 --> 00:27:38,649
What are we going to do? It looks
like Na Woo is really sick.

381
00:28:35,199 --> 00:28:36,568
<i>What the heck are you doing?</i>

382
00:28:36,569 --> 00:28:41,658
<i>Lately, Bok Gil has not been eating well and she's
not doing well. But you want to take her with you?</i>

383
00:28:41,659 --> 00:28:44,658
<i>You. If you're going to be like
this, don't come here again.</i>

384
00:28:44,659 --> 00:28:47,399
<i>You're really mean!</i>

385
00:28:49,999 --> 00:28:54,629
<i>Because she took care of you all night,
do you know she had to go to the ER?</i>

386
00:29:00,299 --> 00:29:02,598
<i>Oh Na Woo</i>

387
00:29:02,599 --> 00:29:05,599
<i>Who are you thinking about?</i>

388
00:29:07,819 --> 00:29:09,419
Soo In.

389
00:29:10,149 --> 00:29:14,238
<i>I'm a little jealous.</i>

390
00:29:14,239 --> 00:29:19,329
<i>Rather than me, you're
thinking of a different girl.</i>

391
00:29:22,829 --> 00:29:25,138
It's not like that.

392
00:29:25,139 --> 00:29:28,349
<i>You look good.</i>

393
00:29:29,319 --> 00:29:31,438
Me?

394
00:29:31,439 --> 00:29:36,809
<i>You laugh a lot more than before. And
you know how to care for someone.</i>

395
00:29:37,559 --> 00:29:44,829
<i>It's a relief that you aren't alone now.</i>

396
00:29:50,359 --> 00:29:52,289
I'm sorry.

397
00:29:53,429 --> 00:29:56,018
For sending you off like that.

398
00:29:56,019 --> 00:30:02,689
<i>No, I'm sorry. For leaving like that.</i>

399
00:30:03,779 --> 00:30:05,559
Soo In.

400
00:30:07,789 --> 00:30:11,679
<i>Right now, you have someone
you're worrying about, right?</i>

401
00:30:14,869 --> 00:30:18,629
<i>You are curious about her
because you miss her.</i>

402
00:30:22,599 --> 00:30:25,899
<i>Call her. Hurry.</i>

403
00:30:29,059 --> 00:30:35,459
<i>Oh Na Woo</i>

404
00:30:45,519 --> 00:30:50,018
<i>Jong Hyeon. Jong Hyeon. Hello?</i>

405
00:30:50,019 --> 00:30:53,428
<i>Hey, if you called, then talk!</i>

406
00:30:53,429 --> 00:30:57,249
<i>Jong Hyeon, where are you right now?</i>

407
00:31:23,009 --> 00:31:29,948
♪<i> After a tiring day has passed two people
in the moonlight form a single shadow</i>♪

408
00:31:29,949 --> 00:31:34,948
- Did you run here again?  - I
thought you might just leave.

409
00:31:34,949 --> 00:31:36,488
I told you we can see each other tomorrow.

410
00:31:36,489 --> 00:31:40,549
No, my cold's gone. It's really gone.

411
00:31:41,969 --> 00:31:48,519
♪<i> Happiness that seems reachable
is still faintly there</i> ♪

412
00:31:49,949 --> 00:31:55,178
♪<i> But your sorrowful heart </i>♪

413
00:31:55,179 --> 00:31:59,988
I'm okay though. ♪<i> casts a
shadow even over your dream</i> ♪

414
00:31:59,989 --> 00:32:03,168
When you are healed from your cold,.
 ♪ Remember so that even if it hurts, ♪

415
00:32:03,169 --> 00:32:05,428
let's go ride our bicycles. ♪ <i>will
be next to the one who loves you.</i> ♪

416
00:32:05,429 --> 00:32:09,128
- How much are you planning on yelling at me
this time? - Forget it if you don't like it.

417
00:32:09,129 --> 00:32:12,469
No... teach me.

418
00:32:14,599 --> 00:32:21,138
Okay then.  ♪ <i>Sometimes, even if
the path appears too far away</i> ♪

419
00:32:21,139 --> 00:32:23,038
It's been a long time since I've ridden this. It's fun.
 <i>even if within your sad heart you are shedding tears,</i>

420
00:32:23,039 --> 00:32:29,259
♪ <i>even if within your mournful
heart you are shedding tears,</i> ♪

421
00:32:33,259 --> 00:32:36,719
Miss Bok Gil, I'm here.

422
00:32:38,399 --> 00:32:43,279
Bok Gil. Miss Bok Gil.

423
00:32:45,119 --> 00:32:47,179
Where did she go now?

424
00:32:48,789 --> 00:32:52,059
Miss Bok Gil, are you there?

425
00:32:54,349 --> 00:32:56,139
Bok Gil...

426
00:32:58,139 --> 00:33:02,558
Bok Gil! What's wrong with you?

427
00:33:02,559 --> 00:33:05,528
Bok Gil! Bok Gil! What's wrong with you?

428
00:33:05,529 --> 00:33:07,948
- Bok Gil!<i>- Human.</i>

429
00:33:07,949 --> 00:33:10,179
<i>What's wrong with me?</i>

430
00:33:32,689 --> 00:33:35,289
♪<i> open your eyes and shout </i>♪

431
00:33:38,859 --> 00:33:41,168
<i>This is totally awesome.</i>

432
00:33:41,169 --> 00:33:43,928
<i>Congratulations, writer Hyeon Jong Hyeon.</i>

433
00:33:43,929 --> 00:33:44,879
<i>Thank you.</i>

434
00:33:44,880 --> 00:33:47,738
<i>I saw "A Beautiful Mind."</i>

435
00:33:47,739 --> 00:33:51,358
<i>- And what... - It was fun.</i>

436
00:33:51,359 --> 00:33:53,478
<i>Thank you for all this time.</i>

437
00:33:53,479 --> 00:33:55,068
<i>If you're really thankful,</i>

438
00:33:55,069 --> 00:34:00,148
<i>To the goddess, try to package me nicely...</i>

439
00:34:00,149 --> 00:34:01,778
<i>Are you the next story?.</i>

440
00:34:01,779 --> 00:34:06,888
<i>Bok Gil, you must be so happy because
there are so many people who love you.</i>

441
00:34:06,889 --> 00:34:09,908
<i>Each and every day...</i>

442
00:34:09,909 --> 00:34:12,068
<i>is precious.</i>

443
00:34:12,069 --> 00:34:14,728
<i>Miss Bok Gil, should we start?</i>

444
00:34:14,729 --> 00:34:17,028
<i>Are they real stars?</i>

445
00:34:17,029 --> 00:34:19,758
<i>Endure it just a bit longer!</i>

446
00:34:19,759 --> 00:34:21,648
<i>You're a veterinarian.</i>

447
00:34:21,649 --> 00:34:24,088
<i>Surgery...</i>

448
00:34:24,089 --> 00:34:28,059
<i>Please do it.</i>

