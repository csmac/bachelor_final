﻿1
00:00:02,875 --> 00:00:05,386
ANNOUNCER: <i>The National
Football League welcomes you</i>

2
00:00:05,412 --> 00:00:09,173
<i>to the Pepsi Super Bowl
XLIX Halftime Show.</i>

3
00:00:09,198 --> 00:00:11,198
(music playing)

4
00:00:11,198 --> 00:00:16,198
<font color="#ffff00">Sync and Corrected by er1ck9</font>
<font color="#ffffff">www.addic7ed.com</font>

5
00:00:16,206 --> 00:00:18,206
("Roar" by Katy Perry)

6
00:00:19,132 --> 00:00:21,132
(music continues)

7
00:00:21,158 --> 00:00:24,858
Super Bowl, are you ready to rock?

8
00:00:27,802 --> 00:00:28,834
KATY PERRY:
<i>Come on!</i>

9
00:00:31,260 --> 00:00:33,964
<i>♪ I used to bite my tongue
and hold my breath ♪</i>

10
00:00:33,965 --> 00:00:36,695
<i>♪ Scared to rock the boat
and make a mess ♪</i>

11
00:00:36,700 --> 00:00:38,791
<i>♪ So I sit quietly ♪</i>

12
00:00:39,378 --> 00:00:41,580
<i>♪ Agreed politely ♪</i>

13
00:00:41,604 --> 00:00:44,307
<i>♪ I guess that I forgot I had a choice ♪</i>

14
00:00:44,326 --> 00:00:47,210
<i>♪ I let you push me past
the breaking point ♪</i>

15
00:00:47,214 --> 00:00:49,217
<i>♪ I stood for nothing ♪</i>

16
00:00:49,574 --> 00:00:51,778
<i>♪ So I fell for everything ♪</i>

17
00:00:51,889 --> 00:00:54,850
<i>♪ You held me down but I got up ♪</i>

18
00:00:54,853 --> 00:00:57,125
<i>♪ Already brushing off the dust ♪</i>

19
00:00:57,128 --> 00:01:00,055
<i>♪ You hear my voice,
you hear that sound ♪</i>

20
00:01:00,056 --> 00:01:02,355
<i>♪ Like thunder gonna shake the ground ♪</i>

21
00:01:02,358 --> 00:01:05,465
<i>♪ You held me down but I got up ♪</i>

22
00:01:05,466 --> 00:01:07,620
<i>♪ Get ready 'cause I've had enough ♪</i>

23
00:01:07,622 --> 00:01:08,975
<i>♪ I see it all ♪</i>

24
00:01:08,977 --> 00:01:09,977
<i>♪ I see it now ♪</i>

25
00:01:10,059 --> 00:01:14,770
<i>♪ I got the eye of the tiger, a fighter ♪</i>

26
00:01:14,771 --> 00:01:17,110
<i>♪ Dancing through the fire ♪</i>

27
00:01:17,112 --> 00:01:20,045
<i>♪ 'Cause I am a champion ♪</i>

28
00:01:20,048 --> 00:01:23,819
<i>♪ And you're gonna hear me roar ♪</i>

29
00:01:23,981 --> 00:01:27,762
<i>♪ Louder, louder than a lion ♪</i>

30
00:01:27,765 --> 00:01:30,745
<i>♪ 'Cause I am a champion ♪</i>

31
00:01:30,747 --> 00:01:34,243
<i>♪ And you're gonna hear me roar ♪</i>

32
00:01:41,659 --> 00:01:45,404
<i>♪ You're gonna hear me... ♪</i>
(roaring)

33
00:01:49,429 --> 00:01:51,663
("Dark Horse" by Katy Perry and Juicy J)

34
00:01:52,454 --> 00:01:55,899
(music playing)

35
00:02:06,507 --> 00:02:08,293
<i>♪ I knew you were. ♪</i>

36
00:02:08,319 --> 00:02:10,456
<i>♪ You were gonna come to me ♪</i>

37
00:02:10,456 --> 00:02:12,155
<i>♪ And here you are ♪</i>

38
00:02:12,181 --> 00:02:13,924
<i>♪ But you better choose carefully ♪</i>

39
00:02:13,924 --> 00:02:17,590
<i>♪ ‘Cause I, I’m capable of anything ♪</i>

40
00:02:17,616 --> 00:02:21,256
<i>♪ Of anything and everything ♪</i>

41
00:02:21,256 --> 00:02:24,817
<i>♪ Make me your Aphrodite ♪</i>

42
00:02:24,843 --> 00:02:28,377
<i>♪ Make me your one and only ♪</i>

43
00:02:28,377 --> 00:02:32,943
<i>♪ But don’t make me your enemy, ♪</i>

44
00:02:32,969 --> 00:02:36,721
<i>♪ your enemy, your enemy ♪</i>

45
00:02:36,721 --> 00:02:40,391
<i>♪ So you wanna play with magic ♪</i>

46
00:02:40,417 --> 00:02:41,986
<i>♪ Boy, you should know ♪</i>

47
00:02:42,012 --> 00:02:43,868
<i>♪ what you're falling for ♪</i>

48
00:02:43,868 --> 00:02:46,868
<i>♪ Baby do you dare to do this? ♪</i>

49
00:02:46,894 --> 00:02:51,474
<i>♪ Cause I’m coming at
you like a dark horse ♪</i>

50
00:02:51,474 --> 00:02:55,333
<i>♪ Are you ready for, ready for ♪</i>

51
00:02:55,359 --> 00:02:59,191
<i>♪ a perfect storm, perfect storm ♪</i>

52
00:02:59,191 --> 00:03:01,863
<i>♪ Cause once you’re mine,
once you’re mine ♪</i>

53
00:03:03,285 --> 00:03:05,254
<i>♪ There’s no going back ♪</i>

54
00:03:08,690 --> 00:03:12,328
KATY PERRY:
<i>Ladies and gentlemen, Lenny Kravitz!</i>

55
00:03:12,353 --> 00:03:14,353
("I Kissed a Girl" by Katy Perry)

56
00:03:14,404 --> 00:03:18,323
<i>♪ This was never the way I planned ♪</i>

57
00:03:18,544 --> 00:03:21,190
<i>♪ Not my intention ♪</i>

58
00:03:21,853 --> 00:03:25,487
<i>♪ I got so brave, drink in hand ♪</i>

59
00:03:25,513 --> 00:03:28,481
<i>♪ Lost my discretion ♪</i>

60
00:03:28,648 --> 00:03:33,211
<i>♪ It's not what, I'm used to ♪</i>

61
00:03:33,237 --> 00:03:36,264
<i>♪ Just wanna try you on ♪</i>

62
00:03:36,448 --> 00:03:40,572
<i>♪ I'm curious for you ♪</i>

63
00:03:40,598 --> 00:03:43,050
<i>♪ Caught my attention ♪</i>

64
00:03:43,678 --> 00:03:47,345
<i>♪ I kissed a girl and I liked it ♪</i>

65
00:03:47,371 --> 00:03:51,037
<i>♪ The taste of her cherry chap stick ♪</i>

66
00:03:51,268 --> 00:03:54,826
<i>♪ I kissed a girl just to try it ♪</i>

67
00:03:54,852 --> 00:03:58,409
<i>♪ I hope my boyfriend don't mind it ♪</i>

68
00:03:58,702 --> 00:04:02,134
<i>♪ It felt so wrong
It felt so right ♪</i>

69
00:04:02,465 --> 00:04:05,805
<i>♪ Don't mean I'm in love tonight ♪</i>

70
00:04:06,129 --> 00:04:09,385
<i>♪ I kissed a girl and I liked it ♪</i>

71
00:04:11,810 --> 00:04:13,540
<i>♪ I liked it ♪</i>

72
00:04:25,825 --> 00:04:28,425
("Teenage Dream" by Katy Perry)

73
00:04:28,450 --> 00:04:30,450
(music playing)

74
00:04:39,851 --> 00:04:41,258
<i>♪ You think I'm pretty ♪</i>

75
00:04:41,260 --> 00:04:43,734
<i>♪ Without any make-up on ♪</i>

76
00:04:43,736 --> 00:04:44,764
<i>♪ You think I'm funny ♪</i>

77
00:04:44,766 --> 00:04:47,379
<i>♪ When I tell the puch line wrong ♪</i>

78
00:04:47,381 --> 00:04:48,816
<i>♪ I know you get me ♪</i>

79
00:04:48,818 --> 00:04:53,351
<i>♪ So I'll let my walls come down, down ♪</i>

80
00:04:55,161 --> 00:04:56,906
<i>♪ Before you met me ♪</i>

81
00:04:56,907 --> 00:04:59,295
<i>♪ I was a wreck but things ♪</i>

82
00:04:59,296 --> 00:05:00,768
<i>♪ were kinda heavy ♪</i>

83
00:05:00,869 --> 00:05:02,300
<i>♪ You brought me to life ♪</i>

84
00:05:02,301 --> 00:05:04,826
<i>♪ now every February ♪</i>

85
00:05:04,827 --> 00:05:10,028
<i>♪ You'll be my valentine, valentine ♪</i>

86
00:05:10,301 --> 00:05:12,588
<i>♪ You make me ♪</i>

87
00:05:12,599 --> 00:05:14,345
<i>♪ Feel like I'm living a ♪</i>

88
00:05:14,346 --> 00:05:16,606
<i>♪ Teenage Dream ♪</i>

89
00:05:16,607 --> 00:05:18,165
<i>♪ The way you turn me on ♪</i>

90
00:05:18,166 --> 00:05:20,516
<i>♪ I can't sleep ♪</i>

91
00:05:20,517 --> 00:05:21,727
<i>♪ Let's runaway ♪</i>

92
00:05:21,728 --> 00:05:23,600
<i>♪ And don't ever look back ♪</i>

93
00:05:23,601 --> 00:05:25,685
<i>♪ Don't ever look back ♪</i>

94
00:05:25,686 --> 00:05:27,743
<i>♪ My heart stops ♪</i>

95
00:05:27,744 --> 00:05:29,633
<i>♪ When you look at me ♪</i>

96
00:05:29,634 --> 00:05:31,940
<i>♪ Just one touch ♪</i>

97
00:05:31,941 --> 00:05:33,329
<i>♪ Now baby I believe ♪</i>

98
00:05:33,330 --> 00:05:35,383
<i>♪ This is real ♪</i>

99
00:05:35,384 --> 00:05:36,705
<i>♪ So take a chance ♪</i>

100
00:05:36,706 --> 00:05:38,787
<i>♪ And don't ever look back ♪</i>

101
00:05:38,788 --> 00:05:40,326
<i>♪ Don't ever look back ♪</i>

102
00:05:40,427 --> 00:05:42,619
<i>♪ I might get your heart racing ♪</i>

103
00:05:42,620 --> 00:05:44,585
<i>♪ In my skin-tight jeans ♪</i>

104
00:05:44,586 --> 00:05:47,570
<i>♪ Be your teenage dream tonight ♪</i>

105
00:05:47,571 --> 00:05:50,383
<i>♪ Let you put your hands on me ♪</i>

106
00:05:50,384 --> 00:05:52,576
<i>♪ In my skin-tight jeans ♪</i>

107
00:05:52,577 --> 00:05:55,466
<i>♪ Be your teenage dream tonight ♪</i>

108
00:05:55,467 --> 00:05:56,424
<i>♪ Tonight ♪</i>

109
00:05:56,425 --> 00:05:57,273
<i>♪ Tonight. ♪</i>

110
00:05:58,753 --> 00:06:02,188
Make some noise, Super Bowl!

111
00:06:05,213 --> 00:06:07,213
("California Gurls" by Katy Perry 
Ft. Snoop Dogg)

112
00:06:10,800 --> 00:06:13,000
<i>♪ I know a place ♪</i>

113
00:06:13,025 --> 00:06:13,899
<i>Sing that!</i>

114
00:06:13,900 --> 00:06:16,900
<i>Where the grass is really greener</i>

115
00:06:18,500 --> 00:06:20,950
<i>♪ Warm, wet and wild ♪</i>

116
00:06:21,700 --> 00:06:24,700
<i>♪ There must be something in the water ♪</i>

117
00:06:26,100 --> 00:06:28,650
<i>♪ Sippin' gin and juice ♪</i>

118
00:06:29,450 --> 00:06:32,950
<i>♪ Laying underneath the palm trees, undone ♪</i>

119
00:06:33,000 --> 00:06:36,000
<i>♪ The boys break their necks ♪</i>

120
00:06:36,500 --> 00:06:38,700
<i>♪ Tryin' to creep a little sneak peek ♪</i>

121
00:06:38,700 --> 00:06:40,700
<i>♪ Oh, oh-oh, oh-oh, oh-oh ♪</i>

122
00:06:40,720 --> 00:06:44,620
<i>♪ California gurls, we're unforgettable ♪</i>

123
00:06:44,650 --> 00:06:48,150
<i>♪ Daisy dukes, bikinis on top ♪</i>

124
00:06:48,160 --> 00:06:49,972
<i>♪ Sun-kissed skin, so hot, ♪</i>

125
00:06:49,998 --> 00:06:51,860
<i>♪ will melt your popsicle ♪</i>

126
00:06:51,900 --> 00:06:55,400
<i>♪ Oh-oh, oh-oh, oh-oh ♪</i>

127
00:06:55,410 --> 00:06:59,410
<i>♪ California gurls, we're undeniable ♪</i>

128
00:06:59,420 --> 00:07:02,920
<i>♪ Fine, fresh, fierce, we got it on lock ♪</i>

129
00:07:03,100 --> 00:07:05,009
<i>♪ West Coast represent, ♪</i>

130
00:07:05,035 --> 00:07:07,100
<i>♪ now put your hands up ♪</i>

131
00:07:07,110 --> 00:07:10,610
<i>♪ Oh, oh-oh, oh-oh, oh-oh ♪</i>

132
00:07:10,635 --> 00:07:12,635
<i>California!</i>

133
00:07:21,998 --> 00:07:23,998
(music playing)

134
00:07:24,024 --> 00:07:26,024
("Get Ur Freak On" by Missy Elliott)

135
00:07:28,050 --> 00:07:34,450
<i>♪ Gimme me some new... ♪</i>

136
00:07:34,451 --> 00:07:35,001
Let's go.

137
00:07:35,204 --> 00:07:36,380
<i>♪ Missy be puttin it down ♪</i>

138
00:07:36,406 --> 00:07:37,529
<i>♪ Im the hottest round ♪</i>

139
00:07:37,797 --> 00:07:39,447
<i>♪ Ill told yall mutha... ♪</i>

140
00:07:39,473 --> 00:07:41,097
<i>♪ Yall can stop me now ♪</i>

141
00:07:41,140 --> 00:07:42,113
- Come on!
- <i>♪ Listen to me now ♪</i>

142
00:07:42,139 --> 00:07:43,265
- <i>♪ I'm lastin twenty rounds ♪</i>
- Come on!

143
00:07:43,330 --> 00:07:44,830
<i>♪ And if you want me, nigga ♪</i>

144
00:07:44,856 --> 00:07:46,230
<i>♪ then come and get me now, bounce ♪</i>

145
00:07:46,243 --> 00:07:47,443
<i>♪ Is you with me now, bounce ♪</i>

146
00:07:47,469 --> 00:07:48,668
<i>♪ The biggie biggie bounce, bounce ♪</i>

147
00:07:48,825 --> 00:07:49,936
<i>♪ I kno you dig the way ♪</i>

148
00:07:49,962 --> 00:07:51,250
<i>♪ i sw..sw...switched ma style  ♪</i>

149
00:07:51,616 --> 00:07:52,316
- Holla.
- Holla.

150
00:07:52,317 --> 00:07:54,036
- Come on, Missy.
- <i>♪ People sing around ♪</i>

151
00:07:54,062 --> 00:07:55,041
<i>♪ Now people gather round ♪</i>

152
00:07:55,067 --> 00:07:56,087
<i>♪ Now people jump around ♪</i>

153
00:07:56,434 --> 00:08:04,434
<i>♪ Getcho freak on.. go... ♪</i>

154
00:08:05,161 --> 00:08:07,161
<i>♪ Getcha Getcha Getcha
Getcha Getcha freak on ♪</i>

155
00:08:07,635 --> 00:08:16,400
<i>♪ Getcho freak on.. go... ♪</i>

156
00:08:16,478 --> 00:08:17,978
<i>♪ Getcha Getcha Getcha
Getcha Getcha freak on ♪</i>

157
00:08:18,203 --> 00:08:20,283
<i>If you a fly gal get
your nails done</i>

158
00:08:20,309 --> 00:08:22,219
<i>Get a pedicure,
get your hair did</i>

159
00:08:22,244 --> 00:08:24,464
("Work It" by Missy Elliott)
<i>♪ Is it worth it, let me work it ♪</i>

160
00:08:24,466 --> 00:08:27,268
<i>♪ I put my thing down,
flip it and reverse it ♪</i>

161
00:08:27,293 --> 00:08:29,293
<i>♪ ti esrever dna ti nwod gniht ym tup i ♪</i>

162
00:08:29,318 --> 00:08:31,103
<i>♪ ti esrever dna ti nwod gniht ym tup i ♪</i>

163
00:08:31,104 --> 00:08:33,494
<i>♪ If you got a..., let me search it ♪</i>

164
00:08:33,720 --> 00:08:36,010
<i>♪ and find out how hard I gotta work you ♪</i>

165
00:08:36,635 --> 00:08:38,145
<i>♪ ti esrever dna ti nwod gniht ym tup i ♪</i>

166
00:08:38,170 --> 00:08:40,170
<i>♪ ti esrever dna ti nwod gniht ym tup i ♪</i>

167
00:08:41,663 --> 00:08:43,464
<i>♪ Is it worth it, let me work it ♪</i>

168
00:08:43,466 --> 00:08:46,268
<i>♪ I put my thing down,
flip it and reverse it ♪</i>

169
00:08:46,293 --> 00:08:48,293
<i>♪ ti esrever dna ti nwod gniht ym tup i ♪</i>

170
00:08:48,318 --> 00:08:50,103
<i>♪ ti esrever dna ti nwod gniht ym tup i ♪</i>

171
00:08:50,104 --> 00:08:52,494
<i>♪ If you got a..., let me search it ♪</i>

172
00:08:52,720 --> 00:08:55,010
<i>♪ and find out how hard I gotta work you ♪</i>

173
00:08:55,635 --> 00:08:57,145
<i>♪ ti esrever dna ti nwod gniht ym tup i ♪</i>

174
00:08:57,170 --> 00:08:59,170
<i>♪ ti esrever dna ti nwod gniht ym tup i ♪</i>

175
00:08:59,695 --> 00:09:04,195
<i>Music make you lose control.
Music make you lose control.</i>

176
00:09:04,220 --> 00:09:06,220
<i>Let's go!</i>

177
00:09:06,245 --> 00:09:08,176
("Lose Control" by Missy Elliott
Ft. Ciara, Fatman Scoop)

178
00:09:08,201 --> 00:09:10,201
(music playing)

179
00:09:10,245 --> 00:09:13,514
<i>♪ I've got a cute face, Chubby waist ♪</i>

180
00:09:13,539 --> 00:09:15,539
<i>♪ Thick legs in shape ♪</i>

181
00:09:15,564 --> 00:09:17,418
<i>♪ Rump shaking both ways ♪</i>

182
00:09:17,419 --> 00:09:18,921
<i>♪ Make you do a double take ♪</i>

183
00:09:18,946 --> 00:09:20,922
<i>♪ Planet Rocka show stopper ♪</i>

184
00:09:20,923 --> 00:09:22,905
<i>♪ Flo froppa head knocker ♪</i>

185
00:09:22,948 --> 00:09:24,925
<i>♪ Beat stalla tail dropper ♪</i>

186
00:09:24,973 --> 00:09:26,972
<i>♪ Do my thang motherf.... ♪</i>

187
00:09:26,998 --> 00:09:30,507
<i>♪ Everybody here, Get it outta control ♪</i>

188
00:09:30,532 --> 00:09:32,532
<i>♪ Get your backs off the wall ♪</i>

189
00:09:32,557 --> 00:09:34,434
<i>♪ Cause misdemeanor said so ♪</i>

190
00:09:34,459 --> 00:09:36,459
<i>♪ Throw your hands in the air ♪</i>

191
00:09:36,484 --> 00:09:38,405
<i>♪ Throw your hands in the air ♪</i>

192
00:09:38,430 --> 00:09:40,430
<i>♪ Throw your hands in the air ♪</i>

193
00:09:40,455 --> 00:09:42,455
- <i>Everybody</i>
- <i>Music makes you lose control.</i>

194
00:09:51,480 --> 00:09:53,680
("Firework" by Katy Perry)

195
00:09:53,705 --> 00:09:55,705
(music playing)

196
00:09:56,934 --> 00:10:00,659
<i>♪ Do you ever feel like a plastic bag ♪</i>

197
00:10:00,860 --> 00:10:02,431
<i>♪ Drifting through the wind ♪</i>

198
00:10:02,640 --> 00:10:04,400
<i>♪ Wanting to start again ♪</i>

199
00:10:04,673 --> 00:10:08,500
<i>♪ Do you ever feel, feel so paper thin ♪</i>

200
00:10:08,650 --> 00:10:10,285
<i>♪ Like a house of cards ♪</i>

201
00:10:10,311 --> 00:10:12,178
<i>♪ one blow from caving in ♪</i>

202
00:10:15,075 --> 00:10:18,687
<i>♪ Do you ever feel already buried deep ♪</i>

203
00:10:18,913 --> 00:10:20,924
<i>♪ Six feet under scream but no ♪</i>

204
00:10:20,950 --> 00:10:22,817
<i>♪ one seems to hear a thing ♪</i>

205
00:10:23,808 --> 00:10:25,537
<i>♪ Do you know that there's ♪</i>

206
00:10:25,563 --> 00:10:27,218
<i>♪ still a chance for you ♪</i>

207
00:10:27,386 --> 00:10:29,231
<i>♪ Cause there's a spark in you ♪</i>

208
00:10:29,455 --> 00:10:34,266
<i>♪ You just gotta ignite the light ♪</i>

209
00:10:34,474 --> 00:10:38,082
<i>♪ And let it shine ♪</i>

210
00:10:38,290 --> 00:10:42,097
<i>♪ Just own the night ♪</i>

211
00:10:42,123 --> 00:10:45,266
<i>♪ like the Fourth of July ♪</i>

212
00:10:48,649 --> 00:10:52,196
<i>♪ Cause baby you're a firework ♪</i>

213
00:10:52,492 --> 00:10:56,209
<i>♪ Come on show 'em what you worth ♪</i>

214
00:10:56,410 --> 00:10:59,565
<i>♪ Make 'em go "Oh, oh, oh!" ♪</i>

215
00:10:59,781 --> 00:11:03,601
<i>♪ As you shoot across the sky-y-y ♪</i>

216
00:11:04,017 --> 00:11:07,821
<i>♪ Baby you're a firework ♪</i>

217
00:11:08,022 --> 00:11:11,693
<i>♪ Come on let your colors burst ♪</i>

218
00:11:11,851 --> 00:11:15,100
<i>♪ Make 'em go "Oh, oh, oh!" ♪</i>

219
00:11:15,170 --> 00:11:20,415
<i>♪ You're gonna leave 'em ♪
♪ fallin' down-own-own ♪</i>

220
00:11:20,700 --> 00:11:22,800
<i>♪ Boom, boom, boom ♪</i>

221
00:11:23,059 --> 00:11:26,484
<i>♪ Even brighter than the moon, moon, moon ♪</i>

222
00:11:26,692 --> 00:11:30,474
<i>♪ It's always been inside of you, you, you ♪</i>

223
00:11:30,682 --> 00:11:34,845
<i>♪ And now it's time to let it through ♪</i>

224
00:11:35,106 --> 00:11:38,881
<i>♪ Cause baby you're a firework ♪</i>

225
00:11:39,082 --> 00:11:42,878
<i>♪ Come on let your colors burst ♪</i>

226
00:11:43,002 --> 00:11:46,129
<i>♪ Make 'em go "Oh, oh, oh!" ♪</i>

227
00:11:46,344 --> 00:11:50,583
<i>♪ You're gonna leave 'em going "Oh, oh, oh!" ♪</i>

228
00:11:50,783 --> 00:11:54,296
<i>♪ Boom, boom, boom ♪</i>

229
00:11:54,497 --> 00:11:58,270
<i>♪ Even brighter than the moon, moon, moon ♪</i>

230
00:11:58,471 --> 00:12:01,360
<i>♪ Boom, boom, boom ♪</i>

231
00:12:01,560 --> 00:12:07,195
<i>♪ Even brighter than the moon, moon, moon ♪</i>

232
00:12:21,610 --> 00:12:24,575
Thank you, God Bless America!

233
00:12:31,177 --> 00:12:33,860
ANNOUNCER: <i>The National Football
League thanks you for</i>

234
00:12:33,886 --> 00:12:36,908
<i>watching the Pepsi Super
Bowl XLIX Halftime Show.</i>

235
00:12:38,448 --> 00:12:43,928
<font color="#ffff00">Sync and Corrected by er1ck9</font>
<font color="#ffffff">www.addic7ed.com</font>

