﻿1
00:00:33,300 --> 00:00:34,671
Noodles are noodles.

2
00:00:35,341 --> 00:00:36,771
Eggs are just eggs.

3
00:00:49,641 --> 00:00:53,101
Kyung Joo, I just cooked some noodles, and it's insane.

4
00:00:53,341 --> 00:00:54,940
Come over, I'll make you some.

5
00:00:55,671 --> 00:00:57,501
How can you even eat?

6
00:00:58,141 --> 00:00:59,470
You animal.

7
00:01:01,701 --> 00:01:02,870
Animal?

8
00:01:03,841 --> 00:01:05,001
Animal...

9
00:01:07,910 --> 00:01:09,910
What are you doing? You animal.

10
00:01:10,300 --> 00:01:13,740
What will you do if we miss the last train?

11
00:01:14,141 --> 00:01:15,501
You animal.

12
00:01:16,201 --> 00:01:17,600
Animal.

13
00:01:20,800 --> 00:01:23,501
Hey, if you call me an animal once more,

14
00:01:23,501 --> 00:01:25,070
I will kill you.

15
00:01:27,671 --> 00:01:30,270
You're losing your mind as well.

16
00:01:37,070 --> 00:01:38,841
I don't feel full at all.

17
00:01:44,800 --> 00:01:46,470
Why am I so angry?

18
00:01:52,270 --> 00:01:53,800
He said he's not angry.

19
00:01:54,470 --> 00:01:56,440
Why does he suddenly dislike me?

20
00:01:56,770 --> 00:01:59,570
Why did he hug me and make me breakfast?

21
00:02:00,270 --> 00:02:01,371
Darn it.

22
00:02:01,701 --> 00:02:03,201
If he dislikes me, fine.

23
00:02:03,570 --> 00:02:05,371
Why did he end it like this?

24
00:02:13,341 --> 00:02:15,970
Kwon Si Hyun, you scumbag!

25
00:03:24,700 --> 00:03:26,640
You went overboard, Tae Hee.

26
00:03:27,341 --> 00:03:29,871
You said you wouldn't die because of a breakup.

27
00:04:11,440 --> 00:04:13,801
Bring your assignment to the office,

28
00:04:13,871 --> 00:04:16,070
and don't forget to get a receipt.

29
00:04:16,070 --> 00:04:17,700
- Okay. - Okay.

30
00:04:17,940 --> 00:04:19,371
Another thing.

31
00:04:19,371 --> 00:04:20,840
Let's not miss...

32
00:04:20,840 --> 00:04:22,671
post-project parties or gatherings.

33
00:04:22,671 --> 00:04:24,971
We have to hang out in the office from now on.

34
00:04:24,971 --> 00:04:26,541
Let's all try to get along.

35
00:04:28,971 --> 00:04:30,471
Tae Hee, sign the register.

36
00:04:33,301 --> 00:04:35,671
You'll come to the dinner later, won't you?

37
00:04:36,770 --> 00:04:38,400
Sure, I'll be there.

38
00:04:38,801 --> 00:04:43,440
(Process Validation Report for Myoungjeong Atopy Stem Cell Drug)

39
00:04:50,371 --> 00:04:52,440
- What is it? - The new drug.

40
00:04:52,840 --> 00:04:55,371
The results are bad, and they might pull the plug.

41
00:04:55,801 --> 00:04:56,971
Are you okay?

42
00:04:56,971 --> 00:04:58,671
The rumors are false. We won't stop.

43
00:04:59,340 --> 00:05:01,340
I'll apply for a change in testing.

44
00:05:02,501 --> 00:05:04,871
If it's approved, we can release the drug sooner.

45
00:05:05,171 --> 00:05:06,671
It won't be easy.

46
00:05:08,501 --> 00:05:10,041
If it's about the chairwoman...

47
00:05:10,041 --> 00:05:13,070
No. I can get the approval moved up.

48
00:05:15,501 --> 00:05:17,940
I hope JK doesn't hear any of this.

49
00:05:20,041 --> 00:05:21,111
Okay.

50
00:05:44,270 --> 00:05:45,440
I want you...

51
00:05:46,371 --> 00:05:48,440
to live more freely.

52
00:05:59,340 --> 00:06:02,200
- Give me that. - Don't take it. It's a warning.

53
00:06:05,770 --> 00:06:07,001
I'm a doctor.

54
00:06:07,940 --> 00:06:09,640
I prescribe what I need.

55
00:06:43,041 --> 00:06:45,541
I have something to tell you. Spare me some time.

56
00:06:56,940 --> 00:06:58,041
Let's sit.

57
00:07:05,241 --> 00:07:06,440
Marriage?

58
00:07:07,371 --> 00:07:09,001
I'm sure I misheard you.

59
00:07:09,440 --> 00:07:12,241
No. You heard me correctly.

60
00:07:13,001 --> 00:07:15,471
I want to marry Si Hyun.

61
00:07:16,241 --> 00:07:18,640
Can you say it in a way that's more understandable?

62
00:07:19,171 --> 00:07:23,171
As you know, we've known each other longer that you knew my mom.

63
00:07:23,601 --> 00:07:25,900
And we aren't that naive.

64
00:07:26,671 --> 00:07:28,340
You called me to say that?

65
00:07:29,471 --> 00:07:33,640
Did you think saying that would shock us into changing our minds?

66
00:07:34,400 --> 00:07:37,371
If this is a joke, stop it right now.

67
00:07:38,900 --> 00:07:40,840
The look of shock on your face...

68
00:07:40,840 --> 00:07:42,671
makes me think I did something wrong.

69
00:07:44,041 --> 00:07:45,741
You shouldn't be...

70
00:07:45,741 --> 00:07:48,971
more shocked than I was when I saw you with another woman.

71
00:07:49,801 --> 00:07:52,200
You don't seem to believe me. Shall I go on?

72
00:07:52,770 --> 00:07:54,400
Potter Seol Young Won.

73
00:07:57,900 --> 00:07:58,971
That's enough.

74
00:07:59,101 --> 00:08:00,570
It was a coincidence.

75
00:08:01,070 --> 00:08:04,070
Don't blame me. Blame your carelessness.

76
00:08:04,671 --> 00:08:07,200
It's not a bad deal. You have nothing to lose.

77
00:08:08,301 --> 00:08:09,671
What do you mean?

78
00:08:09,671 --> 00:08:11,640
Whoever gets married,

79
00:08:11,971 --> 00:08:15,301
you'll get what you need to run your business.

80
00:08:15,301 --> 00:08:18,440
You'll get to continue seeing the woman you love,

81
00:08:19,700 --> 00:08:22,700
and I get to stop watching JK use my mom.

82
00:08:22,700 --> 00:08:24,041
Aren't I right?

83
00:08:24,041 --> 00:08:25,770
Based on what you said,

84
00:08:26,210 --> 00:08:27,710
I can't really tell.

85
00:08:29,380 --> 00:08:31,280
I'll talk to Si Hyun.

86
00:08:32,481 --> 00:08:33,540
Go ahead.

87
00:08:34,680 --> 00:08:36,880
Do you remember Lawyer Do Ji Young...

88
00:08:36,880 --> 00:08:38,741
who helped JK a few months ago?

89
00:08:39,981 --> 00:08:42,581
You already knew we did that.

90
00:08:42,981 --> 00:08:45,140
You just ignored it because it was too troublesome.

91
00:08:47,880 --> 00:08:49,180
Yes, we're like that.

92
00:08:49,711 --> 00:08:51,810
We're mean. We act without thinking,

93
00:08:51,810 --> 00:08:54,841
and we don't care if anyone suffers because of us.

94
00:08:55,510 --> 00:08:57,280
You won't be able to stop us.

95
00:08:59,510 --> 00:09:01,310
Get us officially engaged.

96
00:09:01,741 --> 00:09:04,140
Then I'll convince Si Hyun and go study abroad together.

97
00:09:06,810 --> 00:09:09,741
Why are you asking me and not your mom?

98
00:09:12,441 --> 00:09:13,640
Because there's no use.

99
00:09:15,280 --> 00:09:16,810
People who are in love...

100
00:09:17,741 --> 00:09:19,241
can't listen to what others say.

101
00:09:23,741 --> 00:09:25,581
My mom hasn't been able to sleep.

102
00:09:26,081 --> 00:09:28,441
She was like that, but her condition has worsened.

103
00:09:29,581 --> 00:09:31,880
It seems like she slept well on some days,

104
00:09:31,880 --> 00:09:34,040
but on other days, she stays up the night.

105
00:09:34,040 --> 00:09:36,481
She can barely wake up because she takes sleeping pills.

106
00:09:37,640 --> 00:09:39,111
Who do you think caused that?

107
00:09:51,010 --> 00:09:53,380
- Hello, Chairwoman Ha. - Hello.

108
00:09:53,380 --> 00:09:55,540
Hello, I'm Choi Go Ya.

109
00:09:55,540 --> 00:09:56,981
Hello.

110
00:09:56,981 --> 00:09:59,140
- It's Tae Hee. Hey! - Hello, Tae Hee.

111
00:09:59,140 --> 00:10:01,310
- Gosh, Tae Hee. - It's been so long.

112
00:10:03,680 --> 00:10:05,040
I brought steamed buns.

113
00:10:05,040 --> 00:10:07,581
- My gosh. - Steamed buns.

114
00:10:07,581 --> 00:10:08,680
It's hot.

115
00:10:08,680 --> 00:10:11,211
Thanks, Tae Hee. They look delicious.

116
00:10:11,211 --> 00:10:12,841
- My gosh. - It's hot.

117
00:10:12,841 --> 00:10:14,981
Thanks, Tae Hee.

118
00:10:14,981 --> 00:10:16,910
Where did you get the money?

119
00:10:16,910 --> 00:10:18,081
Come and sit.

120
00:10:18,081 --> 00:10:19,841
Enjoy.

121
00:10:20,341 --> 00:10:22,640
You have to be careful. That's all sugar.

122
00:10:22,640 --> 00:10:25,040
It's okay. I'll only have a little.

123
00:10:25,241 --> 00:10:27,180
- Gosh. - Where did you get this?

124
00:10:27,441 --> 00:10:29,640
These are genuine, old-style steamed buns.

125
00:10:30,410 --> 00:10:33,040
I was at Yangpyeong to do a project.

126
00:10:33,111 --> 00:10:35,510
That means you went with Si Hyun, right?

127
00:10:37,010 --> 00:10:38,040
I did.

128
00:10:38,040 --> 00:10:39,540
Why didn't you come with him?

129
00:10:39,540 --> 00:10:40,611
Goodness.

130
00:10:40,611 --> 00:10:43,441
- He's busy. - He isn't even going to school.

131
00:10:43,441 --> 00:10:45,741
- Why is he busy? - Hey.

132
00:10:48,810 --> 00:10:51,941
Goodness. You both come here whenever you two get in a fight.

133
00:11:16,410 --> 00:11:19,711
If you're finished, go get your own water.

134
00:11:20,981 --> 00:11:23,981
My baby, eat slowly.

135
00:11:26,780 --> 00:11:27,841
Good girl.

136
00:11:53,841 --> 00:11:55,841
Now, now. Don't cry.

137
00:12:10,180 --> 00:12:11,540
- Go, now. - I'll be leaving.

138
00:12:11,540 --> 00:12:14,010
- Okay. Bye. - Bye.

139
00:12:14,010 --> 00:12:16,481
- Goodbye. - Come visit again.

140
00:12:16,910 --> 00:12:19,241
Let's go.

141
00:12:20,211 --> 00:12:21,280
Poor girl.

142
00:12:21,880 --> 00:12:24,211
I knew he would make her pay for his cuteness.

143
00:12:24,410 --> 00:12:26,481
I will go get him.

144
00:12:26,481 --> 00:12:30,441
After a storm comes a calm.

145
00:12:30,441 --> 00:12:31,941
Give them some time.

146
00:12:32,241 --> 00:12:35,981
What's she saying to her? She isn't even in her right mind.

147
00:12:37,981 --> 00:12:39,010
Okay.

148
00:12:40,081 --> 00:12:41,481
Goodbye.

149
00:12:41,481 --> 00:12:43,241
Go home safely.

150
00:12:43,241 --> 00:12:44,941
- Thank you. - Goodbye.

151
00:12:51,010 --> 00:12:55,010
Hello, busy President Myung. To what do I owe this honor?

152
00:12:55,280 --> 00:12:56,280
Ms. Jo.

153
00:12:58,140 --> 00:13:01,581
I wanted to discuss your son's matter again.

154
00:13:01,810 --> 00:13:04,441
Goodness, President Myung.

155
00:13:04,640 --> 00:13:08,510
I felt really bad after leaving you like that on that day.

156
00:13:10,341 --> 00:13:12,640
I have a favor to ask too.

157
00:13:13,540 --> 00:13:15,780
I remember seeing you with Assemblyman Kim Se Cheol...

158
00:13:15,780 --> 00:13:17,741
on our Supporters' Night.

159
00:13:20,040 --> 00:13:21,341
Could you...

160
00:13:22,010 --> 00:13:24,211
set up a meeting for me?

161
00:13:24,880 --> 00:13:28,040
Oh, he's my cousin-in-law.

162
00:13:28,040 --> 00:13:31,640
Okay. I'll ask him and call you back.

163
00:13:36,481 --> 00:13:38,481
- We are the best! - We are the best!

164
00:13:38,611 --> 00:13:40,410
- All right. - Cheers.

165
00:13:40,410 --> 00:13:41,441
Welcome...

166
00:13:41,441 --> 00:13:44,341
(Ilpoom Pork Skin)

167
00:13:44,341 --> 00:13:46,880
- Hey. - Oh, hey.

168
00:13:47,481 --> 00:13:48,941
There's Tae Hee.

169
00:13:48,941 --> 00:13:51,341
- Hey, Tae Hee. - Tae Hee's here.

170
00:13:51,341 --> 00:13:53,211
Tae Hee, sit here.

171
00:13:53,211 --> 00:13:55,341
She's not going to sit next to you.

172
00:13:55,341 --> 00:13:57,441
- Why? Come on. - In your dreams.

173
00:13:57,441 --> 00:13:58,581
Why aren't you drinking?

174
00:13:58,581 --> 00:14:00,040
- Cheers. - Cheers.

175
00:14:00,380 --> 00:14:02,540
Let's play a drinking game, and the game starts now

176
00:14:02,540 --> 00:14:04,780
Baskin Robbins 31

177
00:14:04,780 --> 00:14:07,310
Be cute and fresh

178
00:14:07,310 --> 00:14:08,341
Two.

179
00:14:08,341 --> 00:14:10,140
- Come on. - Gosh.

180
00:14:10,140 --> 00:14:12,211
- My gosh. - Eun Hye, Eun Hye.

181
00:14:12,211 --> 00:14:15,241
- My gosh. - You have to drink.

182
00:14:15,341 --> 00:14:18,380
Drink, drink, until you die

183
00:14:19,241 --> 00:14:21,010
- Come on. - He ruined it.

184
00:14:21,010 --> 00:14:22,841
What's with the vibe?

185
00:14:23,140 --> 00:14:25,780
- We didn't order those. - It's on the house.

186
00:14:30,841 --> 00:14:31,910
Thanks.

187
00:14:33,540 --> 00:14:34,540
Drink up.

188
00:14:35,010 --> 00:14:36,640
You did really well on the assignment.

189
00:14:36,841 --> 00:14:38,640
Let's enter the contest together.

190
00:14:39,441 --> 00:14:41,941
- I'll think about it. - Okay.

191
00:14:41,941 --> 00:14:42,941
Cheers.

192
00:14:46,081 --> 00:14:49,040
Hey, I have this theme, and it's really amazing.

193
00:14:49,310 --> 00:14:51,981
I said I'll think about it.

194
00:14:52,180 --> 00:14:54,441
Forget it. Don't ever ask again.

195
00:14:55,310 --> 00:14:56,380
I'm sorry.

196
00:14:57,040 --> 00:14:58,081
Take a seat.

197
00:14:59,581 --> 00:15:01,040
Hey, why aren't you drinking?

198
00:15:01,280 --> 00:15:02,640
Come on, drink up.

199
00:15:03,341 --> 00:15:06,640
Guys, let's make a toast.

200
00:15:14,780 --> 00:15:16,741
You're here quite often.

201
00:15:22,540 --> 00:15:23,780
What are you talking about?

202
00:15:24,410 --> 00:15:28,481
What do you mean? You want me to be the CEO of JK Bio?

203
00:15:29,180 --> 00:15:30,310
Like I said,

204
00:15:31,140 --> 00:15:32,640
I want you to take the position...

205
00:15:34,010 --> 00:15:36,111
regardless of our marriage.

206
00:15:38,241 --> 00:15:40,640
Do you not think I can release the new drug?

207
00:15:41,880 --> 00:15:43,111
I don't want you to strain yourself.

208
00:15:43,111 --> 00:15:44,611
When did you ever care...

209
00:15:45,981 --> 00:15:47,611
about my overworking myself?

210
00:15:49,780 --> 00:15:52,241
If you want to call off our wedding, tell me.

211
00:15:52,241 --> 00:15:53,280
No.

212
00:15:54,441 --> 00:15:56,140
I'm not sure if you'd believe me,

213
00:15:56,140 --> 00:15:59,810
but I was going to leave JK Bio to you even if we got married.

214
00:16:01,481 --> 00:16:02,880
You don't believe me.

215
00:16:04,180 --> 00:16:06,481
I always believed you.

216
00:16:09,010 --> 00:16:10,280
We have to merge...

217
00:16:11,380 --> 00:16:13,211
and eliminate JK Bio's deficit.

218
00:16:13,941 --> 00:16:16,611
That's the only way I can inherit.

219
00:16:17,280 --> 00:16:18,481
But...

220
00:16:20,611 --> 00:16:22,180
I don't want to do this anymore.

221
00:16:22,941 --> 00:16:25,680
I thought it would be best to leave this business...

222
00:16:26,611 --> 00:16:29,241
to a family member, to someone whom I can trust.

223
00:16:31,140 --> 00:16:32,481
I really mean it.

224
00:16:33,510 --> 00:16:37,140
I'll consider this offer as something I've never heard.

225
00:16:38,211 --> 00:16:39,481
I met Soo Ji.

226
00:16:41,040 --> 00:16:43,111
We were wrong about...

227
00:16:44,410 --> 00:16:46,341
Soo Ji and Si Hyun's relationship.

228
00:16:51,581 --> 00:16:53,540
They want to get married.

229
00:16:55,180 --> 00:16:59,010
I thought for a long time about how I could bring this up.

230
00:17:00,611 --> 00:17:02,180
But this is the best I can do.

231
00:17:03,010 --> 00:17:06,441
We said we'd discuss our children's matter together,

232
00:17:07,510 --> 00:17:09,211
but I never thought it'd be about this.

233
00:17:16,681 --> 00:17:18,181
I... I doubt it.

234
00:17:20,280 --> 00:17:22,280
I need to talk to Soo Ji.

235
00:17:31,681 --> 00:17:32,981
Please don't misunderstand.

236
00:17:33,981 --> 00:17:36,381
At first, I needed you and Myoungjeong.

237
00:17:37,881 --> 00:17:40,711
But then, I found out that you were trustworthy.

238
00:17:42,040 --> 00:17:43,741
I know that you are very considerate of me,

239
00:17:44,340 --> 00:17:45,741
and I know that...

240
00:17:46,110 --> 00:17:49,540
you don't consider our marriage as a part of your business.

241
00:17:49,810 --> 00:17:51,381
I'm thankful for all that.

242
00:17:54,741 --> 00:17:55,911
But?

243
00:17:56,010 --> 00:17:58,181
But if this is the case with our children,

244
00:18:00,381 --> 00:18:03,010
I don't think we should make a move.

245
00:18:03,481 --> 00:18:04,580
Are you sure...

246
00:18:05,681 --> 00:18:07,080
there are no other reasons?

247
00:18:13,241 --> 00:18:15,211
I'm going to keep doing...

248
00:18:16,280 --> 00:18:17,840
what I've been doing...

249
00:18:18,881 --> 00:18:20,411
regardless of our children.

250
00:18:22,040 --> 00:18:23,580
Come on, drink up.

251
00:18:23,681 --> 00:18:25,510
- This is great. - Okay.

252
00:18:25,510 --> 00:18:26,711
- Drink. - Let's drink.

253
00:18:26,711 --> 00:18:28,641
- Cheers. - Cheers.

254
00:18:28,641 --> 00:18:29,840
Cheers.

255
00:18:30,510 --> 00:18:31,711
Kwon Si Hyun.

256
00:18:31,711 --> 00:18:33,941
- What is she saying? - Let's drink.

257
00:18:34,441 --> 00:18:36,181
Cheers, Si Hyun.

258
00:18:36,481 --> 00:18:38,141
- Kwon Si Hyun? - Who's that?

259
00:18:38,141 --> 00:18:39,941
- Who's Kwon Si Hyun? - I don't know.

260
00:18:40,981 --> 00:18:43,911
Hey, Kwon Si Hyun, let's have a shot.

261
00:18:45,141 --> 00:18:47,481
- I'm not Kwon Si Hyun. - Who's that?

262
00:18:47,481 --> 00:18:50,010
- Slow down. - Don't drink too much.

263
00:18:51,741 --> 00:18:53,110
Is she okay?

264
00:18:53,510 --> 00:18:56,141
Si Hyun, have some food.

265
00:18:56,211 --> 00:18:57,840
- Here. - She's drunk.

266
00:18:57,840 --> 00:18:59,241
- She's drunk. - Totally.

267
00:18:59,241 --> 00:19:01,340
- Come on, Si Hyun. - She lost it.

268
00:19:01,681 --> 00:19:05,481
Hey. She locked her phone. What now?

269
00:19:07,110 --> 00:19:10,040
Hey, hey, you've had enough.

270
00:19:11,911 --> 00:19:14,481
- She's a mess. - What should we do?

271
00:19:16,110 --> 00:19:19,711
Hello, I'm Kwon Si Hyun. We met by the mural I painted.

272
00:19:20,040 --> 00:19:22,981
I found your mail address on your leaflet.

273
00:19:23,681 --> 00:19:24,911
I'd like to meet you.

274
00:19:25,610 --> 00:19:27,441
Could you spare me some time?

275
00:19:33,510 --> 00:19:35,810
(To Seol Young Won)

276
00:19:40,580 --> 00:19:42,141
(Sent successfully)

277
00:20:02,911 --> 00:20:04,911
(Se Joo)

278
00:20:08,580 --> 00:20:09,681
What?

279
00:20:09,780 --> 00:20:12,280
Hey. I think you should come over here.

280
00:20:18,780 --> 00:20:21,080
What do you want me to do?

281
00:20:21,080 --> 00:20:23,610
You're the only person I can call.

282
00:20:24,211 --> 00:20:25,381
Shall I leave her?

283
00:20:36,711 --> 00:20:39,040
Si Hyun, have another drink.

284
00:20:39,441 --> 00:20:41,280
What's wrong with you?

285
00:20:41,280 --> 00:20:43,981
Who is Kwon Si Hyun? What's his major?

286
00:20:43,981 --> 00:20:45,040
Kwon Si Hyun?

287
00:20:45,881 --> 00:20:47,241
Kwon Si Hyun...

288
00:20:47,981 --> 00:20:49,711
majors in me, you guys.

289
00:20:51,340 --> 00:20:53,141
He's my type. He's mine.

290
00:20:54,840 --> 00:20:56,840
Oh, my. Si Hyun.

291
00:20:57,010 --> 00:20:58,941
Why aren't you drinking?

292
00:20:58,941 --> 00:21:01,280
I'm fine, thanks. I had enough.

293
00:21:01,941 --> 00:21:03,181
Food?

294
00:21:03,241 --> 00:21:04,481
I'll get you some.

295
00:21:05,110 --> 00:21:08,381
- I'll make a wrap. - No, you don't have to.

296
00:21:08,381 --> 00:21:11,040
- I'm fine, Tae Hee. - You don't want to eat?

297
00:21:11,911 --> 00:21:13,141
Where's my glass?

298
00:21:13,141 --> 00:21:15,241
- Stop drinking. - Don't give her any.

299
00:21:15,540 --> 00:21:17,010
- Kwon Si Hyun. - Tae Hee.

300
00:21:17,010 --> 00:21:18,580
What's wrong with you?

301
00:21:19,040 --> 00:21:20,211
Stop it.

302
00:21:20,310 --> 00:21:21,411
Why?

303
00:21:23,711 --> 00:21:26,381
- That's enough. - Come on.

304
00:21:26,381 --> 00:21:27,441
Not again.

305
00:21:27,911 --> 00:21:29,211
Drink up.

306
00:21:29,340 --> 00:21:31,181
Mr. Kwon Si Hyun.

307
00:21:31,211 --> 00:21:32,711
Will you have a drink with me?

308
00:21:34,010 --> 00:21:35,610
- Stop it. - Stop it.

309
00:21:39,741 --> 00:21:41,911
It's nice to meet you.

310
00:21:42,241 --> 00:21:44,211
- What? - My name is...

311
00:21:44,681 --> 00:21:46,381
Si Hyun.

312
00:21:47,310 --> 00:21:48,481
Yoon Jin.

313
00:21:49,040 --> 00:21:51,080
Call me Si Hyun.

314
00:21:51,310 --> 00:21:52,840
Get a grip.

315
00:21:53,241 --> 00:21:57,381
Hey. Will you please call me Si Hyun?

316
00:21:57,381 --> 00:21:58,810
You had enough to drink.

317
00:21:59,141 --> 00:22:03,080
Will you just call me Si Hyun?

318
00:22:03,181 --> 00:22:05,040
Why should they call you Kwon Si Hyun?

319
00:22:06,211 --> 00:22:07,381
Why?

320
00:22:08,181 --> 00:22:11,881
Because I want to keep hearing that name.

321
00:22:17,681 --> 00:22:18,941
Let's go home, Tae Hee.

322
00:22:19,411 --> 00:22:20,780
Kwon Si Hyun's here.

323
00:22:21,510 --> 00:22:22,810
Kwon Si Hyun.

324
00:22:22,941 --> 00:22:25,840
Kwon Si Hyun, what took you so long?

325
00:22:26,381 --> 00:22:28,580
Wow, it's Kwon Si Hyun.

326
00:22:28,580 --> 00:22:29,641
Let's go.

327
00:22:29,681 --> 00:22:31,580
- What for? - We're leaving.

328
00:22:31,580 --> 00:22:33,441
I don't want to.

329
00:22:34,280 --> 00:22:35,641
That's him?

330
00:22:36,241 --> 00:22:37,641
Take care.

331
00:22:38,510 --> 00:22:40,411
- See you. - Bye.

332
00:22:40,411 --> 00:22:42,510
- That's Kwon Si Hyun. - He drives a sports car.

333
00:22:42,510 --> 00:22:44,310
A sports car? No way.

334
00:22:44,310 --> 00:22:45,381
That's crazy.

335
00:22:52,211 --> 00:22:55,241
(My daughter)

336
00:22:55,881 --> 00:22:57,141
My goodness.

337
00:23:04,780 --> 00:23:06,280
(Madam Myung)

338
00:23:14,241 --> 00:23:15,711
(1 new text message)

339
00:23:19,481 --> 00:23:22,441
Pick up. I heard you met Mr. Kwon.

340
00:23:22,810 --> 00:23:24,040
Come home immediately.

341
00:23:42,711 --> 00:23:45,481
Are you busy? I'm nearby. I'll come over.

342
00:23:45,481 --> 00:23:46,580
Now?

343
00:23:47,741 --> 00:23:50,241
- There's no room to park. - No.

344
00:23:50,241 --> 00:23:51,580
- Come a bit later. - Here?

345
00:23:51,741 --> 00:23:53,340
I'm almost...

346
00:23:55,141 --> 00:23:56,580
You're not at the restaurant, are you?

347
00:23:56,941 --> 00:23:59,941
- What? - You went out, didn't you?

348
00:23:59,941 --> 00:24:01,481
Will you wake up?

349
00:24:03,040 --> 00:24:05,610
I am at the restaurant. Slow down.

350
00:24:05,681 --> 00:24:08,441
I'm coming to check. Wait right there.

351
00:24:13,481 --> 00:24:16,681
- Not that way. - Get going already, you fool.

352
00:24:17,381 --> 00:24:19,941
Why did you grab me so tightly?

353
00:24:39,040 --> 00:24:41,610
Gosh. Let's go home.

354
00:24:46,780 --> 00:24:49,610
- Let's go home. - You go on your own.

355
00:24:49,741 --> 00:24:51,411
You abandoned me.

356
00:24:52,110 --> 00:24:53,641
Now, you'll give me a ride?

357
00:24:55,540 --> 00:24:56,780
Why?

358
00:24:57,381 --> 00:24:59,080
Why not carry me?

359
00:24:59,711 --> 00:25:01,780
Like you used to.

360
00:25:02,510 --> 00:25:03,911
Carry me and...

361
00:25:05,540 --> 00:25:06,580
just...

362
00:25:08,810 --> 00:25:10,080
throw me away.

363
00:25:17,080 --> 00:25:18,741
What do you want me to do?

364
00:25:40,911 --> 00:25:42,211
Don't go.

365
00:25:46,641 --> 00:25:48,280
Don't go.

366
00:25:48,641 --> 00:25:50,310
We have to go home.

367
00:25:50,610 --> 00:25:52,310
No.

368
00:25:52,741 --> 00:25:54,741
I don't want you to go.

369
00:25:55,381 --> 00:25:58,310
I said I was sorry.

370
00:26:08,981 --> 00:26:10,181
I didn't...

371
00:26:10,911 --> 00:26:12,141
I didn't...

372
00:26:12,780 --> 00:26:15,911
I didn't keep my word and read the paper plane.

373
00:26:16,840 --> 00:26:18,610
I said I was sorry.

374
00:26:18,610 --> 00:26:20,681
I said that's not the reason.

375
00:26:20,681 --> 00:26:22,580
Then what is it?

376
00:26:38,881 --> 00:26:41,241
Let's stay like this for a while.

377
00:26:42,780 --> 00:26:43,941
For just a while.

