1
00:00:27,494 --> 00:00:30,930
An uneven coastline,
and azure...

2
00:00:31,131 --> 00:00:34,965
Azure, azure, azure...

3
00:00:35,168 --> 00:00:38,331
... constitute the C�te d'Azur,
the French Riviera.

4
00:00:38,805 --> 00:00:42,104
The English have invented
a few landmarks of the coast:

5
00:00:42,308 --> 00:00:44,469
- The Promenade des Anglais.
- Nice.

6
00:00:44,677 --> 00:00:46,372
- The H�tel des Anglais.
- Menton.

7
00:00:46,579 --> 00:00:49,639
The Eglise anglaise,
also called English Church...

8
00:00:49,849 --> 00:00:53,307
... and the British Pharmacy.

9
00:00:55,255 --> 00:01:00,989
ALONG THE COAST

10
00:01:27,420 --> 00:01:31,618
Cinematography

11
00:02:00,587 --> 00:02:05,251
Editing

12
00:02:14,267 --> 00:02:18,727
Narrated by

13
00:02:58,378 --> 00:03:01,074
The natives are not the subject
of this film.

14
00:03:01,281 --> 00:03:02,942
Cogolin.

15
00:03:03,616 --> 00:03:07,575
Fulfilling the classic clich�,
they are always old and charming.

16
00:03:08,454 --> 00:03:11,218
Let's leave them
between the donkey and the cow.

17
00:03:11,858 --> 00:03:13,849
Our subject is the crowd:

18
00:03:14,060 --> 00:03:18,690
The tourists, the curious,
the emigrants, the amateurs,

19
00:03:18,898 --> 00:03:24,461
the passengers who discover this coast
and gather there to spend their free time.

20
00:03:35,415 --> 00:03:39,784
In 40 years, Nice's population grew
from 100,000 to 240,000 inhabitants.

21
00:03:39,986 --> 00:03:44,616
104,762 extra sleepers
overpopulate it every night.

22
00:03:44,824 --> 00:03:47,452
Cannes awakes with 32,003 emigrants,

23
00:03:47,660 --> 00:03:53,326
and Saint-Tropez, a village of 4000
souls, harbors 50,000 bodies in August.

24
00:03:53,533 --> 00:03:56,161
What are they looking for
in the Riviera?

25
00:03:56,369 --> 00:03:58,064
When did they start to come?

26
00:03:59,739 --> 00:04:03,470
The first patient was maybe
Cornelia Salonina,

27
00:04:03,676 --> 00:04:08,238
a Roman empress, who had
a fragile condition and broken nerves.

28
00:04:08,448 --> 00:04:11,178
She spent the winter
in Cimiez' thermal baths.

29
00:04:11,384 --> 00:04:12,942
Nice.

30
00:04:18,858 --> 00:04:22,123
The first tourist was maybe
the Cardinal Maurice of Savoy

31
00:04:22,328 --> 00:04:23,727
who, despite his 50 years of age,

32
00:04:23,930 --> 00:04:25,761
took off his habit
in order to marry his niece,

33
00:04:25,965 --> 00:04:27,796
Princess Marie-Louise.

34
00:04:28,001 --> 00:04:29,298
She was 14.

35
00:04:29,769 --> 00:04:33,296
Their honeymoon took place
in a room still visited today.

36
00:04:33,506 --> 00:04:35,736
2 rue Jules Gilly, Nice.

37
00:04:36,109 --> 00:04:37,736
They spent the winter there.

38
00:04:37,944 --> 00:04:40,003
Inventing "swallow tourism":

39
00:04:40,213 --> 00:04:43,740
The possibility to find, in any season,
not only the sun,

40
00:04:43,950 --> 00:04:45,850
but also the wild palm tree,

41
00:04:46,052 --> 00:04:47,644
the mimosa,

42
00:04:47,854 --> 00:04:49,082
the cypress,

43
00:04:49,289 --> 00:04:50,756
the cypresses,

44
00:04:50,957 --> 00:04:52,083
the maritime pine,

45
00:04:52,292 --> 00:04:53,657
the pink laurel,

46
00:04:53,860 --> 00:04:57,819
the Indian fig opuntia, which Moroccans
call the Christian fig tree,

47
00:04:58,031 --> 00:05:04,197
and the agave which lives for 100 years,
grows flowers once, and dies.

48
00:05:04,404 --> 00:05:07,339
During the 18th century,
in order to travel from Calais to Nice,

49
00:05:07,540 --> 00:05:09,371
one had to travel
in a stagecoach for 16 days,

50
00:05:09,575 --> 00:05:11,839
stopping at 107 relay stations.

51
00:05:12,045 --> 00:05:14,172
Cannes, the 107th stop,

52
00:05:14,380 --> 00:05:18,976
grew in popularity due to the plague,
closing Nice's gates to Lord Brougham.

53
00:05:19,185 --> 00:05:20,584
Lord Brougham.

54
00:05:20,787 --> 00:05:22,880
Seduced by his quarantine in Cannes,

55
00:05:23,089 --> 00:05:25,785
and stayed there for 40 years
and even more.

56
00:05:25,992 --> 00:05:27,584
Following his lead, many English,

57
00:05:27,794 --> 00:05:30,024
among which, Queen Victoria,
came to the city.

58
00:05:30,229 --> 00:05:31,787
English dukes,

59
00:05:31,998 --> 00:05:33,556
the Russian grand dukes,

60
00:05:33,766 --> 00:05:36,997
many titles and even a few names.

61
00:05:37,203 --> 00:05:39,068
Dante Alighieri,

62
00:05:39,272 --> 00:05:41,763
the Italian consul in Nice.

63
00:05:42,542 --> 00:05:44,476
�mile Zola

64
00:05:44,677 --> 00:05:47,976
spent his honeymoon
in the H�tel de Paris in Monaco.

65
00:05:48,781 --> 00:05:50,408
Friedrich Nietzsche,

66
00:05:50,616 --> 00:05:53,915
the third Zarathustra
around the village of Eze.

67
00:05:54,721 --> 00:05:56,746
Lucien Guitry,

68
00:05:56,956 --> 00:05:59,151
his poultry,

69
00:05:59,359 --> 00:06:02,021
the garden in St. Pons les M�res.

70
00:06:02,962 --> 00:06:05,556
A partly sunny morning in Monaco,

71
00:06:05,765 --> 00:06:09,394
a teenager who looked
like Guillaume Apollinaire.

72
00:06:09,602 --> 00:06:10,933
And Colette,

73
00:06:11,137 --> 00:06:13,605
who looked like a cat of Saint-Tropez,

74
00:06:13,806 --> 00:06:17,003
was writing Break of Day.

75
00:06:17,643 --> 00:06:19,577
Giraudoux, in Porquerolles,

76
00:06:19,779 --> 00:06:23,044
wrote Suzanne et le Pacifique.

77
00:06:25,017 --> 00:06:27,952
Painters also idealized the Riviera.

78
00:06:28,154 --> 00:06:30,748
- In the paintings of Joseph Vernet...
- Antibes.

79
00:06:30,957 --> 00:06:33,585
... one could see the tower
of the Ch�teau Grimaldi.

80
00:06:33,793 --> 00:06:38,127
In the ch�teau,
Picasso's work is displayed.

81
00:06:38,331 --> 00:06:41,698
A painter and a ceramist,
Picasso worked year-round.

82
00:06:41,901 --> 00:06:46,361
The only serious competition he finds
to his painting and firing,

83
00:06:46,572 --> 00:06:47,664
the sun.

84
00:06:49,842 --> 00:06:52,504
The sun's carriage springs up
from the water.

85
00:06:52,712 --> 00:06:54,907
It takes humanity along
in its burning race,

86
00:06:55,114 --> 00:06:58,777
and leaves it lying in the evening,
to be born again in the morning,

87
00:06:58,985 --> 00:07:01,545
and then starts again
the cycle of a slow labor,

88
00:07:01,754 --> 00:07:04,416
which requires overcoming
reserve and boredom,

89
00:07:04,624 --> 00:07:07,320
at the risk of losing your head,
or your skin

90
00:07:07,527 --> 00:07:11,623
which is judged by its tan.

91
00:07:42,528 --> 00:07:45,554
When you get to this point,
you have to cover yourself.

92
00:07:47,333 --> 00:07:49,631
And since from the useful
comes the beautiful,

93
00:07:49,836 --> 00:07:52,498
the cover becomes an accessory.

94
00:08:18,865 --> 00:08:22,028
They came to find the sun,
they found oblivion.

95
00:08:22,235 --> 00:08:23,361
Where are they?

96
00:08:23,569 --> 00:08:25,161
They are far away.

97
00:08:25,371 --> 00:08:27,271
Far from the coast.

98
00:08:27,473 --> 00:08:28,735
Far from everywhere.

99
00:08:28,941 --> 00:08:30,533
This is what we call exoticism.

100
00:08:31,844 --> 00:08:34,278
A pagoda-shaped shop,

101
00:08:34,480 --> 00:08:36,914
- it is Sunday in Beijing.
- Fr�jus.

102
00:08:38,451 --> 00:08:40,715
A Sudanic mosque,

103
00:08:40,920 --> 00:08:43,286
- Africa talks to you.
- Fr�jus.

104
00:08:43,756 --> 00:08:46,554
- A bulb, it is the Orient Palace.
- Menton.

105
00:08:47,026 --> 00:08:49,893
A Persian building, the Arabian Nights.

106
00:08:50,096 --> 00:08:51,256
In Monte Carlo.

107
00:08:51,864 --> 00:08:53,661
A Russian church,

108
00:08:54,233 --> 00:08:56,258
a carriage:

109
00:08:56,469 --> 00:08:58,869
- Moscow, 1900.
- Nice.

110
00:08:59,505 --> 00:09:01,405
A swimmer,

111
00:09:01,607 --> 00:09:03,575
a subway entrance:

112
00:09:03,776 --> 00:09:06,609
- Paris, 1900.
- Cannes.

113
00:09:07,680 --> 00:09:13,983
Finally, the most exotic of all:
The exotic gardens.

114
00:09:24,497 --> 00:09:26,431
A favorite place for lovers of nature

115
00:09:26,632 --> 00:09:29,624
who long for the silence
of vegetation.

116
00:09:44,183 --> 00:09:45,616
Despite the guide's explanations,

117
00:09:45,818 --> 00:09:48,048
they don't try to remember
the names of the plants.

118
00:09:48,421 --> 00:09:51,288
They'd rather have the plants
remember theirs.

119
00:09:53,859 --> 00:09:55,258
Write one's own name,

120
00:09:55,461 --> 00:09:58,521
or the name of a woman,
hoping to see her appear.

121
00:10:05,805 --> 00:10:08,933
For want of seeing Bardot,
talking to her, touching her,

122
00:10:09,141 --> 00:10:11,541
they come to drink in Saint-Tropez.

123
00:10:12,778 --> 00:10:17,647
Through a natural compensation system,
appetites change.

124
00:10:17,850 --> 00:10:19,249
They come to eat in Cannes,

125
00:10:19,452 --> 00:10:21,818
for want of meeting Sophia Loren
at the Festival.

126
00:10:26,926 --> 00:10:30,123
For want of seeing Matisse,
they come to see his grave.

127
00:10:30,329 --> 00:10:33,025
Alive or dead, famous people
have an audience,

128
00:10:33,232 --> 00:10:36,759
like Cro-Magnon, the first famous man,
and the oldest dead,

129
00:10:36,969 --> 00:10:40,132
receiving the tribute of the visitors before
they go see the Troph�e d'Auguste...

130
00:10:40,339 --> 00:10:41,363
La Turbie.

131
00:10:41,574 --> 00:10:43,474
...the Roman aqueduct...
- Fr�jus.

132
00:10:43,676 --> 00:10:45,371
... and the 13 moth-eaten
marble blocks

133
00:10:45,578 --> 00:10:47,910
intended for the August temple
in Narbonne,

134
00:10:48,114 --> 00:10:50,810
engulfed during a storm.

135
00:10:51,017 --> 00:10:52,917
Saint-Tropez.

136
00:10:54,487 --> 00:10:57,388
Let's return to the museum
that which belonged to Caesar.

137
00:10:57,590 --> 00:10:59,888
Let's leave the antiquities,

138
00:11:00,092 --> 00:11:03,858
and rehabilitate the inhabited relics,
the most precious of the Riviera:

139
00:11:04,063 --> 00:11:05,428
The age-old trees.

140
00:11:08,467 --> 00:11:11,163
A gigantic elm,
planted in the time of Sully.

141
00:11:11,370 --> 00:11:13,065
In Ramatuelle.

142
00:11:13,272 --> 00:11:16,241
A 1000-year-old olive tree.

143
00:11:16,442 --> 00:11:17,500
In Beaulieu.

144
00:11:18,444 --> 00:11:21,379
A bald cypress
from Canada and Louisiana,

145
00:11:21,580 --> 00:11:24,572
cutting made
by Monsieur Robert in 1797.

146
00:11:24,784 --> 00:11:26,547
In Toulon.

147
00:11:28,320 --> 00:11:31,778
- A century-old oak.
- In Saint-Cassien.

148
00:11:32,792 --> 00:11:35,522
And hackberries,
whose shadow is salutary.

149
00:11:35,728 --> 00:11:37,525
In Cogolin.

150
00:11:40,232 --> 00:11:42,223
Opposed to this dark color,

151
00:11:42,435 --> 00:11:45,063
there is the bright color
of the markets.

152
00:11:49,775 --> 00:11:56,146
But the tourists prefer the trendy
colors, yellow and blue.

153
00:12:08,527 --> 00:12:12,486
Passing fancies, hotels are painted
yellow and blue.

154
00:12:15,768 --> 00:12:17,895
Blue wins.

155
00:12:18,104 --> 00:12:19,662
All women want to be fashionable.

156
00:12:19,872 --> 00:12:22,466
All women wear blue,

157
00:12:24,176 --> 00:12:26,667
except the English,

158
00:12:27,947 --> 00:12:30,313
those learning to swim,

159
00:12:31,016 --> 00:12:33,951
and the Germans,
who are dedicated to green.

160
00:12:51,403 --> 00:12:54,372
Camping being the airiest form
of freedom,

161
00:12:54,573 --> 00:12:56,939
and the most ceremonial,

162
00:12:57,143 --> 00:13:01,204
it is very comfortable,
in the majesty of antique chairs.

163
00:13:01,413 --> 00:13:04,075
They retire under their tents.

164
00:13:06,786 --> 00:13:11,280
These parks, overpopulated with merry
people attracted by the Latin shore,

165
00:13:11,490 --> 00:13:16,052
foreshadow the dead people
seeking eternal rest there.

166
00:13:16,996 --> 00:13:21,558
In both cases, space is limited
because of its good quality.

167
00:13:22,535 --> 00:13:25,629
It is a well-rated coast.

168
00:13:26,639 --> 00:13:30,006
The Riviera is France's
most beautiful cemetery,

169
00:13:30,209 --> 00:13:35,010
with a great view
and a perpetually-renewed sea.

170
00:13:35,214 --> 00:13:37,682
The Riviera's deceased is of quality.

171
00:13:37,883 --> 00:13:41,148
The landscape welcomes him
with a well-deserved rest.

172
00:13:41,353 --> 00:13:43,981
He is discreet like a saddened heart
with no tears,

173
00:13:44,190 --> 00:13:46,818
humble like a hoop without roses,

174
00:13:47,393 --> 00:13:50,624
quiet like a field without honor,

175
00:13:50,830 --> 00:13:53,492
childish like a cradle,

176
00:13:53,699 --> 00:13:56,793
silent like the nearby forest,

177
00:13:57,169 --> 00:14:01,765
and surprising like, behind this wall,

178
00:14:01,974 --> 00:14:04,033
the giant Virgin.

179
00:14:04,243 --> 00:14:06,177
Saint Jean Cap Ferrat.

180
00:14:08,781 --> 00:14:11,272
The Holy Mother missed
her entrance in the Church,

181
00:14:11,483 --> 00:14:13,041
the cock has fallen from the bell tower,

182
00:14:13,252 --> 00:14:17,780
tipped by a bowl,
in the manner of this game of boules.

183
00:14:25,064 --> 00:14:27,362
Fortune turns with the roulette,

184
00:14:27,566 --> 00:14:29,534
as well as the wheel.

185
00:14:29,735 --> 00:14:32,704
- Death took Isadora Duncan.
- In Nice.

186
00:14:33,239 --> 00:14:36,231
Jean-Gabriel Domergue is ready
in the front of his tomb,

187
00:14:36,442 --> 00:14:39,206
which he built
like others build houses,

188
00:14:39,411 --> 00:14:42,903
less eternal, but full of the charm of life.

189
00:14:43,115 --> 00:14:44,343
The Charmer

190
00:14:44,550 --> 00:14:45,812
tenderness

191
00:14:46,018 --> 00:14:47,007
FRENZY

192
00:14:47,219 --> 00:14:48,709
FINALLY

193
00:14:56,028 --> 00:14:57,962
The villa

194
00:14:58,163 --> 00:15:00,825
embodies the craziness of design.

195
00:15:37,937 --> 00:15:41,805
Abandoned gardens, forgotten villas...

196
00:15:42,007 --> 00:15:43,133
This is cinema.

197
00:15:48,180 --> 00:15:54,141
Blasco lb��ez' friends enjoyed
coming to this retirement home for writers.

198
00:15:54,353 --> 00:15:56,548
Others liked hotels.

199
00:15:56,755 --> 00:16:01,749
What touches them and attaches them
to a particular hotel,

200
00:16:01,961 --> 00:16:03,451
is the greeting of the doorman.

201
00:16:05,197 --> 00:16:08,132
According to the barometer
of his familiarity,

202
00:16:08,334 --> 00:16:11,201
you are somebody or you're not.

203
00:16:57,549 --> 00:16:59,540
Doorman for 20 years
at the Grand H�tel...

204
00:16:59,752 --> 00:17:01,344
Cannes.

205
00:17:01,553 --> 00:17:03,418
... this one continues the game,

206
00:17:03,622 --> 00:17:05,385
greeting the lesser wallets

207
00:17:05,591 --> 00:17:09,891
with an irritating nostalgia
for the bigger ones.

208
00:17:19,838 --> 00:17:22,272
Where is the former Grand H�tel?

209
00:17:25,177 --> 00:17:27,270
Where is the Eden Roc?

210
00:17:27,813 --> 00:17:29,337
Where is Eden?

211
00:17:30,516 --> 00:17:32,814
The entire coast longs for it.

212
00:17:33,352 --> 00:17:35,718
The entire coast writes its name.

213
00:17:48,734 --> 00:17:51,703
Pedal boats on the other
side of EDEN.

214
00:17:53,505 --> 00:17:56,167
You always lack a penny
to make one franc.

215
00:17:56,375 --> 00:17:57,364
CLIMB OF THE DREAM
DEAD END

216
00:17:57,576 --> 00:17:59,510
The forbidden Eden sparks off a cry.

217
00:18:01,113 --> 00:18:03,274
The carnival is born from nostalgia.

218
00:20:33,799 --> 00:20:35,733
The carnival burns.

219
00:20:35,934 --> 00:20:37,492
The earth shakes.

220
00:20:37,703 --> 00:20:40,729
The sun goes mad.
The water takes its shape.

221
00:20:40,939 --> 00:20:45,000
The carnival is dead,
silence is golden.

222
00:20:45,544 --> 00:20:50,038
Silence as an answer to so many
questions, to volcanoes, to swirls.

223
00:20:50,249 --> 00:20:51,944
Eden exists.

224
00:20:52,251 --> 00:20:53,878
It is a dawn.

225
00:20:54,086 --> 00:20:56,782
Eden exists,

226
00:20:57,389 --> 00:20:59,653
it is an island.

227
00:23:40,118 --> 00:23:45,647
Heaven was a beach, and a pine cone.

228
00:23:45,857 --> 00:23:49,349
But the nostalgia of Eden is a garden.

229
00:23:54,199 --> 00:23:56,531
It is no longer the C�te d'Azur,

230
00:23:56,735 --> 00:23:59,135
but a transplanted garden.

231
00:23:59,337 --> 00:24:04,138
It is an idea of a garden
with flowers, grass and columns.

232
00:24:07,245 --> 00:24:09,577
Forgetting the sea nearby,

233
00:24:09,781 --> 00:24:12,215
the pond is a regret,

234
00:24:12,417 --> 00:24:16,547
the water lily is a lingering sigh.

235
00:24:24,196 --> 00:24:26,687
These are fake Eves,

236
00:24:26,898 --> 00:24:28,456
fake Adams,

237
00:24:28,667 --> 00:24:30,294
fake Cupids,

238
00:24:30,502 --> 00:24:32,561
misleading Venuses,

239
00:24:33,004 --> 00:24:37,338
fake caves and fake nymphs.

240
00:24:38,410 --> 00:24:40,640
Reveries are shared,

241
00:24:40,846 --> 00:24:43,747
but not the garden.

242
00:24:44,149 --> 00:24:46,310
The fake Eden is not for us,

243
00:24:46,518 --> 00:24:48,884
neither is Eden.

244
00:24:49,654 --> 00:24:52,054
And worse,

245
00:24:52,257 --> 00:24:55,283
the summer season comes to an end.
