﻿1
00:00:01,000 --> 00:00:05,341
Subtitles by DramaFever

2
00:00:50,450 --> 00:00:54,820
- Should I order another bottle?
- No, this is enough.

3
00:00:56,580 --> 00:01:00,820
<i>Episode 8</i>

4
00:01:01,180 --> 00:01:02,340
I'll call you.

5
00:01:06,590 --> 00:01:09,400
We have a room ready for you.

6
00:01:09,400 --> 00:01:11,200
You can go with her.

7
00:01:13,600 --> 00:01:15,580
No, that's okay.

8
00:01:31,290 --> 00:01:33,470
- What are you doing?
- What?

9
00:01:33,910 --> 00:01:35,650
Give me the key and go back.

10
00:01:36,520 --> 00:01:39,120
I thought you wanted me to become
Lee Jin Sook's protege.

11
00:01:39,120 --> 00:01:41,470
This is the first task Jin Sook gave me.

12
00:01:51,010 --> 00:01:52,070
Stop it.

13
00:01:52,710 --> 00:01:55,560
- Stop what?
- What you're doing now.

14
00:01:55,560 --> 00:01:57,210
What are you talking about?

15
00:01:57,210 --> 00:01:58,730
Ji Hyung Min.

16
00:02:05,160 --> 00:02:09,100
I decide and act for myself.

17
00:02:10,550 --> 00:02:11,810
Where is Soo now?

18
00:02:11,810 --> 00:02:16,140
I know Hye Soo, the woman Soo loved,
was one of your girls.

19
00:02:17,980 --> 00:02:20,500
It was you who put
Hye Soo with Soo.

20
00:02:20,500 --> 00:02:24,150
And it was also you who told
Safari Soo's weakness.

21
00:02:29,130 --> 00:02:31,980
Please, let's stop it right here.

22
00:02:33,850 --> 00:02:35,550
So you knew that...

23
00:02:36,750 --> 00:02:38,670
that bastard tried to kill me?

24
00:02:44,280 --> 00:02:45,480
So you knew.

25
00:02:47,640 --> 00:02:49,580
Were you the one who threatened
the prison warden?

26
00:02:49,580 --> 00:02:51,470
Did you pressure him to erase
everything? Did you?

27
00:02:51,470 --> 00:02:53,220
And that's why you told me
to leave the business.

28
00:02:53,220 --> 00:02:56,230
It'll never happen again.
I promise. Soo promised, too.

29
00:02:56,230 --> 00:02:59,940
Are you... praising that
bastard in front of me now?

30
00:02:59,940 --> 00:03:01,780
Are you praising him that
he keeps his promises?

31
00:03:01,780 --> 00:03:04,260
You started this fight,
so you should end it!

32
00:03:05,410 --> 00:03:09,070
Now that I found out, there will be
no more war between you two!

33
00:03:17,670 --> 00:03:19,260
You know what?

34
00:03:20,600 --> 00:03:24,720
Soo was found out by Ji Hyung Min.

35
00:03:26,240 --> 00:03:30,520
If you leave Soo like that,
it'll become dangerous for you.

36
00:03:32,150 --> 00:03:33,340
You know that?

37
00:03:35,330 --> 00:03:36,740
His name is Soo.

38
00:03:36,740 --> 00:03:38,180
He's Doctor's Son's
right-hand man.

39
00:03:38,630 --> 00:03:40,100
Make sure you remember him.

40
00:03:41,860 --> 00:03:44,480
He tried to kill Jin Sook?

41
00:03:44,480 --> 00:03:45,460
That's right.

42
00:03:49,210 --> 00:03:51,930
You can leave after two hours.
That's what everyone does.

43
00:03:53,580 --> 00:03:55,050
I guess you've done this many times.

44
00:03:55,880 --> 00:03:58,410
The numbers on informants are from
entertainment establishments.

45
00:04:06,480 --> 00:04:08,730
Am I doing okay?

46
00:04:12,390 --> 00:04:14,000
Are you sleeping?

47
00:04:52,890 --> 00:04:54,410
Do you want me to turn off the light?

48
00:06:48,660 --> 00:06:49,660
What are you doing?

49
00:06:54,430 --> 00:06:59,040
I'm going to be working for
Jin Sook from now on.

50
00:07:02,390 --> 00:07:04,660
I don't want one of them
to be my first.

51
00:07:07,950 --> 00:07:10,100
They'll like it that way even more.

52
00:07:10,100 --> 00:07:11,290
If it's your first time.

53
00:07:13,850 --> 00:07:15,580
Report to Lee Jin Sook
about an hour later.

54
00:07:15,580 --> 00:07:17,500
Tell her I passed out right away.

55
00:08:32,420 --> 00:08:34,310
Did you treat him well
before you sent him away?

56
00:08:36,260 --> 00:08:37,380
Yes.

57
00:08:37,680 --> 00:08:38,780
How?

58
00:08:39,640 --> 00:08:40,820
Excuse me?

59
00:08:42,380 --> 00:08:44,360
I don't think he's an
easy one to deal with.

60
00:08:46,350 --> 00:08:47,690
Did you sleep with him?

61
00:08:49,070 --> 00:08:51,680
No, not yet.

62
00:08:53,430 --> 00:08:57,340
Good job. You got him
up to the room anyway.

63
00:08:58,550 --> 00:09:01,650
And a man like Ji Hyung Min won't
change just because you slept with him once.

64
00:09:01,650 --> 00:09:05,660
That man. He's police, right?

65
00:09:06,860 --> 00:09:07,650
So?

66
00:09:08,410 --> 00:09:11,120
I just wondering what I should do.

67
00:09:11,640 --> 00:09:16,240
Let's see how it goes.
We don't know how things will turn out.

68
00:09:17,320 --> 00:09:19,510
And it won't hurt to know police.

69
00:09:21,090 --> 00:09:24,160
You can use that room from now on.

70
00:09:24,160 --> 00:09:25,900
We need to see each
other often. Don't we?

71
00:09:28,420 --> 00:09:29,360
And...

72
00:09:33,790 --> 00:09:35,010
You can buy things
you need with this.

73
00:09:57,520 --> 00:10:00,420
Soo Min.
Yoon Soo Min!

74
00:10:02,310 --> 00:10:04,500
Clean your room, okay?

75
00:10:06,960 --> 00:10:09,580
Am I your helper?
Am I your maid?

76
00:10:09,580 --> 00:10:11,580
Why do I need to clean?

77
00:10:14,340 --> 00:10:16,270
Do you know what time it is?

78
00:10:16,270 --> 00:10:18,460
Why are you coming back so late?

79
00:10:19,470 --> 00:10:22,330
I don't know. I'm coming back
from my part-time job.

80
00:10:23,370 --> 00:10:25,500
This is your part-time job?
This? Huh?

81
00:10:26,950 --> 00:10:28,140
I don't know!

82
00:10:28,560 --> 00:10:30,250
Yoon Soon Min, you!

83
00:10:30,280 --> 00:10:32,270
Hey, open up.
Open up!

84
00:10:33,680 --> 00:10:35,950
Soo Min, are you hungry?

85
00:10:41,480 --> 00:10:42,730
You're sleeping again!

86
00:10:42,730 --> 00:10:45,430
Why aren't you studying?
Aren't you going to go to college?

87
00:10:45,640 --> 00:10:46,770
I'm not going to college.

88
00:10:46,770 --> 00:10:49,540
Why aren't you going to college?
What are you going to do then?

89
00:11:09,290 --> 00:11:13,930
<i>This is your sister's specialty.
Kimchi fried rice. Hope you enjoy it. Love you.</i>

90
00:11:50,390 --> 00:11:51,840
Goodbye, Gyung Mi.

91
00:13:06,980 --> 00:13:10,130
It's me, Jin Sook.
I just moved in.

92
00:13:10,680 --> 00:13:17,800
Okay, don't come by to my office.
And please don't ask why.

93
00:13:18,570 --> 00:13:19,680
Okay?

94
00:13:21,210 --> 00:13:24,140
Yeah, I'll see you tomorrow.

95
00:13:31,630 --> 00:13:33,000
You can begin now.

96
00:14:12,580 --> 00:14:14,860
I told you not to call
the guys into the office.

97
00:14:17,060 --> 00:14:18,320
Who are you guys?

98
00:14:45,750 --> 00:14:48,850
Who put you up to this?
Who was it?

99
00:14:51,540 --> 00:14:53,370
Was it Lee Jin Sook?

100
00:14:56,340 --> 00:14:57,560
Doc!

101
00:14:57,560 --> 00:15:01,090
I just heard from Gyung Ho.
Apparently, Jin Sook has hit Soo.

102
00:15:26,700 --> 00:15:28,460
Are you really going to continue this?

103
00:15:28,470 --> 00:15:30,420
That depends on how you do.

104
00:15:32,820 --> 00:15:33,950
Soo!

105
00:15:44,450 --> 00:15:46,500
Let's just end it here.

106
00:15:47,910 --> 00:15:50,960
I was... I was!

107
00:15:53,830 --> 00:15:55,990
I was wrong, Auntie.

108
00:15:58,410 --> 00:16:00,140
You should do better in the future.

109
00:16:11,820 --> 00:16:13,700
I gave in because of you.

110
00:16:13,700 --> 00:16:15,020
Thank you.

111
00:16:29,540 --> 00:16:30,790
This is the end, right?

112
00:16:32,790 --> 00:16:34,060
Okay.

113
00:17:16,360 --> 00:17:18,690
- Should we go have a drink?
- Let's have some rice wine.

114
00:17:35,770 --> 00:17:37,240
Sit down.

115
00:17:46,080 --> 00:17:50,900
I guess you don't have much
stamina nowadays.

116
00:17:58,600 --> 00:18:03,180
You wasted all the goods I gave you
and on top of that...

117
00:18:03,180 --> 00:18:06,770
You sent Commissioner Kang
to the heaven. What a waste.

118
00:18:07,760 --> 00:18:11,540
Doctor's Son is still living
happily ever after.

119
00:18:11,780 --> 00:18:15,810
This is Safari Moon's report card.

120
00:18:18,750 --> 00:18:20,540
What do you think?

121
00:18:23,070 --> 00:18:24,890
How is it?

122
00:18:25,960 --> 00:18:31,810
All your past memories are
flashing through your mind now. Aren't they?

123
00:18:34,610 --> 00:18:38,650
Then you must know now.

124
00:18:38,650 --> 00:18:45,610
Whether you've lived a good life
or lived a life with many regrets.

125
00:18:46,890 --> 00:18:48,630
You have regrets. Don't you?

126
00:18:53,180 --> 00:18:56,430
Considering our relationship so far...

127
00:18:57,830 --> 00:19:01,260
I'll give you another chance.

128
00:19:15,010 --> 00:19:17,540
It's been a long time,
Mr. Safari.

129
00:19:17,540 --> 00:19:20,150
I heard it's a little noisy up there.

130
00:19:22,770 --> 00:19:26,230
Mr. Safari, are you old now?
Why are you like this?

131
00:19:26,230 --> 00:19:29,260
Why are you dragging this on and on?

132
00:19:29,260 --> 00:19:31,720
Whether it's Doctor's Son
or Professor's Son.

133
00:19:31,720 --> 00:19:35,130
Do you want me to
give a special lecture?

134
00:19:49,360 --> 00:19:52,370
Dad, do you want me go up?

135
00:19:54,810 --> 00:19:56,030
Please be patient.

136
00:19:56,030 --> 00:19:57,740
Man, that's not it.

137
00:19:57,740 --> 00:19:59,970
I still think I need to go up to
Seoul and have a clean sweep.

138
00:20:05,260 --> 00:20:08,890
Excuse me. It's 15 days.

139
00:20:09,760 --> 00:20:13,100
Don't forget what went through
your mind during those 15 days.

140
00:20:13,100 --> 00:20:14,460
You understand?

141
00:20:15,500 --> 00:20:16,750
15 days.

142
00:20:33,550 --> 00:20:36,750
What? Did something happen?

143
00:20:38,200 --> 00:20:41,880
No, it's nothing.

144
00:20:47,800 --> 00:20:49,940
- Pil Joon.
- Yes.

145
00:20:51,950 --> 00:20:53,610
Change all these guys.

146
00:20:55,160 --> 00:20:58,230
Get out. I can't
stand the sight you.

147
00:21:24,230 --> 00:21:28,280
Good. I had something
to talk to you about.

148
00:21:28,280 --> 00:21:30,510
I wonder what you want to talk about?

149
00:21:30,620 --> 00:21:33,490
You, did you use me?

150
00:21:35,090 --> 00:21:38,600
Did you use me to get
Doctor's Son?

151
00:21:38,930 --> 00:21:43,380
What the heck are you talking about?
Why would I use you?

152
00:21:45,000 --> 00:21:49,480
Don't try to bullshit me.
I know everything.

153
00:21:49,480 --> 00:21:52,230
I know it was you who
pushed Nang Mahn.

154
00:21:52,230 --> 00:21:53,940
Nang Mahn?

155
00:21:53,950 --> 00:21:56,690
You mean the one who was used
was Nang Mahn?

156
00:21:56,820 --> 00:22:00,880
Don't strain yourself.
That kind of acting won't work here.

157
00:22:01,310 --> 00:22:06,070
Would you believe me if
I get Nang Mahn in front of you?

158
00:22:07,550 --> 00:22:11,470
If that'd make you believe me,
I'll get Nang Mahn right now.

159
00:22:13,820 --> 00:22:17,150
Then, would you believe
my true intentions?

160
00:22:48,600 --> 00:22:49,910
Hello.

161
00:22:50,120 --> 00:22:53,930
Soo! Soo!
Help me!

162
00:22:55,950 --> 00:22:57,390
Where are you?

163
00:22:58,520 --> 00:23:00,140
Okay. I'll be right there.

164
00:23:02,920 --> 00:23:05,200
- What is it?
- It's uncle Nang Mahn.

165
00:23:07,290 --> 00:23:09,590
- He's asking me to save him.
- What?

166
00:23:15,080 --> 00:23:19,430
Why on earth are you doing this to me?
What did I do wrong?

167
00:23:20,550 --> 00:23:22,410
You did many things wrong.

168
00:23:23,500 --> 00:23:27,390
It's tiring to go
back and forth, isn't it?

169
00:23:27,920 --> 00:23:31,250
Who did you talk to just now?

170
00:23:33,710 --> 00:23:36,780
Even if they kill him,
I think I need to go.

171
00:23:36,780 --> 00:23:40,090
No, call him again.

172
00:23:40,090 --> 00:23:41,970
It might be too late already.

173
00:23:42,390 --> 00:23:44,000
Call him Soo.

174
00:24:07,890 --> 00:24:09,130
Uncle Nang Mahn?

175
00:24:09,170 --> 00:24:11,960
Is it Soo? You are one step too late.

176
00:24:11,960 --> 00:24:14,810
<i>It's your turn after Nang Mahn.</i>

177
00:24:15,520 --> 00:24:20,720
So until then, be well.
You understand?

178
00:24:22,140 --> 00:24:25,010
Okay, come and get me.
Really, come and get me!

179
00:24:29,610 --> 00:24:32,290
Where are you, bastard?
Come now!

180
00:24:32,290 --> 00:24:33,630
Soo.

181
00:24:37,710 --> 00:24:39,270
It was Safari.

182
00:25:14,340 --> 00:25:15,470
Long time no see, big sister.

183
00:25:15,470 --> 00:25:17,940
Did you come after you've
been working in the field?

184
00:25:18,630 --> 00:25:20,210
You don't look that great at all.

185
00:25:20,720 --> 00:25:22,470
Thanks for coming.

186
00:25:22,990 --> 00:25:24,460
Where is Nang Mahn?

187
00:25:30,630 --> 00:25:32,650
Why did you do this?

188
00:25:33,350 --> 00:25:34,540
Did I ask you to bring him dead?

189
00:25:37,760 --> 00:25:41,270
This old man was so feisty
that there was no other way.

190
00:25:42,520 --> 00:25:47,020
So with this, you are not
going to doubt my intentions.

191
00:25:51,420 --> 00:25:55,290
You want to have a
village party in a room?

192
00:25:55,290 --> 00:25:57,430
I won't get any respect with that.

193
00:26:01,060 --> 00:26:06,530
I heard even Confucius started
with a few characters.

194
00:26:06,530 --> 00:26:09,150
Any beginning tends to be insufficient.

195
00:26:09,230 --> 00:26:11,110
Then, what about the end?

196
00:26:12,090 --> 00:26:14,730
Are you going to hook me up
with Pusan or something?

197
00:26:16,530 --> 00:26:18,930
That all depends on how well you do.

198
00:26:18,930 --> 00:26:22,720
I don't trust you, yet.

199
00:26:25,540 --> 00:26:31,090
'To bloom a single chrysanthemum,
the cuckoo must have cried since the spring.'

200
00:26:32,410 --> 00:26:33,480
Do you remember that poem?

201
00:26:33,480 --> 00:26:35,620
Poem my ass.

202
00:26:35,620 --> 00:26:37,860
I forgot all those things.

203
00:26:41,410 --> 00:26:43,640
It still feels like yesterday
when I used have

204
00:26:43,640 --> 00:26:46,160
stir-fried Soondae and
a shot of Soju with you.

205
00:26:50,670 --> 00:26:53,900
The Doc has gone over the line.

206
00:26:55,050 --> 00:27:00,780
You know what it means when
Chairman Jo mentions his name?

207
00:27:01,830 --> 00:27:03,340
It's over for Doc.

208
00:27:06,680 --> 00:27:10,750
I'm not going to let that happen.
You understand?

209
00:27:10,750 --> 00:27:12,590
Do you want to save Doc?

210
00:27:12,590 --> 00:27:16,160
If you to save your Doc,
then stop him now.

211
00:27:17,220 --> 00:27:20,630
Soo? I'll take care of that bastard.

212
00:27:22,430 --> 00:27:25,200
You just have to tell me where he is.

213
00:27:26,490 --> 00:27:30,090
The only way both
you and Doc can survive is

214
00:27:30,090 --> 00:27:33,350
for you to put your organization
behind and have power.

215
00:27:34,740 --> 00:27:37,980
I'm going to say this one last time.

216
00:27:39,890 --> 00:27:42,910
Please don't make me
pathetic like this, huh?

217
00:27:44,340 --> 00:27:46,030
Do you know what I mean?

218
00:27:46,030 --> 00:27:47,960
I'm not going to wait long.

219
00:27:51,810 --> 00:27:53,300
The deal comes first.

220
00:27:55,840 --> 00:27:58,700
I still don't trust you.

221
00:27:58,700 --> 00:28:01,090
And yet, you want Soo's
whereabouts?

222
00:28:02,380 --> 00:28:05,680
- I can't do that.
- So you want to make the deal first.

223
00:28:05,680 --> 00:28:07,630
Isn't that according to
the proper business ethics?

224
00:28:08,960 --> 00:28:14,310
I'll think over your proposal about Soo
after our transaction.

225
00:28:29,980 --> 00:28:32,330
Are you really going to do
business with that woman?

226
00:28:33,840 --> 00:28:37,280
This is the real deal this time.
You understand?

227
00:28:37,280 --> 00:28:39,400
Why are you doing it
real all of a sudden?

228
00:28:39,970 --> 00:28:42,530
The Doc and the rest of
his entourage.

229
00:28:42,530 --> 00:28:44,640
Don't you need to clear them out?

230
00:28:45,340 --> 00:28:47,350
Look at this.

231
00:28:47,350 --> 00:28:52,550
Are the rumors true? Are you and her
pillow talking late at night?

232
00:28:53,640 --> 00:28:54,880
Is that right?

233
00:28:54,880 --> 00:28:57,110
You better think before
you open your mouth.

234
00:29:02,630 --> 00:29:07,560
Safari killed Nang Mahn.
I wonder why he did that?

235
00:29:07,560 --> 00:29:09,980
Do you think Nang Mahn looked
around again after he went over?

236
00:29:10,240 --> 00:29:11,620
He could have.

237
00:29:11,630 --> 00:29:14,470
Ah, man... I can't figure this out.

238
00:29:18,330 --> 00:29:22,410
- So you must have a plan.
- What?

239
00:29:22,410 --> 00:29:25,160
- A plan to get rid of Safari.
- Are you getting scared?

240
00:29:25,180 --> 00:29:27,350
Scared? Who's scared?

241
00:29:27,350 --> 00:29:29,140
I'm going to get Safari.

242
00:29:34,100 --> 00:29:36,630
Meth Kim probably knows.

243
00:29:37,530 --> 00:29:40,620
- What?
- Where Safari is.

244
00:29:40,620 --> 00:29:43,090
Isn't the prosecutor's office
holding him now?

245
00:29:50,760 --> 00:29:55,340
What's this?
You scared me.

246
00:29:56,210 --> 00:29:57,690
Have you gone somewhere?

247
00:29:58,940 --> 00:30:01,730
Yeah, I went shopping.

248
00:30:01,730 --> 00:30:03,360
But you didn't buy anything.

249
00:30:05,250 --> 00:30:06,410
There were no items.

250
00:30:09,620 --> 00:30:11,600
Uncle Nang Mahn is dead.

251
00:30:12,120 --> 00:30:14,040
It's Safari's doing.

252
00:30:16,860 --> 00:30:18,290
I wonder why he did that.

253
00:30:21,380 --> 00:30:23,570
I think you know the reason.

254
00:30:24,850 --> 00:30:26,760
How would I know that?

255
00:30:27,110 --> 00:30:28,640
I'll give you a hypothetical example.

256
00:30:31,300 --> 00:30:34,320
In order for Safari to regain your trust...

257
00:30:34,320 --> 00:30:39,510
He killed Nang Mahn to show you
he had nothing to do with the set-up.

258
00:30:39,580 --> 00:30:42,390
I'm sure Safari must have called you.

259
00:30:43,570 --> 00:30:45,610
He would have asked you to trust him.

260
00:30:45,610 --> 00:30:49,350
And he would have wanted a deal.

261
00:30:54,660 --> 00:30:56,450
When is the deal going to happen?

262
00:30:57,330 --> 00:31:01,620
Yes, you're right.

263
00:31:02,650 --> 00:31:04,170
But...

264
00:31:05,880 --> 00:31:08,180
We have to do this deal.

265
00:31:09,380 --> 00:31:11,300
That's the only way we can survive.

266
00:31:12,290 --> 00:31:17,250
This deal is to keep you alive.
This deal is for you.

267
00:31:17,250 --> 00:31:19,950
That's not for me, but
for your greed!

268
00:31:25,250 --> 00:31:27,050
This is a plead as well as a warning.

269
00:31:28,260 --> 00:31:30,300
Do not do this deal.

270
00:31:34,940 --> 00:31:37,620
Chairman Jo from Pusan knows your name.

271
00:31:37,620 --> 00:31:39,730
You know what that means.

272
00:31:40,750 --> 00:31:43,180
I'm not going you let you die.

273
00:31:44,890 --> 00:31:49,060
This time... we need
to do this deal.

274
00:31:59,140 --> 00:32:01,190
Do you remember that
picture from long ago?

275
00:32:02,270 --> 00:32:03,990
Was it Apel Tower?

276
00:32:05,710 --> 00:32:08,140
You looked at the postcard picture often.

277
00:32:09,250 --> 00:32:12,700
There were two cool foreign
models kissing in front of the Tower.

278
00:32:14,620 --> 00:32:17,080
You told me then.

279
00:32:17,080 --> 00:32:19,750
You wanted to make
a lot of money later.

280
00:32:21,350 --> 00:32:24,960
And you wanted to pose in front of
the Tower just like those models.

281
00:32:24,960 --> 00:32:26,780
You told me that was your wish.

282
00:32:34,680 --> 00:32:36,460
You can do that now.

283
00:32:38,340 --> 00:32:43,450
It's different now so,
you can go any time.

284
00:32:45,880 --> 00:32:46,980
Then what about you?

285
00:32:50,100 --> 00:32:54,500
If I go, will you come with me?

286
00:32:57,890 --> 00:33:04,680
If you go, I can go, too.
Anytime.

287
00:33:06,010 --> 00:33:07,580
Jin Sook.

288
00:33:10,840 --> 00:33:13,290
I just want you to be happy.

289
00:33:19,830 --> 00:33:21,480
I'm trying.

290
00:34:40,740 --> 00:34:41,960
You have a phone call.

291
00:34:45,150 --> 00:34:46,330
Who's calling?

292
00:34:46,450 --> 00:34:48,330
<i>It's me.</i>

293
00:34:49,460 --> 00:34:50,880
Is it you, Doc?

294
00:34:51,530 --> 00:34:56,050
Let Jin Sook go.

295
00:34:57,940 --> 00:34:59,120
Please.

296
00:35:02,930 --> 00:35:04,850
Who's letting go of who?

297
00:35:05,760 --> 00:35:09,710
You've spent enough time in this
business to know what you should know.

298
00:35:09,710 --> 00:35:12,210
And yet, you still say
such naive things.

299
00:35:13,860 --> 00:35:16,430
<i>Well, let's just say you are still naive.</i>

300
00:35:16,430 --> 00:35:19,920
Then if I told you to leave this
business, would you leave?

301
00:35:20,700 --> 00:35:22,770
<i>Once you leave, what
are you going to do?</i>

302
00:35:22,770 --> 00:35:24,530
<i>Are you going to get
a job at a company?</i>

303
00:35:24,530 --> 00:35:29,940
<i>If not, would you
run a restaurant? Huh?</i>

304
00:35:33,630 --> 00:35:40,760
You, me and also Jin Sook
will never be able to leave forever.

305
00:35:40,760 --> 00:35:42,420
You know...

306
00:35:42,420 --> 00:35:47,740
There is an way into this
business, but there is no way out.

307
00:35:49,590 --> 00:35:50,940
You can do the deal with me.

308
00:35:52,440 --> 00:35:55,520
Without Jin Sook,
you should do it with me.

309
00:35:58,390 --> 00:36:00,860
That's not a deal.
That's war.

310
00:36:03,110 --> 00:36:05,390
<i>Sounds like you still haven't
figured out the situation.</i>

311
00:36:05,390 --> 00:36:08,610
I'll tell you this one last thing.

312
00:36:08,610 --> 00:36:12,580
You have to die.

313
00:36:12,580 --> 00:36:15,070
That's when this will be over.

314
00:36:16,660 --> 00:36:19,260
Then I'll see you soon.

315
00:37:39,490 --> 00:37:41,130
Excuse me.

316
00:37:59,200 --> 00:38:00,600
President Jung?

317
00:38:02,700 --> 00:38:04,360
Did he want to see me here?

318
00:38:07,550 --> 00:38:08,880
Long time no see.

319
00:38:10,670 --> 00:38:11,730
That's right, President Jung.

320
00:38:11,730 --> 00:38:13,430
I'll get to the point.

321
00:38:15,080 --> 00:38:16,770
Where is Meth Kim now?

322
00:38:17,290 --> 00:38:19,820
- You know, that's...
- I told you already.

323
00:38:20,250 --> 00:38:24,100
What kind of friend I could be
all depends on what you do.

324
00:38:58,010 --> 00:39:00,080
Are you Doctor's Son?

325
00:39:00,470 --> 00:39:01,420
Turn around.

326
00:39:02,940 --> 00:39:04,170
Turn around!

327
00:39:15,360 --> 00:39:18,570
Who was it?
Who brought you here?

328
00:39:19,090 --> 00:39:21,490
- Who was it?
- Doctor.

329
00:39:22,340 --> 00:39:23,630
Doctor's Son.

330
00:39:38,540 --> 00:39:41,830
Please find out where
Meth Kim is now.

331
00:39:42,060 --> 00:39:43,930
What are you going
to do with Meth Kim?

332
00:39:44,730 --> 00:39:47,130
Meth Kim knew who Doctor's Son was.

333
00:39:49,850 --> 00:39:55,660
Okay. The prosecutors are shielding
him so it might be difficult but...

334
00:39:58,240 --> 00:39:59,670
I'll give it a shot.

335
00:40:00,210 --> 00:40:02,490
- But don't get your hopes up.
- Yes.

336
00:40:05,870 --> 00:40:06,710
[Prosecutor Oh Jung Yeon]

337
00:40:07,730 --> 00:40:09,210
Yeah, so what happened?

338
00:40:09,210 --> 00:40:11,310
<i>It was difficult.
I want you to know that.</i>

339
00:40:11,310 --> 00:40:12,480
Did you find out?

340
00:40:12,550 --> 00:40:13,650
<i>Yeah, I found out.</i>

341
00:40:14,110 --> 00:40:16,310
But Meth Kim is at a hospital now.

342
00:40:18,000 --> 00:40:19,380
Okay, thanks.

343
00:40:38,090 --> 00:40:40,720
You wait for us here.
Let's go.

344
00:41:15,650 --> 00:41:18,290
Stop acting and let's go.

345
00:42:08,340 --> 00:42:09,620
Aren't you going to get on?

346
00:42:45,010 --> 00:42:46,980
Who was it?
Who did this?

347
00:42:46,980 --> 00:42:50,070
I don't know.
They came in out of the blue.

348
00:43:06,450 --> 00:43:07,870
It's me President Jung.

349
00:43:07,870 --> 00:43:09,370
Yes, go ahead.

350
00:43:09,370 --> 00:43:11,150
<i>Meth Kim has disappeared.</i>

351
00:43:12,020 --> 00:43:14,320
Someone beat the crap out of
our guys and picked him up.

352
00:43:16,140 --> 00:43:18,090
Did President Jung pick him up?

353
00:43:22,900 --> 00:43:24,880
Someone took Meth Kim.

354
00:43:24,890 --> 00:43:28,170
How did you find out
where Meth Kim was?

355
00:43:28,460 --> 00:43:31,460
The important thing is
who took him.

356
00:43:32,210 --> 00:43:34,140
It was probably Doctor's Son.

357
00:43:49,700 --> 00:43:53,920
What's going on?
You dramatically rescued me and all.

358
00:43:54,870 --> 00:43:58,010
I didn't get you out to chit-chat.

359
00:43:58,010 --> 00:44:00,130
So shut up and sit tight.

360
00:44:22,860 --> 00:44:24,480
Long time no see.

361
00:44:31,300 --> 00:44:33,750
How did you get out this time?

362
00:44:33,750 --> 00:44:35,970
Actually, I wanted to see you soon.

363
00:44:37,720 --> 00:44:40,010
I told you it'd be your turn next.

364
00:44:55,210 --> 00:44:58,660
<i>Doc... Doc!</i>

365
00:44:59,230 --> 00:45:00,230
What's the matter?

366
00:45:18,120 --> 00:45:19,550
You've been calling often.

367
00:45:19,550 --> 00:45:21,720
Where is Soo now?

368
00:45:21,720 --> 00:45:23,520
I'm taking good care of him here.

369
00:45:25,810 --> 00:45:29,530
If you want to rescue your friend,
you know what to do.

370
00:45:29,530 --> 00:45:31,000
Come on over here.

371
00:45:31,430 --> 00:45:33,610
Come alone... quietly.

372
00:45:33,610 --> 00:45:35,130
If not, your friend will be dead.

373
00:45:35,210 --> 00:45:36,480
Don't touch Soo.

374
00:45:36,570 --> 00:45:40,980
That's up to you.
Do you understand what I mean?

375
00:45:50,590 --> 00:45:54,020
<i>He's one of the ones you
are supposed to catch.</i>

376
00:45:55,860 --> 00:46:00,140
<i>Soo... was found out by Ji Hyung Min.</i>

377
00:46:16,240 --> 00:46:18,070
<i>Yeah, it's me.</i>

378
00:46:20,020 --> 00:46:21,470
Is everything okay?

379
00:46:23,900 --> 00:46:27,600
Meth Kim disappeared
from the hospital.

380
00:46:27,600 --> 00:46:28,990
Was it you?

381
00:46:31,900 --> 00:46:33,480
No.

382
00:46:34,580 --> 00:46:38,730
<i>Then, do you
know who did it?</i>

383
00:46:39,480 --> 00:46:40,490
No.

384
00:46:41,680 --> 00:46:44,120
Why do you sound like that?

385
00:46:44,120 --> 00:46:45,630
There's something wrong. Am I right?

386
00:46:47,520 --> 00:46:48,610
<i>Shi Yun.</i>

387
00:46:51,530 --> 00:46:57,080
Mister... help me.

388
00:47:00,470 --> 00:47:04,610
If it's something like that,
I can't help you.

389
00:47:09,660 --> 00:47:11,430
I can get Safari today.

390
00:47:11,430 --> 00:47:14,420
If something goes wrong,
you could get discovered.

391
00:47:14,830 --> 00:47:16,430
I have to go.

392
00:47:16,450 --> 00:47:19,400
<i>Don't go. You need to give up
on Kim Hyun Soo.</i>

393
00:47:19,400 --> 00:47:20,280
I have to go!

394
00:47:20,280 --> 00:47:24,460
You could put yourself
in danger because of Soo.

395
00:47:26,750 --> 00:47:28,160
I will go.

396
00:47:30,310 --> 00:47:31,750
Honestly...

397
00:47:38,340 --> 00:47:40,000
This is too difficult.

398
00:47:42,450 --> 00:47:43,900
<i>Shi Hyun.</i>

399
00:47:47,370 --> 00:47:48,660
<i>Shi Hyun.</i>

400
00:47:50,800 --> 00:47:55,440
Please take care of Jin Sook.

401
00:47:57,890 --> 00:47:59,160
Shi Hyun!

402
00:47:59,500 --> 00:48:00,910
Jung Shi Hyun!

403
00:48:15,280 --> 00:48:17,680
- Get out. I'm going alone.
- Doc.

404
00:48:19,790 --> 00:48:21,240
I'm going alone.

405
00:49:21,040 --> 00:49:22,150
Doctor's Son!

406
00:49:52,490 --> 00:49:53,770
Let Soo go first.

407
00:49:53,770 --> 00:49:59,290
In this situation you are
worried about your friend.

408
00:50:00,390 --> 00:50:05,330
Coming here alone
meant you came ready to die.

409
00:50:07,180 --> 00:50:09,490
I'm not sure whether
you'd believe me but

410
00:50:09,960 --> 00:50:15,350
considering our friendship of the past,
I wasn't going to kill you.

411
00:50:17,250 --> 00:50:23,640
It's all because of you that someone
as naive as me turned this way.

412
00:50:25,210 --> 00:50:27,640
Let go of Soo and we'll talk.

413
00:50:27,640 --> 00:50:30,960
Are you deaf? Once you step in here,
you can't go out.

414
00:50:31,460 --> 00:50:34,690
Nobody can. You got that?

415
00:50:38,660 --> 00:50:40,140
I understand.

416
00:50:45,460 --> 00:50:48,350
- Come on now.
- Hit him.

417
00:52:43,330 --> 00:52:46,880
Wow, then, are you Doctor's Son?

418
00:52:47,610 --> 00:52:48,710
Doctor's Son.

419
00:52:49,440 --> 00:52:52,390
You are truly alone now.

420
00:52:52,620 --> 00:52:57,940
The only thing you can
trust is this and this.

421
00:53:04,400 --> 00:53:07,570
Doctor's Son. Let's settle the scores today.

422
00:53:07,570 --> 00:53:10,430
Whether you are or I am the hubby.

423
00:53:13,720 --> 00:53:15,280
Doctor's Son.

424
00:53:35,470 --> 00:53:37,110
Shi Hyun.

425
00:53:38,280 --> 00:53:40,360
Doctor's Son, Jung Shi Hyun.

426
00:53:42,810 --> 00:53:45,210
I'm going to ask you
for one last favor.

427
00:53:47,600 --> 00:53:50,520
Would you like to go to China?
I'll find out about ferries.

428
00:53:52,420 --> 00:53:53,780
Uncle Duk Bae.

429
00:53:57,290 --> 00:53:59,860
Didn't you say one of us has to die?

430
00:54:03,480 --> 00:54:08,590
That's right. I was talking nonsense.

431
00:54:08,590 --> 00:54:09,930
I'm sorry.

432
00:54:15,560 --> 00:54:17,090
Let me ask you a favor.

433
00:54:21,760 --> 00:54:22,960
Him...

434
00:54:25,380 --> 00:54:26,750
Let Soo go.

435
00:54:29,430 --> 00:54:30,690
I'm sorry.

436
00:54:32,770 --> 00:54:36,850
But I promise I'll protect Jin Sook.

437
00:54:40,730 --> 00:54:42,230
Shi Hyun.

438
00:54:44,390 --> 00:54:46,840
I'll ask you one last time.

439
00:54:47,550 --> 00:54:51,040
Don't resist. It'll be more painful.

440
00:54:52,100 --> 00:54:53,510
Got it?

441
00:54:58,970 --> 00:55:04,920
Okay, let's stop getting sentimental
and finish it quickly.

442
00:55:04,920 --> 00:55:05,990
Okay.

443
00:55:07,590 --> 00:55:09,340
Thanks for understanding.

444
00:55:42,990 --> 00:55:56,420
Subtitles by DramaFever

445
00:56:01,850 --> 00:56:04,350
Sit here and wait for me.
I'll bring it right away.

446
00:56:07,510 --> 00:56:11,070
<i>If you have a lover, you can
meet when you feel down.</i>

447
00:56:11,070 --> 00:56:14,250
<i>And it will make you feel better.
I just thought that it'd be like that.</i>

448
00:56:14,250 --> 00:56:15,620
<i>What is he like?</i>

449
00:56:15,620 --> 00:56:18,480
<i>My lover is a little busy.</i>

450
00:56:18,480 --> 00:56:20,450
<i>You're not seeing anyone?</i>

451
00:56:20,450 --> 00:56:23,050
<i>He doesn't even
think of me as a woman.</i>

452
00:56:25,400 --> 00:56:30,340
<i>The true nature of Pusan has never
been revealed. They are like phantoms.</i>

453
00:56:30,340 --> 00:56:33,740
<i>I believe Pusan's organization
definitely exists.</i>

454
00:56:33,740 --> 00:56:35,790
<i>This one thing is for sure.</i>

455
00:56:35,790 --> 00:56:41,850
If you try to dig deeper,
a lot of your friends will die. It could be you next.

456
00:56:42,930 --> 00:56:44,660
<i>There won't be any deals for a while.</i>

457
00:56:44,660 --> 00:56:45,850
<i>What are you talking about?</i>

458
00:56:45,850 --> 00:56:47,800
<i>Connect me directly to Pusan.</i>

459
00:56:47,800 --> 00:56:49,850
<i>Do you know what kind of place Pusan is?</i>

