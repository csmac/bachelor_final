1
00:04:18,759 --> 00:04:20,261
As you can see,

2
00:04:20,260 --> 00:04:23,730
our Cultural Attache Department
has extended our ESL program

3
00:04:23,722 --> 00:04:28,478
to the rest of Bulgaria
with extraordinary results.

4
00:04:28,477 --> 00:04:31,947
Sofia Private School
has the best scores in the country.

5
00:04:31,938 --> 00:04:33,611
I think you should give
each and every one of yourselves

6
00:04:33,607 --> 00:04:36,110
a round of applause
for this extraordinary accomplishment.

7
00:05:40,590 --> 00:05:42,058
Aah!

8
00:05:49,266 --> 00:05:50,609
No witnesses.

9
00:07:20,482 --> 00:07:22,484
So how's the good doctor this evening?

10
00:07:23,234 --> 00:07:25,737
Happy to have the company
of my old friend.

11
00:07:38,500 --> 00:07:40,423
Attractive, huh?

12
00:07:40,418 --> 00:07:42,716
Yes, very.

13
00:07:43,672 --> 00:07:46,926
So what's it been now, Robert, hmm?

14
00:07:46,925 --> 00:07:49,519
Three years? Four?

15
00:07:51,429 --> 00:07:53,648
It's been four years today, actually.

16
00:07:56,017 --> 00:07:57,519
Look around, my friend.

17
00:07:59,187 --> 00:08:00,734
If you wanted to,

18
00:08:00,730 --> 00:08:04,075
you could go home
with a different one every night.

19
00:08:06,486 --> 00:08:09,035
It's not exactly my style,
you know that.

20
00:08:09,030 --> 00:08:10,327
What, then?

21
00:08:13,118 --> 00:08:15,792
You're still punishing yourself

22
00:08:16,496 --> 00:08:18,874
about the accident, are you'?

23
00:08:19,249 --> 00:08:21,593
Robert, you need to
deal with it, my friend.

24
00:08:21,584 --> 00:08:25,088
I know what carrying that kind
of burden around can do to you.

25
00:08:25,463 --> 00:08:28,512
I appreciate your concern as always,

26
00:08:28,508 --> 00:08:31,853
but as long as I stay here
in Eastern Europe,

27
00:08:31,845 --> 00:08:34,849
all of that stays back in New York.
Out of sight, out of mind.

28
00:08:34,848 --> 00:08:36,350
Well, that's...

29
00:08:37,142 --> 00:08:39,440
It doesn't work that way, my friend.

30
00:08:39,436 --> 00:08:40,562
I think you need therapy.

31
00:08:42,480 --> 00:08:44,482
I'll tell you what I do need.
I need a drink.

32
00:08:45,025 --> 00:08:45,947
Where's our waitress?

33
00:10:35,218 --> 00:10:40,475
So, Vicki, how are we today, hmm?

34
00:10:40,473 --> 00:10:41,975
I'm okay.

35
00:10:41,975 --> 00:10:44,478
Good.

36
00:10:44,477 --> 00:10:47,731
Any more memory lapses, hmm?

37
00:10:48,898 --> 00:10:50,445
Any more bad dreams?

38
00:10:53,695 --> 00:10:56,915
I sense that you're
avoiding conversation today.

39
00:11:02,328 --> 00:11:06,333
Perhaps you'll indulge me
in a brief activity, hmm?

40
00:11:14,257 --> 00:11:19,809
It's a hypnotic technique we use
to open a patient's senses,

41
00:11:19,804 --> 00:11:22,102
to tap into the imagination.

42
00:11:23,266 --> 00:11:27,021
It's really quite harmless.
You can trust me.

43
00:11:27,437 --> 00:11:29,610
You're not afraid of a simple
little blindfold, are you?

44
00:11:30,899 --> 00:11:33,618
You can remove it
whenever you want to.

45
00:11:33,610 --> 00:11:35,032
I promise.

46
00:11:40,742 --> 00:11:45,919
And just relax.

47
00:11:45,914 --> 00:11:48,713
Breathe and relax.

48
00:11:50,710 --> 00:11:55,056
Now, I asked you about your dreams.

49
00:11:58,676 --> 00:12:00,770
Any recently that you recall'?

50
00:12:02,889 --> 00:12:05,062
Yes, one.

51
00:12:06,392 --> 00:12:07,689
Go on.

52
00:12:09,687 --> 00:12:11,815
It's embarrassing, actually.

53
00:12:13,608 --> 00:12:17,738
It's like a re-creation
from something I read long ago.

54
00:12:18,947 --> 00:12:22,417
Have you read Hubert Selby, Jr.,
Last Exit to Brooklyn?

55
00:12:22,408 --> 00:12:24,661
No, I can't say I have.

56
00:12:28,373 --> 00:12:29,670
There is this part at the end

57
00:12:29,666 --> 00:12:35,423
where this junkie chick
climbs into an abandoned car

58
00:12:36,464 --> 00:12:39,092
and lets every guy
in the neighborhood screw her.

59
00:12:39,092 --> 00:12:45,896
Every scumbag and lowlife
for miles around get in this line,

60
00:12:45,890 --> 00:12:48,609
and the line
goes around the block.

61
00:12:48,601 --> 00:12:52,356
How did it affect you, the scene,
when you read it?

62
00:12:53,564 --> 00:12:56,659
At the time, it was disturbing.

63
00:13:00,571 --> 00:13:02,573
But in this dream...

64
00:13:06,786 --> 00:13:09,460
I'm the girl in the abandoned car.

65
00:13:11,332 --> 00:13:14,427
I can never see their faces.

66
00:13:14,419 --> 00:13:19,300
They're hidden by keffiyehs,

67
00:13:19,299 --> 00:13:23,270
you know, those Arabic scarves.

68
00:13:23,261 --> 00:13:25,730
They're pulled over their faces,

69
00:13:25,722 --> 00:13:28,475
and every one of those men...

70
00:13:29,309 --> 00:13:30,561
Go on.

71
00:13:31,602 --> 00:13:32,819
They're all the same man.

72
00:13:34,647 --> 00:13:36,274
Have you thought who he might be...

73
00:13:38,526 --> 00:13:40,779
the man wearing the keffiyeh?

74
00:13:48,703 --> 00:13:49,920
Vicki?

75
00:13:51,122 --> 00:13:53,170
Vicki?

76
00:13:53,166 --> 00:13:54,543
- Vicki?
- What?

77
00:13:56,044 --> 00:13:57,546
You blanked out there.

78
00:13:57,545 --> 00:14:02,176
Went somewhere else
as you answered my questions.

79
00:14:02,175 --> 00:14:03,802
I did?

80
00:14:04,469 --> 00:14:05,686
Answered your questions?

81
00:14:08,181 --> 00:14:10,479
What questions?
What did I say?

82
00:14:10,475 --> 00:14:13,775
Well, you were away
for several moments.

83
00:14:14,437 --> 00:14:16,189
You don't remember
what we were talking about?

84
00:14:19,108 --> 00:14:21,782
No, I don't remember.

85
00:14:35,792 --> 00:14:37,169
One moment, please.

86
00:14:37,835 --> 00:14:40,088
Here she is now.

87
00:14:40,797 --> 00:14:42,174
This is the man
from the embassy, Vicki.

88
00:14:42,173 --> 00:14:43,516
Robert Diggs.

89
00:14:43,508 --> 00:14:45,602
Please meet my wife, Vicki Denov.

90
00:14:45,593 --> 00:14:47,561
Pleased to meet you. Call me Vicki.

91
00:14:47,553 --> 00:14:48,725
Sure.

92
00:14:48,721 --> 00:14:50,894
So you'll be spending some time
with us today then?

93
00:14:50,890 --> 00:14:52,483
Yes, that's correct.

94
00:14:52,475 --> 00:14:54,227
Your English is excellent,
by the way.

95
00:14:54,227 --> 00:14:55,479
It better be.

96
00:14:56,479 --> 00:14:57,856
Shall we? I'm running late.

97
00:14:57,855 --> 00:14:59,573
Sure. It's a pleasure.

98
00:14:59,565 --> 00:15:01,158
Bye.

99
00:15:06,906 --> 00:15:11,002
Proper nouns includes words
like Sofia and Bulgaria.

100
00:15:10,993 --> 00:15:15,749
When writing proper nouns,
you capitalize the first letter.

101
00:15:15,748 --> 00:15:18,092
Pavo, can you give us examples
of proper nouns?

102
00:15:19,669 --> 00:15:22,047
Atea, Mexico,

103
00:15:22,046 --> 00:15:25,346
New York, Istanbul,

104
00:15:25,341 --> 00:15:28,345
Washington, Mexico City,

105
00:15:28,344 --> 00:15:29,812
Cairo.

106
00:15:46,779 --> 00:15:48,406
Tiger Woods.

107
00:15:49,532 --> 00:15:51,034
David Beckham.

108
00:15:54,829 --> 00:15:57,628
Very good. You may sit.

109
00:15:57,623 --> 00:16:01,469
Names, places, and titles.

110
00:16:31,991 --> 00:16:34,164
I'm going to the ambassador's office.

111
00:16:41,375 --> 00:16:43,798
- Robert Diggs.
- Ambassador.

112
00:16:43,794 --> 00:16:45,011
Good to finally meet you.

113
00:16:45,004 --> 00:16:46,597
Pleasure's all mine, ambassador.

114
00:16:48,466 --> 00:16:49,968
Sorry to drag you in like this.

115
00:16:49,967 --> 00:16:51,435
Well, I have to admit, sir,

116
00:16:51,427 --> 00:16:53,646
that your email did arouse my curiosity.

117
00:16:53,638 --> 00:16:55,891
Let's get some air.

118
00:16:55,890 --> 00:16:57,892
Project Sofia?
Sure, I remember.

119
00:16:57,892 --> 00:17:01,772
A counter-terrorism program
based here in the city, defunct now.

120
00:17:01,771 --> 00:17:04,741
Yes. Black ops.

121
00:17:05,483 --> 00:17:08,282
A lot of fuss in the press
about it here, though.

122
00:17:09,070 --> 00:17:12,995
Our agents were good guys,
really great.

123
00:17:12,990 --> 00:17:14,958
They've been able to infiltrate

124
00:17:14,951 --> 00:17:18,797
some of the best organized
terrorist-radical groups

125
00:17:18,788 --> 00:17:20,756
from New York to Madrid,

126
00:17:20,748 --> 00:17:23,422
and they were not able...

127
00:17:23,417 --> 00:17:26,591
not able to get into Sofia, ever.

128
00:17:26,587 --> 00:17:28,715
Not then, not now.

129
00:17:28,714 --> 00:17:33,345
I gotta talk to you about
what's going on with Sofia today.

130
00:17:34,262 --> 00:17:35,514
I thought it was dead in the water.

131
00:17:35,513 --> 00:17:38,062
Yeah. Seemed to be

132
00:17:38,891 --> 00:17:40,234
until yesterday.

133
00:17:44,397 --> 00:17:46,365
Yeah, I read about this this morning.

134
00:17:46,357 --> 00:17:51,409
I've been on the phone all night
with the States dealing with this.

135
00:17:51,404 --> 00:17:54,624
Turns out Washington
knows all about the fella

136
00:17:54,615 --> 00:17:56,834
this vigilante terminated.

137
00:17:56,826 --> 00:17:59,750
He was a target
of highest priority for us,

138
00:17:59,745 --> 00:18:02,794
and we've never been able
to get anywhere near him.

139
00:18:02,790 --> 00:18:05,794
I guess the guys back home
are feeling some heat from higher up,

140
00:18:05,793 --> 00:18:09,138
and now they want to find out
exactly what's going on here.

141
00:18:09,130 --> 00:18:14,102
So I have been specifically instructed
to, quote, unquote, persuade you

142
00:18:14,093 --> 00:18:16,391
to take charge of
the investigation here.

143
00:18:16,387 --> 00:18:17,388
Why me?

144
00:18:17,388 --> 00:18:19,766
Because you are exemplary.

145
00:18:19,765 --> 00:18:21,517
No, seriously.

146
00:18:21,517 --> 00:18:24,487
That's what your FBI friends called you.
They said you were exemplary.

147
00:18:24,478 --> 00:18:28,403
I've never had anybody say
I was exemplary in my whole life.

148
00:18:29,108 --> 00:18:30,234
Leaving law enforcement

149
00:18:30,234 --> 00:18:32,862
was a well-thought-out decision
on my part, ambassador.

150
00:18:35,781 --> 00:18:38,125
Yeah. Yeah, I know.

151
00:18:38,117 --> 00:18:41,872
I know. They told me about that,
and I'm truly sorry.

152
00:18:41,871 --> 00:18:43,214
Come on.

153
00:18:43,205 --> 00:18:48,086
But it seems that the ramifications of this
are very big... politically, globally.

154
00:18:49,879 --> 00:18:55,136
See, these vigilantes have taken on
a whole string of high-profile targets,

155
00:18:55,134 --> 00:18:58,138
targets your FBI guys
told me on the phone last night

156
00:18:58,137 --> 00:19:02,017
that they've been tracking for years
and could never get a look at.

157
00:19:02,558 --> 00:19:04,356
So now they need to know

158
00:19:04,352 --> 00:19:08,482
how come these vigilantes
can get inside when they can't'?

159
00:19:09,148 --> 00:19:10,400
And who are they, these guys?

160
00:19:10,399 --> 00:19:13,027
And what do they know?
How do they know it'?

161
00:19:13,027 --> 00:19:14,870
And why them, not us?

162
00:19:14,862 --> 00:19:16,535
What intel do we have?

163
00:19:16,530 --> 00:19:21,331
They tell me that some militant
is setting up safe houses.

164
00:19:21,327 --> 00:19:25,207
He doesn't know why,
but he knows they're here in villages.

165
00:19:25,873 --> 00:19:27,375
Islamic Jihad.

166
00:19:27,375 --> 00:19:29,343
Am I being pressured
into this assignment, sir?

167
00:19:30,670 --> 00:19:33,799
You have complete disclosure,
full disclosure, everything.

168
00:19:33,798 --> 00:19:35,300
Consul General has told me

169
00:19:35,299 --> 00:19:36,972
that your clearances
have been made complete.

170
00:19:36,967 --> 00:19:39,220
If you need help from local
law enforcement, you have it.

171
00:19:39,220 --> 00:19:41,473
We need to get a handle
on this, Robert.

172
00:19:42,890 --> 00:19:45,109
And you're the man for it.

173
00:19:45,101 --> 00:19:46,273
You're...

174
00:19:46,936 --> 00:19:48,859
Well, you know what you are.

175
00:19:50,314 --> 00:19:53,193
- Exemplary.
- Exemplary.

176
00:21:31,916 --> 00:21:34,089
Wait, wait, wait, wait, wait.
Wait.

177
00:25:11,760 --> 00:25:14,513
Lonewolf. Proceeding with next target.

178
00:28:34,463 --> 00:28:36,557
No witnesses.

179
00:29:04,868 --> 00:29:06,620
These are crazy times.

180
00:29:06,620 --> 00:29:08,748
No. He's clever, this guy,

181
00:29:08,747 --> 00:29:12,377
and based on the efficiency of the hit,
very well equipped.

182
00:29:13,585 --> 00:29:15,212
Any idea who might be behind it?

183
00:29:15,212 --> 00:29:17,590
None whatsoever, but, uh,

184
00:29:17,589 --> 00:29:20,718
it does have me a little bit curious
as to why.

185
00:29:20,717 --> 00:29:22,310
Why don't you give me
your professional opinion?

186
00:29:22,302 --> 00:29:25,226
I mean, what makes somebody
become a vigilante?

187
00:29:25,222 --> 00:29:27,270
I mean, profile him for me.

188
00:29:29,184 --> 00:29:32,108
Well, let's see.

189
00:29:33,730 --> 00:29:37,234
Well, to take responsibility
for the crime, right?

190
00:29:37,234 --> 00:29:40,204
But he hides behind a mask.

191
00:29:40,195 --> 00:29:42,994
It takes guts, patience,

192
00:29:42,990 --> 00:29:45,743
and an odd sort of morality.

193
00:29:46,952 --> 00:29:48,920
And he's gotten away with it
a number of times,

194
00:29:48,912 --> 00:29:52,007
which has probably
emboldened him and...

195
00:29:51,999 --> 00:29:53,842
makes him think
he's not gonna get caught.

196
00:29:56,253 --> 00:29:59,427
Lovely.

197
00:30:01,341 --> 00:30:03,719
Well, let's hope he's wrong about that.

198
00:30:07,055 --> 00:30:08,853
Our friend from the other night.

199
00:30:24,448 --> 00:30:29,796
The fascinating thing about the female form
is its supple nature, is it not?

200
00:30:29,786 --> 00:30:34,508
Its elasticity, its alacrity, and...

201
00:30:37,002 --> 00:30:38,675
variability.

202
00:30:47,179 --> 00:30:49,181
You're a slow starter, my friend,

203
00:30:49,181 --> 00:30:52,560
You have impeccable taste.

204
00:31:00,734 --> 00:31:02,202
She's interested in you.

205
00:31:02,194 --> 00:31:04,492
It's obvious.

206
00:31:07,741 --> 00:31:10,665
Robert, celibacy is for monks,
my friend,

207
00:31:10,660 --> 00:31:13,288
and I'm not convinced
it's very good for them, either.

208
00:31:14,706 --> 00:31:17,380
Look, it's not that I don't want
to meet somebody,

209
00:31:17,376 --> 00:31:18,878
I just...

210
00:31:20,003 --> 00:31:21,971
I don't want to lose somebody, either.

211
00:31:22,881 --> 00:31:25,384
I'm not suggesting you marry her.

212
00:31:25,384 --> 00:31:28,228
Anyway, these Eastern Bloc girls
can be trouble.

213
00:31:28,220 --> 00:31:29,972
No, just keep it short and sweet.

214
00:31:31,098 --> 00:31:34,068
Two dates, three maximum.

215
00:31:36,895 --> 00:31:39,398
There's definitely something about her.

216
00:31:39,398 --> 00:31:43,869
Well, go for it, my friend.

217
00:31:43,860 --> 00:31:47,581
You only live once, allegedly, right'?

218
00:31:47,572 --> 00:31:50,246
What are you doing?
Where you going?

219
00:31:50,242 --> 00:31:51,744
Come on, don't leave me here.

220
00:31:51,743 --> 00:31:53,586
I'm sure I'm not even her type.

221
00:31:53,578 --> 00:31:55,706
Listen, Robert...

222
00:31:56,540 --> 00:32:01,137
I know that losing your wife
was tough, the worst.

223
00:32:01,795 --> 00:32:03,889
I'm going to ask you a question.
You don't have to answer it.

224
00:32:04,881 --> 00:32:09,057
Is it you're afraid
of losing another woman,

225
00:32:09,052 --> 00:32:12,272
or is it you don't think
you deserve to have one, hmm?

226
00:32:14,349 --> 00:32:16,522
I'll see you very soon, my friend.

227
00:32:18,520 --> 00:32:20,397
Very soon.

228
00:33:42,604 --> 00:33:44,777
Wait.

229
00:33:46,233 --> 00:33:48,201
You are stunning.

230
00:33:48,193 --> 00:33:51,743
I'm sorry. I shouldn't have said that.

231
00:33:51,738 --> 00:33:54,742
Would you care to join me, miss?

232
00:33:54,741 --> 00:33:56,243
I'd love to.

233
00:33:56,243 --> 00:33:58,621
I'll go change.

234
00:34:04,209 --> 00:34:08,510
Your dancing is...

235
00:34:08,505 --> 00:34:11,554
It's extraordinary.
I've never seen anything like it.

236
00:34:11,550 --> 00:34:13,894
You're nice for an American.

237
00:34:13,885 --> 00:34:15,387
Thank you.

238
00:34:22,394 --> 00:34:24,146
Am I making you nervous?

239
00:34:24,145 --> 00:34:27,524
No, no, not at all.

240
00:34:27,524 --> 00:34:30,243
It's just that, um...

241
00:34:30,235 --> 00:34:34,490
you, uh...
you remind me of someone.

242
00:34:35,282 --> 00:34:37,626
Your wife perhaps back in America?

243
00:34:37,617 --> 00:34:39,460
This? No, no, no.

244
00:34:39,452 --> 00:34:41,750
I'm not married.

245
00:34:41,746 --> 00:34:43,419
Divorced.

246
00:34:43,415 --> 00:34:46,840
Is it an American custom
to wear the ring of the ex-wife'?

247
00:34:46,835 --> 00:34:50,135
No, no, no. I just wear it because, uh...

248
00:34:50,130 --> 00:34:51,006
Because you still love her?

249
00:34:52,924 --> 00:34:56,269
Wait. No, no, no.
You don't understand.

250
00:34:57,304 --> 00:34:58,521
Let me explain.

251
00:35:01,099 --> 00:35:04,194
Well, she, uh,
she died a few years ago.

252
00:35:05,353 --> 00:35:06,445
I'm sorry.

253
00:35:06,438 --> 00:35:07,985
No, no.

254
00:35:09,190 --> 00:35:11,238
I should go.

255
00:35:12,027 --> 00:35:14,280
No, no.

256
00:35:14,279 --> 00:35:16,577
Please tell me your name.

257
00:35:18,116 --> 00:35:19,163
Ursula.

258
00:35:20,785 --> 00:35:21,911
Ursula.

259
00:35:22,829 --> 00:35:23,876
I'm Robert.

260
00:35:24,748 --> 00:35:26,421
Nice to meet you, Robert.

261
00:35:26,416 --> 00:35:29,795
Well, look, I'd really love to see you again.

262
00:35:29,794 --> 00:35:31,671
Can I have your number?

263
00:35:43,475 --> 00:35:47,355
So you blanked out
completely, like last time?

264
00:35:47,354 --> 00:35:49,197
Yes.

265
00:35:49,189 --> 00:35:51,863
Do you recall anything this time'?

266
00:35:51,858 --> 00:35:53,326
Nothing.

267
00:35:53,318 --> 00:35:57,915
Well, think and tell me
what you do remember.

268
00:35:57,906 --> 00:36:00,580
It was a few days ago.

269
00:36:00,575 --> 00:36:02,293
I was in the kitchen.

270
00:36:02,285 --> 00:36:04,504
Mihail had left for work.

271
00:36:06,790 --> 00:36:10,385
The next thing I remember
it was night time.

272
00:36:10,377 --> 00:36:11,879
I'm in bed.

273
00:36:12,629 --> 00:36:15,178
I hadn't shown up for work all day.

274
00:36:15,173 --> 00:36:17,926
And you don't remember anything
that happened that day?

275
00:36:17,926 --> 00:36:20,145
Nothing.

276
00:36:20,136 --> 00:36:22,230
The next morning when I woke up,

277
00:36:22,222 --> 00:36:27,319
I was so exhausted,
I could hardly get out of bed.

278
00:36:27,310 --> 00:36:28,937
Are you still having the headaches?

279
00:36:48,957 --> 00:36:51,301
Are you still having the headaches?

280
00:36:53,044 --> 00:36:53,920
Sometimes.

281
00:36:56,798 --> 00:37:00,177
- Have you heard of syncope?
- No.

282
00:37:00,927 --> 00:37:03,476
It's complete loss of consciousness,

283
00:37:04,305 --> 00:37:07,354
interruption of awareness
of one's self and surroundings.

284
00:37:08,268 --> 00:37:10,896
If you feel yourself blanking out
again, it's simple.

285
00:37:11,438 --> 00:37:13,190
Just take deep breaths.

286
00:37:13,523 --> 00:37:14,900
Deep breaths.

287
00:37:15,775 --> 00:37:17,527
All right.

288
00:37:24,868 --> 00:37:26,370
Mr. Diggs.

289
00:37:27,078 --> 00:37:29,001
A couple of people I want you to meet.

290
00:37:29,789 --> 00:37:32,759
Officer Alexander Mulichev,
Officer Greg Ospasov.

291
00:37:32,751 --> 00:37:35,721
Sofia's best,
and they both speak English.

292
00:37:41,217 --> 00:37:43,060
Gregor Ospasov. A pleasure.

293
00:37:43,052 --> 00:37:45,271
Great to meet you.
Please call me Aleks.

294
00:37:46,347 --> 00:37:49,817
They're gonna give you all the help
that local law enforcement can provide.

295
00:37:50,393 --> 00:37:51,360
They're good guys...

296
00:37:51,352 --> 00:37:53,150
Trust them with your life.

297
00:38:23,384 --> 00:38:28,140
The deceased is an explosives expert,
affiliated with Islamic Jihad.

298
00:38:28,139 --> 00:38:29,482
Of course.

299
00:38:29,474 --> 00:38:31,818
The killer shot the dog, also.

300
00:38:31,810 --> 00:38:34,939
This is the 12th connected killing
you've seen in six months?

301
00:38:34,938 --> 00:38:36,940
Yeah, that's right,

302
00:38:36,940 --> 00:38:39,068
and just one piece
of forensic evidence,

303
00:38:39,067 --> 00:38:42,537
a Romanian brand of cigarette butt
with lipstick

304
00:38:42,529 --> 00:38:44,998
was found on the sidewalk
near one of the killings.

305
00:38:44,989 --> 00:38:46,081
It could belong to anyone.

306
00:38:46,074 --> 00:38:50,580
There is also a melted VHS tape
from an Arabic souvenir shop.

307
00:38:52,247 --> 00:38:53,920
Completely clean. Untraceable.

308
00:38:53,915 --> 00:38:56,464
Unknown origin.

309
00:38:56,918 --> 00:39:00,092
So, now we've got
a long-range sniper killing.

310
00:39:00,088 --> 00:39:02,967
Any forensic evidence
at the shooter's post?

311
00:39:02,966 --> 00:39:05,389
Gunshot residue, hairs,
fibers, shells, anything?

312
00:39:05,385 --> 00:39:08,855
We're working on it.
Sifting the dirt.

313
00:39:36,332 --> 00:39:38,175
600 meters.

314
00:39:40,879 --> 00:39:43,883
Robert Diggs. U.S. Embassy.

315
00:39:43,882 --> 00:39:45,805
I'm gonna go take a look.

316
00:40:16,998 --> 00:40:18,500
Robert?

317
00:40:35,642 --> 00:40:37,895
Nah. If that were the case,

318
00:40:37,894 --> 00:40:41,364
the body would have wound up
somewhere over here in this proximity.

319
00:40:41,356 --> 00:40:44,610
No, the killer let him finish his prayer,

320
00:40:44,609 --> 00:40:47,112
and then hit when he stood up
and was just about to turn.

321
00:40:47,111 --> 00:40:48,738
That's why the body wound up here.

322
00:40:48,738 --> 00:40:51,457
So that means the shots
didn't come from there.

323
00:40:52,825 --> 00:40:54,418
They came from there.

324
00:40:55,870 --> 00:40:57,247
Huh?

325
00:41:22,397 --> 00:41:24,741
Hey, look at this.

326
00:41:26,526 --> 00:41:28,403
Marks from a rifle stand, huh?

327
00:43:16,177 --> 00:43:17,349
Vicki.

328
00:44:56,694 --> 00:44:57,695
Vicki...

329
00:45:27,183 --> 00:45:28,730
I completely understand.

330
00:45:28,726 --> 00:45:31,320
Hey, look, I'll have to
call you right back, okay?

331
00:45:32,355 --> 00:45:33,982
The printout you asked for.

332
00:45:36,359 --> 00:45:39,408
This is all they were able to retrieve
from the VHS tape?

333
00:45:39,403 --> 00:45:40,780
Sorry, it was too damaged.

334
00:45:42,156 --> 00:45:44,204
- Even with the enhancement?
- Even with.

335
00:45:58,547 --> 00:46:00,265
This just came in.

336
00:46:00,258 --> 00:46:01,760
We've gone international.

337
00:46:01,759 --> 00:46:04,603
The press picked on the Jinn murders.

338
00:46:04,595 --> 00:46:06,768
Son of a bitch.

339
00:46:06,764 --> 00:46:08,766
Have you seen this?

340
00:46:08,766 --> 00:46:10,268
It's not my fault.

341
00:46:10,268 --> 00:46:12,191
This was leaked by your friends back home.

342
00:46:12,186 --> 00:46:13,403
Sit down.

343
00:46:13,396 --> 00:46:15,023
I told them I thought they were nuts,

344
00:46:15,022 --> 00:46:16,945
but they said that they wanted
the world to know

345
00:46:16,941 --> 00:46:19,194
that they had their best man
on the case.

346
00:46:19,193 --> 00:46:20,445
With all due respect, sir,

347
00:46:20,444 --> 00:46:23,789
was there any thought given
to compromising the investigation?

348
00:46:23,781 --> 00:46:27,285
I'm a political appointee.
I have this job

349
00:46:27,285 --> 00:46:30,084
because I gave the president's
election committee a bag of money.

350
00:46:30,079 --> 00:46:31,752
I don't get to make decisions.

351
00:46:31,747 --> 00:46:34,500
They want this out there,
well, it's out there.

352
00:46:35,251 --> 00:46:37,094
The best case I can suggest is,

353
00:46:37,086 --> 00:46:39,760
maybe it'll rouse up a person of interest.
I don't know.

354
00:46:39,755 --> 00:46:41,098
They're desperate. I told you that.

355
00:46:41,090 --> 00:46:43,684
You think tipping our hand like this
is gonna help expedite that?

356
00:46:43,676 --> 00:46:46,850
Robert, I am the messenger.
Don't shoot me.

357
00:46:46,846 --> 00:46:50,100
They didn't like your fried videotape,
so they said they were going with this.

358
00:46:50,099 --> 00:46:52,477
I have car dealerships at home.

359
00:46:52,476 --> 00:46:54,023
I can get you a great deal on wheels,

360
00:46:54,020 --> 00:46:58,150
but I have to admit I'm a little bit
over my head on this stuff.

361
00:46:58,149 --> 00:47:00,243
Bulgaria was not the post I wanted.

362
00:47:00,234 --> 00:47:01,451
I was hoping for Paris.

363
00:47:01,444 --> 00:47:03,538
That's what they hinted at, Paris.

364
00:47:03,529 --> 00:47:05,873
Maybe I didn't give them
enough money.

365
00:47:05,865 --> 00:47:07,867
I don't know.

366
00:47:07,867 --> 00:47:10,996
Your guys want results, Robert.

367
00:47:10,995 --> 00:47:13,748
We are doing the best we can.

368
00:47:21,672 --> 00:47:24,471
That was a lucky find,
the art exhibit.

369
00:47:24,467 --> 00:47:26,515
He's a local painter.

370
00:47:26,510 --> 00:47:29,514
Funny, I don't think I've ever
come across one before.

371
00:47:29,513 --> 00:47:32,266
Maybe you just haven't been
paying attention to what's around you.

372
00:47:32,266 --> 00:47:33,688
Maybe not.

373
00:47:36,771 --> 00:47:40,150
So what made you come to Sofia?

374
00:47:42,360 --> 00:47:44,988
Change of scenery, new opportunities,
that sort of thing.

375
00:47:46,989 --> 00:47:49,583
And you like your employer,
the government of the U.S.?

376
00:47:49,992 --> 00:47:52,871
Yeah, fine.

377
00:47:52,870 --> 00:47:54,793
What about you?
You like what you do?

378
00:47:54,789 --> 00:47:56,086
I love it.

379
00:47:57,917 --> 00:48:00,636
I'd like to live in the mountains one day,

380
00:48:00,628 --> 00:48:02,380
away from the city.

381
00:48:24,568 --> 00:48:25,569
I have to go.

382
00:48:26,862 --> 00:48:29,331
Really? So soon?

383
00:48:29,323 --> 00:48:31,121
What time do you go on tonight?
I'd love to come and see you.

384
00:48:31,784 --> 00:48:32,660
No, not tonight.

385
00:48:33,744 --> 00:48:35,462
I'm sorry, Robert. I have to go.

386
00:48:38,332 --> 00:48:40,460
You're very nice, Robert.

387
00:48:40,459 --> 00:48:42,257
I'd like it if you call me.

388
00:48:43,379 --> 00:48:44,676
Okay, I will.

389
00:49:07,445 --> 00:49:11,325
Next target, Abdullah Said.

390
00:51:06,063 --> 00:51:07,531
Heh.

391
00:51:13,529 --> 00:51:15,202
Wait a minute.

392
00:51:17,074 --> 00:51:18,451
It's a bag.

393
00:51:20,077 --> 00:51:21,579
That's it.

394
00:51:31,046 --> 00:51:33,014
The last time we were together,

395
00:51:34,091 --> 00:51:38,562
you became agitated when I mentioned
your parents, remember?

396
00:51:39,888 --> 00:51:41,390
When I asked you if you missed them?

397
00:51:42,099 --> 00:51:43,021
Now...

398
00:51:45,185 --> 00:51:47,654
I want you to cast
your mind back, Vicki.

399
00:51:47,646 --> 00:51:50,741
I want you to think about
that little girl,

400
00:51:51,692 --> 00:51:55,822
what life was like for her
after her parents died.

401
00:52:20,387 --> 00:52:22,264
Aah!

402
00:52:28,771 --> 00:52:30,819
Aah!

403
00:53:10,479 --> 00:53:17,112
Now... I'm going to ask you some questions
that may seem strange,

404
00:53:18,070 --> 00:53:21,620
but I want you to answer them
naturally, okay?

405
00:53:22,616 --> 00:53:26,871
Now, lie back, get comfortable.

406
00:53:32,000 --> 00:53:34,128
All right, good.

407
00:53:34,128 --> 00:53:38,474
And relax and breathe.

408
00:53:38,465 --> 00:53:40,217
Breathe.

409
00:53:40,217 --> 00:53:42,970
Will you take off your glasses, please'?

410
00:53:53,021 --> 00:53:54,989
Tell me your full name.

411
00:53:56,066 --> 00:53:59,070
Victoria Denov.

412
00:53:59,069 --> 00:54:00,696
What year were you born in?

413
00:54:02,406 --> 00:54:06,377
1970s... Seventy-...

414
00:54:06,368 --> 00:54:09,838
'77, I think.

415
00:54:10,247 --> 00:54:11,920
What city were you born in?

416
00:54:14,543 --> 00:54:16,261
Sofia.

417
00:54:17,546 --> 00:54:21,301
No, Paris.

418
00:54:23,260 --> 00:54:24,432
Wait...

419
00:54:24,428 --> 00:54:26,647
What is your mother's first name?

420
00:54:26,638 --> 00:54:28,811
Your mother's first name, Vicki.

421
00:54:28,807 --> 00:54:30,980
Now, you must try and answer
these questions.

422
00:54:34,563 --> 00:54:36,190
What is your mother's name?

423
00:54:44,114 --> 00:54:46,208
I can't...
I can't remember.

424
00:54:46,200 --> 00:54:49,454
Calm... calm down.

425
00:54:49,453 --> 00:54:52,798
Breathe. Breathe.

426
00:54:52,790 --> 00:54:57,296
Memory loss is a natural reaction
in some cases,

427
00:54:57,294 --> 00:54:58,386
so breathe.

428
00:54:59,338 --> 00:55:01,386
That's it now.

429
00:55:01,381 --> 00:55:05,887
Sometimes it's a product of something
as simple as stress, okay?

430
00:55:09,932 --> 00:55:12,526
You guys blame everything
on stress.

431
00:55:15,729 --> 00:55:19,074
So I'm not going crazy?

432
00:55:19,066 --> 00:55:22,991
No, you're not going crazy.

433
00:57:46,296 --> 00:57:49,846
Does this feel right to you?

434
00:57:49,841 --> 00:57:51,843
It feels perfect to me.

435
00:57:54,179 --> 00:57:56,523
It feels perfectly right to me, too.

436
01:02:31,414 --> 01:02:34,918
How are you today?

437
01:02:34,918 --> 01:02:37,797
I think you know
what I'm going to ask you.

438
01:02:37,796 --> 01:02:40,800
The same thing I ask you
every time we meet.

439
01:02:44,594 --> 01:02:47,473
Are you ready to tell me
about your parents...

440
01:02:49,557 --> 01:02:54,609
the family tragedy
that made you what you are today?

441
01:02:55,772 --> 01:02:58,571
What's your thought?
Do you feel something about that'?

442
01:03:00,902 --> 01:03:02,904
I don't know why we meet

443
01:03:02,904 --> 01:03:06,374
if you're not going to participate
in our sessions.

444
01:03:07,117 --> 01:03:10,462
Tell me about your parents' death.

445
01:03:52,287 --> 01:03:55,006
Allahu Akbar!

446
01:04:37,415 --> 01:04:38,507
Excuse me, um...

447
01:04:38,500 --> 01:04:40,252
isn't Ursula supposed
to be on at 6:00 tonight?

448
01:04:40,251 --> 01:04:41,969
No. She's not in tonight.

449
01:04:48,259 --> 01:04:49,761
- Robert.
- Hey.

450
01:04:49,761 --> 01:04:51,013
Are you leaving?

451
01:04:51,012 --> 01:04:52,434
I was supposed to meet the girl here.

452
01:04:52,430 --> 01:04:54,432
No, stay with me.
I'll buy you a drink.

453
01:04:54,432 --> 01:04:55,934
Excuse me, my friend.

454
01:04:55,934 --> 01:04:57,902
Two dark scotches, please,
on the rocks.

455
01:04:57,894 --> 01:05:01,444
I don't get it. We had an amazing time
together, it was all great.

456
01:05:01,439 --> 01:05:03,567
I was supposed to meet her here
and she doesn't show.

457
01:05:03,566 --> 01:05:06,160
Ever the hopeless romantic.

458
01:05:06,152 --> 01:05:10,658
Remember, Robert, it might just be
your passport she's falling in love with.

459
01:05:10,657 --> 01:05:13,080
That's a little cynical, isn't it, Aaron'?

460
01:05:13,076 --> 01:05:15,204
So it's love, is it'?

461
01:05:16,079 --> 01:05:20,960
Anyway, how's that case of yours?
Any progress to report?

462
01:05:20,959 --> 01:05:22,176
Nothing yet.

463
01:05:22,168 --> 01:05:24,717
I don't think I'll ever find out
who this killer is.

464
01:05:24,712 --> 01:05:25,964
There she is.

465
01:07:28,378 --> 01:07:29,880
What was that about?

466
01:07:29,879 --> 01:07:32,177
I don't know.
Maybe it's a part of her act.

467
01:07:33,841 --> 01:07:35,058
There she goes.

468
01:07:38,388 --> 01:07:39,560
I'm going after her.

469
01:09:24,994 --> 01:09:27,713
Ursula! Ursula, please, it's Robert.

470
01:09:27,705 --> 01:09:28,706
Just open the door.

471
01:09:28,706 --> 01:09:30,333
Robert?

472
01:09:39,676 --> 01:09:41,349
Come on, sweetie,
I just want to talk to you.

473
01:09:41,344 --> 01:09:42,846
Please let me in.

474
01:09:49,560 --> 01:09:51,938
You must leave.

475
01:09:51,938 --> 01:09:53,281
It's okay.

476
01:09:53,272 --> 01:09:54,569
All right? It's all right.

477
01:09:54,565 --> 01:09:58,035
- Stop, stop, stop.
- What, what?

478
01:09:58,027 --> 01:09:59,199
I'm sick, okay?

479
01:09:59,195 --> 01:10:00,697
What are you talking about'?

480
01:10:00,697 --> 01:10:02,699
Something is happening to me.

481
01:10:02,699 --> 01:10:04,372
The headaches, the blackouts.

482
01:10:04,367 --> 01:10:06,210
It's like I'm going insane.

483
01:10:06,202 --> 01:10:07,795
We'll just leave Sofia.

484
01:10:07,787 --> 01:10:09,789
We can go anywhere you want to go.

485
01:10:14,377 --> 01:10:16,220
I can't.

486
01:10:16,212 --> 01:10:18,135
Come with me. Please, please.

487
01:10:19,048 --> 01:10:21,301
There is something important I must do.

488
01:10:22,260 --> 01:10:23,477
I need two days.

489
01:10:23,469 --> 01:10:25,767
Two days? Why?
Why not right now'?

490
01:10:25,763 --> 01:10:31,065
Listen, there is a cafe
next to the National Theater.

491
01:10:31,060 --> 01:10:32,858
Meet me there at noon
the day after tomorrow.

492
01:10:33,730 --> 01:10:34,777
Please.

493
01:10:37,942 --> 01:10:40,946
No questions. That's all I ask.

494
01:10:42,864 --> 01:10:44,207
Two days.

495
01:11:21,861 --> 01:11:25,206
Multiple personality disorder.

496
01:11:27,450 --> 01:11:28,793
Perfect.

497
01:11:34,457 --> 01:11:36,300
Lonewolf,

498
01:11:36,292 --> 01:11:39,296
I am ready for your final instructions.

499
01:11:48,679 --> 01:11:49,851
Excellent.

500
01:11:50,681 --> 01:11:52,558
Excellent.

501
01:11:52,558 --> 01:11:54,185
Proceed to the final target.

502
01:12:09,617 --> 01:12:11,369
Hey...

503
01:13:55,890 --> 01:13:56,982
Excuse me.

504
01:14:18,746 --> 01:14:20,123
Get out of the way.

505
01:14:59,537 --> 01:15:01,130
Aah!

506
01:15:36,532 --> 01:15:37,704
Aah! Aah!

507
01:15:43,039 --> 01:15:45,212
No witnesses.

508
01:15:53,632 --> 01:15:54,975
Aah!

509
01:20:33,996 --> 01:20:35,623
Give me your full name.

510
01:20:36,540 --> 01:20:37,837
Victoria Denov.

511
01:20:42,838 --> 01:20:44,215
What year were you born?

512
01:20:47,760 --> 01:20:49,353
What is your mother's first name?

513
01:20:55,851 --> 01:20:58,024
What city were you born in?

514
01:21:00,189 --> 01:21:01,691
Sofia.

515
01:21:03,567 --> 01:21:06,946
No, Paris?

516
01:21:32,096 --> 01:21:33,439
Hello?

517
01:21:33,430 --> 01:21:35,057
You look tired.

518
01:21:37,226 --> 01:21:39,320
Where are you?

519
01:21:48,195 --> 01:21:49,697
How is your knee?

520
01:21:49,696 --> 01:21:51,369
What did you say?

521
01:21:54,243 --> 01:21:55,665
Where are you?

522
01:21:57,579 --> 01:21:59,456
You look very handsome.

523
01:22:00,541 --> 01:22:01,918
Where are you?

524
01:22:01,917 --> 01:22:04,761
Robert, I'm sorry.

525
01:22:05,504 --> 01:22:07,632
I love you, but I can't.

526
01:22:08,966 --> 01:22:12,015
Wait. Wait.

527
01:22:12,010 --> 01:22:13,933
Please, don't go.

528
01:23:00,809 --> 01:23:02,061
Papers are reporting

529
01:23:02,060 --> 01:23:05,189
that one of the top military strategists
of the T.l.J.'s been killed

530
01:23:05,189 --> 01:23:06,862
in the strike at the madrasa.

531
01:23:06,857 --> 01:23:10,612
It is confirmed it was a front
for training terrorists.

532
01:23:17,117 --> 01:23:18,494
- Wait.
- Yeah?

533
01:23:55,322 --> 01:23:56,995
Archive this.

534
01:24:00,369 --> 01:24:01,586
Close the file.

535
01:24:31,024 --> 01:24:32,367
Aaron,

536
01:24:32,985 --> 01:24:35,488
I'm sure you're with a patient,
but, uh...

537
01:24:37,072 --> 01:24:39,996
I really need to speak with you.
It's important.

538
01:24:40,242 --> 01:24:41,744
Call me back.

539
01:24:56,383 --> 01:24:59,057
See, Mihail?

540
01:24:59,052 --> 01:25:01,271
Even the exemplary one
couldn't figure it out.

541
01:25:01,263 --> 01:25:02,560
Yes, sir.

542
01:25:02,848 --> 01:25:06,478
I told you dissociative identity
would work.

543
01:25:07,644 --> 01:25:09,271
She's ready for Paris.

544
01:25:09,271 --> 01:25:10,898
Yeah, I agree.

545
01:25:10,897 --> 01:25:14,527
What about our next assignment?

546
01:25:14,526 --> 01:25:17,871
A dossier will be
at your hotel tomorrow.

547
01:25:17,863 --> 01:25:20,787
Just keep doing
what we've been doing,

548
01:25:20,782 --> 01:25:24,582
but put her under narco-analysis
with Paris, okay?

549
01:25:24,578 --> 01:25:26,376
Very well, sir. Okay.

550
01:25:26,371 --> 01:25:28,339
See ya.

551
01:25:29,166 --> 01:25:30,213
Good work.

552
01:25:31,418 --> 01:25:32,761
Good luck.

