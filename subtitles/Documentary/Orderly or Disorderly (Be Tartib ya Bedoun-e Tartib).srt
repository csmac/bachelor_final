1
00:00:16,556 --> 00:00:20,936
Orderly or Disorderly?

2
00:00:23,053 --> 00:00:26,278
Sound. Camera.
Orderly, scene 1, take 1.

3
00:00:27,241 --> 00:00:28,396
Come!

4
00:01:10,945 --> 00:01:11,779
Cut.

5
00:01:12,543 --> 00:01:16,859
Sound. Camera.
Disorderly, scene 1, take 1.

6
00:01:17,236 --> 00:01:18,511
Come, children!

7
00:01:31,653 --> 00:01:32,611
Cut.

8
00:01:32,731 --> 00:01:33,836
Sound. Camera.

9
00:01:34,180 --> 00:01:37,236
Disorderly, scene 2, take 1.


10
00:02:19,821 --> 00:02:20,976
Cut.

11
00:02:21,096 --> 00:02:24,747
Sound. Camera.
Orderly, scene 2, take 1.



12
00:03:04,340 --> 00:03:06,145
That's fine.
Cut.

13
00:03:06,572 --> 00:03:09,953
Sound. Camera.
Orderly, scene 3, take 1.

14
00:04:39,808 --> 00:04:41,449
That's fine.
Cut.

15
00:04:42,237 --> 00:04:46,307
Sound. Camera.
Disorderly, scene 3, take 1.

16
00:06:06,432 --> 00:06:09,616
- How long has it been?
- One minute.

17
00:06:10,535 --> 00:06:12,274
It's even past one minute.

18
00:06:14,014 --> 00:06:15,589
It's taking a very long time.

19
00:06:16,771 --> 00:06:20,447
- Well it's supposed to take time.
That's what we're trying to show.

20
00:06:21,268 --> 00:06:23,237
Disorderly, it takes time.

21
00:06:23,664 --> 00:06:26,388
- Yes, but you can't see
what they're doing.

22
00:06:26,508 --> 00:06:29,342
- Well, they're getting on,
what else would they be doing?

23
00:06:29,966 --> 00:06:34,036
We'll take one scene from inside,
it'll show better. It takes time.

24
00:06:40,121 --> 00:06:42,353
They're pushing, it shows.
Yes.

25
00:07:05,707 --> 00:07:08,365
How long has it been?
- It's even past two minutes.

26
00:07:08,485 --> 00:07:11,713
It's taken very long. I should
continue filming, no?

27
00:07:13,059 --> 00:07:14,799
- The longer it takes, the better.

28
00:07:15,455 --> 00:07:18,672
That's what we're saying with this
film. We want to say it takes time.

29
00:07:18,792 --> 00:07:20,871
It's an experiment, we're
not cheating are we.

30
00:07:20,991 --> 00:07:23,464
There are 40 kids, 1 bus and
they're getting on little by little.

31
00:07:23,584 --> 00:07:25,288
- More! They're 60 to 70 kids!

32
00:07:25,408 --> 00:07:28,882
- 60, whatever. Anyway, they're...

33
00:07:29,002 --> 00:07:31,245
- Before it was orderly, now...

34
00:07:31,365 --> 00:07:34,330
 It'll show when we go shoot
from inside. They're pushing.

35
00:07:37,122 --> 00:07:40,088
So let's go inside now. Shoot from
inside, and see how it is.

36
00:08:29,205 --> 00:08:29,960
Cut.

37
00:08:31,017 --> 00:08:33,209
Fine. Now let's shoot one take
with an orderly fasion too.

38
00:08:33,791 --> 00:08:34,281
Camera.

39
00:09:01,361 --> 00:09:02,026
That's enough.

40
00:09:02,262 --> 00:09:02,595
Cut.

41
00:09:03,243 --> 00:09:04,249
Sound. Camera.

42
00:09:04,915 --> 00:09:06,709
Orderly, scene 4, take 1.

43
00:09:42,395 --> 00:09:42,859
Cut.

44
00:09:43,408 --> 00:09:44,354
Sound. Camera.

45
00:09:44,730 --> 00:09:47,032
Disorderly, scene 4, take 1.

46
00:10:30,589 --> 00:10:31,604
Fine.
Cut.

47
00:10:32,037 --> 00:10:32,921
Sound. Camera.

48
00:10:33,140 --> 00:10:35,485
Disorderly, scene 6, take 1.

49
00:11:37,812 --> 00:11:38,608
Fine.
Cut.

50
00:11:39,212 --> 00:11:40,227
Sound. Camera.

51
00:11:40,492 --> 00:11:42,523
Orderly, scene 6, take 1.

52
00:12:05,712 --> 00:12:06,194
Cut.

53
00:12:07,349 --> 00:12:08,181
Sound. Camera.

54
00:12:09,091 --> 00:12:11,069
Orderly, scene 5, take 1.

55
00:12:21,433 --> 00:12:22,510
- He crossed.

56
00:12:22,630 --> 00:12:23,368
He went.

57
00:12:24,082 --> 00:12:24,931


58
00:12:28,143 --> 00:12:29,176
Well, it's orderly.

59
00:12:35,867 --> 00:12:37,057
... in the middle of the street.

60
00:12:37,075 --> 00:12:38,698
- It's didn't go well. 
- They ruined it.

61
00:12:39,072 --> 00:12:41,522
That man, damn. Look he's
going straight in the...

62
00:12:41,759 --> 00:12:42,730
Where's is he passing?

63
00:12:42,748 --> 00:12:43,711
- He's gone in the center.

64
00:12:43,754 --> 00:12:44,726
- Cut.
- Cut.

65
00:12:45,814 --> 00:12:46,960
Sound. Camera.

66
00:12:47,713 --> 00:12:50,129
Orderly, scene 5, take 2.

67
00:12:51,853 --> 00:12:52,929
- Let's see how this one goes.

68
00:12:58,937 --> 00:13:00,460
- These ones are going,
that side.

69
00:13:06,891 --> 00:13:07,502
- Cut.

70
00:13:07,861 --> 00:13:10,067
... had asked this...

71
00:13:10,102 --> 00:13:13,533
- Yes.
- ... controlled. - ... stopped.

72
00:13:13,912 --> 00:13:16,669
- That's why he stands there.
- ... they'd stop so we can shoot.

73
00:13:23,863 --> 00:13:25,710
- She went!
- She went.

74
00:13:26,349 --> 00:13:27,207
- It's ruined.

75
00:13:28,458 --> 00:13:29,229
- Cut. Cut.

76
00:13:30,428 --> 00:13:31,058
Camera.

77
00:13:32,323 --> 00:13:33,154
Yes, it begins.

78
00:13:34,178 --> 00:13:35,491
- Reprising...
- Cut.

79
00:13:36,454 --> 00:13:37,032
Camera.

80
00:13:42,584 --> 00:13:44,285
Where's this man going?
What the...

81
00:13:44,405 --> 00:13:46,120
He can't controle this place.

82
00:13:46,733 --> 00:13:49,402
I say, ask him to just controle
this place for just a minute.

83
00:13:50,146 --> 00:13:53,502
We have one take. If we could
just take one orderly...

84
00:13:53,622 --> 00:13:55,638
- It won't work.
- But why won't it?

85
00:13:56,088 --> 00:13:58,565
... make it somehow, so that we could

86
00:13:58,994 --> 00:14:00,946
If we could just shoot one take.

87
00:14:00,981 --> 00:14:05,313
- They're not listening to him. You can
see that. They're not listening to him.

88
00:14:05,433 --> 00:14:07,718
Well, let's at least do one thing.
Let's take disorderly.

89
00:14:08,182 --> 00:14:10,493
Disorderly, scene 5, take 1.

90
00:14:15,895 --> 00:14:17,671
...

91
00:14:20,857 --> 00:14:22,442
- Where should we shoot the orderly?

92
00:14:22,478 --> 00:14:25,707
- Orderly, I don't know. Let's ask
and see where it can be shot.

93
00:14:25,730 --> 00:14:27,095
- ...

94
00:14:28,382 --> 00:14:29,091


95
00:15:06,462 --> 00:15:08,572
Subs by hirvi14

