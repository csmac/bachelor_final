1
00:01:00,569 --> 00:01:05,836
Back from the strange world
underwater, scientists study the samples.

2
00:01:07,209 --> 00:01:12,078
One of the foremost scholars in the world
in his field, Dr. Pawlowski,

3
00:01:12,181 --> 00:01:16,015
studies the DNA sequences of foraminifera.

4
00:01:16,118 --> 00:01:22,250
What looks esoteric is in fact one of the
fundamental questions about life on Earth.

5
00:01:24,126 --> 00:01:28,756
In the same way that cosmologists search
for the origins of the universe,

6
00:01:29,098 --> 00:01:35,367
the scientists here are tracing back
the evolution of life to its earliest stages.

7
00:01:38,874 --> 00:01:43,368
Sometimes the building blocks
of the sequences all seem to fit.

8
00:01:46,048 --> 00:01:50,075
Jan, what have you found today so far
on the sample that we found?

9
00:01:50,185 --> 00:01:52,050
- Three new species.
- Three new species.

10
00:01:52,154 --> 00:01:54,645
Three new species on the dish.
That's fantastic.

11
00:01:54,757 --> 00:01:58,716
- This is from the ROMEO site.
- Yeah, from the ROMEO site.

12
00:01:58,827 --> 00:02:04,288
It's one small silver and two elongated ones.
I don't know what it is.

13
00:02:04,400 --> 00:02:06,334
We have to do the DNA, too.
We don't know...

14
00:02:06,435 --> 00:02:08,596
Is this a great moment?

15
00:02:09,371 --> 00:02:12,363
- Yeah, yeah, this is.
- Yeah, any time you increase

16
00:02:12,474 --> 00:02:15,932
the known diversity of these types
of creatures, it's pretty exciting.

17
00:02:16,045 --> 00:02:19,014
Yeah. That is very special.

18
00:02:27,990 --> 00:02:30,652
Apologies to rock musicians everywhere.

19
00:02:33,896 --> 00:02:37,354
Once the importance
of the discovery has sunk in,

20
00:02:37,466 --> 00:02:42,665
Sam Bowser and his group plan to celebrate
the event in their own way.

21
00:02:45,841 --> 00:02:49,641
They are rehearsing for
a late-night outdoor concert.

22
00:03:46,835 --> 00:03:51,636
After the helicopter had dropped us off
back at McMurdo,

23
00:03:52,107 --> 00:03:58,706
nobody was around. The sundial showed
that it was close to 1:00 a.m.

24
00:04:13,629 --> 00:04:18,760
It did not feel like night,
so we had a look around.

25
00:04:18,867 --> 00:04:24,669
This unobtrusive building
had raised my curiosity for quite a while.

26
00:05:01,744 --> 00:05:06,738
Here amongst unripe tomatoes,
we ran into this young man.

27
00:05:07,483 --> 00:05:09,781
How did he end up in this place?

28
00:05:10,085 --> 00:05:13,418
Oh, yeah, well, you know, I like to say,

29
00:05:13,522 --> 00:05:16,286
if you take everybody who's not tied down,
they all sort of

30
00:05:16,391 --> 00:05:18,518
fall down to the bottom of the planet, so,

31
00:05:18,627 --> 00:05:21,596
you know, I haven't been...
That's how we got here, you know.

32
00:05:21,697 --> 00:05:23,927
We're all at loose ends
and here we are together.

33
00:05:24,032 --> 00:05:26,523
I remember
when I first got down here I sort of

34
00:05:26,635 --> 00:05:29,798
enjoyed the sensation of recognizing people
with my tribal markings.

35
00:05:29,905 --> 00:05:32,999
You know, I was like,
"Hey, these are my people."

36
00:05:33,108 --> 00:05:38,740
PhDs washing dishes and, you know,
linguists on a continent with no languages

37
00:05:38,847 --> 00:05:40,872
and that sort of thing, yeah. It's great.

38
00:05:40,983 --> 00:05:44,510
Yeah, specifically I was in
a graduate program, and we had lined up

39
00:05:44,620 --> 00:05:49,114
to do some work with
one of the people who was

40
00:05:49,224 --> 00:05:53,456
identified as a native speaker
and a competent native speaker of

41
00:05:53,562 --> 00:05:56,531
one of the languages
of the Winnebago people, the Ho-Chunk,

42
00:05:56,632 --> 00:05:58,623
I think is how they pronounced it, and...

43
00:05:59,067 --> 00:06:01,627
To make a complicated story short,

44
00:06:01,737 --> 00:06:07,733
he ran into New Age ideologues who made
insipid claims about black and white magic

45
00:06:07,843 --> 00:06:10,209
embedded in the grammar of this language.

46
00:06:10,312 --> 00:06:12,507
Some of the oral tradition
that had been passed along...

47
00:06:12,614 --> 00:06:15,708
Hence, in this stupid trend of academia,

48
00:06:15,818 --> 00:06:19,049
it would be better to let the language die
than preserve it.

49
00:06:19,154 --> 00:06:20,781
...you know, I could document a language...

50
00:06:20,889 --> 00:06:24,188
He had to destroy his entire PhD research.

51
00:06:24,693 --> 00:06:28,060
So just imagine, you know, 90%

52
00:06:28,163 --> 00:06:31,462
of languages will be extinct
probably in my lifetime.

53
00:06:31,633 --> 00:06:33,498
It's a catastrophic impact

54
00:06:33,869 --> 00:06:37,168
to an ecosystem to talk
about that kind of extinction.

55
00:06:37,272 --> 00:06:40,435
Culturally, we're talking
about the same thing. I mean,

56
00:06:40,542 --> 00:06:43,705
you know, what if you lost all of

57
00:06:43,946 --> 00:06:47,507
Russian literature, or something like that,
or Russian, you know? If you took all of the

58
00:06:47,616 --> 00:06:51,985
Slavic languages and just they went
away, you know, and no more Tolstoy.

59
00:06:53,055 --> 00:06:57,253
It occurred to me that in the time
we spent with him in the greenhouse,

60
00:06:57,359 --> 00:07:00,624
possibly three or four languages had died.

61
00:07:01,897 --> 00:07:05,355
In our efforts to preserve
endangered species,

62
00:07:05,467 --> 00:07:08,459
we seem to overlook something
equally important.

63
00:07:10,239 --> 00:07:14,505
To me,
it is a sign of a deeply disturbed civilization

64
00:07:14,610 --> 00:07:19,570
where tree huggers and whale huggers
in their weirdness are acceptable,

65
00:07:19,681 --> 00:07:23,447
while no one embraces
the last speakers of a language.

66
00:07:32,461 --> 00:07:36,454
McMurdo is full of characters
like our linguist.

67
00:07:37,032 --> 00:07:41,366
The bleak Motel 6-drabness of
the corridors is misleading.

68
00:07:43,171 --> 00:07:48,108
Behind every door there is someone
with a special story to tell.

69
00:07:51,480 --> 00:07:57,612
Back in the '80s, I took a garbage
truck across Africa from London to Nairobi.

70
00:07:58,086 --> 00:08:01,852
That was a trip. Four months in
a garbage truck. It was horrible.

71
00:08:01,957 --> 00:08:06,257
On numerous occasions we came pretty
close to, I don't know about dying,

72
00:08:06,361 --> 00:08:08,488
but pretty close
to being in some straits where

73
00:08:08,597 --> 00:08:11,998
we didn't know if we were gonna get back
out of it, you know.

74
00:08:12,100 --> 00:08:15,069
We got taken over by the military in Uganda,

75
00:08:15,170 --> 00:08:18,901
and we were kidnapped, basically.
Truck was turned around

76
00:08:19,007 --> 00:08:22,499
and we were going back to Entebbe.
We got out of that one.

77
00:08:22,611 --> 00:08:28,277
We were trying to wait for
this ferry in Wadi Halfa,

78
00:08:28,684 --> 00:08:31,278
the one that blew up and 800 people died.

79
00:08:31,386 --> 00:08:34,082
Well, we didn't get on that one.
We took off across a desert,

80
00:08:34,189 --> 00:08:37,955
and we got stuck. We got stuck for five days

81
00:08:38,393 --> 00:08:45,060
of absolute agony, of clawing
this truck with... We were using plates,

82
00:08:45,167 --> 00:08:49,536
just the dinner plates that we were using
for dinner, clawing at the tires.

83
00:08:50,172 --> 00:08:54,006
We had no water.
He had used all the water tanks for gasoline,

84
00:08:54,109 --> 00:08:57,272
so basically we had a cup of water a day
or two cups.

85
00:08:57,546 --> 00:09:00,174
Her story goes on forever.

86
00:09:00,282 --> 00:09:02,682
She dealt with a bout of malaria,

87
00:09:02,851 --> 00:09:08,483
with a herd of angry elephants pursuing her
through tsetse fly-invested swamps.

88
00:09:08,590 --> 00:09:12,890
Got caught in a civil war,
spent a night in a bombed-out airport,

89
00:09:12,995 --> 00:09:15,964
with rebels fighting and shooting
in a barroom brawl,

90
00:09:16,064 --> 00:09:19,431
and was finally rescued
by drunk Russian pilots,

91
00:09:19,534 --> 00:09:22,560
slaloming around crater holes
in the runway.

92
00:09:22,771 --> 00:09:26,537
This is how you get yourself
to any place in Antarctica.

93
00:09:28,577 --> 00:09:33,207
At the so-called Freak Train event
at one of McMurdo's bars,

94
00:09:33,582 --> 00:09:38,815
Karen is, not surprisingly,
one of the most popular performers.

95
00:09:41,823 --> 00:09:45,657
This is her famous
"Travel as hand luggage" act.

96
00:09:50,699 --> 00:09:52,564
Yeah, take her home.

97
00:10:03,045 --> 00:10:04,569
- Thought of another one.
- Yeah.

98
00:10:04,679 --> 00:10:08,945
I traveled from Ecuador to Lima, Peru
in a sewer pipe.

99
00:10:11,053 --> 00:10:15,888
Forgot to mention that.
I hitchhiked once from Denver to Bolivia

100
00:10:16,224 --> 00:10:20,388
and back up,
and we got a ride from a truck in...

101
00:10:20,495 --> 00:10:26,229
It was a flatbed truck with three huge sewer
pipes on the back, so I spent... It was days

102
00:10:26,668 --> 00:10:31,799
in the back of this truck, in a sewer pipe,
watching the world go by just like that.

103
00:10:31,907 --> 00:10:34,307
That's all you could see.

104
00:10:36,445 --> 00:10:40,575
Travel for those who have been
deprived of freedom means even more.

105
00:10:41,349 --> 00:10:44,682
These are the ones you'll find in Antarctica.

106
00:10:44,786 --> 00:10:48,153
Libor Zicha works as a utility mechanic.

107
00:10:48,256 --> 00:10:51,419
He lived like a prisoner
behind the Iron curtain.

108
00:10:51,660 --> 00:10:54,390
You escaped.
And how big a drama was that?

109
00:10:54,496 --> 00:10:59,957
Oh, it was, wasn't a drama, but...

110
00:11:00,068 --> 00:11:04,300
The tragic events surrounding his escape
haunt him to this day.

111
00:11:04,739 --> 00:11:07,003
If we can...

112
00:11:09,611 --> 00:11:13,308
- You do not have to talk about it.
- Okay. Thank you.

113
00:11:14,282 --> 00:11:19,049
For me, the best description of
hunger is a description of bread.

114
00:11:20,188 --> 00:11:23,123
A poet said that once, I think,

115
00:11:24,526 --> 00:11:28,189
and for me the best description of freedom
is what you have in front of you.

116
00:11:28,296 --> 00:11:29,627
You are traveling a lot.

117
00:11:29,731 --> 00:11:30,891
- That's right, yeah.
- Show us.

118
00:11:31,032 --> 00:11:35,935
That's my freedom,
and I will be glad to show you.

119
00:11:38,673 --> 00:11:43,201
He keeps a rucksack packed
and ready to go at all times.

120
00:11:43,311 --> 00:11:48,578
Inside is everything he needs
to set out in a moment's notice:

121
00:11:48,683 --> 00:11:54,747
a sleeping bag, a tent, clothes,
cooking utensils.

122
00:11:57,492 --> 00:11:59,187
How much weight is this all?

123
00:11:59,294 --> 00:12:05,028
It's... I usually don't go over 20 kilos.
That's my limit,

124
00:12:05,133 --> 00:12:08,330
and it's a limit also for airlines.

125
00:12:12,741 --> 00:12:17,075
Some of the contents of his backpack
are quite surprising.

126
00:12:32,594 --> 00:12:35,461
That's about the size of the raft.

127
00:12:35,564 --> 00:12:38,931
- How quickly can you leave?
- Oh, I am always ready.

128
00:12:39,067 --> 00:12:45,939
My bag is always prepared,
and I am always ready for adventure

129
00:12:47,342 --> 00:12:49,810
and exploring new horizons.

130
00:12:56,785 --> 00:13:00,016
Back in the days of Amundsen,
Scott and Shackleton,

131
00:13:00,322 --> 00:13:03,883
scientific exploration of Antarctica began,

132
00:13:03,992 --> 00:13:08,691
and this opening of the unknown continent
is their great achievement.

133
00:13:11,733 --> 00:13:16,261
But one thing about the early explorers
does not feel right.

134
00:13:18,373 --> 00:13:22,935
The obsession to be the first one
to set his foot on the South Pole.

135
00:13:25,847 --> 00:13:29,874
It was for personal fame
and the glory of the British Empire.

136
00:13:31,486 --> 00:13:36,890
This is Shackleton's original hut,
preserved unchanged for 100 years.

137
00:13:41,263 --> 00:13:44,061
But, in a way, from the South Pole onwards

138
00:13:44,165 --> 00:13:46,963
there was no further expansion possible,

139
00:13:47,068 --> 00:13:51,801
and the Empire started to fade
into the abyss of history.

140
00:13:54,242 --> 00:13:58,008
It all looks now like an extinct supermarket.

141
00:14:06,221 --> 00:14:09,850
On a cultural level,
it meant the end of adventure.

142
00:14:11,493 --> 00:14:16,362
Exposing the last unknown spots
of this Earth was irreversible,

143
00:14:16,765 --> 00:14:20,826
but it feels sad
that the South Pole or Mount Everest

144
00:14:20,935 --> 00:14:23,995
were not left in peace in their dignity.

145
00:14:27,676 --> 00:14:32,136
It may be a futile wish
to keep a few white spots on our maps,

146
00:14:32,380 --> 00:14:37,317
but human adventure, in its original sense,
lost its meaning,

147
00:14:37,419 --> 00:14:41,321
became an issue for the
Guinness Book of World Records.

148
00:14:45,994 --> 00:14:50,090
Scott and Amundsen
were clearly early protagonists,

149
00:14:50,432 --> 00:14:54,630
and from there on
it degenerated into absurd quests.

150
00:14:55,136 --> 00:15:00,130
A Frenchman crossed the Sahara Desert
in his car set in reverse gear,

151
00:15:00,608 --> 00:15:04,840
and I am waiting for the first barefoot runner
on the summit of Everest

152
00:15:04,946 --> 00:15:09,508
or the first one hopping into the South Pole
on a pogo stick.

153
00:15:11,286 --> 00:15:16,417
Well, I had this idea of breaking
a Guinness record in every continent,

154
00:15:17,092 --> 00:15:18,719
and Antarctica would be the sixth,

155
00:15:19,194 --> 00:15:22,960
so, now I'm trying to think of a way
to get to Antarctica.

156
00:15:23,064 --> 00:15:26,556
Ashrita Furman did not want
to travel this way,

157
00:15:26,668 --> 00:15:30,661
because he already holds a
Guinness record in this discipline.

158
00:15:31,706 --> 00:15:33,469
And also in this one.

159
00:15:33,575 --> 00:15:38,603
So, he decided upon the more prosaic
approach and took an airplane.

160
00:15:38,913 --> 00:15:40,505
We flew down to Antarctica.

161
00:15:40,615 --> 00:15:42,708
Anyway, it was thrilling
because I'm in Antarctica,

162
00:15:42,817 --> 00:15:44,717
and I'm trying to break a Guinness record.

163
00:15:44,819 --> 00:15:47,151
Being in Antarctica
is like being on the moon.

164
00:15:47,255 --> 00:15:51,351
It's so... I mean, it's so peaceful.
It's so pure.

165
00:15:51,459 --> 00:15:54,917
It's so desolate.
I mean, it's just a great place.

166
00:16:00,101 --> 00:16:04,834
Antarctica is not the moon,
even though sometimes it feels like it.

167
00:16:07,208 --> 00:16:08,732
Yet, on this planet,

168
00:16:08,843 --> 00:16:13,803
McMurdo comes closest to what
a future space settlement would look like.

169
00:16:23,892 --> 00:16:28,022
We left McMurdo for the penguin colony
at cape Royds.

170
00:16:28,530 --> 00:16:31,055
Everyone spoke about penguins,

171
00:16:31,166 --> 00:16:36,069
however, the questions I had
were not so easily answered.

172
00:16:38,606 --> 00:16:41,507
I was referred to a penguin expert out there

173
00:16:41,810 --> 00:16:44,574
who had studied them for almost 20 years.

174
00:16:46,548 --> 00:16:49,415
I was told that he was a taciturn man,

175
00:16:49,517 --> 00:16:54,819
who, in his solitude, was not much into
conversation with humans anymore.

176
00:16:55,490 --> 00:16:59,017
But Dr. Ainley gave his best effort.

177
00:16:59,127 --> 00:17:01,687
Well, here we are at Cape Royds.

178
00:17:02,797 --> 00:17:08,030
This is 2006,
and it's just about the 100th anniversary

179
00:17:08,136 --> 00:17:12,402
of the first penguin study
that was ever done,

180
00:17:12,674 --> 00:17:15,575
which was done here at Cape Royds by

181
00:17:15,677 --> 00:17:18,646
a person that was part
of the Shackleton expedition.

182
00:17:22,250 --> 00:17:26,209
They all had a good winter,
and they're very fat.

183
00:17:27,121 --> 00:17:28,554
They've

184
00:17:29,724 --> 00:17:34,388
claimed their territories and eggs have
been laid and females have left,

185
00:17:34,496 --> 00:17:38,728
and now there's just males
that are sitting on eggs,

186
00:17:39,934 --> 00:17:44,166
using their fat reserves
and waiting for females to return

187
00:17:44,272 --> 00:17:47,139
to relieve them and then go to sea.

188
00:17:49,511 --> 00:17:52,309
I tried to keep the conversation going.

189
00:17:52,780 --> 00:17:57,240
Dr. Ainley, I read somewhere
that there are gay penguins.

190
00:17:57,819 --> 00:17:59,810
What are your observations?

191
00:18:04,092 --> 00:18:06,026
I've never...

192
00:18:07,495 --> 00:18:10,555
Or strange sexual behavior.
Can you talk about...

193
00:18:10,698 --> 00:18:16,136
Yeah, there has been... I've seen
triangular relationships where there's

194
00:18:17,238 --> 00:18:21,641
one female and two males,
and the female lays the egg,

195
00:18:23,144 --> 00:18:28,309
or eggs, and the males and the female
trade off over the season.

196
00:18:31,052 --> 00:18:37,924
There are mis-identities, initially,
of the sex of penguins.

197
00:18:40,361 --> 00:18:44,627
Somebody recently described
what they call prostitution where

198
00:18:46,100 --> 00:18:50,969
a female, who is out
collecting rocks for her nest,

199
00:18:51,072 --> 00:18:53,438
and, of course, some penguins are...

200
00:18:53,541 --> 00:18:55,975
The only way they collect rocks
is to steal them from others.

201
00:18:56,077 --> 00:18:59,205
So, in order to do that,
they have to be very submissive

202
00:18:59,547 --> 00:19:04,610
in order to get close to a male,
who's maybe advertising for a mate,

203
00:19:04,719 --> 00:19:11,352
and so she'll come in, sit in his nest,
and sometimes they'll copulate.

204
00:19:11,626 --> 00:19:14,789
But, really, her idea is to get a rock,

205
00:19:14,896 --> 00:19:18,662
and so, as soon as she can,
she escapes with a rock.

206
00:19:21,936 --> 00:19:27,670
Dr. Ainley, is there such thing
as insanity among penguins?

207
00:19:28,509 --> 00:19:32,605
I try to avoid the definition of insanity
or derangement.

208
00:19:32,714 --> 00:19:38,050
I don't mean that a penguin
might believe he or she is Lenin

209
00:19:38,152 --> 00:19:42,612
or Napoleon Bonaparte,
but could they just go crazy

210
00:19:42,724 --> 00:19:45,818
because they've had enough of
their colony?

211
00:19:50,098 --> 00:19:54,296
Well, I've never seen a penguin
bashing its head against a rock.

212
00:19:56,104 --> 00:19:58,937
They do get disoriented.

213
00:19:59,040 --> 00:20:03,272
They end up in places they shouldn't be,
a long way from the ocean.

214
00:20:07,882 --> 00:20:12,581
These penguins are all heading
to the open water to the right.

215
00:20:16,824 --> 00:20:20,920
But one of them caught our eye,
the one in the center.

216
00:20:22,130 --> 00:20:26,624
He would neither go towards the feeding
grounds at the edge of the ice,

217
00:20:26,734 --> 00:20:28,998
nor return to the colony.

218
00:20:31,005 --> 00:20:35,704
Shortly afterwards, we saw him heading
straight towards the mountains,

219
00:20:35,810 --> 00:20:37,835
some 70 kilometers away.

220
00:20:40,615 --> 00:20:44,312
Dr. Ainley explained
that even if he caught him

221
00:20:44,419 --> 00:20:46,580
and brought him back to the colony,

222
00:20:46,688 --> 00:20:50,852
he would immediately head right back
for the mountains.

223
00:20:53,261 --> 00:20:54,660
But why?

224
00:21:05,573 --> 00:21:09,236
One of these disoriented,
or deranged, penguins

225
00:21:09,577 --> 00:21:12,307
showed up at the New Harbor diving camp,

226
00:21:12,413 --> 00:21:16,179
already some 80 kilometers away
from where it should be.

227
00:21:21,022 --> 00:21:25,959
The rules for the humans
are do not disturb or hold up the penguin.

228
00:21:26,060 --> 00:21:29,996
Stand still and let him go on his way.

229
00:21:33,568 --> 00:21:38,631
And here, he's heading off into the interior
of the vast continent.

230
00:21:39,674 --> 00:21:44,941
With 5, 000 kilometers ahead of him,
he's heading towards certain death.

231
00:21:57,425 --> 00:22:01,486
The last field camp we visited
was at Mount Erebus.

232
00:22:02,597 --> 00:22:06,499
This active volcano is 12, 500 feet high.

233
00:22:07,802 --> 00:22:13,240
It is of particular importance, as inside
the crater the magma of the inner earth

234
00:22:13,441 --> 00:22:15,466
is directly exposed.

235
00:22:17,178 --> 00:22:20,477
There are only two other such volcanoes
in the world,

236
00:22:20,948 --> 00:22:24,907
one in the congo and the other in Ethiopia.

237
00:22:25,486 --> 00:22:28,512
Because of political strife in those places,

238
00:22:28,623 --> 00:22:33,424
it is actually easier to conduct field studies
here in Antarctica.

239
00:22:35,329 --> 00:22:40,323
First thing, we were instructed in
the etiquette of dealing with this volcano.

240
00:22:41,269 --> 00:22:45,365
One very important thing to keep in mind
when you're on the crater

241
00:22:45,473 --> 00:22:49,773
is that the lava lake
could explode at any time,

242
00:22:49,877 --> 00:22:55,679
and if it does, it's vital to keep
your attention faced toward the lava lake

243
00:22:56,250 --> 00:23:00,050
and watch for bombs
that are tracking up into the air

244
00:23:00,188 --> 00:23:05,251
and try to pick out the ones that might be
coming toward you and step out of the way.

245
00:23:05,893 --> 00:23:11,160
The last thing you wanna do is turn away
from the crater or run or crouch down.

246
00:23:11,299 --> 00:23:15,759
Keep your attention toward the lava lake,
look up and move out of the way.

247
00:23:17,705 --> 00:23:22,699
We were fortunate that the lava
lake was not enshrouded in mist this day.

248
00:23:24,078 --> 00:23:27,070
This here is the new observation camera.

249
00:23:28,716 --> 00:23:33,119
William Mclntosh is the leader
of the team of volcanologists here.

250
00:23:34,222 --> 00:23:38,215
This camera is designed for prison riots
or to be explosion proof,

251
00:23:38,693 --> 00:23:42,220
and it's coated with this thick
Teflon housing.

252
00:23:42,897 --> 00:23:46,333
Here's the lens here. This is a camera.

253
00:23:46,767 --> 00:23:52,467
The camera inside is made by a small
company in Canada, Extreme CCTV.

254
00:23:52,940 --> 00:23:56,273
The inside housing is specifically
designed for explosion...

255
00:23:57,712 --> 00:23:58,701
...to be explosion-proof.

256
00:23:58,813 --> 00:24:02,943
There's a bang from the lava lake
right now. No bombs, though.

257
00:24:10,825 --> 00:24:14,124
This is the magma lake
filmed 30 years ago.

258
00:24:17,064 --> 00:24:21,660
At that time, there was a bold attempt
to descend into the crater.

259
00:24:30,945 --> 00:24:33,641
Halfway down there is a plateau.

260
00:24:33,948 --> 00:24:38,248
From there, it is a gaping hole straight down
into the magma.

261
00:24:58,539 --> 00:25:01,337
They were in for near disaster.

262
00:25:11,519 --> 00:25:17,856
The magma exploded, striking one of the
climbers, who got away with minor injuries.

263
00:25:26,067 --> 00:25:30,766
Today, the lava is monitored
by Dr. Mclntosh's camera.

264
00:26:02,870 --> 00:26:07,807
Dr. clive Oppenheimer, a true Englishman
from cambridge University,

265
00:26:07,908 --> 00:26:13,744
surprised us with his tweed outfit, which
he wears as a tribute to the explorers of old.

266
00:26:14,882 --> 00:26:19,376
He analyzes gas emissions
from volcanoes all over the world.

267
00:26:19,987 --> 00:26:23,047
If this were one of those active
volcanoes in lndonesia,

268
00:26:23,157 --> 00:26:26,558
I'd be far more circumspect
about standing on the crater rim.

269
00:26:26,660 --> 00:26:29,561
This is a very benign form of volcanism,

270
00:26:29,930 --> 00:26:35,732
and even the eruptions we've seen in the
historic period are relatively minor affairs.

271
00:26:36,203 --> 00:26:38,967
If we go back into the geological record,

272
00:26:39,073 --> 00:26:40,836
we see that there are huge

273
00:26:42,009 --> 00:26:46,070
volcanic eruptions,
massive, explosive eruptions that produced

274
00:26:46,614 --> 00:26:48,582
thousands of cubic miles of pumice,

275
00:26:48,682 --> 00:26:52,482
showering large parts of the Earth
with fine ash,

276
00:26:52,953 --> 00:26:56,946
and these have been demonstrated
to have had a strong impact on climate,

277
00:26:57,425 --> 00:27:00,917
and one of the biggest of these events,
74,000 years ago,

278
00:27:01,328 --> 00:27:04,422
has been argued even to have affected
our human ancestors

279
00:27:04,532 --> 00:27:06,432
and may have played an important role in

280
00:27:06,801 --> 00:27:10,237
the origins and dispersal of early humans.

281
00:27:13,574 --> 00:27:17,772
So these events will recur, and I think
the more we understand about them,

282
00:27:17,878 --> 00:27:22,406
the better we can prepare for
their eventuality.

283
00:27:27,321 --> 00:27:32,315
For this and many other reasons,
our presence on this planet

284
00:27:32,560 --> 00:27:35,393
does not seem to be sustainable.

285
00:27:35,496 --> 00:27:40,490
Our technological civilization makes us
particularly vulnerable.

286
00:27:41,969 --> 00:27:46,497
There is talk all over the scientific
community about climate change.

287
00:27:47,875 --> 00:27:52,710
Many of them agree the end of human life
on this Earth is assured.

288
00:27:55,783 --> 00:27:59,549
Human life is part of
an endless chain of catastrophes,

289
00:28:00,020 --> 00:28:04,218
the demise of the dinosaurs being just
one of these events.

290
00:28:05,893 --> 00:28:08,123
We seem to be next.

291
00:28:14,268 --> 00:28:19,535
And when we are gone, what will happen
thousands of years from now in the future?

292
00:28:22,977 --> 00:28:26,413
Will there be alien archeologists
from another planet

293
00:28:26,747 --> 00:28:30,979
trying to find out what we were doing
at the South Pole?

294
00:28:33,320 --> 00:28:38,155
They will descend into the tunnels
that we had dug deep under the pole.

295
00:28:40,327 --> 00:28:46,095
It is still minus 70 degrees here,
and that's why this place has outlived

296
00:28:46,233 --> 00:28:48,758
all the large cities in the world.

297
00:28:52,439 --> 00:28:54,339
They walk on and on.

298
00:29:14,028 --> 00:29:15,552
And then this.

299
00:29:16,130 --> 00:29:21,033
As if we had wanted to leave one remnant
of our presence on this planet,

300
00:29:21,635 --> 00:29:26,038
they would find a frozen sturgeon,
mysteriously hidden away

301
00:29:26,540 --> 00:29:30,567
beneath the mathematically precise
true South Pole.

302
00:29:51,465 --> 00:29:56,402
They stash it back away
into its frozen shrine for another eternity.

303
00:29:59,673 --> 00:30:03,769
And then they find more,
memories of a world once green.

304
00:30:07,381 --> 00:30:12,648
As if the human race wanted to preserve
at least some lost beauty of this Earth,

305
00:30:13,120 --> 00:30:17,523
they left this,
framed in a garland of frozen popcorn.

306
00:30:27,901 --> 00:30:30,495
Back at the base camp of Mount Erebus,

307
00:30:35,309 --> 00:30:37,903
due to the considerable altitude,

308
00:30:38,245 --> 00:30:42,147
once in a while the volcanologists
need medical care.

309
00:30:46,253 --> 00:30:48,847
But soon we find them back at work.

310
00:31:57,057 --> 00:31:59,048
My face is frozen.

311
00:32:38,732 --> 00:32:40,700
Quite cold up here today.

312
00:32:46,840 --> 00:32:51,675
Just by having that fantastic lava lake
down there with all that energy,

313
00:32:51,945 --> 00:32:56,848
we still have to bring old petrol generators
up to the crater rim.

314
00:33:08,662 --> 00:33:14,362
Man versus Machine, Chapter 53.
Professor Clive Oppenheimer on Erebus.

315
00:33:16,470 --> 00:33:20,668
Hands in pockets,
waiting for it to start spontaneously.

316
00:33:22,075 --> 00:33:24,407
He could be waiting a long time.

317
00:33:25,846 --> 00:33:29,714
Have you ever seen two men kiss
on the top of Erebus before?

318
00:33:31,685 --> 00:33:33,243
Pushing back the frontiers.

319
00:33:33,353 --> 00:33:34,980
It's R-18, okay?

320
00:33:37,558 --> 00:33:39,549
I like working with Harry.

321
00:33:44,665 --> 00:33:47,031
Along the slopes of the volcano

322
00:33:47,267 --> 00:33:53,399
there are vents where steam creates
so-called fumaroles, bizarre chimneys of ice,

323
00:33:53,740 --> 00:33:56,675
sometimes reaching two stories in height.

324
00:34:04,551 --> 00:34:07,611
It is possible to descend into some of them.

325
00:34:09,556 --> 00:34:14,892
You only have to be careful
to avoid the ones containing toxic gasses.

326
00:37:26,720 --> 00:37:30,121
At the foot of Erebus, out on the sea ice,

327
00:37:30,223 --> 00:37:34,091
the two tallest buildings on this continent
are located.

328
00:37:34,828 --> 00:37:38,696
In these hangars,
scientific payloads are being readied

329
00:37:38,832 --> 00:37:41,858
for their balloon launch
into the stratosphere.

330
00:37:55,782 --> 00:37:59,513
We were interested in
the neutrino detection project.

331
00:37:59,619 --> 00:38:03,282
Scientists are planning
to lift an observation instrument

332
00:38:03,390 --> 00:38:06,655
40 kilometers up into the stratosphere

333
00:38:06,760 --> 00:38:10,821
in search of almost
undetectable subatomic particles.

334
00:38:17,270 --> 00:38:22,333
As it rises, this small-looking bubble
of helium will expand

335
00:38:22,509 --> 00:38:27,003
to fill the entire skin,
which here still looks like a white rope.

336
00:38:28,048 --> 00:38:33,714
It will eventually form a gigantic globe
more than 300 feet in diameter.

337
00:38:35,922 --> 00:38:38,015
When it reaches the stratosphere,

338
00:38:38,125 --> 00:38:42,118
the detector will scan
thousands of square miles of ice

339
00:38:42,229 --> 00:38:46,996
without encountering electrical
disturbances from the inhabited world.

340
00:38:48,168 --> 00:38:52,502
Prior to the launch,
we were inside the hangar.

341
00:38:52,606 --> 00:38:57,509
The neutrino project is led by
Dr. Gorham of the University of Hawaii.

342
00:38:57,744 --> 00:39:03,307
So, what we're trying to do
with this instrument is to be the first

343
00:39:03,416 --> 00:39:08,479
scientific group to detect the highest
energy neutrinos in the universe, we hope.

344
00:39:08,989 --> 00:39:12,789
Yeah, but, Dr. Gorham,
what exactly is a neutrino?

345
00:39:13,460 --> 00:39:18,227
The neutrino is... It's the most ridiculous
particle you could imagine.

346
00:39:18,465 --> 00:39:22,561
A billion neutrinos went through my nose
as we were talking.

347
00:39:22,836 --> 00:39:26,237
A trillion, a trillion of them
went through my nose just now,

348
00:39:26,339 --> 00:39:28,102
and they did nothing to me.

349
00:39:28,208 --> 00:39:31,905
They pass through all of the matter
around us continuously,

350
00:39:32,179 --> 00:39:37,344
in a huge, huge blast of particles
that does nothing at all.

351
00:39:37,651 --> 00:39:41,519
They're like...
They almost exist in a separate universe,

352
00:39:41,621 --> 00:39:43,714
but we know, as physicists,
we can measure them,

353
00:39:43,824 --> 00:39:48,193
we can make precision predictions
and measurements. They exist,

354
00:39:48,295 --> 00:39:50,957
but we can't get our hands on them,

355
00:39:51,064 --> 00:39:53,897
because they seem to just exist
in another place,

356
00:39:54,334 --> 00:39:59,294
and yet without neutrinos, the beginning
of the universe would not have worked.

357
00:39:59,539 --> 00:40:02,064
We would not have the matter
that we have today,

358
00:40:02,175 --> 00:40:05,110
because you couldn't create
the elements without the neutrinos.

359
00:40:05,212 --> 00:40:08,773
In the very, very earliest few seconds
of the big bang,

360
00:40:08,882 --> 00:40:12,443
the neutrinos were the dominant particle,
and they actually determined

361
00:40:12,552 --> 00:40:17,046
much of the kinetics of the production
of the elements we know.

362
00:40:17,157 --> 00:40:20,183
So, the universe can't exist the way it is
without the neutrinos,

363
00:40:20,327 --> 00:40:23,888
but they seem
to be in their own separate universe,

364
00:40:23,997 --> 00:40:26,966
and we're trying to actually
make contact with that

365
00:40:27,067 --> 00:40:29,661
otherworldly universe of neutrinos.

366
00:40:30,837 --> 00:40:34,329
And as a physicist, even though

367
00:40:35,308 --> 00:40:39,369
I understand it mathematically
and I understand it intellectually,

368
00:40:39,512 --> 00:40:41,605
it still hits me in the gut

369
00:40:42,182 --> 00:40:45,583
that there is something here around

370
00:40:45,685 --> 00:40:49,485
surrounding me almost like
some kind of spirit or god

371
00:40:49,589 --> 00:40:51,420
that I can't touch,

372
00:40:51,758 --> 00:40:54,318
but I can measure it.

373
00:40:54,427 --> 00:40:55,553
I can make a measurement.

374
00:40:55,662 --> 00:40:58,927
It's like measuring the spirit world
or something like that.

375
00:40:59,032 --> 00:41:01,592
You can go out and touch these things.

376
00:41:03,103 --> 00:41:07,597
Not surprisingly, we found
this incantation in Hawaiian language

377
00:41:07,707 --> 00:41:09,641
on the side of his detector.

378
00:41:10,377 --> 00:41:13,710
It was as if spirits had to be invoked.

379
00:41:15,782 --> 00:41:19,718
What would we see if we could film
the impact of a neutrino?

380
00:41:20,720 --> 00:41:25,851
What you would see is, you would see
a lightning bolt about 10 meters long,

381
00:41:26,159 --> 00:41:28,093
about that thick,

382
00:41:28,194 --> 00:41:32,494
and it would blast at the speed of light
over this 10 meter distance,

383
00:41:32,599 --> 00:41:36,399
and you would see the most beautiful
blue light your eyes have ever seen.

384
00:41:36,569 --> 00:41:38,628
It happens in about...

385
00:41:40,974 --> 00:41:43,738
The entire impulse of radio waves

386
00:41:43,843 --> 00:41:47,404
is up and down in probably

387
00:41:47,514 --> 00:41:51,006
one one-hundred billionth of a second.

388
00:41:51,785 --> 00:41:56,188
It just goes bang and it's gone,
and that's what we're looking for.

389
00:42:24,384 --> 00:42:28,445
There is a beautiful saying by an American,

390
00:42:29,723 --> 00:42:35,628
a philosopher, Alan Watts,
and he used to say that through our eyes,

391
00:42:35,895 --> 00:42:38,329
the universe is perceiving itself,

392
00:42:38,798 --> 00:42:43,428
and through our ears, the universe
is listening to its cosmic harmonies,

393
00:42:43,870 --> 00:42:48,273
and we are the witness
through which the universe

394
00:42:49,342 --> 00:42:52,834
becomes conscious of its glory,
of its magnificence.

