﻿1
00:00:16,520 --> 00:00:19,750
I'll never understand
the ponies in this town!

2
00:00:19,750 --> 00:00:22,120
Everywhere I went, they
were all gussied up

3
00:00:22,120 --> 00:00:23,740
and lookin' at me funny!

4
00:00:23,740 --> 00:00:26,100
Kept asking if I was 'nervous'!

5
00:00:26,100 --> 00:00:28,940
Did they forget the
wedding is tomorrow?

6
00:00:29,050 --> 00:00:33,460
Oh, maybe they're just as excited
for us to be married as I am.

7
00:00:36,300 --> 00:00:38,880
Well, they're going
to feel awfully silly

8
00:00:38,880 --> 00:00:41,520
when they realize they've
got the wrong date.

9
00:00:43,020 --> 00:00:44,720
The invitations are wrong!

10
00:00:44,720 --> 00:00:47,000
Tis says the wedding is today!

11
00:00:47,000 --> 00:00:49,190
But I got such a
good deal on them...

12
00:00:49,190 --> 00:00:51,390
Everypony in town got these!

13
00:00:51,390 --> 00:00:53,430
The princesses have even RSVP'd!

14
00:00:53,430 --> 00:00:55,610
I told you we should have eloped!

15
00:00:55,610 --> 00:00:59,110
Oh dear. The caterer,
the flowers, the musicians!

16
00:00:59,110 --> 00:01:01,110
We've got to move
it all to today!

17
00:01:01,110 --> 00:01:01,940
What?!

18
00:01:02,440 --> 00:01:04,350
Where's my wedding planner?!

19
00:01:08,510 --> 00:01:11,130
You told me you could do it for
half of what the others charged,

20
00:01:11,130 --> 00:01:13,070
and then you sent the invitations

21
00:01:13,070 --> 00:01:16,480
to everypony in town
with the wrong date!

22
00:01:19,000 --> 00:01:19,860
Muffin?

23
00:02:01,820 --> 00:02:04,770
I really messed up
on those invitations!

24
00:02:04,770 --> 00:02:06,400
I feel just awful, Doc!

25
00:02:06,400 --> 00:02:09,720
Hmph, perhaps that explains
why I never got mine!

26
00:02:10,830 --> 00:02:13,680
I told Cranky I could get 'em
printed for cheap,

27
00:02:13,680 --> 00:02:15,490
but that meant hiring somepony

28
00:02:15,490 --> 00:02:18,420
with no experience
using a printing press...

29
00:02:22,880 --> 00:02:27,410
Oh, I wish there was a way I could
go back in time and fix all this!

30
00:02:30,260 --> 00:02:33,440
Going back in time
is old thinking, my friend.

31
00:02:33,680 --> 00:02:35,840
I was working off a
cutting-edge theory

32
00:02:35,840 --> 00:02:38,640
of making time come
forward to you...

33
00:02:41,490 --> 00:02:44,240
My life's work, decades,
speaking, centuries, really,

34
00:02:44,240 --> 00:02:46,000
of research and experimentation,

35
00:02:46,000 --> 00:02:47,450
and I nearly had it cracked!

36
00:02:47,450 --> 00:02:49,560
Turns out there's a
magic spell for it...

37
00:02:49,730 --> 00:02:50,840
who knew?

38
00:02:54,170 --> 00:02:58,000
But there are so many things
that magic can't explain,

39
00:02:58,000 --> 00:03:00,910
where science and mathematics
are the real magic!

40
00:03:00,910 --> 00:03:03,320
Like these? They're pretty...

41
00:03:03,320 --> 00:03:06,480
Ah, yes, my flameless fireworks.

42
00:03:06,480 --> 00:03:08,830
I never could quite figure out
how to get them to ignite.

43
00:03:08,830 --> 00:03:12,130
How did you learn to make
all this stuff anyway?

44
00:03:12,220 --> 00:03:14,920
I've been studying
science my whole life.

45
00:03:14,920 --> 00:03:18,220
Ever since a particularly
traumatic experience as a foal,

46
00:03:18,220 --> 00:03:21,000
I've been looking for ways to
make sense of the world around me.

47
00:03:21,000 --> 00:03:25,170
Science provides explanations of
things we never thought possible!

48
00:03:26,200 --> 00:03:26,610
But...

49
00:03:26,610 --> 00:03:28,330
why did we come here again?

50
00:03:29,770 --> 00:03:33,080
Oh! Because I accidentally
sent out invitations

51
00:03:33,080 --> 00:03:35,040
for Cranky and Matilda's wedding

52
00:03:35,040 --> 00:03:38,030
with today's date
instead of tomorrow's!

53
00:03:38,030 --> 00:03:39,920
Great whickering stallions!

54
00:03:39,920 --> 00:03:41,440
I completely forgot!

55
00:03:41,440 --> 00:03:43,780
And I still need to
get my suit tailored!

56
00:03:49,150 --> 00:03:51,840
Rarity?  Rarity!

57
00:03:59,420 --> 00:04:02,280
Please! You've got to help me!

58
00:04:03,060 --> 00:04:05,070
I lost track of time,
unbelievably,

59
00:04:05,070 --> 00:04:06,700
and forgot that the
wedding is this afternoon!

60
00:04:12,110 --> 00:04:13,480
Have you seen Rarity?

61
00:04:13,480 --> 00:04:16,880
She's got to open the sleeves of
my suit and she's got to do it now!

62
00:04:17,860 --> 00:04:19,780
Oh, haha, thank goodness.

63
00:04:19,780 --> 00:04:21,200
Lead on, my friend.

64
00:04:30,290 --> 00:04:32,370
Er, why have you brought me here?

65
00:04:32,370 --> 00:04:34,010
Rarity would never set hoof in-

66
00:04:40,140 --> 00:04:43,370
Great whickering stallions,
they've got style!

67
00:04:44,530 --> 00:04:45,700
Gentlecolts!

68
00:04:45,700 --> 00:04:47,980
I'm facing certain calamity,

69
00:04:47,980 --> 00:04:51,080
and I couldn't help noticing
your remarkable fashion sense!

70
00:04:51,080 --> 00:04:53,930
Could I have the name
of your incredible tailor?

71
00:04:54,050 --> 00:04:56,810
Oh yeah, man, his name is me.

72
00:04:56,940 --> 00:04:59,420
Me'. What an unfortunate name.

73
00:04:59,420 --> 00:05:03,290
No, man, like, I manufacture
all of my own garments.

74
00:05:03,290 --> 00:05:04,690
We all do, man.

75
00:05:04,690 --> 00:05:05,870
Then you've got to help me!

76
00:05:05,870 --> 00:05:07,390
I need this suit tailored!

77
00:05:07,390 --> 00:05:08,400
It's an emergency!

78
00:05:09,360 --> 00:05:12,980
Sorry, man, we're just
about to start the finals.

79
00:05:13,410 --> 00:05:15,790
What's this word you keep using?

80
00:05:15,790 --> 00:05:17,040
Man'?

81
00:05:17,040 --> 00:05:19,200
I dunno, man, but guess what?

82
00:05:19,200 --> 00:05:22,690
Our fourth didn't show,
so if you roll with us,

83
00:05:22,690 --> 00:05:24,490
we'll alter your suit for you.

84
00:05:33,600 --> 00:05:36,340
I'm sorry, gentlecolts,
but I will not bowl.

85
00:05:36,340 --> 00:05:37,950
The splits, the spares!

86
00:05:37,950 --> 00:05:39,760
There are simply
too many variables!

87
00:05:39,760 --> 00:05:42,810
Variables?
What are you talking about, man?

88
00:05:42,810 --> 00:05:44,810
Just throw the ball straight!

89
00:05:44,810 --> 00:05:46,640
Hold on. Straight?

90
00:05:49,450 --> 00:05:53,640
Very well. I'll try your
straight technique.

91
00:05:53,640 --> 00:05:56,110
It just might be crazy
enough to work.

92
00:06:01,130 --> 00:06:03,010
Do we know what
they're on about?

93
00:06:03,400 --> 00:06:05,280
The way they're
huddled up like that,

94
00:06:05,280 --> 00:06:08,540
I'd say it's either a friendship
problem or a monster attack.

95
00:06:08,650 --> 00:06:10,980
A monster attack?! Blast!

96
00:06:11,160 --> 00:06:13,500
I'm performing at the
ceremony this afternoon,

97
00:06:13,500 --> 00:06:16,300
and I still haven't sorted
out what to play.

98
00:06:16,300 --> 00:06:19,980
How am I meant to practice
with a monster invading Ponyville?

99
00:06:19,980 --> 00:06:21,720
Maybe it's just a
friendship problem,

100
00:06:21,720 --> 00:06:24,060
and it'll all be cleared up
in half an hour or so.

101
00:06:24,980 --> 00:06:26,580
I hope so.

102
00:06:27,650 --> 00:06:28,900
Where's Pinkie Pie?!

103
00:06:28,900 --> 00:06:31,140
I need my wedding planner!

104
00:06:46,490 --> 00:06:49,040
Oh, no! On my wedding day?!

105
00:06:49,040 --> 00:06:51,570
Somepony's gotta help me!

106
00:06:55,740 --> 00:06:56,650
You!

107
00:06:57,140 --> 00:06:58,050
Me?

108
00:06:58,050 --> 00:07:01,310
I'm to move an entire wedding
from tomorrow to today!

109
00:07:01,310 --> 00:07:02,960
But nopony's asked
me to organize

110
00:07:02,960 --> 00:07:05,040
anything since Twilight
came to town.

111
00:07:05,040 --> 00:07:06,290
So you'll do it?

112
00:07:06,540 --> 00:07:10,500
I used to be the best
organizer in all of Ponyville.

113
00:07:10,500 --> 00:07:11,960
You bet I'll—

114
00:07:17,650 --> 00:07:19,400
Come on! We better
get to the salon

115
00:07:19,400 --> 00:07:21,460
before that monster flattens it!

116
00:07:28,920 --> 00:07:31,060
What am I gonna do?

117
00:07:32,360 --> 00:07:33,290
Matilda!

118
00:07:33,410 --> 00:07:36,340
I feel so bad about the invitations!

119
00:07:36,340 --> 00:07:37,990
Is there anything I can do—

120
00:07:37,990 --> 00:07:39,990
FLOWERS!

121
00:07:42,180 --> 00:07:44,130
You want Matilda's arrangements...

122
00:07:44,130 --> 00:07:45,290
today?!

123
00:07:46,710 --> 00:07:48,260
This is awful!

124
00:07:48,260 --> 00:07:50,760
The horror, the horror!

125
00:07:50,760 --> 00:07:53,080
So, there's no way you can do it?

126
00:07:53,280 --> 00:07:55,620
We don't even have
Matilda's flowers in yet,

127
00:07:55,620 --> 00:07:56,820
much less arranged!

128
00:07:57,460 --> 00:07:59,140
This is a disaster!

129
00:07:59,140 --> 00:08:01,770
Okay. Thanks anyway.

130
00:08:04,680 --> 00:08:08,680
Look, girls!
A broken stem on one of the zinneas!

131
00:08:08,680 --> 00:08:10,130
Whaat?!

132
00:08:11,080 --> 00:08:14,900
Oh, the horror, the horror!

133
00:08:16,560 --> 00:08:17,470
I have to admit,

134
00:08:17,470 --> 00:08:20,070
when Matilda said we needed
this place ready by today,

135
00:08:20,070 --> 00:08:21,350
I was a little nervous.

136
00:08:21,350 --> 00:08:24,910
With you by my side,
I knew we'd get it done in time.

137
00:08:24,910 --> 00:08:28,480
There is nothing like
a best friend, is there?

138
00:08:28,480 --> 00:08:30,520
Anything's possible when
you know somepony

139
00:08:30,520 --> 00:08:32,850
as well as we know each other!

140
00:08:34,920 --> 00:08:36,100
What was that?

141
00:08:36,100 --> 00:08:38,820
There's some monster
attacking Ponyville or something.

142
00:08:38,820 --> 00:08:40,360
What is it this time?

143
00:08:40,360 --> 00:08:42,180
Creature from the Everfree Forest?

144
00:08:42,180 --> 00:08:45,440
Uh, I think it's some
sort of bugbear.

145
00:08:46,190 --> 00:08:48,150
Did you say bugbear?

146
00:08:48,150 --> 00:08:49,640
It found me!

147
00:08:49,770 --> 00:08:51,640
What are you talking
about, Bon Bon?

148
00:08:52,760 --> 00:08:54,210
My name isn't Bon Bon.

149
00:08:54,350 --> 00:08:56,470
It's Special Agent Sweetie Drops.

150
00:08:56,470 --> 00:09:00,070
I work for a super-secret
anti-monster agency in Canterlot,

151
00:09:00,070 --> 00:09:02,120
or at least I did until the bugbear went missing

152
00:09:02,120 --> 00:09:04,050
from Tartarus a few years back.

153
00:09:04,050 --> 00:09:06,520
What are you talking about?

154
00:09:06,520 --> 00:09:09,290
When it escaped, we had to
shut down the whole agency.

155
00:09:09,290 --> 00:09:10,790
Every last shred of evidence

156
00:09:10,790 --> 00:09:13,210
of the organization's
existence was destroyed.

157
00:09:13,210 --> 00:09:16,050
Celestia commanded
complete deniability.

158
00:09:17,160 --> 00:09:18,290
...What?

159
00:09:18,290 --> 00:09:20,150
It was me who
captured the bugbear.

160
00:09:20,150 --> 00:09:23,630
I had to go deep cover here in Ponyville
and assume the name Bon Bon.

161
00:09:23,630 --> 00:09:27,850
I never thought it'd be able to
track me, but now it has.

162
00:09:27,850 --> 00:09:32,120
Are you saying our whole
friendship was based on a lie?!

163
00:09:32,120 --> 00:09:35,960
I'm sorry, Lyra! I couldn't tell
you for your own protection!

164
00:09:36,100 --> 00:09:39,190
B-b-but the lunches! The,
the long talks!

165
00:09:39,190 --> 00:09:40,680
The benches we sat on!

166
00:09:40,680 --> 00:09:42,680
None of that was real?!

167
00:09:43,090 --> 00:09:46,760
It was all real.
You're my very best friend.

168
00:09:51,040 --> 00:09:52,290
I've got to go find a crowd

169
00:09:52,290 --> 00:09:54,230
to blend into before
I put you in danger!

170
00:09:54,230 --> 00:09:55,800
I'll see you at the wedding.

171
00:09:56,600 --> 00:09:59,470
Fine! But we're going to
talk about this later!

172
00:10:00,980 --> 00:10:03,770
I need my ring today,
no matter the cost!

173
00:10:03,990 --> 00:10:06,490
...as long as it doesn't
cost any extra.

174
00:10:07,550 --> 00:10:09,540
Seven/ten split, man.

175
00:10:09,540 --> 00:10:11,540
Harshest of the harsh.

176
00:10:11,540 --> 00:10:15,770
But if you pick this up,
we win the whole shebang!

177
00:10:18,510 --> 00:10:22,480
Doc! I've finally figured
out how I can help!

178
00:10:22,480 --> 00:10:25,650
Your flameless fireworks
look just like flowers!

179
00:10:25,650 --> 00:10:27,670
I'll use them for the wedding!

180
00:10:28,160 --> 00:10:31,000
Great whickering stallions, wait!

181
00:10:39,730 --> 00:10:42,840
Wait! The flameless fireworks
are extremely volatile!

182
00:10:42,840 --> 00:10:44,570
Without knowing what
the trigger is,

183
00:10:44,570 --> 00:10:46,400
they could go off
at any moment!

184
00:10:47,490 --> 00:10:49,410
My word, is that a bugbear?

185
00:10:53,410 --> 00:10:57,050
Oh, there are so many
things I'm forgetting!

186
00:10:57,050 --> 00:10:59,880
I hope Cranky remembers
to tell the musicians!

187
00:10:59,880 --> 00:11:04,100
Oh, oh, I'll never get my
mane done in time!

188
00:11:04,100 --> 00:11:06,390
You must relax, my dear!

189
00:11:06,390 --> 00:11:08,520
We can handle anything!

190
00:11:08,520 --> 00:11:12,820
We once did a pony's hair
during the ceremony!

191
00:11:12,820 --> 00:11:15,030
Oh, it's true, it's true!

192
00:11:15,030 --> 00:11:16,840
They really are the best!

193
00:11:16,840 --> 00:11:19,000
Matilda, I've just got to say,

194
00:11:19,000 --> 00:11:21,450
I already feel like we're family!

195
00:11:21,610 --> 00:11:22,670
You do?

196
00:11:22,670 --> 00:11:27,590
Of course! I'm Steven Magnet,
Cranky's best beast!

197
00:11:27,590 --> 00:11:29,360
You're Steven Magnet?

198
00:11:29,360 --> 00:11:31,070
What did you expect, a bugbear?

199
00:11:31,070 --> 00:11:34,080
I've known Cranky forev-er!

200
00:11:34,080 --> 00:11:35,270
Surely he must have told you

201
00:11:35,270 --> 00:11:38,930
about the time he saved me
from Flash Freeze Lake?

202
00:11:39,410 --> 00:11:40,950
You're Steven Magnet.

203
00:11:40,950 --> 00:11:44,310
Oh, I know, I know! Typical Cranky,

204
00:11:44,310 --> 00:11:47,140
to leave out minor details,
like the fact that I'm,

205
00:11:47,140 --> 00:11:49,680
y'know, a sea monster, right?

206
00:11:49,680 --> 00:11:53,190
I just love that old burro!

207
00:11:53,190 --> 00:11:57,430
I'm sorry, Steven, I guess
I assumed you were a pony.

208
00:11:57,430 --> 00:12:00,810
Um, I had no idea you had
such adventures together.

209
00:12:00,810 --> 00:12:03,800
Oh, honey, you don't
know the half of it!

210
00:12:03,800 --> 00:12:05,280
But let me tell you something,

211
00:12:05,280 --> 00:12:07,330
in all that we've been
through together,

212
00:12:07,330 --> 00:12:11,410
the only thing he ever cared
about was finding you.

213
00:12:12,500 --> 00:12:13,380
Really?

214
00:12:13,650 --> 00:12:16,410
Well, that and a baldness cure.

215
00:12:16,550 --> 00:12:19,320
He is the sweetest
thing, isn't he?

216
00:12:19,320 --> 00:12:21,760
All the stress I've
put myself through.

217
00:12:21,760 --> 00:12:23,600
All the stress I've
put him through!

218
00:12:23,600 --> 00:12:27,120
The only thing that matters
is that we're together!

219
00:12:27,240 --> 00:12:30,670
The wedding isn't the
important thing, the marriage is.

220
00:12:30,670 --> 00:12:33,460
Oh, goodness gracious.

221
00:12:33,460 --> 00:12:37,610
If you believe that,
I have got a bridge to sell you!

222
00:12:37,610 --> 00:12:39,860
All these ponies
travelling to Ponyville,

223
00:12:39,860 --> 00:12:41,600
putting on uncomfortable clothes,

224
00:12:41,600 --> 00:12:43,390
sitting through a long ceremony,

225
00:12:43,390 --> 00:12:46,770
you think any of them care
about the marriage?

226
00:12:47,480 --> 00:12:53,200
Honey, the wedding is everything.

227
00:13:06,760 --> 00:13:09,640
Ugh, all these wedding
songs are so...

228
00:13:09,640 --> 00:13:10,910
standard.

229
00:13:11,030 --> 00:13:14,400
I want Matilda and Cranky's
wedding to be special.

230
00:13:37,560 --> 00:13:38,100
Thanks,

231
00:13:38,100 --> 00:13:41,140
but I'm not sure that's
appropriate for a wedding, is it?

232
00:13:57,250 --> 00:13:58,840
That's more like it!

233
00:14:48,160 --> 00:14:51,380
Stop! I'm going to be
late for the wedding!

234
00:15:54,230 --> 00:15:57,050
What is life?
Is it nothing more than

235
00:15:57,050 --> 00:15:59,300
the endless search
for a cutie mark?

236
00:15:59,300 --> 00:16:01,140
And what is a cutie mark,

237
00:16:01,140 --> 00:16:02,980
but a constant reminder
that we're all

238
00:16:02,980 --> 00:16:06,770
only one bugbear attack
away from oblivion?

239
00:16:06,770 --> 00:16:08,610
And what of the poor gator?

240
00:16:08,610 --> 00:16:10,670
Flank forver blank,

241
00:16:10,670 --> 00:16:15,230
destined to an existential swim
down the river of life, to...

242
00:16:15,230 --> 00:16:17,370
an unknowable destiny?

243
00:16:26,820 --> 00:16:28,670
Something like that might work.

244
00:16:31,970 --> 00:16:34,030
What do you mean,
you left it on the counter?!

245
00:16:34,030 --> 00:16:35,610
I thought you were bringing it!

246
00:16:35,610 --> 00:16:38,070
This is just wonderful!

247
00:16:41,610 --> 00:16:44,440
I handled the gifts for Cadance
and Shining Armor!

248
00:16:44,440 --> 00:16:46,960
You were supposed to do
this one, remember?

249
00:16:46,960 --> 00:16:50,260
Well, we can't just come to
this wedding empty-hoofed!

250
00:16:53,360 --> 00:16:56,350
It's alright.
He always cries at weddings.

251
00:16:57,870 --> 00:17:00,850
...Though usually it's not
until the wedding starts...

252
00:17:01,850 --> 00:17:05,600
These flameless fireworks look
even better than flowers!

253
00:17:06,490 --> 00:17:08,200
Attention, everypony!

254
00:17:08,200 --> 00:17:09,550
Our friends have done it!

255
00:17:09,550 --> 00:17:12,030
They defeated the bugbear!

256
00:17:15,520 --> 00:17:16,200
Hey.

257
00:17:16,500 --> 00:17:17,380
Hello.

258
00:17:18,500 --> 00:17:22,070
So, uh, you didn't happen to
mention our earlier conversation

259
00:17:22,070 --> 00:17:27,400
about my  secret identity
to anypony, did you?

260
00:17:27,970 --> 00:17:29,860
No.
I did not.

261
00:17:30,680 --> 00:17:33,710
And you're not the only one
with a secret, y'know.

262
00:17:33,710 --> 00:17:35,140
You know those expensive,

263
00:17:35,140 --> 00:17:38,760
imported oats you were saving
for a 'special occasion'?

264
00:17:38,760 --> 00:17:40,720
I cooked them all and ate them!

265
00:17:40,890 --> 00:17:41,860
All of them!

266
00:17:44,150 --> 00:17:45,370
Oohoo!

267
00:17:45,370 --> 00:17:48,170
It's sort of thrilling to reveal your deepest,
darkest secrets!

268
00:17:49,090 --> 00:17:52,040
That's what best friends are for.

269
00:17:56,990 --> 00:18:01,380
Next time, you can just bring
your own gift, and I'll bring mine.

270
00:18:01,490 --> 00:18:02,390
Fine.

271
00:18:03,990 --> 00:18:04,740
There you are!

272
00:18:05,860 --> 00:18:06,950
My suit has vanished

273
00:18:06,950 --> 00:18:09,030
and this was the only
thing left in my closet!

274
00:18:09,370 --> 00:18:10,580
How do I look?

275
00:18:10,580 --> 00:18:12,770
Like a million bits!

276
00:18:13,690 --> 00:18:16,480
Great whickering stallions,
look at the time!

277
00:18:16,480 --> 00:18:18,150
We'd better get inside.

278
00:18:18,150 --> 00:18:19,350
Allons-y!

279
00:18:21,990 --> 00:18:25,200
I can't believe I lost my hair!

280
00:18:25,200 --> 00:18:26,640
I look ridiculous.

281
00:18:26,750 --> 00:18:29,450
The love of my life deserves
better than this!

282
00:18:29,450 --> 00:18:31,870
Have no fear, Cranky my dear,

283
00:18:31,870 --> 00:18:36,310
it's Steven Magnet's
moustache to the rescue!

284
00:18:42,110 --> 00:18:44,240
Aw, thanks, buddy.

285
00:18:44,240 --> 00:18:46,240
Oh, no problem.

286
00:18:46,240 --> 00:18:49,870
Now you get in there and marry that donkey!

287
00:18:50,920 --> 00:18:53,450
Well, is everypony here?

288
00:18:55,410 --> 00:18:57,380
All set, Mayor!

289
00:18:57,380 --> 00:19:00,250
Dearly beloved, we are
gathered here today

290
00:19:00,250 --> 00:19:02,630
to join these two in matrimony.

291
00:19:02,810 --> 00:19:04,650
As I look around this room,

292
00:19:04,650 --> 00:19:08,820
I can only imagine how
uncomfortable Cranky must be.

293
00:19:13,840 --> 00:19:18,520
But I also see so many
ponies from all trots of life,

294
00:19:18,760 --> 00:19:21,280
brought together by love.

295
00:19:21,760 --> 00:19:26,550
Cranky searched all across this
great land of ours to find Matilda,

296
00:19:26,550 --> 00:19:29,320
and no matter what
obstacles kept them apart,

297
00:19:29,320 --> 00:19:31,680
love would finally
bring them together,

298
00:19:31,810 --> 00:19:35,570
just as it has brought all
of us together now.

299
00:19:35,570 --> 00:19:37,080
It's remarkle to me,

300
00:19:37,080 --> 00:19:39,590
how a story like Cranky's
search for Matilda

301
00:19:39,590 --> 00:19:43,220
could fill this room with such a
unique collection of ponies!

302
00:19:43,220 --> 00:19:48,600
It makes you realize that everypony
is the star of their own story,

303
00:19:48,810 --> 00:19:50,960
and it's not just
the main characters

304
00:19:50,960 --> 00:19:53,390
in our stories that
make life so rich!

305
00:19:53,620 --> 00:19:57,060
It's everypony, those
who play big parts,

306
00:19:57,060 --> 00:19:59,710
and those who play small.

307
00:20:00,520 --> 00:20:02,790
If it weren't for everypony
in this room,

308
00:20:02,790 --> 00:20:05,200
and many more who
couldn't be here today,

309
00:20:05,200 --> 00:20:07,510
Cranky and Matilda's
lives wouldn't be

310
00:20:07,510 --> 00:20:10,320
as full and vibrant as they are.

311
00:20:14,200 --> 00:20:17,540
And so, in front of all
these loved ones...

312
00:20:17,540 --> 00:20:20,390
Cranky, do you take this donkey

313
00:20:20,390 --> 00:20:22,890
to be your lawfully wedded wife?

314
00:20:22,890 --> 00:20:24,890
You bet I do!

315
00:20:25,010 --> 00:20:26,930
And do you, Matilda,

316
00:20:26,930 --> 00:20:30,660
take this donkey to be your
lawfully wedded husband?

317
00:20:31,730 --> 00:20:33,120
I do!

318
00:20:33,350 --> 00:20:35,080
Then I'm proud to say,

319
00:20:35,330 --> 00:20:39,490
I now pronounce you
jack and jenny!

320
00:20:58,900 --> 00:21:02,570
Of course!
They need love to ignite!

321
00:21:02,570 --> 00:21:05,060
How could I have missed it?!

322
00:21:12,570 --> 00:21:14,130
You know something, girls?

323
00:21:14,240 --> 00:21:17,620
We are so lucky to
live in this town.

324
00:21:20,000 --> 00:21:21,510
I love you all!

325
00:21:21,810 --> 00:21:24,960
Ow! That's where the
bugbear bit me!

326
00:21:24,960 --> 00:21:26,170
Sorry!

327
00:21:32,840 --> 00:21:34,840
<i>Subtitles by
MLPSubtitles</i>

328
00:21:35,010 --> 00:21:37,010
<i>http://www.mlpsubtitles.com</i>

329
00:21:37,170 --> 00:21:39,180
<i>Transcript:
OmegaBowser</i>

330
00:21:39,340 --> 00:21:41,340
<i>Synchronization:
OmegaBowser</i>

331
00:21:41,500 --> 00:21:43,500
<i>Misc:
GoFish</i>

