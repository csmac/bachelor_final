1
00:00:03,060 --> 00:00:05,399
- Hey, I just got the...
- Hey.

2
00:00:05,400 --> 00:00:07,100
Why are you drinking beer
at 10:00 in the morning?

3
00:00:07,101 --> 00:00:11,200
Uh, we are out of orange juice.

4
00:00:12,400 --> 00:00:14,000
No, we're not.
It's right behind the...

5
00:00:14,010 --> 00:00:16,570
Beer.

6
00:00:17,940 --> 00:00:20,309
I like beer.

7
00:00:20,310 --> 00:00:23,010
Anyway, I just got
the estimate from our painter.

8
00:00:23,020 --> 00:00:24,819
Oh, wow.
How much would it cost

9
00:00:24,820 --> 00:00:28,420
if we used paint
instead of liquid gold?

10
00:00:29,550 --> 00:00:30,850
He said it'll be done
by Saturday.

11
00:00:30,860 --> 00:00:33,020
But to avoid inhaling
all the paint fumes,

12
00:00:33,030 --> 00:00:34,529
he suggested
we stay in a hotel.

13
00:00:34,530 --> 00:00:35,829
Hotel?
[Scoffs]

14
00:00:35,830 --> 00:00:37,560
How bad can the fumes be?

15
00:00:38,400 --> 00:00:40,250
Bad enough
to cause brain damage.

16
00:00:40,251 --> 00:00:42,430
[Scoffs]

17
00:00:42,440 --> 00:00:44,600
How much brain damage?

18
00:00:47,210 --> 00:00:48,670
Here I thought we could have

19
00:00:48,671 --> 00:00:50,909
a nice, romantic night away.

20
00:00:50,910 --> 00:00:52,339
Really?

21
00:00:52,340 --> 00:00:54,340
"Birthday" stuff?

22
00:00:54,350 --> 00:00:56,679
Mm, maybe.
If it's a nice hotel.

23
00:00:56,680 --> 00:00:58,519
- Oh, you mean...
- Yes, Jeff.

24
00:00:58,520 --> 00:01:01,349
I want a bathroom
right in the room.

25
00:01:01,350 --> 00:01:03,520
[Knock at door]

26
00:01:04,560 --> 00:01:05,720
Howdy, neighbors.

27
00:01:05,721 --> 00:01:07,720
Hey, Liz.
What's up?

28
00:01:07,730 --> 00:01:09,690
I have a bit
of an announcement.

29
00:01:09,700 --> 00:01:11,150
I've come to the conclusion

30
00:01:11,151 --> 00:01:14,429
that I don't need men
to have a good time.

31
00:01:14,430 --> 00:01:16,450
Men agree.

32
00:01:17,500 --> 00:01:18,600
Now, I'll admit,

33
00:01:18,601 --> 00:01:20,639
I did enjoy
that little rabbit Russell

34
00:01:20,640 --> 00:01:22,610
thumping away on top of me.

35
00:01:28,280 --> 00:01:30,779
[Scoffs]
But we all know he's a cad.

36
00:01:30,780 --> 00:01:33,419
So I've decided
to become an empowered woman.

37
00:01:33,420 --> 00:01:36,419
Oh. What does
today's empowered woman do?

38
00:01:36,420 --> 00:01:38,219
Oh, all sorts of things.

39
00:01:38,220 --> 00:01:41,289
Have you ever examined yourself
with a hand mirror?

40
00:01:41,290 --> 00:01:42,990
[Spits up noisily]

41
00:01:45,980 --> 00:01:47,260
Oh, say,

42
00:01:47,270 --> 00:01:49,330
can you guys watch my cats
this weekend?

43
00:01:49,340 --> 00:01:50,830
Especially Marty.

44
00:01:50,840 --> 00:01:53,469
He'll need his eye sopped.
It's gotten all milky again.

45
00:01:53,470 --> 00:01:57,070
And, oh, how he fights
those suppositories.

46
00:01:57,080 --> 00:01:58,380
Just...

47
00:01:59,860 --> 00:02:02,079
Well, look, we can't.

48
00:02:02,080 --> 00:02:04,479
We're going away
to a nice hotel.

49
00:02:04,480 --> 00:02:06,519
- Yeah.
- Private bath...

50
00:02:06,520 --> 00:02:07,819
[Mutters]

51
00:02:07,820 --> 00:02:08,869
Oh, sexy.

52
00:02:08,870 --> 00:02:10,219
[Laughter]

53
00:02:10,220 --> 00:02:12,659
I look forward to the stories.

54
00:02:12,660 --> 00:02:16,160
Err on the side
of way too graphic.

55
00:02:20,330 --> 00:02:21,970
Okay, here's
the key to our place.

56
00:02:21,971 --> 00:02:23,500
The painter will be
finished on Saturday,

57
00:02:23,501 --> 00:02:24,870
so we need you to
go in on Sunday

58
00:02:24,871 --> 00:02:25,800
and close all the windows.

59
00:02:25,801 --> 00:02:28,509
<i>No problemo.</i>
[Chuckles]

60
00:02:28,510 --> 00:02:30,640
Don't touch any of my stuff.

61
00:02:31,840 --> 00:02:34,579
- Ah, hey, losers.
- Hey.

62
00:02:34,580 --> 00:02:35,579
Whoa.

63
00:02:35,580 --> 00:02:37,549
Time has not been kind

64
00:02:37,550 --> 00:02:38,919
to Dennis the menace.

65
00:02:38,920 --> 00:02:41,990
[Laughter]

66
00:02:42,820 --> 00:02:43,890
Dennis the menace--

67
00:02:43,891 --> 00:02:46,289
precocious rascal,
adored the world over.

68
00:02:46,290 --> 00:02:47,459
I'll take it.

69
00:02:47,460 --> 00:02:49,029
The big news is,

70
00:02:49,030 --> 00:02:50,740
I booked myself
on a cruise this weekend,

71
00:02:50,750 --> 00:02:55,080
and I'm told the passengers
are almost all ladies.

72
00:02:56,470 --> 00:02:57,970
Anyway, I'd say
the chances of me dropping

73
00:02:57,971 --> 00:03:01,370
ye old anchor this weekend
are pretty high.

74
00:03:02,390 --> 00:03:03,770
And by "anchor," I do mean...

75
00:03:03,780 --> 00:03:05,929
Yeah, we know what you mean.

76
00:03:05,930 --> 00:03:09,110
Uh, penis?

77
00:03:13,890 --> 00:03:16,750
Yeah, we just gave this guy
a key to our apartment.

78
00:03:18,490 --> 00:03:20,590
Wait, you're gonna be
on that cruise all weekend?

79
00:03:20,591 --> 00:03:22,230
- Mm-hmm.
- Well, this is perfect.

80
00:03:22,231 --> 00:03:24,930
Not for the women on the ship
who aren't strong swimmers.

81
00:03:26,230 --> 00:03:27,800
No, he's not gonna be
in his new apartment,

82
00:03:27,801 --> 00:03:29,399
so we could stay there?

83
00:03:29,400 --> 00:03:30,600
That's fine by me.

84
00:03:30,601 --> 00:03:32,340
Just don't get any
of your marriage cobwebs

85
00:03:32,341 --> 00:03:34,809
on my furniture.

86
00:03:34,810 --> 00:03:36,570
Well, what do you say, Aud?

87
00:03:36,580 --> 00:03:38,010
It'd save us a chunk of change.

88
00:03:38,011 --> 00:03:39,779
Yeah. Sleeping
at Russell's?

89
00:03:39,780 --> 00:03:41,179
Sure. Yeah.

90
00:03:41,180 --> 00:03:43,949
We were just talking about
how it doesn't burn enough

91
00:03:43,950 --> 00:03:46,080
when we pee.

92
00:03:49,020 --> 00:03:54,449
<font color="#3399FF">Sync and correction by Mlmlte</font>
<font color="#3399FF">www.addic7ed.com</font>

93
00:04:14,450 --> 00:04:17,619
[phone rings]

94
00:04:17,620 --> 00:04:19,119
Russell Dunbar's office.

95
00:04:19,120 --> 00:04:20,550
Ahoy there, Timmy!

96
00:04:20,560 --> 00:04:22,220
This is your
cruise director Russell

97
00:04:22,221 --> 00:04:25,420
aboard the <i>s. S. Intercourse.</i>

98
00:04:25,430 --> 00:04:28,429
Ah, the rare single entendre.

99
00:04:28,430 --> 00:04:30,400
Yeah, this is
actually the perfect spot.

100
00:04:30,401 --> 00:04:32,330
I'll be the first man
the chicks see

101
00:04:32,331 --> 00:04:33,500
when they come onboard.

102
00:04:33,501 --> 00:04:35,739
Oh, I just hope
there won't be a stampede

103
00:04:35,740 --> 00:04:37,200
at the refund desk.

104
00:04:37,210 --> 00:04:39,709
I already checked.
There's no refunds.

105
00:04:39,710 --> 00:04:41,589
Ooh, here they come.

106
00:04:41,590 --> 00:04:42,759
Hey.

107
00:04:42,760 --> 00:04:45,040
Hi.

108
00:04:47,920 --> 00:04:50,770
[Chattering]

109
00:04:53,470 --> 00:04:55,889
Timmy, what's going on?

110
00:04:55,890 --> 00:04:59,260
These chicks,
they're not young.

111
00:05:00,160 --> 00:05:02,359
Actually, none of them.

112
00:05:02,360 --> 00:05:05,060
Timmy, why is this happening?
Oh, I don't know, sir.

113
00:05:05,070 --> 00:05:08,300
Sometimes bad things happen
to horrible people.

114
00:05:09,240 --> 00:05:11,070
Can you get online
and look up this cruise?

115
00:05:11,071 --> 00:05:13,970
It's supposed to be filled
with red-hot ladies.

116
00:05:16,310 --> 00:05:19,130
Oh, this is brilliant!

117
00:05:20,910 --> 00:05:22,919
What you thought
were red-hot ladies

118
00:05:22,920 --> 00:05:27,089
are in fact a group of women
known as the "red <i>hat</i> ladies."

119
00:05:27,090 --> 00:05:29,019
- What's that?
- Well...

120
00:05:29,020 --> 00:05:31,489
It says here that
the red hat society gals--

121
00:05:31,490 --> 00:05:33,690
and I'm quoting
when I say "gals"--

122
00:05:33,700 --> 00:05:38,460
are a group
of fun, frisky women over 50.

123
00:05:39,270 --> 00:05:41,369
I'm sorry? Over 50?

124
00:05:41,370 --> 00:05:44,970
Oh, my God. I'm knee-deep
in granny cranny?

125
00:05:45,870 --> 00:05:48,209
Oh, sir, I had no plans
for the weekend,

126
00:05:48,210 --> 00:05:49,740
but now with
the amount of laughing

127
00:05:49,741 --> 00:05:50,940
I'll be doing at your expense,

128
00:05:50,950 --> 00:05:54,479
I find myself overbooked.

129
00:05:54,480 --> 00:05:55,979
It's best I get started.

130
00:05:55,980 --> 00:05:57,749
[Laughs]

131
00:05:57,750 --> 00:06:00,619
Fix this. Get me out of here,
all right? Asap.

132
00:06:00,620 --> 00:06:01,750
[Ship's horn blares]

133
00:06:01,760 --> 00:06:03,289
Oh. Oh, no.
Oh, no.

134
00:06:03,290 --> 00:06:04,820
[Cheering]

135
00:06:04,830 --> 00:06:07,059
[Calypso music]

136
00:06:07,060 --> 00:06:09,379
Timmy, get me out of--
Timmy!

137
00:06:09,380 --> 00:06:12,999
Timmy, call the coast guard!

138
00:06:13,000 --> 00:06:15,400
Timmy!
Timmy, save me!

139
00:06:21,440 --> 00:06:23,680
Wow, look at this place.

140
00:06:24,480 --> 00:06:26,180
Wow!

141
00:06:27,480 --> 00:06:29,619
Oh!

142
00:06:29,620 --> 00:06:32,549
Have you ever seen
a more spectacular view?

143
00:06:32,550 --> 00:06:34,920
I haven't.

144
00:06:36,060 --> 00:06:37,689
[Laughs]

145
00:06:37,690 --> 00:06:39,090
I know we make fun of Russell,

146
00:06:39,091 --> 00:06:41,990
but I gotta say,
this place is pretty classy.

147
00:06:42,000 --> 00:06:44,030
[Music plays suddenly]

148
00:06:46,270 --> 00:06:49,669
Oh, I may have spoken too soon.

149
00:06:49,670 --> 00:06:51,439
Well, we can judge him,

150
00:06:51,440 --> 00:06:53,609
or...

151
00:06:53,610 --> 00:06:55,339
We can share love,

152
00:06:55,340 --> 00:06:57,179
and we can make love.

153
00:06:57,180 --> 00:06:59,950
[Chuckles]

154
00:07:01,880 --> 00:07:04,719
Say, how about champagne?

155
00:07:04,720 --> 00:07:06,219
Ooh, did you bring any?

156
00:07:06,220 --> 00:07:07,550
No, but there's
a bottle coming up

157
00:07:07,560 --> 00:07:09,320
and out of that table
right now.

158
00:07:15,560 --> 00:07:19,399
Oh. Yeah.
Thank you, ladies!

159
00:07:19,400 --> 00:07:21,080
That's great.

160
00:07:22,620 --> 00:07:25,089
[Phone rings]

161
00:07:25,090 --> 00:07:26,909
- Hello?
- Hey.

162
00:07:26,910 --> 00:07:30,479
These red hat loonies
are coming for me, Tim.

163
00:07:30,480 --> 00:07:33,449
Well, put on your arm floaties
and swim for it.

164
00:07:33,450 --> 00:07:35,280
I guess I'll just
hunker down in my cabin

165
00:07:35,281 --> 00:07:37,319
and try to ignore
the sounds of breasts

166
00:07:37,320 --> 00:07:40,289
dragging down the corridor.

167
00:07:40,290 --> 00:07:42,309
Sir, how old are these women?

168
00:07:42,310 --> 00:07:45,359
I don't know--
40? 90?

169
00:07:45,360 --> 00:07:46,290
Let's be honest.

170
00:07:46,291 --> 00:07:48,590
After 28, it's all the same.

171
00:07:48,600 --> 00:07:50,860
I only asked
because it seems as though

172
00:07:50,870 --> 00:07:53,200
they're only just a few years
older than you are, sir.

173
00:07:53,201 --> 00:07:54,630
Oh, ba-ba-ba--
Timmy, Timmy.

174
00:07:54,640 --> 00:07:55,600
You're breaking up.

175
00:07:55,601 --> 00:07:57,870
Ship's going through a tunnel.

176
00:07:59,270 --> 00:08:00,439
Ugh.

177
00:08:00,440 --> 00:08:03,630
Oh, please allow me.

178
00:08:05,980 --> 00:08:08,279
Nicely blown.

179
00:08:08,280 --> 00:08:10,920
[Chuckles]
I get that a lot.

180
00:08:11,750 --> 00:08:12,849
I'm Sylvia.

181
00:08:12,850 --> 00:08:14,650
Uh, Russell.
[Chuckles]

182
00:08:14,660 --> 00:08:17,089
Hmm. Care to dance,
Russell?

183
00:08:17,090 --> 00:08:18,459
Oh, no, thank you.

184
00:08:18,460 --> 00:08:21,059
I'm not much
of a dancer, per se.

185
00:08:21,060 --> 00:08:22,529
Ohh...

186
00:08:22,530 --> 00:08:25,700
[Music playing]

187
00:08:27,930 --> 00:08:30,840
Although this <i>is</i> my jam.

188
00:08:32,310 --> 00:08:34,139
Ooh-wah! Ooh-wah!

189
00:08:34,140 --> 00:08:35,739
Come on, get you some!

190
00:08:35,740 --> 00:08:38,879
Yeah! Don't be shy.
Get some of this.

191
00:08:38,880 --> 00:08:40,449
Mm! Mm!

192
00:08:40,450 --> 00:08:42,499
- Whoo!
- Okay, gals!

193
00:08:42,500 --> 00:08:43,819
Do the dolphin!

194
00:08:43,820 --> 00:08:46,199
Whoo! Whoo!

195
00:08:46,200 --> 00:08:49,260
Whoo! Whoo!

196
00:08:54,560 --> 00:08:55,730
Ah, the Binghams.

197
00:08:55,731 --> 00:08:57,760
Join me for breakfast?

198
00:08:58,630 --> 00:08:59,670
Why don't I sit here

199
00:08:59,671 --> 00:09:01,770
so you can sit
across from your lovely--

200
00:09:07,110 --> 00:09:08,470
gee, I'm just wondering why...

201
00:09:08,480 --> 00:09:11,580
- Don't want to look at him.
- Fine by me.

202
00:09:12,680 --> 00:09:13,849
Hmm.

203
00:09:13,850 --> 00:09:15,479
Well, then...

204
00:09:15,480 --> 00:09:18,849
Elbows in
when the eggs arrive, yes?

205
00:09:18,850 --> 00:09:20,820
We had a fight
at Russell's last night,

206
00:09:20,821 --> 00:09:23,089
and, of course,
Jeff refused to apologize.

207
00:09:23,090 --> 00:09:24,520
- I did apologize.
- You did not.

208
00:09:24,530 --> 00:09:26,859
I did too.

209
00:09:26,860 --> 00:09:28,330
I'm sorry.
I don't mean to be rude,

210
00:09:28,331 --> 00:09:30,660
but I couldn't help
but overhear.

211
00:09:30,670 --> 00:09:34,200
But we could always
look at the videotape.

212
00:09:39,110 --> 00:09:41,609
What videotape?

213
00:09:41,610 --> 00:09:43,039
As one might expect

214
00:09:43,040 --> 00:09:46,559
from a perv
like Mr. dunbar...

215
00:09:46,560 --> 00:09:48,380
His apartment is rigged
with the latest

216
00:09:48,381 --> 00:09:51,319
in video surveillance
equipment.

217
00:09:51,320 --> 00:09:53,719
Oh, that's disgusting!

218
00:09:53,720 --> 00:09:56,060
And doesn't he have to tell us
if we're being taped?

219
00:09:56,061 --> 00:09:59,020
Only if he
releases it publicly.

220
00:10:00,760 --> 00:10:02,799
In his employ, I've become
somewhat of a scholar

221
00:10:02,800 --> 00:10:06,429
on this dark,
dank corner of the law.

222
00:10:06,430 --> 00:10:07,499
[Sighs]

223
00:10:07,500 --> 00:10:09,499
I'm glad that it's recorded.

224
00:10:09,500 --> 00:10:11,239
Now we can settle this thing.

225
00:10:11,240 --> 00:10:13,809
Yes, we can.
Let's go to Russell's.

226
00:10:13,810 --> 00:10:17,460
- Let's.
- Fine.

227
00:10:25,890 --> 00:10:28,689
I don't know why
Audrey asked you to go with me.

228
00:10:28,690 --> 00:10:31,029
I know how to open the windows.

229
00:10:31,030 --> 00:10:34,030
You're supposed to close them.

230
00:10:37,300 --> 00:10:39,540
- I'm so glad you came.
- Mm.

231
00:10:46,710 --> 00:10:49,210
Oh, we've never been in here
without them.

232
00:10:49,220 --> 00:10:51,620
It's kind of weird.
I know.

233
00:10:53,150 --> 00:10:54,439
Hey.

234
00:10:54,440 --> 00:10:56,089
You know what we should do?

235
00:10:56,090 --> 00:10:58,359
I think I'm thinking
what you're thinking.

236
00:10:58,360 --> 00:10:59,820
Yeah.

237
00:10:59,830 --> 00:11:01,760
Let's go eat
some of their cereal.

238
00:11:01,761 --> 00:11:05,059
Uh, I, uh...

239
00:11:05,060 --> 00:11:07,260
I had a slightly
different thought.

240
00:11:13,410 --> 00:11:16,970
But after this,
I'm gonna crush some cereal.

241
00:11:28,500 --> 00:11:31,220
[Exhales]

242
00:11:39,800 --> 00:11:43,199
[Laughs]

243
00:11:43,200 --> 00:11:45,639
Very funny, Sylvia.

244
00:11:45,640 --> 00:11:47,139
Two husbands in the ground,

245
00:11:47,140 --> 00:11:49,039
and you still know
how to laugh.

246
00:11:49,040 --> 00:11:50,640
[Laughs]

247
00:11:53,480 --> 00:11:57,349
Okay, I've pulled up
the footage from yesterday.

248
00:11:57,350 --> 00:11:59,680
There we are.

249
00:11:59,690 --> 00:12:03,319
Ew! That's what
we look like kissing?

250
00:12:03,320 --> 00:12:05,260
I like <i>my</i> moves.

251
00:12:08,560 --> 00:12:09,890
Oh, oh, oh, oh!
Did you remember

252
00:12:09,900 --> 00:12:11,459
to turn off the coffee maker?

253
00:12:11,460 --> 00:12:13,829
And there it is.
Audrey derails the sex train.

254
00:12:13,830 --> 00:12:15,499
I don't know.

255
00:12:15,500 --> 00:12:18,369
[Sighs]
Now I'm gonna be distracted.

256
00:12:18,370 --> 00:12:20,040
You know what?
I just remembered.

257
00:12:20,041 --> 00:12:22,169
I did.
I did turn it off.

258
00:12:22,170 --> 00:12:23,340
He so didn't turn it off.

259
00:12:23,341 --> 00:12:24,810
You just said whatever it took

260
00:12:24,811 --> 00:12:26,740
to get me on my back.

261
00:12:26,750 --> 00:12:28,750
So the coffee pot is on.
What's the worst that happens?

262
00:12:28,751 --> 00:12:32,179
We get home,
and there's coffee?

263
00:12:32,180 --> 00:12:34,050
That's not the point.
The point is you lied

264
00:12:34,051 --> 00:12:35,649
just to get what you want.

265
00:12:35,650 --> 00:12:37,400
You're the one that always
has to make a big deal

266
00:12:37,410 --> 00:12:38,390
out of nothing.

267
00:12:38,391 --> 00:12:39,720
You can't just let go

268
00:12:39,730 --> 00:12:40,820
and have a good time.

269
00:12:40,830 --> 00:12:41,990
Am I right, Timmy?

270
00:12:41,991 --> 00:12:45,359
I'm really just here
to work the remote.

271
00:12:45,360 --> 00:12:47,760
Well, you wanna know
what his problem is?

272
00:12:47,770 --> 00:12:50,369
It's not difficult.
Play, pause, so forth.

273
00:12:50,370 --> 00:12:53,539
He never cares at all
about things that matter to me.

274
00:12:53,540 --> 00:12:56,070
Everything he says
is just designed to...

275
00:12:56,080 --> 00:12:57,140
Get you on your back.

276
00:12:57,141 --> 00:12:58,679
So you said, and I remember.

277
00:12:58,680 --> 00:13:00,940
Um...

278
00:13:00,950 --> 00:13:02,760
Let's try to focus
on the apology.

279
00:13:02,770 --> 00:13:04,250
That's what gets us all home.

280
00:13:04,251 --> 00:13:06,049
Little later.
Fast forward.

281
00:13:06,050 --> 00:13:09,689
[Sped-up dialogue]

282
00:13:09,690 --> 00:13:11,219
You look like angry puppets.

283
00:13:11,220 --> 00:13:15,560
"I'm mad at you."
"I'm mad at you."

284
00:13:17,230 --> 00:13:21,060
Just let it play then, shall I?

285
00:13:21,070 --> 00:13:22,430
You know I need my mind at ease

286
00:13:22,431 --> 00:13:23,900
in order to get in the mood,

287
00:13:23,901 --> 00:13:26,400
especially for birthday stuff.

288
00:13:26,410 --> 00:13:28,109
Birthday--

289
00:13:28,110 --> 00:13:30,570
- let's just say
I give him a cake.

290
00:13:30,580 --> 00:13:32,339
Go ahead.

291
00:13:32,340 --> 00:13:33,709
[Sighs]

292
00:13:33,710 --> 00:13:35,109
Sorry.

293
00:13:35,110 --> 00:13:36,610
[Laughs]
I did apologize.

294
00:13:36,620 --> 00:13:38,079
Thank you, video.

295
00:13:38,080 --> 00:13:39,849
That is not a real apology.

296
00:13:39,850 --> 00:13:41,919
You did not mean that at all.

297
00:13:41,920 --> 00:13:43,290
I did too.
What do you think, Timmy?

298
00:13:43,291 --> 00:13:44,290
Yeah, what do you think?

299
00:13:44,291 --> 00:13:45,359
Oh, good.

300
00:13:45,360 --> 00:13:48,529
There's no way
this ends poorly for me.

301
00:13:48,530 --> 00:13:50,290
Sorry.

302
00:13:50,300 --> 00:13:54,780
Sorry.

303
00:13:55,900 --> 00:13:58,769
Certainly the monotone
suggests a lack of conviction

304
00:13:58,770 --> 00:14:00,469
or even mild interest.

305
00:14:00,470 --> 00:14:02,169
Thank you.

306
00:14:02,170 --> 00:14:03,609
That being said,

307
00:14:03,610 --> 00:14:05,839
given what we know
of Mr. Bingham's limitations

308
00:14:05,840 --> 00:14:07,279
in voice modulation,

309
00:14:07,280 --> 00:14:09,849
I found the apology
to be quite earnest.

310
00:14:09,850 --> 00:14:12,219
See?

311
00:14:12,220 --> 00:14:13,519
I'm a great guy.

312
00:14:13,520 --> 00:14:14,519
[Sighs]

313
00:14:14,520 --> 00:14:16,669
Uh, that's not what I said.

314
00:14:16,670 --> 00:14:19,160
I...
Nothing.

315
00:14:19,990 --> 00:14:22,090
Hey, wait, roll that back.

316
00:14:23,630 --> 00:14:25,729
[Sped-up dialogue]

317
00:14:25,730 --> 00:14:28,429
You know, we wouldn't have
to keep going through this

318
00:14:28,430 --> 00:14:31,840
if just for once you would...

319
00:14:39,280 --> 00:14:41,609
The video giveth...

320
00:14:41,610 --> 00:14:43,450
The video taketh away.

321
00:14:45,980 --> 00:14:48,389
Okay, look, pearl...

322
00:14:48,390 --> 00:14:51,119
You've been divorced two years.

323
00:14:51,120 --> 00:14:52,870
We all know
that Bernie's gonna do

324
00:14:52,880 --> 00:14:54,990
what Bernie's gonna do.

325
00:14:56,830 --> 00:14:58,329
I'm more concerned with pearl.

326
00:14:58,330 --> 00:15:00,199
Okay, everyone!

327
00:15:00,200 --> 00:15:03,069
Let's see those
statement necklaces!

328
00:15:03,070 --> 00:15:04,740
[Laughs]

329
00:15:06,300 --> 00:15:08,239
Ohh...

330
00:15:08,240 --> 00:15:11,479
Mm-hmm, that's right.
Nailed it.

331
00:15:11,480 --> 00:15:14,309
Huh? Huh?

332
00:15:14,310 --> 00:15:15,950
How do you think
it feels to know

333
00:15:15,951 --> 00:15:19,480
every time I turn my back
my husband is mocking me?

334
00:15:19,490 --> 00:15:23,050
A little better than
if I were to do it to your face.

335
00:15:26,990 --> 00:15:29,529
Maybe that wasn't
the best apology.

336
00:15:29,530 --> 00:15:30,829
But look...

337
00:15:30,830 --> 00:15:32,799
At least I don't do that.

338
00:15:32,800 --> 00:15:34,100
Every time
we get in an argument,

339
00:15:34,101 --> 00:15:35,549
she walks out the room.

340
00:15:35,550 --> 00:15:37,830
I was hurt, Jeff.

341
00:15:37,840 --> 00:15:40,969
Excuse me for not wanting you
to see me vulnerable.

342
00:15:40,970 --> 00:15:42,720
Actually, Mrs. Bingham.

343
00:15:42,730 --> 00:15:45,080
It may help him understand
if he sees you like that.

344
00:15:45,081 --> 00:15:46,890
I'll switch to the kitchen cam.

345
00:15:46,900 --> 00:15:48,979
There's a kit--
kitchen cam?

346
00:15:48,980 --> 00:15:50,880
Mm-hmm.

347
00:15:56,950 --> 00:15:59,489
Whoa.

348
00:15:59,490 --> 00:16:01,990
Angry birds, indeed.

349
00:16:07,330 --> 00:16:09,569
What the...

350
00:16:09,570 --> 00:16:11,999
Adam, I think
we have a problem!

351
00:16:12,000 --> 00:16:13,369
I know.

352
00:16:13,370 --> 00:16:15,139
This cereal is expired.

353
00:16:15,140 --> 00:16:17,009
Is it from this morning,

354
00:16:17,010 --> 00:16:18,340
or do I have until midnight?

355
00:16:18,341 --> 00:16:20,579
No. I have paint
all over my back.

356
00:16:20,580 --> 00:16:22,009
Oh, my God.

357
00:16:22,010 --> 00:16:23,750
It must be from the door.

358
00:16:24,980 --> 00:16:27,049
Well...
It's not too bad.

359
00:16:27,050 --> 00:16:28,049
No.

360
00:16:28,050 --> 00:16:29,100
No. I can just get a brush

361
00:16:29,101 --> 00:16:30,419
and smooth it over.

362
00:16:30,420 --> 00:16:32,989
I think we're okay.
Is that the only spot?

363
00:16:32,990 --> 00:16:34,459
Where else did we do it?

364
00:16:34,460 --> 00:16:36,059
Let me see.

365
00:16:36,060 --> 00:16:38,499
Okay, we started here, right,

366
00:16:38,500 --> 00:16:41,060
and then we took it over here.

367
00:16:41,070 --> 00:16:44,939
We brought it to...

368
00:16:44,940 --> 00:16:46,450
Uh-oh.

369
00:16:48,570 --> 00:16:52,009
- Is that...
- Yup.

370
00:16:52,010 --> 00:16:54,010
That's your butt.

371
00:16:55,450 --> 00:16:58,819
So then the first cowboy says,

372
00:16:58,820 --> 00:17:03,300
"no, no, no,
I said 'posse.'"

373
00:17:03,310 --> 00:17:06,459
[laughter]

374
00:17:06,460 --> 00:17:09,159
What's the matter, Russell?

375
00:17:09,160 --> 00:17:10,310
Don't you get it?

376
00:17:10,311 --> 00:17:12,960
You see, "posse" sounds like...

377
00:17:12,970 --> 00:17:14,700
No, no, no.
I know, I know, I know.

378
00:17:15,930 --> 00:17:19,500
It's very clever.
That's a good one.

379
00:17:19,510 --> 00:17:21,139
Are you having any fun?

380
00:17:21,140 --> 00:17:24,109
Well, not the kind of fun
I was hoping to have.

381
00:17:24,110 --> 00:17:25,539
Mm-hmm.

382
00:17:25,540 --> 00:17:27,659
And don't get me wrong.
You guys are great.

383
00:17:27,660 --> 00:17:28,949
I mean, pearl,

384
00:17:28,950 --> 00:17:31,700
if you were 100 years younger,
I mean, who knows.

385
00:17:34,490 --> 00:17:38,019
But aren't there any
young chicks on this raft?

386
00:17:38,020 --> 00:17:40,089
Oh, yes. <i>Younger.</i>

387
00:17:40,090 --> 00:17:41,829
- Younger?
- Mm-hmm.

388
00:17:41,830 --> 00:17:43,990
Yeah! We got
a couple gals in our group.

389
00:17:44,000 --> 00:17:47,300
See, if they're under 50,
they wear pink hats.

390
00:17:48,170 --> 00:17:49,469
Pink hats.

391
00:17:49,470 --> 00:17:51,840
Well, okay.

392
00:17:53,140 --> 00:17:54,670
Pink hats...

393
00:17:55,560 --> 00:17:58,010
Gonna use this, pearl,
real quick.

394
00:17:59,390 --> 00:18:00,979
A-ha!

395
00:18:00,980 --> 00:18:03,530
Okay! Excuse me
one second.

396
00:18:07,290 --> 00:18:10,090
Well, hello there,
youngish lady.

397
00:18:17,930 --> 00:18:21,260
Well, hello, Russell.

398
00:18:22,070 --> 00:18:23,869
Oh, for...
[Ship's horn blares]

399
00:18:23,870 --> 00:18:25,340
Sake!

400
00:18:31,610 --> 00:18:33,809
Well, maybe
I'll just leave then.

401
00:18:33,810 --> 00:18:36,109
Oh, yeah, thanks a lot.

402
00:18:36,110 --> 00:18:37,910
What?

403
00:18:37,920 --> 00:18:39,949
Your little video idea
just made things worse.

404
00:18:39,950 --> 00:18:42,449
Look, let's not shift
the blame to me.

405
00:18:42,450 --> 00:18:44,150
Clearly you both have

406
00:18:44,160 --> 00:18:46,120
some deep-seated resentments
toward each other.

407
00:18:46,121 --> 00:18:48,530
- Yeah, we're married.
- And I could've done better.

408
00:18:48,531 --> 00:18:49,759
Go on.

409
00:18:49,760 --> 00:18:52,099
Anyway,
there's work to be done.

410
00:18:52,100 --> 00:18:54,329
If you truly want
your relationship to grow,

411
00:18:54,330 --> 00:18:56,630
then you're going to need
to communicate better.

412
00:18:56,640 --> 00:18:59,600
So I suggest getting out
those emotional shovels

413
00:18:59,610 --> 00:19:01,290
and digging until
you uncover everything

414
00:19:01,291 --> 00:19:03,439
that needs to be said.

415
00:19:03,440 --> 00:19:06,280
I'll leave you to it.

416
00:19:08,080 --> 00:19:09,509
Wow.

417
00:19:09,510 --> 00:19:11,250
He really laid it out there,
didn't he?

418
00:19:11,251 --> 00:19:13,549
Yeah, I guess so.

419
00:19:13,550 --> 00:19:15,790
Quite a bit to think about.

420
00:19:17,090 --> 00:19:19,219
[Sighs]

421
00:19:19,220 --> 00:19:21,489
Sounds like
a lot of work, doesn't it?

422
00:19:21,490 --> 00:19:24,779
A lot. Plus it's Timmy,
for crying out loud!

423
00:19:24,780 --> 00:19:26,360
What does he know?

424
00:19:26,370 --> 00:19:28,030
We've been married
for 16 years.

425
00:19:28,031 --> 00:19:29,730
I think we kind of know
what we're doing.

426
00:19:29,740 --> 00:19:33,900
"I suggest you get out
your emotional shovels."

427
00:19:33,910 --> 00:19:36,069
Shovel this, pal.

428
00:19:36,070 --> 00:19:39,740
Idiot.
[Laughs]

429
00:19:39,750 --> 00:19:43,080
I really must stop
trying to help people.

430
00:19:44,450 --> 00:19:46,580
Okay, thank you.
Thanks.

431
00:19:46,590 --> 00:19:48,620
So the upholstery place
said they can fix it by tonight,

432
00:19:48,621 --> 00:19:50,850
but we have to
get it there right now.

433
00:19:50,860 --> 00:19:52,359
But the cushions are sewn in.

434
00:19:52,360 --> 00:19:54,390
We'd have to bring
the whole chair, and it's big.

435
00:19:54,391 --> 00:19:57,630
Hey, they got it in,
we can get it out.

436
00:20:00,030 --> 00:20:02,299
How'd they get it in?

437
00:20:02,300 --> 00:20:03,869
I don't know,

438
00:20:03,870 --> 00:20:05,799
but we've gotta do something.

439
00:20:05,800 --> 00:20:08,039
Okay, okay, let me
just think for a second.

440
00:20:08,040 --> 00:20:10,339
[Sighs]

441
00:20:10,340 --> 00:20:13,660
"Sweep your chimney for you,
governor?"

442
00:20:15,880 --> 00:20:17,779
Okay, how about this?

443
00:20:17,780 --> 00:20:19,419
We put the chair back

444
00:20:19,420 --> 00:20:21,250
and then cover the paint
with a sharpie.

445
00:20:21,251 --> 00:20:22,700
I mean, the way Jeff drinks,

446
00:20:22,701 --> 00:20:24,819
he'll never know
the difference.

447
00:20:24,820 --> 00:20:28,290
Oh, hey, guys.

448
00:20:29,230 --> 00:20:30,959
Oh, I closed your windows,

449
00:20:30,960 --> 00:20:32,830
just like you asked.

450
00:20:50,410 --> 00:20:53,980
Oh, it's
so beautifully designed.

451
00:20:53,990 --> 00:20:56,819
And the way
the light flickers off it...

452
00:20:56,820 --> 00:20:59,059
I just hope
I can do it justice.

453
00:20:59,060 --> 00:21:02,829
Yeah, try to do a good job
on the necklace too.

454
00:21:02,830 --> 00:21:07,730
<font color="#3399FF">Sync and correction by Mlmlte</font>
<font color="#3399FF">www.addic7ed.com</font>

