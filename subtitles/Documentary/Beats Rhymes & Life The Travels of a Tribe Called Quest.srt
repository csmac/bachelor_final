﻿[Script Info]
Title: HorribleSubs
ScriptType: v4.00+
WrapStyle: 0
PlayResX: 848
PlayResY: 480
ScaledBorderAndShadow: yes

[V4+ Styles]
Format: Name, Fontname, Fontsize, PrimaryColour, SecondaryColour, OutlineColour, BackColour, Bold, Italic, Underline, StrikeOut, ScaleX, ScaleY, Spacing, Angle, BorderStyle, Outline, Shadow, Alignment, MarginL, MarginR, MarginV, Encoding
Style: Default,Open Sans Semibold,36,&H00FFFFFF,&H000000FF,&H00020713,&H00000000,-1,0,0,0,100,100,0,0,1,1.7,0,2,0,0,28,1
Style: main,Open Sans Semibold,36,&H00FFFFFF,&H000000FF,&H00020713,&H00000000,-1,0,0,0,100,100,0,0,1,1.7,0,2,0,0,28,0
Style: italics,Open Sans Semibold,36,&H00FFFFFF,&H000000FF,&H00020713,&H00000000,-1,1,0,0,100,100,0,0,1,1.7,0,2,0,0,28,0
Style: flashback,Open Sans Semibold,36,&H00FFFFFF,&H000000FF,&H00020713,&H00000000,-1,0,0,0,100,100,0,0,1,1.7,0,2,0,0,28,1
Style: sign_2467_13__1__One_Fist_,Open Sans Semibold,36,&H00000500,&H000000FF,&H009AA0A8,&H00000000,1,1,0,0,100,100,0,0,1,2,0,3,40,53,87,1
Style: top,Open Sans Semibold,36,&H00FFFFFF,&H000000FF,&H00000000,&H00000000,0,0,0,0,100,100,0,0,1,2,1,8,13,13,24,0
Style: sign_1072_10_Kenka_Banchou,Open Sans Semibold,45,&H00000007,&H000000FF,&H003BFFEC,&H002E54E3,1,0,0,0,100,100,0,0,1,3,3,1,100,40,260,1
Style: sign_1072_11_Otome,Open Sans Semibold,45,&H003800BC,&H000000FF,&H00FFCCFE,&H00000000,1,1,0,0,100,100,0,0,1,4,0,3,40,253,300,1
Style: flashbackitalics,Open Sans Semibold,36,&H00FFFFFF,&H000000FF,&H00020713,&H00000000,-1,1,0,0,100,100,0,0,1,1.7,0,2,0,0,28,0
Style: italicstop,Open Sans Semibold,36,&H00FFFFFF,&H000000FF,&H00020713,&H00000000,-1,1,0,0,100,100,0,0,1,1.7,0,8,0,0,28,0
Style: flashbacktop,Open Sans Semibold,36,&H00FFFFFF,&H000000FF,&H00020713,&H00000000,-1,0,0,0,100,100,0,0,1,1.7,0,8,0,0,28,0
Style: main - copyright bump,Open Sans Semibold,36,&H00FFFFFF,&H000000FF,&H00020713,&H00000000,-1,0,0,0,100,100,0,0,1,1.7,0,2,0,0,28,0
Style: sign_290_4_Shishiku_Academy,Open Sans Semibold,24,&H00369BC0,&H000000FF,&H00749466,&H00000000,1,0,0,0,100,100,0,0,1,4,0,8,40,40,27,1
Style: sign_9879_113_Missing_Missing,Open Sans Semibold,27,&H00030F0C,&H000000FF,&H00629157,&H00000000,1,0,0,0,100,100,0,0,1,4,0,1,453,40,207,1
Style: sign_2559_24_2nd_Year_Cherry_,Open Sans Semibold,27,&H00030F0C,&H000000FF,&H00EC54CA,&H00000000,1,0,0,0,100,100,0,0,1,4,0,1,53,40,207,1
Style: sign_6042_68_No_smoking,Open Sans Semibold,33,&H002C25B2,&H000000FF,&H00819FB8,&H00000000,1,0,0,0,100,100,0,0,1,4,0,8,40,40,27,1

[Events]
Format: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text

Dialogue: 0,0:00:01.36,0:00:06.96,main,Yuta,0000,0000,0000,,My solo debut was set to be \Nat the school I attended.
Dialogue: 0,0:00:08.21,0:00:11.16,main,Yuta,0000,0000,0000,,Not just any school, but the \Nrowdiest school in the country...
Dialogue: 0,0:00:12.07,0:00:22.08,sign_290_4_Shishiku_Academy,Sign,0000,0000,0000,,Shishiku Private Academy
Dialogue: 0,0:00:12.88,0:00:15.54,main,Yuta,0000,0000,0000,,At Shishiku Academy.
Dialogue: 0,0:00:18.91,0:00:24.25,sign_1072_10_Kenka_Banchou,,0000,0000,0000,,{\fad(438,221)}Kenka Bancho
Dialogue: 0,0:00:20.04,0:00:24.25,sign_1072_11_Otome,,0000,0000,0000,,{\fad(653,221)}Otome
Dialogue: 0,0:00:25.30,0:00:28.99,main,Yuta,0000,0000,0000,,It was a totally transparent \Nattempt to generate buzz.
Dialogue: 0,0:00:29.50,0:00:33.51,main,Yuta,0000,0000,0000,,But I didn't really care.
Dialogue: 0,0:00:43.84,0:00:46.85,main,Yuta,0000,0000,0000,,Right... that's how I felt, at the time.
Dialogue: 0,0:01:00.68,0:01:02.52,main,Yuta,0000,0000,0000,,And if it wasn't for who I met,
Dialogue: 0,0:01:03.37,0:01:04.98,main,,0000,0000,0000,,I probably still would.
Dialogue: 0,0:01:17.18,0:01:20.14,sign_2467_13__1__One_Fist_,Eptitle sign,0000,0000,0000,,{\fad(480,519)}#6  "The Idol Punk Arrives!"
Dialogue: 0,0:01:19.62,0:01:20.47,top,Toto,0000,0000,0000,,What the...
Dialogue: 0,0:01:23.39,0:01:26.02,main,Hinako,0000,0000,0000,,Where have I seen him before?
Dialogue: 0,0:01:24.64,0:01:27.91,main,Yuta,0000,0000,0000,,Come on, girls, you can't \Nfollow me to school.
Dialogue: 0,0:01:31.68,0:01:33.06,main,Hinako,0000,0000,0000,,It's the guy from the poster!
Dialogue: 0,0:01:36.18,0:01:39.02,main,Yuta,0000,0000,0000,,What's up? Are you one of my fans, too?
Dialogue: 0,0:01:39.02,0:01:40.06,main,Yuta,0000,0000,0000,,Want an autograph?
Dialogue: 0,0:01:40.06,0:01:41.68,main,Hinako,0000,0000,0000,,N-No thanks!
Dialogue: 0,0:01:41.68,0:01:43.08,main,Hinako,0000,0000,0000,,But what are you doing here?
Dialogue: 0,0:01:44.28,0:01:45.81,main,Yuta,0000,0000,0000,,You mean you don't know?
Dialogue: 0,0:01:45.81,0:01:50.42,top,Yuta,0000,0000,0000,,I'm a second year in the Cherry Blossom Class\Nhere at Shishiku Academy...
Dialogue: 0,0:01:46.71,0:01:52.67,sign_2559_24_2nd_Year_Cherry_,Sign,0000,0000,0000,,{\fad(271,1)}2nd Year Cherry Blossom Class\N{\fs30}Yuta Mirako
Dialogue: 0,0:01:50.42,0:01:52.13,top,Yuta,0000,0000,0000,,Yuta Mirako.
Dialogue: 0,0:01:54.79,0:01:56.64,main,Yuta,0000,0000,0000,,You're quite a cutie.
Dialogue: 0,0:01:56.64,0:02:00.81,main,Yuta,0000,0000,0000,,Your earnestness makes you seem naïve.\NIt's like I want to take care of you.
Dialogue: 0,0:02:00.81,0:02:01.94,main,Yuta,0000,0000,0000,,I think you might be my type.
Dialogue: 0,0:02:01.94,0:02:04.02,main,Hinako,0000,0000,0000,,I'm a boy!
Dialogue: 0,0:02:04.02,0:02:07.81,main,Yuta,0000,0000,0000,,Huh. True enough,\Nyou're wearing a Shishiku uniform.
Dialogue: 0,0:02:08.38,0:02:10.27,main,Yuta,0000,0000,0000,,But that's weird...
Dialogue: 0,0:02:11.96,0:02:16.87,main,Yuta,0000,0000,0000,,Me, mistake a guy for a girl?
Dialogue: 0,0:02:18.19,0:02:20.53,main,Yuta,0000,0000,0000,,I mean, look at how slender your wrists are.
Dialogue: 0,0:02:23.45,0:02:25.97,main,Hinako,0000,0000,0000,,I-I get that a lot.
Dialogue: 0,0:02:25.97,0:02:29.53,main,Hinako,0000,0000,0000,,It's true I'm pretty short, \Nand not very well-built...
Dialogue: 0,0:02:29.53,0:02:31.46,main,Hinako,0000,0000,0000,,But I assure you, I'm a guy.
Dialogue: 0,0:02:33.62,0:02:36.89,main,Yuta,0000,0000,0000,,Well, I'm going to be dropping \Nby school for a bit,
Dialogue: 0,0:02:36.89,0:02:37.84,main,Yuta,0000,0000,0000,,so I'll be looking for you.
Dialogue: 0,0:02:38.10,0:02:39.51,main,Hinako,0000,0000,0000,,For a while?
Dialogue: 0,0:02:39.51,0:02:40.41,main,Yuta,0000,0000,0000,,Exactly.
Dialogue: 0,0:02:40.96,0:02:43.96,main,Yuta,0000,0000,0000,,Maybe next time I can be really thorough...
Dialogue: 0,0:02:44.67,0:02:47.85,main,Yuta,0000,0000,0000,,And then I'll know for sure\Nwhether you're a boy or not.
Dialogue: 0,0:02:49.07,0:02:51.60,main,Toto,0000,0000,0000,,Hey, could you give it a rest, man?
Dialogue: 0,0:02:51.60,0:02:53.86,main,Toto,0000,0000,0000,,You're bothering Hikaru.
Dialogue: 0,0:02:53.86,0:02:54.98,main,,0000,0000,0000,,Hands off already.
Dialogue: 0,0:02:54.98,0:02:57.01,main,Kon,0000,0000,0000,,He might be mild-mannered, \Nbut he's the top first-year.
Dialogue: 0,0:02:57.01,0:02:58.38,main,Kon,0000,0000,0000,,Either give him some space,
Dialogue: 0,0:02:58.38,0:03:00.32,main,,0000,0000,0000,,or we're going to have a problem.
Dialogue: 0,0:03:01.00,0:03:02.92,main,Yuta,0000,0000,0000,,Top of the first-years, huh?
Dialogue: 0,0:03:04.71,0:03:06.74,main,Yuta,0000,0000,0000,,And who are you two supposed to be?
Dialogue: 0,0:03:07.09,0:03:09.46,main,Toto,0000,0000,0000,,We're Hikaru's bros.
Dialogue: 0,0:03:10.85,0:03:11.84,main,Yuta,0000,0000,0000,,Bros?
Dialogue: 0,0:03:15.36,0:03:18.02,main,Yuta,0000,0000,0000,,What a load of crap. Seriously...
Dialogue: 0,0:03:18.61,0:03:21.54,main,Yuta,0000,0000,0000,,I see the first-years don't amount to much.
Dialogue: 0,0:03:26.41,0:03:29.12,main,Yuta,0000,0000,0000,,Rintaro. It's been a while.
Dialogue: 0,0:03:29.12,0:03:31.01,main,Yuta,0000,0000,0000,,Didn't think you'd be the one to call on me.
Dialogue: 0,0:03:31.01,0:03:32.25,main,,0000,0000,0000,,That's new.
Dialogue: 0,0:03:33.61,0:03:36.25,main,Kira,0000,0000,0000,,Stop messing with the first-years.
Dialogue: 0,0:03:36.25,0:03:38.53,main,Yuta,0000,0000,0000,,Oh, you saw that, huh?
Dialogue: 0,0:03:38.53,0:03:41.45,main,Yuta,0000,0000,0000,,Well, he's an interesting one, isn't he?
Dialogue: 0,0:03:42.02,0:03:43.53,main,Yuta,0000,0000,0000,,Almost like a girl.
Dialogue: 0,0:03:43.94,0:03:45.51,main,Yuta,0000,0000,0000,,Or maybe...
Dialogue: 0,0:03:46.39,0:03:49.81,main,Yuta,0000,0000,0000,,Whoa there, no need for the scary look.
Dialogue: 0,0:03:49.81,0:03:51.79,main,Yuta,0000,0000,0000,,I won't do anything.
Dialogue: 0,0:03:52.81,0:03:56.85,main,Yuta,0000,0000,0000,,He's got a pair of pains in the ass \Ncalling themselves his bros, after all.
Dialogue: 0,0:03:56.85,0:03:59.19,main,Kira,0000,0000,0000,,You haven't changed at all.
Dialogue: 0,0:04:00.75,0:04:02.63,main,Kira,0000,0000,0000,,Not a bit.
Dialogue: 0,0:04:03.14,0:04:06.01,main,,0000,0000,0000,,Still torn up over ancient history.
Dialogue: 0,0:04:10.50,0:04:11.98,main,Murata,0000,0000,0000,,Hey, did you hear about this?
Dialogue: 0,0:04:11.98,0:04:21.99,sign_6042_68_No_smoking,Sign,0000,0000,0000,,No Smoking
Dialogue: 0,0:04:12.42,0:04:15.60,main,Murata,0000,0000,0000,,The guy at the top of the\Nsecond-years besides Kira, Mirako,
Dialogue: 0,0:04:15.60,0:04:17.49,main,,0000,0000,0000,,is gonna perform a solo \Nconcert here at the school.
Dialogue: 0,0:04:17.49,0:04:18.93,main,Tanaka,0000,0000,0000,,Hell yeah, I did.
Dialogue: 0,0:04:18.93,0:04:21.99,main,Tanaka,0000,0000,0000,,A punk and an idol? Dude keeps busy.
Dialogue: 0,0:04:21.99,0:04:27.23,main,Nakajima,0000,0000,0000,,I heard Mirako was messing with the \Ntop first-year, Hikaru Onigashima.
Dialogue: 0,0:04:27.23,0:04:31.03,main,Smamura,0000,0000,0000,,Yeah... guess it's coming soon.
Dialogue: 0,0:04:31.03,0:04:31.95,main,Together,0000,0000,0000,,A power play!
Dialogue: 0,0:04:39.44,0:04:41.65,flashback,Yuta,0000,0000,0000,,But that's weird...
Dialogue: 0,0:04:42.13,0:04:47.39,flashback,Yuta,0000,0000,0000,,Me, mistake a guy for a girl?
Dialogue: 0,0:04:51.04,0:04:52.88,flashback,Kira,0000,0000,0000,,Are you actually...
Dialogue: 0,0:04:54.30,0:04:56.15,flashback,,0000,0000,0000,,...Hikaru Onigashima?
Dialogue: 0,0:05:04.37,0:05:08.84,main,Yuta,0000,0000,0000,,Yeah, definitely looks like a girl to me.
Dialogue: 0,0:05:10.14,0:05:13.38,main,Yuta,0000,0000,0000,,And this is the top first-year? Seriously?
Dialogue: 0,0:05:18.59,0:05:19.79,main,Hinako,0000,0000,0000,,Mirako-senpai?!
Dialogue: 0,0:05:20.30,0:05:22.87,main,Yuta,0000,0000,0000,,No need to shout. I hear you.
Dialogue: 0,0:05:23.71,0:05:25.80,main,Yuta,0000,0000,0000,,Good morning, Hikaru-chan.
Dialogue: 0,0:05:25.80,0:05:28.50,main,Hinako,0000,0000,0000,,G-Good morning...
Dialogue: 0,0:05:28.50,0:05:29.52,main,Hinako,0000,0000,0000,,Wait, no!
Dialogue: 0,0:05:29.52,0:05:30.84,main,,0000,0000,0000,,Could you get away, please?
Dialogue: 0,0:05:30.84,0:05:32.59,main,Yuta,0000,0000,0000,,Yeah, yeah...
Dialogue: 0,0:05:32.59,0:05:35.77,main,Yuta,0000,0000,0000,,We're both guys. What's the big deal?
Dialogue: 0,0:05:36.02,0:05:38.94,main,Yuta,0000,0000,0000,,Or maybe you really are a girl?
Dialogue: 0,0:05:42.12,0:05:46.01,main,Hinako,0000,0000,0000,,You're one of the top second-years,\Naren't you, Mirako-senpai?
Dialogue: 0,0:05:46.01,0:05:47.87,main,Hinako,0000,0000,0000,,In that case, please fight me.
Dialogue: 0,0:05:47.87,0:05:51.48,main,Hinako,0000,0000,0000,,You'll see whether I'm a girl or not, then.
Dialogue: 0,0:05:52.50,0:05:53.94,main,Yuta,0000,0000,0000,,I see.
Dialogue: 0,0:05:53.94,0:05:56.13,main,,0000,0000,0000,,Well, your punch is the real deal, at least.
Dialogue: 0,0:05:57.17,0:05:59.92,main,Yuta,0000,0000,0000,,Are you going to be okay without \Nyour little friends, though?
Dialogue: 0,0:06:00.20,0:06:02.17,main,Hinako,0000,0000,0000,,My friends have nothing to do with this.
Dialogue: 0,0:06:02.44,0:06:04.72,main,Yuta,0000,0000,0000,,You're right, they don't.
Dialogue: 0,0:06:05.54,0:06:09.09,main,Yuta,0000,0000,0000,,Friends just end up betraying \Nyou when you need them, anyway.
Dialogue: 0,0:06:11.12,0:06:15.22,main,Hinako,0000,0000,0000,,What is it about them that bothers you?
Dialogue: 0,0:06:17.12,0:06:18.15,main,Yuta,0000,0000,0000,,You...
Dialogue: 0,0:06:18.98,0:06:21.45,main,,0000,0000,0000,,That's like something Rintaro would say.
Dialogue: 0,0:06:22.46,0:06:23.76,main,Hinako,0000,0000,0000,,Rintaro?
Dialogue: 0,0:06:25.81,0:06:28.92,main,Yuta,0000,0000,0000,,Damn it... Everybody thinks \Nthey know everything.
Dialogue: 0,0:06:43.24,0:06:47.16,main,Houou,0000,0000,0000,,It's been six months and\Nnine days since I came to Shishiku.
